\section{Variants of incrementality}

While the proof system presented before provides a very flexible framework, it might make sense to restrict it, allowing for a simpler implementation.
The choice of which level of incrementality to allow has by far the largest impact, and we discuss this issue in more depth in~\cite{Kremer2020}.
Note that we always assume that the lifting is performed incrementally (or rather partially in the spirit of~\cite{Collins1991}), but vary which constraints are considered and how the projection is performed.

The easiest variant would be to compute a full CAD from scratch for every theory call, using no form of incrementality at all. This variant not only avoids any bookkeeping -- like \emph{origins} that we described before -- but also allows integrating external tools that are not built with incremental operations in mind.
We call this variant \emph{no incrementality} (\SolverCADIncNone).
Please consider~\cite{Kremer2018b} for some experiences with integrating external tools for this purpose.

Secondly, we can compute a full CAD for every individual theory call but retain the information from the previous theory call and only extend it. This approach exploits that consecutive theory calls are usually very similar, but ignores the possibility to only consider part of the constraints to obtain a conclusive answer. We call this variant \emph{naive incrementality} (\SolverCADIncNaive).

The next step is to only consider the constraints one after another. In what we call \emph{simple incrementality} (\SolverCADIncSimple) we only add a single constraint, complete the CAD, and check whether a solution can be found. This allows to avoid considering \emph{hard} constraints in some cases.

Finally, we have implemented the fully incremental projection from the above proof system which we call \emph{full incrementality} (\SolverCADIncFull) and a variant where polynomials are only \emph{hidden} instead of removed (\SolverCADIncFull-hide). The latter one was implemented primarily to mitigate frequent recomputations when using \emph{equational constraints} as described in \cref{sec:cad:equational-constraints}, though equational constraints are not exploited here.

Note that we may also consider to allow backtracking constraints in a different order than how they were added. We call this \emph{non-chronological backtracking} and discuss it in some depth in~\cite{Kremer2020}. It comes rather naturally with the presented notion of origins but has only a negligible impact on practical performance.

\begin{table}
	\centering
	\input{experiments/output/table-cad-incrementality}
	\caption{Experimental results for different variants of incrementality.}\label{tbl:cad:incrementality-results}
\end{table}

Some results for these different variants of incrementality are presented in \cref{tbl:cad:incrementality-results}, showing that the benefits of incremental CAD computations significantly outweigh the costs of additional bookkeeping.

\begin{figure}
	\centering
	\includetikz{06c-cad-incrementality}
	\caption{Experimental results for \SolverCADIncFull for SAT and UNSAT.}\label{fig:cad:incrementality:sat-unsat}
\end{figure}

We can see from \cref{fig:cad:incrementality:sat-unsat} that the gain is much more substantial for unsatisfiable problems.
This may be surprising, considering that we need to compute a full CAD in the case of unsatisfiability which can not benefit from incrementality at all.
However, unsatisfiable problem instances oftentimes yield a sequence of theory calls (of which many are satisfiable individually) which can benefit significantly from incrementality.
Satisfiable problem instances, on the other hand, are usually either solved very quickly or never and thus the improvements due to incrementality are not that significant here.
One view could be that one needs to be ``lucky'' to find a satisfying assignment while unsatisfiability is often found more systematically.

\cref{fig:cad:incrementality:sat-unsat} shows that after more than one second, only very few problems are found to be satisfiable (less than $2\%$) but a significant number of unsatisfiable problems are still solved (more than $14\%$).
Naturally, only instances that take a considerable time to solve in the first place can be subject to significant run time improvements.
