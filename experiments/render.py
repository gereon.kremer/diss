import filecmp
import jinja2
from jinja2 import Environment, FileSystemLoader
import logging
import os
import os.path
import re

import config
import database

OUTPUT_DIR = "output/"

os.makedirs(OUTPUT_DIR, exist_ok = True)

jenv = jinja2.Environment(
    loader = FileSystemLoader('.')
)
jenv.globals["getSolvers"] = database.getSolvers
jenv.globals["numberOfFiles"] = database.numberOfFiles
jenv.globals["texName"] = config.texName
jenv.globals["shortName"] = config.shortName
jenv.globals["getStats"] = database.getStats
jenv.globals["getAllStats"] = database.getAllStats
jenv.globals["getPossibleAnswers"] = database.getPossibleAnswers
jenv.globals["getCummulativeData"] = database.getCummulativeData
jenv.globals["solvedByXnotY"] = database.solvedByXnotY
jenv.globals["solvedNotByX"] = database.solvedNotByX
jenv.globals["refsolver"] = "smtrat_SAT"

def write_if_changed(f):
	def wrapper(filename, *args, **kwargs):
		tmpfile = filename + ".tmp"
		f(tmpfile, *args, **kwargs)
		if not os.path.exists(OUTPUT_DIR + filename):
			os.rename(OUTPUT_DIR + tmpfile, OUTPUT_DIR + filename)
		elif not filecmp.cmp(OUTPUT_DIR + filename, OUTPUT_DIR + tmpfile):
			os.rename(OUTPUT_DIR + tmpfile, OUTPUT_DIR + filename)
		else:
			os.remove(OUTPUT_DIR + tmpfile)
	return wrapper

def add_file_suffix(filename, suffix):
	base,ext = os.path.splitext(filename)
	return "%s_%s%s" % (base,suffix,ext)

@write_if_changed
def write_scatter_data(filename, data):
	f = open(OUTPUT_DIR + filename, "w")
	for d in data:
		f.write("%f\t%f\n" % d)
	f.close()

def write_plot_data_percentage(filename, data, cap = None):
	"""
	filename is relative to OUTPUT_DIR
	data should be an iterable over tuples with (value,count)
	cap can be set to add a final point add (total,cap)
	"""
	total = sum(map(lambda x: x[1], data))
	cur = 0
	f = open(OUTPUT_DIR + filename, "w")
	f.write("0\t0\n")
	for d in data:
		cur += d[1]
		f.write("%f\t%f\n" % (100.0 * cur / total, d[0]))
	if cap is not None:
		f.write("%f\t%f\n" % (100.0 * cur / total, cap))
	f.close()

@write_if_changed
def write_cummulative_plot_data(filename, data, min = None, max = None):
	"""
	filename is relative to OUTPUT_DIR
	data should be an iterable over tuples with (value,count)
	min and max can insert artificial points for at the start and end
	"""
	cur = 0
	f = open(OUTPUT_DIR + filename, "w")
	if min is not None:
		f.write("0\t%s\n" % (min,))
	for d in data:
		cur += d[1]
		f.write("%s\t%s\n" % (cur, d[0]))
	if max is not None:
		f.write("%s\t%s\n" % (cur, max))
	f.close()

def write_bar_chart_data(filename, data, count = 2):
	f = open(OUTPUT_DIR + filename, "w")
	for i in range(count):
		for d in data:
			f.write("%s\t" % d[i])
		f.write('\n')
	f.close()

@write_if_changed
def scatter_main_file(filename, solver1, solver2, where = None):
	f = open(OUTPUT_DIR + filename, "w")
	for d in database.scatterData(solver1, solver2, where):
		f.write("%f\t%f\n" % (d["runtime1"], d["runtime2"]))
	f.close()

@write_if_changed
def scatter_xto_file(filename, solver1, solver2, where = None):
	f = open(OUTPUT_DIR + filename, "w")
	if where is None: xto_where = "(answer1=\'timeout\')"
	else: xto_where = "%s AND (answer1=\'timeout\')" % where
	for d in database.scatterData(solver1, solver2, xto_where):
		f.write("%f\n" % d["runtime2"])
	f.close()

@write_if_changed
def scatter_yto_file(filename, solver1, solver2, where = None):
	f = open(OUTPUT_DIR + filename, "w")
	if where is None: yto_where = "(answer2=\'timeout\')"
	else: yto_where = "%s AND (answer2=\'timeout\')" % where
	for d in database.scatterData(solver1, solver2, yto_where):
		f.write("%f\n" % d["runtime1"])
	f.close()

def scatter(filename, solver1, solver2, where = None):
	logging.info('Writing scatter plot data for %s vs. %s to %s' % (solver1, solver2, filename))	
	scatter_main_file(filename, solver1, solver2, where)
	scatter_xto_file(add_file_suffix(filename, "xto"), solver1, solver2, where)
	scatter_yto_file(add_file_suffix(filename, "yto"), solver1, solver2, where)

@write_if_changed
def table(filename, selection, template = 'table', override = {}, **kwargs):
	logging.info('Writing %s to %s' % (template, filename))
	tpl = jenv.get_template(template + '.tpl')
	f = open(OUTPUT_DIR + filename, "w")
	f.write(tpl.render(solvers = filter(lambda s: s["solver"] in selection, database.getStats()), override = override, **kwargs))
	f.close()

@write_if_changed
def table_solved_within(filename, solver, times = [0.1, 1, 10, 20, 60, 120]):
	logging.info('Writing solved-within table for %s to %s' % (solver, filename))
	data = []
	for t in times:
		data.append((t, database.solved_within_time(solver, t)))
	tpl = jenv.get_template('table-solved-within.tpl')
	f = open(OUTPUT_DIR + filename, "w")
	f.write(tpl.render(data = data))
	f.close()

@write_if_changed
def table_overview(filename):
	logging.info('Writing overview table to %s' % filename)
	short = { 'memout': 'MO', 'timeout': 'TO', 'invalid': '?', 'unknown': 'unk', 'segfault': 'segf' }
	shorten = lambda x: short[x] if x in short else x
	answers = list(map(shorten, database.existing_answers()))
	data = database.existing_answers_by_solver()
	solvers = {}
	for d in data:
		if not d['solver'] in solvers:
			solvers[d['solver']] = {}
		solvers[d['solver']][shorten(d['answer'])] = d['count']
	for s in solvers:
		sum = 0
		for a in solvers[s]: sum = sum + solvers[s][a]
		solvers[s]['sum'] = sum
	tpl = jenv.get_template('table-overview.tpl')
	f = open(OUTPUT_DIR + filename, "w")
	f.write(tpl.render(solvers = solvers, answers = answers))
	f.close()

def plot_data(filename_pattern, solver, **kwargs):
	for s in solver:
		logging.info("Writing plot data for %s" % s)
		data = database.getCummulativeData(s, **kwargs)
		write_cummulative_plot_data(filename_pattern % s, data)

def writeTable(filename, selection, override = {}, **kwargs):
	logging.info("Writing table to %s" % filename)
	tpl = jenv.get_template('table.tpl')
	
	f = open(OUTPUT_DIR + filename, "w")
	f.write(tpl.render(solvers = filter(lambda s: s["solver"] in selection, database.getStats()), override = override, **kwargs))
	f.close()

def writeDebugTable(filename):
	logging.info("Writing table to %s" % filename)
	tpl = jenv.get_template('table-debug.tpl')
	f = open(OUTPUT_DIR + filename, "w")
	f.write(tpl.render(solvers = database.getStats()))
	f.close()

def writePlotData(selection):
	for s in selection:
		logging.info("Writing plot data for %s" % s)
		(total, data) = database.getCummulativeData(s)
		cur = 0
		f = open(OUTPUT_DIR + "plot_data_%s.data" % s, "w")
		f.write("0\t0\n")
		for d in data:
			cur += d["cnt"]
			f.write("%f\t%f\n" % (100.0 * cur / total, d["runtime"]))
		f.write("%f\t%f\n" % (100.0 * cur / total, 60))
		f.close()

def writeScatterData(filename, solver1, solver2):
	logging.info("Writing scatter plot data for %s vs. %s" % (solver1, solver2))
	f = open(OUTPUT_DIR + filename, "w")
	for d in database.scatterData(solver1, solver2):
		f.write("%f\t%f\n" % (d["runtime1"], d["runtime2"]))
	f.close()

def file_statistics(filename):
	pass

def file_property(filename, property):
	logging.info('Writing file property plot data for %s to %s' % (property, filename))
	write_cummulative_plot_data(filename, database.file_info_data(property))

def file_property_str(filename, property):
	logging.info('Writing file property bar chart data for %s to %s' % (property, filename))
	write_bar_chart_data(filename, [('answer', 'num')] + database.file_info_data_str(property))

@write_if_changed
def table_properties(filename, specs):
	logging.info('Writing custom properties table to %s' % filename)
	head = ['property', 'solved']
	data = []
	for spec in specs:
		data.append((spec[0], database.file_with_info_count(spec[1])))

	tpl = jenv.get_template('table-generic.tpl')
	f = open(OUTPUT_DIR + filename, "w")
	f.write(tpl.render(tabular_spec = 'XS[table-format=5]', head = head, data = data))
	f.close()

@write_if_changed
def scatter_solvers(filename, solvers):
	logging.info('Writing solver scatter plot data to %s' % filename)
	f = open(OUTPUT_DIR + filename, "w")
	for d in database.getStats():
		if re.match(solvers, d["solver"]) != None:
			f.write("%s\t%d\t%d\n" % (d["solver"], d["sat"], d["unsat"]))
	f.close()
