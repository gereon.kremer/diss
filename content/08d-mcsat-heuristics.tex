\section{Heuristics}

As already discussed, the \MCSAT proof system is rather a framework than an actual algorithm and we need to implement various heuristics.
These include the overall scheduling of rules, the choice of an assignment finder, and an explanation function, but also topics we have not covered yet, in particular the variable ordering.
Of course, a wide range of further heuristics is present, much like what we presented in \cref{ssec:cad:heuristics:other}, that we do not discuss here.

\subsection{Variable ordering\label{ssec:mcsat:heuristics:variable-ordering}}

\silentdefindex{mcsat:variable-ordering}
In \cref{algo:mcsat:implementation:pick} of \cref{algo:mcsat:implementation} we pick a decision variable without any further specification other than that it can be either a Boolean or a theory variable and is not yet assigned.
One possible implementation can be found in~\cite{Jovanovic2012} with a static theory ordering and an ordering on Boolean variables governed by whether the respective constraint is univariate under the partial theory model.
Another possibility that is used for linear arithmetic only in~\cite{Jovanovic2013} treats theory variables and Boolean variables more uniformly and allows the (theory) variable ordering to change.

We analyzed those and more variants in~\cite{Nalbach2019}, showing that selecting a proper variable ordering -- or rather a dynamic heuristic to select one -- is both an important factor for practical performance and all but trivial.
In fact, we find in~\cite{Nalbach2019} -- which contains a more detailed discussion of these issues than presented here -- that our two best heuristics perform almost equally good, but for apparently different reasons as a hybrid between the two shows rather poor results.

While a dynamic ordering of theory variables seems to be beneficial, it also brings new problems for the \MCSAT framework itself.
As already discussed in \cref{ssec:preliminaries:fol:extensions}, we might see extended polynomial constraints that can not be evaluated as the only remaining variable is not its left-hand side, but one from its root expression.
Under a static variable ordering -- like described in~\cite{Jovanovic2012} -- the left-hand side is always the largest among all involved variables, preventing this issue.
Our approach to resolving this issue is to \emph{disable} constraints from an \emph{incompatible} variable ordering and reactivate them once the variable ordering is compatible again as described in~\cite{Nalbach2019}.

Furthermore, all arguments for the (refutational) completeness of \MCSAT depend on a static theory variable ordering, which, in fact, is not just a technicality. Considering the following \cref{ex:incompleteness-dynamic-ordering} we see that a dynamic ordering on the theory variables indeed has the potential to produce infinite loops.

\begin{example}{Incompleteness under dynamic theory ordering}{incompleteness-dynamic-ordering}
	Let $x_1 = 2 \land x_1 = 2 x_2 \land x_2 = 2 x_1$ be the input formula and assume we use an explanation function based on Fourier--Motzkin variable elimination (from \cref{ssec:mcsat:Fourier-Motzkin}) or virtual substitution (from \cref{ssec:mcsat:virtual-substitution}). Consider the following sequence of explanations:
	\begin{align*}
		& \Seq{x_1 = 2 x_2, x_1 = 2, x_2 \mapsto 2} && \implies x_2 = 1 \\
		& \Seq{x_2 = 2 x_1, x_2 = 1, x_1 \mapsto 2} && \implies x_1 = 0.5 \\
		& \Seq{x_1 = 2 x_2, x_1 = 0.5, x_2 \mapsto 2} && \implies x_2 = 0.25 \\
		& \Seq{x_2 = 2 x_1, x_2 = 0.25, x_1 \mapsto 2} && \implies x_1 = 0.125
	\end{align*}
	We trust the reader to realize that we can continue this sequence arbitrarily and that it is only made possible because we can switch arbitrarily between the variable ordering $x_1 < x_2$ and $x_2 < x_1$.
\end{example}

On a practical note, we neither observed such issues in practice nor do they seem particular likely. On the contrary, we rather expect a dynamic theory variable ordering to settle on some ordering that stays (almost) static instead of changing over and over again.
To settle this issue in theory, we suggest to impose a restriction on when the variable ordering may change similar to when we may restart the whole solver.
If we force the variable ordering to stay constant for growing periods of time, the solver is bound to eventually solve the problem within one such period, following the argument from \cref{ssec:cdcl:restart}.
