\section{Explanation functions and termination}\label{sec:mcsat:explanation-functions}

\MCSAT itself is agnostic of the theory being used and the most important component that performs theory reasoning is the explanation function. We define an explanation function, roughly following~\cite{Moura2013}, as follows.

\begin{definition}{\MCSAT explanation function}{mcsat:explanation}\silentdefindex{mcsat:explanation-function}
	Let $M$ be an \MCSAT trail that is \emph{infeasible}. We call
	\[
		E(M) \rightarrow \Phi_\T
	\]
	an \emph{explanation function} if for every infeasible trail $M$ it generates a \emph{valid theory lemma} that is \emph{inconsistent} with $M$.
	We call $E$ \emph{incomplete} if it fails to generate a valid theory lemma under certain conditions. Unless stated otherwise, we assume all explanation functions to be \emph{complete}.
\end{definition}

Let us illustrate this definition using a few examples.
What the explanation looks like mainly depends on the theory and why the trail is infeasible.
The easiest case occurs if the trail is infeasible only due to theory literals from the trail as shown in \cref{ex:mcsat:explain:boolean-conflict}. Here we essentially employ the same reasoning as in a regular lazy SMT-style theory solver: we certify infeasibility of a set of theory atoms using an infeasible subset of these theory atoms.

\begin{example}{Boolean conflict in a trail}{mcsat:explain:boolean-conflict}
	Let $M = \Seq{y < 0, x^2 > 2, x \mapsto 3, y > 1}$ and observe that $M$ is consistent but infeasible. A suitable explanation is $(y \geq 0 \lor y \leq 1)$.

	In general, if literals $L_1 \land \dots \land L_k$ are infeasible with $L_1, \dots, L_k \in M$ then $(\neg L_1 \lor \dots \lor \neg L_k)$ is a suitable explanation.
\end{example}

Unfortunately, it may get more complicated if theory assignments need to be considered. Let us first review the effects of blindly applying the above technique to a slightly changed input problem in \cref{ex:mcsat:explain:theory-conflict-naive}.

\begin{example}{Naive handling of theory conflicts}{mcsat:explain:theory-conflict-naive}
	Let $M = \Seq{x + y < 0, x^2 > 2, x \mapsto 3, y > 1}$ and observe that $M$ is consistent but infeasible: we substitute $x \mapsto 3$ into $x + y < 0$ and obtain $y < -3$ which conflicts with $y > 1$.
	Generating a suitable explanation as before -- where we use $x \neq 3$ as the negation of $x \mapsto 3$ -- we obtain $(x + y \geq 0 \lor x \neq 3 \lor y \leq 1)$.
	We observe that we can satisfy the new explanation clause easily, for example, by $x \mapsto 3.1$. However, this not only leads us into the same conflict, it also indicates that we can do this infinitely often by slightly changing the assignment for $x$:
	\[ x \mapsto 3.1, x \mapsto 3.01, x \mapsto 3.001, \dots \]
\end{example}

Toherefore, simply negating a theory assignment $x \mapsto 3$ by constructing a disequality $x \neq 3$ is insufficient, as it might pave the way to nontermination.
Let us reconsider the conceptual idea of finding the assignment $x \mapsto 3$ in \cref{sec:mcsat:assignment-finding}, in particular the idea that we identify a \emph{satisfying sign-invariant region} and only select $x \mapsto 3$ as a representative from this region.

Following this intuition, the negation of the theory assignment $x \mapsto 3$ should cover a whole region of assignments (though not necessarily the same region that we constructed in the assignment finder). Let us rework the last example and try to find such a region we can exclude in \cref{ex:mcsat:explain:theory-conflict-region}.

\begin{example}{Regions for theory conflicts}{mcsat:explain:theory-conflict-region}
	\vspace*{-2ex}
	\begin{minipage}{0.68\textwidth}
		As before, let $M = \Seq{x + y < 0, x^2 > 2, x \mapsto 3, y > 1}$ and observe that $M$ is still consistent but infeasible due to $x + y < 0$, $x \mapsto 3$ and $y > 1$.
		Let us now identify the underlying conflict here, independent of the concrete assignment of $x$. We rewrite $y > 1$ to $-y < -1$ and add it to $x + y < 0$ to obtain $x < -1$ which conflicts with $x \mapsto 3$.
	\end{minipage}\hfill{}
	\begin{minipage}{0.3\textwidth}
		\includetikz{07d-explanation-conflict}
	\end{minipage}
	
	We can also deduce this graphically from the plot on the right.
	Observe that the common solution space of the two inequalities $x + y < 0$ and $y > 1$ expands to the left starting at (but not including) $x = -1$, nicely coinciding with the constraint we obtained algebraically.

	Following this line of argument, we can thus infer that $x < -1$ and generate a new explanation $(x + y \geq 0 \lor y \leq 1 \lor x < -1)$.
\end{example}

Excluding \emph{some} region instead of a single assignment is not enough, as we could still come across an infinite sequence of such regions as we show in \cref{ex:mcsat:explain:theory-conflict-infinitely-many-regions} and -- for an arguably more plausible explanation function -- \cref{ex:mcsat:explain:theory-conflict-infinitely-many-regions-newton}.

\begin{example}{Regions of minimal size for theory conflicts}{mcsat:explain:theory-conflict-infinitely-many-regions}
	As before, let $M = \Seq{x + y < 0, x^2 > 2, x \mapsto 3, y > 1}$ and observe that $M$ is still consistent but infeasible due to $x + y < 0$, $x \mapsto 3$ and $y > 1$.
	
	Let us assume the explanation function tries to avoid sequences like $x \mapsto 3.1$, $x \mapsto 3.01$, $x \mapsto 3.001$, \dots~by excluding regions of a certain minimum size, say $1$.
	For $x \mapsto 3$ we could generate the valid explanation $(x + y \geq 0 \lor y \leq 1 \lor x < 3 \lor x > 4)$.

	This however only shifts the problem a bit, allowing for a sequence like $x \mapsto 3$, $x \mapsto 5$, $x \mapsto 7$, \dots\ and, furthermore, may fail if the region we could exclude is smaller than the minimum size we impose.
\end{example}

While the very simple explanation function from \cref{ex:mcsat:explain:theory-conflict-infinitely-many-regions} highlights the fundamental problem, one might argue that it is not only naive but also rather contrived to make the point.
Therefore, we propose another explanation function that we hope looks somewhat more reasonable.
We already observed that real roots separate the sign-invariant regions and our approach allows us to argue about univariate problems, and, thus, it might seem promising to employ the numerical go-to tool for such problems: Newton's method.

\begin{example}{Employing Newton iteration for theory conflicts}{mcsat:explain:theory-conflict-infinitely-many-regions-newton}

	Let us consider $M = \Seq{y = x^2, y \leq 0, x \mapsto 10}$. We observe that $M$ is consistent but infeasible. It might seem reasonable to employ a classical method for such a problem like the Newton method.

	We propose two possibilities to employ Newton-style reasoning.
	Let $f(x) = x^2$ and $x^* = 10$ and observe that we must cross a root of $f(x)$ in order to reach a satisfiable region due to $y \leq 0$.

	We first consider how to exclude the half-open spaces towards $-\infty$ and $\infty$.
	If we can show that no root exists for any $x > x^*$ (or $x < x^*$) we can exclude $[x^*, \infty)$ (or $(-\infty,x^*]$) by reasoning that $f(x^*) \neq 0$ and $f'(x) \neq 0$ for all $x > x^*$ (or $x < x^*$).
	Here, we can generate the explanation $(y \neq x^2 \lor y > 0 \lor x < 10)$.

	From $x^*$ towards the roots of $f(x)$ we can employ the Newton iteration as follows. If we can show that we did not step over a root of $f(x)$ with a single Newton step, we can exclude the interval between the last and the current $x$ value:
	\[
		x' = x^* - \frac{f(x^*)}{f'(x^*)} = 10 - \frac{10^2}{2 \cdot 10} = 5
	\]
	To certify that $f(x)$ has no root for $x \in [5,10]$, we can employ interval arithmetic to see that $0 \not\in f([5,10]) = [25,100]$, use the \emph{intermediate value theorem} giving us $f(5) > 0$, $f(10) > 0$, $f'([5,10]) > 0$, and thus $f(x) \neq 0$ for all $x \in [5,10]$, or even algebraic tools like Sturm's theorem.
	Consequently, we can provide the explanation $(y \neq x^2 \lor y > 0 \lor x < 5 \lor 10 < x)$.

	Subsequent Newton steps could however yield smaller and smaller intervals that converge towards $x = 0$, but never actually reach it. The sequence
	\begin{align*}
		& x \mapsto 4 && x' = 4 - \frac{f(4)}{f'(4)} = 4 - \frac{16}{8} = 2 && 0 \not\in f([2,4]) \\
		& x \mapsto 1 && x' = 1 - \frac{f(1)}{f'(1)} = 1 - \frac{1}{2} = 0.5 && 0 \not\in f([0.5,1]) \\
		& x \mapsto 0.25 && x' = 0.25 - \frac{f(0.25)}{f'(0.25)} = 0.25 - \frac{0.0625}{0.5} = 0.125 && 0 \not\in f([0.125,0.25])
	\end{align*}
	excludes the intervals $[2,4]$, $[0.5,1]$, $[0.125, 0.25]$ and may continue indefinitely, but never actually reach zero.
\end{example}

Considering the previous examples of unsatisfactory explanation functions, we now give a criterion that ensures termination and is met by most of the explanation function that we present afterward.
It comes back to one of the first concepts introduced in \cref{sec:mcsat:definition}, the \emph{finite basis}.
In all of the above examples, we failed to construct explanations from such a finite basis but instead allowed the explanation function to construct infinitely many different theory atoms for a single input formula.

\begin{definition}{Finite-basis property}{mcsat:finite-basis-property}\silentdefindex{mcsat:finite-basis-property}
	Let $E$ be an explanation function and $\varphi$ some input formula.
	We say that $E$ \emph{satisfies the finite-basis property} if for every $\varphi$ all new theory atoms that $E$ may generate for this $\varphi$ come from a finite basis $\Basis$.
\end{definition}

The intuition is essentially that an explanation function may not use the (partial) model to \emph{construct} the explanation, but only to \emph{guide} which explanation (that is constructed from the formula) should be returned. Theories over finite domains are an exception here, as only finitely many models exist anyway and, thus, it is sufficient to exclude models individually -- though we probably could achieve significant improvements by excluding whole regions.

Note that an explanation function that satisfies the finite-basis property ensures termination of \MCSAT, formalized in the following \cref{thm:mcsat:finite-basis-termination}. We refer to~\cite[Theorem 1]{Moura2013} for a proof.

\begin{theorem}{Finite-basis property implies termination}{mcsat:finite-basis-termination}
	Let $E$ be a (complete) explanation function that satisfies the finite basis property.
	Then \MCSAT equipped with $E$ always terminates.
\end{theorem}

We feel that discussing what methods may be usable as an explanation function and which properties imply the finite-basis property can be rather insightful.
Very abstractly, we can characterize the task of an explanation function as follows:
given a problem in $n$ variables and a model in $k < n$ variables that falsifies the input problem, provide a formula in these $k$ variables that
\begin{enumerate*}
	\item follows from the given problem and
	\item is still falsified by the model.
\end{enumerate*}

This closely resembles problems like \emph{finding interpolants} or \emph{quantifier elimination} in general, and, in fact, most explanation functions we present later are based on quantifier elimination procedures.
The fundamental idea is to eliminate the variables not contained in the model and let the resulting formula describe a region \emph{around the model} to generalize from a single theory assignment.

All quantifier elimination methods -- at least all that we discuss in this context -- share an interesting property: they heavily rely on a fixed variable ordering to iteratively eliminate these variables.
Furthermore, the (partial) theory model is not used in the quantifier elimination process itself, but only \emph{selects} the appropriate part of the resulting formula that is then used for the explanation.
Thus, we can give a meta-argument for termination, based on this observation.

\begin{theorem}{Finite-basis property by quantifier elimination}{mcsat:finite-basis-by-qe}
	Let $E_x$ be a \emph{quantifier elimination procedure} that eliminates $x$ from a given formula and $\A$ a theory model.
	We assume that $E_x$ returns a \emph{finite} formula and only uses constraints of a higher theory dimension to create new constraints of a lower theory dimension.
	An explanation function based on $E_x$ can be obtained as follows:
	\begin{enumerate*}
		\item apply $E_x$ for every $x \not\in \A$,
		\item convert the resulting formula to conjunctive normal form, and
		\item select a clause $C$ such that $\A \not\models C$.
	\end{enumerate*}

	Such an explanation function satisfies the finite basis property.
\end{theorem}
\begin{proof}
	We observe that every application of $E_x$ only creates new constraints that no longer contain $x$ and are thus on a \emph{lower theory level}.
	Furthermore, the set of new constraints is \emph{finite} -- as $E_x$ returns a finite formula -- and $E_x$ works independently of $\A$ and only constraints of a higher decision level contribute to new constraints of a lower decision level.

	We immediately derive that $E_x$ never constructs new constraints on the \emph{highest theory level}.
	Thus we can obtain all possible constraints on the second-highest decision level by applying $E_x$ on all (finitely many) possible combinations of the input constraints.
	Applying this argument iteratively gives all constraints on all lower theory levels.
	As the result of $E_x$ is always finite, we obtain finitely many constraints that constitute our finite basis.
\end{proof}

Intuitively, we exploited that information \emph{flows downwards} with respect to the theory levels, thus simply ``letting any information flow down'' allows us to compute all constraints that may ever come up.
Also note that this argument is not necessarily specific to quantifier elimination procedures, but could be applied more generally to explanation functions that exhibit this kind of information flow.

This discussion immediately gives us some insight into when the finite-basis property may break: if constraints are, whatever reason, constructed using other constraints from a lower level.
While this may just be how the method works, we can also end up in such a case if we (repeatedly) change the variable ordering.
