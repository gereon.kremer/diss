#include <cassert>
#include <iostream>

#include <CoCoA/library.H>
#include <carl/core/MultivariatePolynomial.h>
#include <carl/core/polynomialfunctions/Factorization.h>
#include <carl/core/polynomialfunctions/Resultant.h>

namespace cad {
	auto example_projection() {
		std::cout << "CAD::example projection" << std::endl;
		using Rational = mpq_class;
		using Poly = carl::MultivariatePolynomial<Rational>;
		auto x = carl::freshRealVariable("x");
		auto y = carl::freshRealVariable("y");
		
		auto p = (Poly(y)*y - Poly(x)*x*x + Poly(3)*x - Poly(2)).toUnivariatePolynomial(y);
		auto q = ((Poly(2)*x + Poly(1))*(Poly(y) - Poly(1)) - Poly(3)*x).toUnivariatePolynomial(y);
		std::cout << "\tp = " << p << std::endl;
		std::cout << "\tq = " << q << std::endl;
		std::cout << "\tcoeffs(p): " << p.coefficients() << std::endl;
		std::cout << "\tcoeffs(q): " << q.coefficients() << std::endl;
		std::cout << "\tdisc(p): " << carl::discriminant(p) << " = " << carl::factorization(Poly(carl::discriminant(p))) << std::endl;
		std::cout << "\tdisc(q): " << carl::discriminant(q) << " = " << carl::factorization(Poly(carl::discriminant(q))) << std::endl;
		std::cout << "\tres(p,q): " << carl::resultant(p,q) << " = " << carl::factorization(Poly(carl::resultant(p,q))) << std::endl;
	}
}

namespace lazard {
	auto newPRing(CoCoA::ring base, const std::string& name) {
		auto Qx = CoCoA::NewPolyRing(base, CoCoA::symbols(name));
		auto QCx = CoCoA::CoeffEmbeddingHom(Qx);
		auto x = CoCoA::indets(Qx)[0];
		return std::make_tuple(Qx, QCx, x);
	}
	auto newQRing(CoCoA::ring base, CoCoA::RingElem poly) {
		auto adjx = CoCoA::NewQuotientRing(base, CoCoA::ideal(poly));
		auto Hx = CoCoA::QuotientingHom(adjx);
		return std::make_tuple(adjx, Hx);
	}
	auto extractZeroFactor(CoCoA::RingElem poly, CoCoA::RingElem factor) {
		auto factorization = CoCoA::factor(poly);
		std::cout << "\tfactor(" << poly << ") = " << factorization << std::endl;
		for (const auto& f: factorization.myFactors()) {
			if (f == factor) {
				std::cout << "\tZero factor: " << f << std::endl;
				return f;
			}
		}
		assert(false);
		return poly;
	}

	namespace naive {
		void example_1() {
			std::cout << "Lazard::naive::example 1" << std::endl;
			auto Q = CoCoA::RingQQ();
			auto [Qx, QCx, x] = newPRing(Q, "x");
			auto [Qadjx, QHx] = newQRing(Qx, x*x-2);
			auto [Qy, QCy, y] = newPRing(Qadjx, "y");
			auto [Qadjy, QHy] = newQRing(Qy, y*y-2);
			auto [Qz, QCz, z] = newPRing(Qadjy, "z");
			
			auto q = QCz(QHy(QCy(QHx(x))-y))*z;
			std::cout << "\tq = (x-y)*z = " << q << std::endl;
		}
		void example_2() {
			std::cout << "Lazard::naive::example 2" << std::endl;
			auto Q = CoCoA::RingQQ();
			auto [Qx, QCx, x] = newPRing(Q, "x");
			auto [Qadjx, QHx] = newQRing(Qx, 16*x*x*x*x-24*x*x+6);
			auto [Qy, QCy, y] = newPRing(Qadjx, "y");
			auto [Qadjy, QHy] = newQRing(Qy, y*y*y*y-6*y*y+6);
			auto [Qz, QCz, z] = newPRing(Qadjy, "z");
			
			auto q = QCz(QHy(QCy(QHx(2*x))-y))*z;
			std::cout << "\tq = (2*x-y)*z = " << q << std::endl;
		}
		void example_3() {
			std::cout << "Lazard::naive::example 3" << std::endl;
			auto Q = CoCoA::RingQQ();
			auto [Qx, QCx, x] = newPRing(Q, "x");
			auto [Qadjx, QHx] = newQRing(Qx, x*x-2);
			auto [Qy, QCy, y] = newPRing(Qadjx, "y");
			auto [Qadjy, QHy] = newQRing(Qy, y*y*y*y-2);
			auto [Qz, QCz, z] = newPRing(Qadjy, "z");
			
			auto q = QCz(QHy(QCy(QHx(x))-y*y))*z;
			std::cout << "\tq = (x-y*y)*z = " << q << std::endl;
		}
	}
	namespace proper {
		void example_1() {
			std::cout << "Lazard::proper::example 1" << std::endl;
			auto Q = CoCoA::RingQQ();
			auto [Qx, QCx, x] = newPRing(Q, "x");
			auto [Qadjx, QHx] = newQRing(Qx, x*x-2);
			auto [Qy, QCy, y] = newPRing(Qadjx, "y");
			auto f = extractZeroFactor(y*y-2, y-QCy(QHx(x)));
			auto [Qadjy, QHy] = newQRing(Qy, f);
			auto [Qz, QCz, z] = newPRing(Qadjy, "z");
			
			auto q = QCz(QHy(QCy(QHx(x))-y))*z;
			std::cout << "\tq = (x-y)*z = " << q << std::endl;
		}
		void example_2() {
			std::cout << "Lazard::naive::example 2" << std::endl;
			auto Q = CoCoA::RingQQ();
			auto [Qx, QCx, x] = newPRing(Q, "x");
			auto [Qadjx, QHx] = newQRing(Qx, 16*x*x*x*x-24*x*x+6);
			auto [Qy, QCy, y] = newPRing(Qadjx, "y");
			auto f = extractZeroFactor(y*y*y*y-6*y*y+6, y-QCy(QHx(2*x)));
			auto [Qadjy, QHy] = newQRing(Qy, f);
			auto [Qz, QCz, z] = newPRing(Qadjy, "z");
			
			auto q = QCz(QHy(QCy(QHx(2*x))-y))*z;
			std::cout << "\tq = (2*x-y)*z = " << q << std::endl;
		}
		void example_3() {
			std::cout << "Lazard::proper::example 3" << std::endl;
			auto Q = CoCoA::RingQQ();
			auto [Qx, QCx, x] = newPRing(Q, "x");
			auto [Qadjx, QHx] = newQRing(Qx, x*x-2);
			auto [Qy, QCy, y] = newPRing(Qadjx, "y");
			auto f = extractZeroFactor(y*y*y*y-2, y*y-QCy(QHx(x)));
			auto [Qadjy, QHy] = newQRing(Qy, f);
			auto [Qz, QCz, z] = newPRing(Qadjy, "z");
			
			auto q = QCz(QHy(QCy(QHx(x))-y*y))*z;
			std::cout << "\tq = (x-y*y)*z = " << q << std::endl;
		}
	}
}

void Example() {
	std::cout << "Example of finite field with sqrt(2) and sqrt(3)" << std::endl;

	auto Q = CoCoA::RingQQ();
	
	auto [Qx, QCx, x] = lazard::newPRing(Q, "x");
	
	auto cf = x*x*x*x-6*x*x+6;
	auto [Qadjx, QHx] = lazard::newQRing(Qx, cf);
	auto Qy = CoCoA::NewPolyRing(Qadjx, CoCoA::symbols("y"));
	auto QCy = CoCoA::CoeffEmbeddingHom(Qy);
	auto y = CoCoA::indets(Qy)[0];
	//auto cg = y*y*y - 16;
	//auto cg = QCy(QHx(2*x)) - y;
	auto cg = y*y*y*y-6*y*y+6;
	
	
	auto Qadjy = CoCoA::NewQuotientRing(Qy, CoCoA::ideal(cg));
	auto QHy = CoCoA::QuotientingHom(Qadjy);
	
	auto Qz = CoCoA::NewPolyRing(Qadjy, CoCoA::symbols("z"));
	auto QCz = CoCoA::CoeffEmbeddingHom(Qz);
	auto z = CoCoA::indets(Qz)[0];
	
	auto p = QCz(QHy(QCy(QHx(2*x))-y))*z;

	std::cout << "Q = " << Q << std::endl;
	std::cout << "Q[x] = " << Qx << std::endl;
	std::cout << "Q(x) = " << Qadjx << std::endl;
	std::cout << "Q(x)[y] = " << Qy << std::endl;
	std::cout << "factor(" << cg << ") = " << CoCoA::factor(cg) << std::endl;
	std::cout << "Q(x)(y) = " << Qadjy << std::endl;
	std::cout << "Q(x)(y)[z] = " << Qz << std::endl;
	std::cout << "p = " << p << " = " << CoCoA::CanonicalRepr(p) << std::endl;
	/*
	auto x2 = QCy(QHx(x));
	{
		auto q = (x2*x2*x2*x2 - 6*x2*x2 + 8) * (y+2);
		std::cout << q << std::endl;
	}
	{
		auto Qxy = CoCoA::NewPolyRing(Q, CoCoA::symbols("x,y"));
		auto x = CoCoA::indets(Qxy)[0];
		auto y = CoCoA::indets(Qxy)[1];
		auto p = (x*x*x*x - 6*x*x + 6) * (y+2);
		std::cout << p << std::endl;
		std::cout << p / (x*x-2) << std::endl;
	}
	{
		auto q = x2*x2*y + 2*x2*x2 - 4*y - 8;
		std::cout << q << std::endl;
	}*/
}



int main() {
	try {
		cad::example_projection();
		Example();
		lazard::naive::example_1();
		lazard::proper::example_1();
		lazard::naive::example_2();
		lazard::proper::example_2();
		lazard::naive::example_3();
		lazard::proper::example_3();
	} catch (const CoCoA::ErrorInfo& e) {
		std::cout << e << std::endl;
	}
	
	//auto test = 1*x*x*x*x - 9*x*x + 9;
	//auto fact = CoCoA::factor(test).myFactors();
	//std::cout << test << " -> " << fact << std::endl;
	
	//std::cout << "CG: " << cg << std::endl;
	//auto fact = CoCoA::factor(cg).myFactors();
	//std::cout << fact << std::endl;
	//std::cout << QHom(cg) << std::endl;
}
