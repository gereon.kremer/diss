\section{Contributions}\label{sec:introduction:contributions}

In this thesis, we present novel approaches that roughly fall into the following categories:
\begin{enumerate}[itemsep=0ex, topsep=0ex]
	\item embedding and adapting the CAD method into an SMT compliant theory solver for nonlinear real arithmetic,
	\item extending and using this theory solver for other problems like nonlinear integer arithmetic or quantifier elimination,
	\item implementing the \MCSAT proof system with a special focus on modularity, and
	\item theoretical study of the \MCSAT proof system.
\end{enumerate}
Note that by ``novel approaches'' we do not mean to talk about ``as of yet unpublished'' results, but also about results presented in publications that have been (co-) authored by this author. Publications that are relevant for this are listed and discussed in the following \cref{ssec:introduction:contributions:relevant-publications}.
We now give a very brief overview of the contributions of this thesis, deferring a more detailed discussion to \cref{sec:conclusion:contributions}.

We start with a general introduction into CAD that shifts the understanding into a point of view that is more suitable for our application, followed by a novel proof system that allows decomposing the oftentimes ``monolithic'' CAD easily.
We then discuss a number of design choices and heuristics and show how to apply this variant of CAD to integer problems, quantifier elimination, and optimization problems.

The second part is concerned with \MCSAT, starting with a general definition and two extensions for \emph{model refinement} (instead of \emph{model construction}) and optimization.
We then describe a concrete implementation based on a novel embedding into an existing \CDCL-style SAT solver and various possibilities for its main theory reasoning methods -- the \emph{assignment finder} and the \emph{explanation function} -- and evaluate this implementation using several different strategies.
Finally, we discuss some theoretical observations that relate \CDCLT-style SMT solving with \MCSAT-style SMT solving in the hope to underpin some practical observations with theoretical results.


\subsection{Relevant publications}\label{ssec:introduction:contributions:relevant-publications}

We present the list of our publications that are relevant to this thesis and detail the author's contributions to them below.
For full bibliographic references, we refer to the bibliography at the end of this work.

\begin{refsegment}
	\nocite{Corzilius2015,Kremer2016,Abraham2017,Viehmann2017,Haehn2018b,Kremer2018a,Kremer2019,Kremer2020,Nalbach2019}

	\setlength{\biblabelsep}{0pt}
	\sloppy
	\printbiblist[segment=1,heading=none,driver=contributions,env=bibliography]{contributions}
	\fussy
\end{refsegment}

The contributions published in these papers mainly fall into three categories: adaptations and extensions of CAD for SMT solving, research on \MCSAT, and implementation within \SMTRAT.
For all the above papers, the author took part in the discussions and writing of the actual publication. Most publications were attained in cooperation with other researchers and we discuss the individual contributions in more detail now.


The overall architecture of our integration of CAD into SMT is described in~\cite{Kremer2020} which contains most of what is described in \cref{ch:cad-SMT}, though presented as actual algorithms while we formulate it as a proof system here.
The fundamental ideas to perform CAD incrementally predate the author's work and are mostly due to Ulrich Loup (see for example~\cite{Loup2011,Corzilius2012,Loup2013,Loup2018}). The presented approach is however significantly different from previous publications and is based on a completely new implementation conducted by the author. A preliminary presentation of this approach was given by the author at~\cite{Abraham2018}.

While working on the overall framework for incremental CAD, a whole range of individual issues were investigated in the context of CAD.
First, we extended \SMTRAT to allow for efficient solving of nonlinear integer formulae using a branch-and-bound approach, resulting in~\cite{Kremer2016}.
While Florian Corzilius worked on the adaption of virtual substitution, the author was responsible for the required changes in CAD.

Together with Tarik Viehmann, we analyzed how different projection operators (that we describe in \cref{sec:cad:projection-operators}) perform in the context of SMT solving. The work started as his thesis project in~\cite{Viehmann2016} and was then extended to~\cite{Viehmann2017}. The author mainly provided the underlying implementation for the individual projection operators, consulted on ideas for further experiments, and finally took part in writing the publication.

In her thesis~\cite{Haehn2018a}, Rebecca Haehn integrated the usage of equational constraint (as presented in \cref{ssec:cad:equational-constraints}) into our incremental CAD and how they improve the applicability of CAD in practice. Given the deep level of integration into the existing implementation, the author not only contributed essential ideas but also parts of the implementation in the preparation of the subsequent paper~\cite{Haehn2018b}.

This work was accompanied by improvements to the general framework in \SMTRAT and the underlying library \CArL, as well as various additional solving techniques not directly related to this thesis.
Some of them were presented in a series of papers, others remain unpublished, but are freely available as implemented in \CArL and \SMTRAT. While~\cite{Corzilius2012} predates the author's activities, the author contributed major parts of the implementation and writing to both~\cite{Corzilius2015} and~\cite{Kremer2018a}.

Another, though related, line of research was the exploration of the \MCSAT solving framework (originally presented in~\cite{Jovanovic2012,Moura2013}) which we still consider work in progress.
In~\cite{Abraham2017} (based on~\cite{Nalbach2017}) we present Jasper Nalbach's work to employ virtual substitution as a novel explanation backend in \MCSAT in addition to CAD and Fourier--Motzkin (that were previously described in~\cite{Jovanovic2012,Jovanovic2013}). The \MCSAT framework gives a more flexible integration of Boolean and theory reasoning and we explored possible heuristics in~\cite{Nalbach2019}. The author contributed the majority of our implementation of \MCSAT, initially in cooperation with Florian Corzilius and eventually supported by Jasper Nalbach.

In~\cite{Kremer2019}, we had a more theoretical look at \MCSAT by relating it to \CDCLST in terms of its proof complexity. We have proven \MCSAT to be equivalent to \ResST which in turn is equivalent to \CDCLST, as was previously shown in~\cite{Robere2018}. This whole topic was almost exclusively one of the author's side projects.

\subsection{Further publications}

During the time the author worked on the presented topics, the following other publications and talks where attained. Again we refer to the bibliography at the end of this work for full references.

\begin{refsegment}
	\nocite{Abraham2016,Abraham2016a,Abraham2017a,Kremer2018b,Abraham2020}

	\setlength{\biblabelsep}{0pt}
	\sloppy
	\printbiblist[segment=2,heading=none,driver=contributions,env=bibliography]{contributions}
	\fussy
\end{refsegment}

A case study on the applicability of SMT solving -- in particular in comparison to \emph{constraint programming} -- was conducted in~\cite{Abraham2016}, aiming at automated deployment of cloud-based web applications. The author contributed to the problem formulation and encoding as an SMT formula, performed the benchmarking presented in the paper, and took part in the writing process.

The remaining papers~\cite{Abraham2016a,Abraham2017a} and the talk~\cite{Kremer2018b} all aim at presenting the SMT approach with its unique advantages and challenges to different audiences.
While~\cite{Abraham2016a} was targeted at software engineering researchers interested in formal verification, we describe the current state-of-the-art (including our own solver \SMTRAT) to the computer algebra community in~\cite{Abraham2017a}.
The talk~\cite{Kremer2018b} was a result of the \SCSquare project and presented concrete challenges we encountered when integrating established computer algebra software (like Maple or CoCoALib) into an SMT framework.
Finally, we proposed a novel CAD-based decision procedure in~\cite{Abraham2020} that employs a conflict-driven strategy within a theory solver with surprisingly good results.

\newpage
\subsection{Supervised thesis projects}

The author oversaw many thesis projects that mostly explored topics more or less closely related to this work. While some of them resulted in subsequent publications that are mentioned later, the others might be interesting to read as well, covering topics that just did not make it into the present work.

\begin{refsegment}
	\nocite{Neuberger2015,Krueger2015,Volk2015}
	\nocite{Viehmann2016,Winkler2016}
	\nocite{Hentze2017,Nalbach2017,Korp2017,Grobelna2017}
	\nocite{Haehn2018a,Neuhaeuser2018,Neuss2018,Sali2018,Loesbrock2018,Lotze2018,Bartolome2018}
	\nocite{Kuksaus2019,Zaman2019}
	\nocite{Nalbach2020,Franzen2020}

	\setlength{\biblabelsep}{0pt}
	\sloppy
	\printbiblist[segment=3,heading=none,driver=contributions,env=bibliography]{contributions}
	\fussy
\end{refsegment}

Most of these thesis projects also deal with some aspects or special approaches to nonlinear problems, mostly centering around the application of cylindrical algebraic decomposition or the \MCSAT approach.
While~\cite{Haehn2018a} enhanced our CAD implementation with equational constraints, variants for minimal infeasible subsets for CAD were explored in~\cite{Hentze2017}, properties of different projection operators were compared in~\cite{Viehmann2016}, a novel method based on CAD from~\cite{Abraham2020} was implemented in~\cite{Franzen2020}, and a different representation for real algebraic numbers was implemented in~\cite{Winkler2016}.
Meanwhile, \cite{Neuhaeuser2018} employed this CAD implementation for quantifier elimination as described in \cref{sec:cadSMT:qe}.

Some were also targeted at the \MCSAT approach where~\cite{Bartolome2018} made a first attempt at implementing a Fourier--Motzkin-based explanation backend, the theory for an explanation backend based on virtual substitution was laid out in~\cite{Nalbach2017}, and an implementation of the OneCell CAD approach (see \cref{ssec:mcsatimpl:explanation:onecell}) was carried out in~\cite{Neuss2018}.

Several thesis projects were concerned with solving techniques unrelated to CAD or even not meant for nonlinear real arithmetic:
pseudo-Boolean constraints in~\cite{Grobelna2017}, bit-vectors and their usage for nonlinear integer arithmetic in~\cite{Krueger2015}, a solver for difference logic in~\cite{Loesbrock2018}, a novel interpretation of the simplex method in~\cite{Nalbach2020}, uninterpreted functions and different variants for infeasible subsets in~\cite{Neuberger2015}, two linearization techniques based on subtropical satisfiability and reduction to linear integer arithmetic in~\cite{Sali2018}, and finally incremental linearization based on axiom instantiation in~\cite{Zaman2019,Kuksaus2019}.

Lastly, some projects aimed at applying SMT techniques to (more or less) practical applications:
combinatorial system design problems in cooperation with Siemens Belgium in~\cite{Grobelna2017}, planning for robot fleets for the RoboCup Logistics Team ``Carologistics'' in~\cite{Korp2017}, production planning for Robert Bosch GmbH in~\cite{Lotze2018}, and industrial product configuration problems for DAF Trucks N.V. in~\cite{Volk2015}.

\section{Implementation}

As we have already mentioned at several places, the author is currently the main author of our own software \SMTRAT~\cite{SMTRAT,Corzilius2015} which is both a toolbox for and around SMT technology and an SMT solver itself.
This project is closely accompanied by our library \CArL~\cite{CArL,Kremer2018a} that implements many fundamental data structures like polynomials, real algebraic numbers, and formulae as well as algorithms like resultant computation, real root isolation, interval arithmetic, Gröbner bases, or computing set coverings.
Of course, \CArL again relies on other libraries like Boost~\cite{Boost}, CoCoALib~\cite{CoCoALib}, Eigen3~\cite{Eigen3} or GMP~\cite{Granlund}.

Both \CArL and \SMTRAT are freely available as open-source \CPP software.
As the border between \SMTRAT and \CArL is both pretty technical and in some parts historical, but not relevant for this work, we say ``\SMTRAT'' to talk about the whole implementation comprised of both \SMTRAT and \CArL from now on.
Almost everything of what we discuss in this work has been implemented in \SMTRAT and we encourage everyone interested in implementing any of this to study our implementation.
