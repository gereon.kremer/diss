\section{Integer problems}

\silentindex{integer-problem}
We have presented and understood CAD as a decision procedure for nonlinear real problems.
A common requirement in practice is that assignments should be integral, and it is well-known that the theory of nonlinear integer arithmetic is undecidable -- though the search space becomes \emph{smaller}. This is somewhat similar to the linear case, where polynomial algorithms exist for the reals, but linear integer problems have exponential complexity (if we assume $P \neq NP$).

Arguably the most common approach to tackle linear integer problems is called \emph{branch and bound}.
It essentially computes a real solution and -- if this solution is not integral -- excludes some region around this solution so that the removed part does not contain any integral solution. 

The simplest variant of branch and bound roughly works as follows:
Assume the real solution to our formula $\varphi$ is $x = 0.5$, we perform two recursive calls with $\varphi \land x \leq 0$ and $\varphi \land x \geq 1$, respectively. This recursion either eventually finds an integer solution, excludes the whole search space, or continues indefinitely.

The exponential growth due to the recursive calls can be a problem for which we essentially have two approaches.
Instead of issuing recursive calls, we can \emph{lift} the lemma $x \leq 0 \lor x \geq 1$ to the SAT solver and let the SAT solver decide, providing the possibility to retain knowledge from different parts of the search space.
Alternatively, there are other ways to create cuts that do not (necessarily) induce case splits like Gomory cuts or cutting planes. However, these other cuts usually have other issues concerning termination.

For nonlinear integer problems, however, a completely different approach is commonly used.
Most solvers employ \emph{bit blasting} in the spirit of~\cite{Fuhs2007} in some fashion, essentially using a bit-precise encoding of the integer variables -- up to a certain number of bits -- into propositional logic.
This approach is conceptually simple and impressively efficient if a \emph{small} satisfying assignment exists (in terms of the bits necessary to represent it), but also has significant drawbacks: finding \emph{large} models requires a large number of bits which entails a superlinear growth of the propositional formula; determining unsatisfiability is all but trivial and requires additional machinery, if possible at all. 

We have presented an integration of nonlinear decision procedures (including CAD) into branch and bound in~\cite{Kremer2016} and showed that it nicely complements \emph{bit blasting}.
Most prominently, algebraic procedures can find unsatisfiability which is only rarely possible using bit blasting. Furthermore, problem instances whose satisfying assignments are \emph{large} but have a comparably simple algebraic structure can significantly benefit from algebraic procedures.
While our variant lifts \emph{splits} (or \emph{branches}) to the SAT solver, it might be an interesting direction for future research to look for possibilities more similar to cutting planes to avoid case splits.

\begin{table}
	\centering
	\input{experiments/output/table-cad-nia}
	\caption{Experimental results for integer problems.}\label{tbl:cad:nia-results}
\end{table}

Some experimental results -- on the \SMTLIB \QFNIA benchmark set -- are shown in \cref{tbl:cad:nia-results}. Though the branch-and-bound approach on its own is inferior to bit blasting, it complements bit blasting well. In particular, it allows solving a significant number of unsatisfiable problem instances but also helps in case of satisfiability. For a more thorough analysis that also discusses the other theory methods involved -- both \SolverCADNIABB and \SolverCADNIAFull also employ simplex and virtual substitution equipped with branch and bound -- we refer to~\cite{Kremer2016}. Please note that the results in~\cite{Kremer2016} were obtained on the set of \SMTLIB \QFNIA benchmarks of the day which have significantly changed since.
