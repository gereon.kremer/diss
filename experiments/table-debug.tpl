\footnotesize
\newpage
{% set answers = getPossibleAnswers() -%}
\begin{tabularx}{\textwidth}{|l||{{ "Y|" * (answers|length)}}}
	\hline
	\textbf{Solver} & {{ answers|join(" & ")}} \\
	\hline\hline
{%- for s in solvers -%}
	{% set stats = getAllStats(s["solver"]) %}
	{{ texName(s["solver"]) }} &
	{%- for a in answers -%}
		{% if a in stats %}{{ stats[a][0] }}{% endif %} {% if not loop.last %}&{% endif %}
	{%- endfor %} \\
	\hline
{%- endfor %}
\end{tabularx}

\begin{tabularx}{\textwidth}{l{{ "|r" * (solvers|length)}}}
	&
{%- for y in solvers -%}
	{{ shortName(y["solver"]) }} {% if not loop.last %}&{% endif %}
{%- endfor %} \\
\hline
{% for x in solvers %}
	{{ shortName(x["solver"]) }} &
	{%- for y in solvers -%}
		{{ solvedByXnotY(x["solver"],y["solver"]) }} {% if not loop.last %}&{% endif %}
	{%- endfor -%}
	{% if not loop.last %}\\{% endif %}
{%- endfor %}
\end{tabularx}
