\section{Quantifier elimination\label{sec:cadSMT:qe}}

\silentdefindex{cad:quantifier-elimination}
The CAD proof system we presented is concerned with determining the satisfiability of a given problem, as is the overwhelming part of this whole thesis. However, the informed reader may remark that CAD was originally devised to solve a more general problem, namely \emph{quantifier elimination}, and ask whether the presented techniques can be employed for tackling such a quantifier elimination problem.

\begin{definition}{Quantifier elimination}{quantifier-elimination}
	Let $\varphi = \Quantifier{\overline{x}} \Quantifier{y} \varphi'$ be a formula where $y$ is quantified (existentially or universally).
	The \emph{(single) quantifier elimination problem} is to construct a new \emph{quantifier-free} formula $\psi$ such that $\Quantifier{\overline{x}} \psi$ is equivalent to $\varphi$.
	
	The \emph{general quantifier elimination problem} is to eliminate some or all quantifiers from an arbitrary formula in prenex normal form.
\end{definition}

Most importantly, quantifier elimination does not search for a single satisfying sample point but indeed needs a decomposition -- unless all quantifiers are existential quantifiers.
Therefore, most presented ideas that aim for early termination and avoiding projection steps do not work here as a full CAD is needed anyway.
It is nevertheless valuable to know that an implementation of CAD implementing all these techniques can still be used for quantifier elimination, and we do not need to duplicate multiple versions of the projection and lifting mechanisms.

Our solver \SMTRAT was enhanced in~\cite{Neuhaeuser2018} to support quantifier elimination based on the incremental CAD computations.
The implemented approach is mostly based on the descriptions from~\cite{Brown1999}.
Though we can not benefit from most features at the core of \SMTRAT, the quantifier elimination part performs reasonably well when compared to other tools like \QEPCADB or Maple.

Furthermore, we see striking similarities between the inner workings and practical problems of this form of quantifier elimination and local cell constructions like OneCell~CAD from~\cite{Brown2013}, non-uniform CAD~\cite{Brown2015a} (that we both already mentioned in \cref{ssec:intro:related:variant-of-cad}), or \MCSAT-style explanations as described in \cref{sec:mcsat:explanations}.

We mostly think of the process of extracting a particular cell from the CAD and either finding a proper formalism to represent it -- like extended polynomial constraints as defined in \cref{ssec:preliminaries:fol:extensions} or ``extended Tarski formulae'' with \emph{indexed root expressions} from~\cite{Brown1999} -- or converting it to an expression in our standard first-order language -- called ``simple solution formulae'' in~\cite{Brown1999}.
We thus think that future work on this connection could provide additional synergies.
