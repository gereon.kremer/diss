\begin{tabularx}{\linewidth}{lX
S[table-format=4.0]@{~}S[table-format=2.2]@{\,}s
S[table-format=4.0]@{~}S[table-format=2.2]@{\,}s
S[table-format=5.0]@{~}S[table-format=2.1]@{\,}s}
	\toprule
	\textbf{Solver} &
	& \multicolumn{3}{c}{\textbf{SAT}}
	& \multicolumn{3}{c}{\textbf{UNSAT}}
	& \multicolumn{3}{c}{\textbf{overall}}
	\\
	\midrule
{%- for s in solvers|sort(attribute = 'solver')|sort(attribute = 'solved') %}
	{% if s["solver"] in override %}{{ texName(override[s["solver"]]) }}{}{% else %}{{ texName(s["solver"]) }}{}{% endif %} &
	& {{ s["sat"] }} & {{ "%0.2f & \\second{}"|format(s["avgsattime"]) }}
	& {{ s["unsat"] }} & {{ "%0.2f & \\second{}"|format(s["avgunsattime"]) }}
	& {{ s["solved"] }} & {{ "%0.1f & \\percent{}"|format(s["solved"] / numberOfFiles(s["solver"]) * 100) }}
	\\
{%- endfor %}
	\bottomrule
\end{tabularx}
