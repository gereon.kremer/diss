\section{Extending \CDCL to \MCSAT\label{sec:mcsat:minisat}}

\silentdefindex{mcsat:minisat}
The SAT solver in \SMTRAT consists of the MiniSAT solver presented in~\cite{Een2003}, adapted for \CDCLT-style SMT solving so that it incorporates most state-of-the-art techniques for SAT solving and supports working with theory constraints.
It thus implements all rules from \cref{def:mcsat-boolean-reasoning,def:mcsat-conflict-analysis} and combines them in the usual way.

\begin{algorithm}
	\caption{MiniSAT implementation}\label{algo:mcsat:implementation-minisat}
	
	\While{\true}{
		\While{$\neg\BCP\texttt{()}$ \nllabel{algo:mcsat:minisat:BCP}}{
			\If{\texttt{CDCL-style conflict resolution} fails}{\nllabel{algo:mcsat:minisat:conflict}
				\Return{UNSAT} \;
			}
		}
		\texttt{restart heuristically} \nllabel{algo:mcsat:minisat:restart}\;
		\If{unassigned Boolean variable exists}{
			$\mathit{var} \coloneqq$ pick Boolean decision variable \nllabel{algo:mcsat:minisat:pick}\;
			\texttt{perform Boolean decision} on $\mathit{var}$ \nllabel{algo:mcsat:minisat:decide}
		}\Else{
			all variables are assigned, return SAT \nllabel{algo:mcsat:minisat:sat}
		}
	}
\end{algorithm}

We show how this solver works internally -- abstracted and simplified -- in \cref{algo:mcsat:implementation-minisat}.
It essentially corresponds to the MiniSAT method \texttt{search} that implements the search procedure and makes use of \emph{Boolean constraint propagation} (\texttt{BCP}), heuristic restarts, heuristic variable decisions, and \CDCLT-style conflict resolution.
Some technical details have been removed, for example, what MiniSAT calls \emph{assumptions} (clauses with only a single literal) or the occasional removal of rarely used learned clauses.

Compared to the \MCSAT proof system from \cref{def:mcsat-proof-system}, \cref{algo:mcsat:implementation-minisat} implements the proof rules from \cref{def:mcsat-boolean-reasoning,def:mcsat-conflict-analysis} as follows:
\rulename{Decide} in \cref{algo:mcsat:minisat:pick,algo:mcsat:minisat:decide};
\rulename{Propagate} and \rulename{Conflict} in \cref{algo:mcsat:minisat:BCP};
\rulename{Sat} in \cref{algo:mcsat:minisat:sat};
\rulename{Restart} in \cref{algo:mcsat:minisat:restart};
\rulename{Resolve}, \rulename{Consume}, \rulename{Backjump}, \rulename{Unsat} and \rulename{Learn} in \cref{algo:mcsat:minisat:conflict}.
We now add the proof rules from \cref{def:mcsat-theory-reasoning} to this algorithm to obtain \cref{algo:mcsat:implementation}.

\begin{algorithm}
	\caption{MCSAT implementation}\label{algo:mcsat:implementation}

	\While{\true}{
		\While{$\neg\BCP\texttt{()}$ \nllabel{algo:mcsat:implementation:BCP}}{
			\If{\texttt{CDCL-style conflict resolution} fails}{
				\Return{UNSAT} \;
			}
		}
		\texttt{restart heuristically} \;
		\texttt{Check for semantic propagations} \nllabel{algo:mcsat:implementation:semantic-propagation} \;
		\texttt{Check for inconsistent trail (due to BCP)} \nllabel{algo:mcsat:implementation:check-inconsistent} \;
		\If{unassigned variable exists}{
			$\mathit{var} \coloneqq$ pick decision variable \nllabel{algo:mcsat:implementation:pick}\;
			\If{$\mathit{var}$ is theory variable}{
				\If{feasible assignment for $\mathit{var}$ exists}{
					\texttt{perform theory decision} \nllabel{algo:mcsat:implementation:t-decide}
				}\Else{
					\texttt{CDCL-style conflict resolution on explanation} \nllabel{algo:mcsat:implementation:t-conflict}
				}
			}\ElseIf{$\mathit{var}$ is Boolean variable}{
				\If{$\mathit{var}$ or $\neg \mathit{var}$ can be propagated (in the theory)}{
					\texttt{use explanation to propagate $\mathit{var}$ or $\neg \mathit{var}$} \nllabel{algo:mcsat:implementation:t-propagate}
				}\Else{
					\texttt{perform Boolean decision}
				}
			}
		}\Else{
			All variables are assigned, return SAT \nllabel{algo:mcsat:implementation:sat}
		}
	}
\end{algorithm}

The first rule \rulename{T-Propagate} is very generic and can, in theory, be used to inject theory lemmas at any time.
While one may very well think about other possibilities to do so, we only check whether a constraint can be propagated after it has been selected for a Boolean decision in \cref{algo:mcsat:implementation:t-propagate} right now.

The variable selection in \cref{algo:mcsat:implementation:pick} has been extended to select either a Boolean or a theory variable.
In the latter case we perform a theory decision as specified by \rulename{T-Decide} if a suitable assignment exists in \cref{algo:mcsat:implementation:t-decide} or recognise a conflict using \rulename{T-Conflict} in \cref{algo:mcsat:implementation:t-conflict}.
The last two rules \rulename{T-Consume} and \rulename{T-Backjump-Decide} are integrated into the conflict analysis and are thus not explicitly shown here.

To take care of the proper integration of theory reasoning into the Boolean reasoning, we perform two additional checks that do not directly correspond to any of the proof rules.
In \cref{algo:mcsat:implementation:semantic-propagation} we check whether any constraint fully evaluates over the current theory model but is not yet assigned in the Boolean model. If so, we inject a \emph{decision} for this constraint. Note that one could also aim to perform some kind of propagation here, however simply using decisions worked well enough in our setting.

Additionally, we need to check in \cref{algo:mcsat:implementation:check-inconsistent} whether the trail became inconsistent.
This may be somewhat surprising at first glance, given that the Boolean assignment is always chosen to be compatible with the theory value and the theory decision always respects the Boolean assignment.
How can the trail become inconsistent in the first place under these circumstances?

Note that the theory decision (usually) considers only constraints that are \emph{univariate} over the current theory model, but the assignment might fully evaluate a ``not-yet-univariate'' constraint as well. Consider for example $x \cdot y \neq 0$ being assigned to \true, and a theory decision setting $x \mapsto 0$ as no univariate constraint is available.
Though we could leave this issue to the theory decision to detect, it has proven to be significantly easier to do this explicitly beforehand.

In contrast to the more theoretic definition in~\cite{Moura2013}, and also the description of an actual implementation in~\cite{Jovanovic2013}, \cref{algo:mcsat:implementation} gives a more detailed description of how the proof rules interact and how they are scheduled.
