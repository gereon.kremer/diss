% !TEX root = ../../main.tex

\input{content/pictures/tikz-macros}

\begin{tikzpicture}[scale=1]
	\input{content/pictures/05-full-cad-utils}
	
	% 1d cells
	\fill[pattern=dots, pattern color=black!30]
		(-3.1,-3.2) -- (-3.1,-2.8) -- (-1.19,-2.8) -- (-1.19,-3.2) -- cycle;
	\fill[pattern=north west lines, pattern color=black!30]
		(1.69,-3.2) -- (1.69,-2.8) -- (-1.19,-2.8) -- (-1.19,-3.2)  -- cycle;
	\fill[pattern=dots, pattern color=black!30]
		(1.69,-3.2) -- (1.69,-2.8) -- (3.1,-2.8) -- (3.1,-3.2) -- cycle;
	\draw[thick] (1.69,-3.2) -- (1.69,-2.8) (-1.19,-2.8) -- (-1.19,-3.2);

	% 2d cylinders
	\fill[pattern=dots, pattern color=black!30]
		(-3.1,-2) -- (-3.1,3) -- (i1-up) -- (i1-down) -- cycle;
	\fill[pattern=north west lines, pattern color=black!30]
		(i2-down) -- (i2-up) -- (i1-up) -- (i1-down)  -- cycle;
	\fill[pattern=dots, pattern color=black!30]
		(i2-down) -- (i2-up) -- (3.1,3) -- (3.1,-2) -- cycle;

	\tikzgrid{-3}{3}{-2}{3}
	\tikzaxis{-3}{3}{-3}{3}
	\tikzxaxis{-3}{3}{-3}

	\drawconstraints

	\draw[thick] plot [samples=200,range=-4:-2.25, domain=-3:3] function {(2*x*x-x-4)*0.15 - 3};

	\draw[thick,densely dotted] (i1-up) -- (i1-1d);
	\draw[thick,densely dotted] (i2-up) -- (i2-1d);

	\draw[thick, black!75, densely dotted] (-1.75,-3) -- (-1.75,3);
	\draw[thick, black!75, densely dotted] (0.5,-3) -- (0.5,3);
	\draw[thick, black!75, densely dotted] (2.5,-3) -- (2.5,3);

	\fill (-1.75,-3) circle (2pt);
	\fill (-1.19,-3) circle (2pt);
	\fill (0.5,-3) circle (2pt);
	\fill (1.69,-3) circle (2pt);
	\fill (2.5,-3) circle (2pt);

	\node at (-2.5,-1.65) {\tiny $C_1 {\times} \R$};
	\node at (0.5,-1.65) {\tiny $C_2 {\times} \R$};
	\node at (2.5,-1.65) {\tiny $C_3 {\times} \R$};

	\node at (-2.25,-2.65) {\tiny $C_1$};
	\node at (0.25,-2.65) {\tiny $C_2$};
	\node at (2.75,-2.65) {\tiny $C_3$};
\end{tikzpicture}
