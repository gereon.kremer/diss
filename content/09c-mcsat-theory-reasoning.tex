\section{Theory reasoning in practice}\label{sec:mcsat:practicability}

The simulations we presented in \cref{sec:mcsat:proof-complexity} and \cref{sec:mcsat:equivalency} have a few shortcomings (or rather only hold true under certain assumptions), some of which have already been mentioned.
We now discuss these issues, how they affect the theory or practical implementations, and how one might mitigate them.

\subsection{Completeness of infeasibility check}

We have already issued warnings that the constructions shown in \cref{sec:mcsat:proof-complexity} and \cref{sec:mcsat:equivalency} require a level of power by the \MCSAT subroutines -- in particular $\Infeasible$ -- that is backed by the \MCSAT framework, but not by any practical implementation.

The definition from~\cite{Moura2013} uses the predicate $\Infeasible$ to state that a trail $M$ is not satisfiable in the sense that the literals from $M$ are not satisfiable together with the theory model induced by $M$.
Note that the previous version from~\cite{Jovanovic2012} still used a slightly different version: feasibility only considered the constraints that are \emph{univariate} over the theory model.
Though seemingly small, this is a pivotal difference here.

The presented simulations all require the more general notion of infeasibility from~\cite{Moura2013} to ensure that \emph{feasibility of the trail} coincides with the trail \emph{implying $\false$}.
Practical implementations, however, all follow~\cite{Jovanovic2012} where we only consider univariate constraints for feasibility.
As far as we know, the SMT-based assignment finder from \cref{ssec:mcsat:assignment-finder:SMT} is the only exception to this, though with rather disappointing results.

One might even reasonably argue that this restriction to only consider univariate constraints is not a technical one, but is a fundamental idea in \MCSAT that allows the theory exploration in the first place. A complete implementation of $\Infeasible$ would essentially prevent the theory exploration and move the whole process of theory exploration into $\Infeasible$ that turns into a regular theory solver -- just like the SMT-based assignment finder from \cref{ssec:mcsat:assignment-finder:SMT}.

Let us first observe that employing an ``incomplete'' version of $\Infeasible$ does not make \MCSAT incomplete. It only requires theory exploration whenever $\Infeasible$ ignores non-univariate constraints, but by gradually exploring the whole space of theory models one eventually correctly determines infeasibility.
We thus claim -- without proof -- that all simulations can be adapted accordingly to accommodate for an incomplete implementation of $\Infeasible$.

The basic idea is as follows: instead of immediately finding infeasibility, we guess a partial theory assignment (via the \rulename{T-Decide} rule) until it becomes infeasible -- note that $\Infeasible$ is definitely complete once all but one variable are assigned -- exclude a region (via the \rulename{T-Conflict} rule) around the partial assignment (in the space of theory assignments) and backtrack. This usually happens multiple times for a certain variable until the whole space is excluded for this variable and we have to change the assignment of the previous variable.
We thereby accumulate witnesses for infeasibility and combine them to contain less and less variables until we eventually get back to our starting point and can derive infeasibility from univariate constraints.

While this might only seem like an issue of reformulating the proofs and making them (a lot) more technical and arguably ugly, it gives rise to a fundamental problem.
We required the lengths of equivalent proofs to only differ polynomially. However, the theory exploration described above essentially enumerates invariant regions -- we usually think about CAD cells here -- and there can be exponentially many of them.

This, unfortunately, conflicts with our hope of a \emph{polynomial} reduction. 
However, this was to be expected: the whole idea of \MCSAT is to move parts of the theory reasoning into the core proof system while \ResST and \CDCLST both \emph{hide} any theory reasoning within their proof rules (of constant cost).
Thus, our core proof system \emph{of course} exhibits bad asymptotic behavior if we consider a hard theory, while the proof system we compare it to completely hides any theory reasoning from its asymptotic complexity.

While the theoretical result still remains if $\Infeasible$ is complete, we also argue that it is relevant in practice as well when $\Infeasible$ is incomplete.
We observed that the computational effort spent on theory reasoning is not \emph{new} but is only made explicit in \MCSAT, and thus the overall computational effort stays (about) the same.

\subsection{Theory-aware equivalency}

We now want to argue that the length of proofs is not a satisfactory measure to assess the computational complexity of performing proofs.
The foregoing discussion essentially shows that we can make a proof system arbitrarily more powerful -- in the sense of proof complexity or algorithmic simulation -- if we make its proof rules more powerful.
Conversely, we can make a proof system less powerful by making its proof rules ever more detailed, possibly executing individual processor instructions.

It is, thus, of utmost importance for a meaningful comparison that the proof systems operate on a similar level of abstraction if we assume that all proof rules take a fixed amount of time.
We propose to treat proof rules that involve \emph{theory queries} -- whatever a \emph{theory query} may be -- in a special way.
To call proof systems equivalent (either \emph{bisimilar} or \emph{algorithmically equivalent}) we require the \emph{number of theory queries} to be (about) the same and let all theory queries have (about) the \emph{same computational effort}.

Reconsidering the proofs for bisimilarity and algorithmic equivalency, we observe that the number of theory queries is always identical.
Every proof rule that involves a theory query is simulated by a set of proof rule applications that involve exactly one theory query as well.
Thus we do not need to worry about the \emph{number of theory queries}.

A closer look at the reductions involving theory queries reveals that they are used to construct the \emph{exact same} clauses from the \emph{exact same} theory constraints.
We can thus argue that any method that would improve upon a particular implementation of the \MCSAT theory queries can directly be used to improve the \CDCLST theory queries, and the other way round.

\subsection{Impact of new literals}

\MCSAT has the ability to add new literals via its $\Explain$ method that is used in the \rulename{T-Propagate} and \rulename{T-Conflict} rules.
Though we could theoretically restrict $\Explain$ to only use existing literals, it would forbid all existing instantiations of \MCSAT we know of, in particular the CAD-based \NLSAT~\cite{Jovanovic2012}. The way $\Explain$ is meant to work -- by eliminating all unassigned variables -- inherently generates new literals.

We know that adding strong theory derivations to \CDCLT brings us from \ResT to \ResST in theory. Restricting $\Explain$ to what we described above, that is a quantifier elimination procedure that removes the unassigned variables, gives us something between:
it provides a way to generate theory lemmas with additional literals, which may make it stronger than \CDCLT without strong theory derivations, but it remains unclear whether we can \emph{practically} generate any arbitrary clause or an equivalent clause -- whatever equivalent would mean -- like we assume in our simulation of the \rulename{Learn} rule. 

Note that we do not consider the simulation of strong theory derivations in the reduction from \CDCLST to \MCSAT \emph{practical} in that sense.
We have shown that we can instrument \MCSAT to construct any valid clause \emph{that we already know}. The reduction, however, did not provide a constructive way to find \emph{novel clauses}, just like \CDCLST.
Finding meaningful lemmas is a difficult problem and actual implementations of \CDCLST have a hard time leveraging the power of strong theory derivations.

This might suggest that real-world implementations of \MCSAT are less powerful than \CDCLST. Instead, \MCSAT might have found a sweet spot between \CDCLT and \CDCLST{} -- and thus between \ResT and \ResST{} -- in that it shows how to generate certain meaningful lemmas from a restricted class, but not a general scheme to generate all valid lemmas.

\subsection{Impact of theory decisions}\label{ssec:mcsat:theory-decisions}

We have seen that theory decisions were not used when we showed the equivalence of \CDCLST and \MCSAT. We did not need them to be as powerful as \CDCLST and could simply ignore them when simulating \MCSAT with \CDCLST without harm.
This suggests that they are not an essential \emph{theoretical} part of \MCSAT after all. They seem to be no more than a tool to steer decisions and the generation of new clauses to a good direction in a heuristic way -- a very effective one in practice though.
