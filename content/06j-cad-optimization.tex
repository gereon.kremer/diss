\section{Optimization\label{sec:cadSMT:optimization}}

\silentdefindex{cad:optimization}
Another extension that gained substantial popularity in the SMT community recently is the support for optimization queries, for example, in~\cite{Bjorner2015,Sebastiani2015b}.
Instead of looking for \emph{some} feasible solution to a first-order logic formula, the goal is to find an \emph{optimal} solution with respect to a given objective function.
This task is oftentimes called \emph{optimization modulo theories} and expands the applicability of SMT (or rather OMT) significantly.

The most popular approach extends both the SAT solver and the theory solver as presented in~\cite{Sebastiani2015a}. In particular, it requires the theory solver to compute an optimal solution with respect to the given set of constraints.
While the simplex method -- being a method meant for optimization tasks in the first place -- provides for this naturally, optimization within this framework for nonlinear problems has not found a lot of traction yet.

Though we never made time to implement the following approach, we propose to employ CAD for this task.
It not only allows for a \emph{complete} method that does not rely on convergence or special problem properties (like convexity) but also permits interesting ways to obtain \emph{almost-optimal} solutions more quickly with strong guarantees on the quality of those.

Let us assume that our objective function is a single variable instead of an arbitrary function. We can simply reduce any objective function $f$ by adding an equality $v = f$ to the set of constraints with $v$ being a fresh variable that we use as a new objective.
Let us furthermore assume, without loss of generality, that we always \emph{minimize}.

The fundamental idea is very simple. We first impose a variable ordering where $v$ is the last variable in the projection, and thus the \emph{first to be lifted}, and compute a \emph{full projection}.
We then split the lifting into two parts: for $v$ we select the \emph{smallest} cell (we have not tried yet) and afterward perform the regular lifting procedure above this selection of $v$.
As soon as we find a solution, it is optimal in the sense that it comes from the \emph{optimal cell} for $v$.

If we are only looking for \emph{a very good} solution we can simply select a sample point that is very close to the lower bound of the cell and give extremely strong guarantees on the value of the objective. For $s$ a satisfying sample point with $s_v \in (\alpha_1, \alpha_2)$ we can give an \emph{absolute} bound on the quality of the solution, namely $s_v - \alpha_1$. We can even allow the user to provide an acceptable error and simply select an appropriate $s_v$, all without the need for any approximation.

Of course, we can even give exact values if the optimal satisfying cell is a closed cell. In this case, the optimal value $s_v$ is exactly at a root, and the regular CAD lifting provides us with an optimal assignment.
Unboundedness can also be detected easily when the very first cell -- that is unbounded -- is satisfiable.
With this, we claim to be on a par with other optimization techniques, at least in terms of expressivity.

We additionally would like to compute solutions for unbounded cells and exact optima from open cells.
While we have not investigated this issue deeply, we propose to borrow terminology and machinery from \emph{virtual substitution}, which allows computing with $\pm\infty$ as well as terms of the form $r \pm \varepsilon$.
These are eliminated within the individual rules of virtual substitution, and we assume that similar techniques can be applied within a lifting step.
