\section{Infeasible subsets\label{sec:cad:infeasible-subsets}}

\silentdefindex{cad:infeasible-subsets}\silentdefindex{minimal-infeasible-subset}
In the case of an inconsistent set of constraints, we would like to provide the SAT solver with an \emph{infeasible subset} of constraints as already discussed in \cref{ssec:SMT:infeasible-subsets}.
Unfortunately, we can not easily derive any information from our state when we determine unsatisfiability, mostly because we have no ``constructive'' criterion that we check -- like we would, for example, for the simplex method -- but only run out of solution candidates. In this sense, we can only perform an \emph{a-posteriori} analysis of an (almost) regular CAD computation.
Therefore, we employ a technique that is very similar to the one described in~\cite{Jaroschek2015} and directly based on the work presented in~\cite{Hentze2017}.
In the following, we assume that our CAD method has run to completion and found no sample points to be satisfying.

When we found the problem to be unsatisfiable, we have for every leaf node in the sample tree at least one constraint that evaluates to $\false$ on this leaf node.
Note that the leaves may not be of full dimension: we may stop lifting a partial sample point if it is already invalidated by a constraint, roughly following~\cite{Collins1991}.
Fundamentally, an infeasible subset is any subset of constraints such that every sample point is still invalidated (or ``covered'') by this subset.

We approach this problem in the spirit of a set cover problem and note the similarity to minimal infeasible subsets from \cref{def:minimal-infeasible-subset}.

\begin{definition}{Set cover problem}{set-cover}\silentdefindex{set-cover}
	Let $\Omega$ be a finite set and $S \subseteq \Powerset(\Omega)$.
	We call $C \subseteq S$ a \emph{set cover of $\Omega$} if
	\[
		\bigcup_{s \in C} s = \Omega
		\textrm{\quad or equivalently \quad}
		\Forall{o \in \Omega} \Exists{s \in S} o \in s
	\]
	We call $C$ a \emph{minimum set cover} if it has minimal cardinality among all set covers and a \emph{minimal set cover} if no proper subset of $C$ is a set cover.
	Finding a \emph{minimum set cover} $C$ for some $\Omega$ and $S$ is called the \emph{set cover problem $SC(\Omega,S)$}.
\end{definition}

The set cover problem is one of the classical NP-hard problems from~\cite{Karp1972}, suggesting that computing an optimal solution is oftentimes infeasible.
There is, however, a simple greedy heuristic which achieves an essentially optimal approximation~\cite{Chvatal1979}.
We use the set cover problem to compute infeasible subsets as follows.

\begin{definition}{Infeasible subsets by set covers}{mis-by-set-cover}\silentdefindex{mis-by-set-cover}
	Let $C$ be constraints and $S$ the sample points constructed by CAD.
	We know that $\Forall{s \in S} \Exists{c \in C} c(s) = \false$ which essentially matches the alternative formulation of the set cover problem.
	Let $S_c = \{ s \in S \mid c(s) = \false \}$ for every $c \in C$, then finding an infeasible subset is equivalent to the set cover problem $SC(S, \{S_c \mid c \in C \})$.
\end{definition}

\begin{theorem}{Correctness of infeasible subsets by set covers}{mis-by-set-cover-correctness}
	Any set cover $X$ that is a solution to $SC(S, \{C_c \mid c \in C \})$ is a proper infeasible subset.
	If $X$ is a minimal (minimum) set cover, it is a minimal (minimum) infeasible subset.
\end{theorem}
\begin{proof}
	We defined the set cover problem such that the elements of the universe are sample points and a sample point is \emph{covered} if a constraint evaluating to $\false$ for this sample point is selected. As a set cover \emph{covers} all sample points, no sample point is satisfiable and the selection of constraints is an infeasible subset.

	If $X$ is a minimal set cover, for each of its proper subsets some sample point remains uncovered and thus is satisfiable. Hence, $X$ has no proper subset that is an infeasible subset and $X$ is a minimal infeasible subset. Analogously, if $X$ is a minimum set cover, it is also a minimum infeasible subset.
\end{proof}

As we have transformed our SMT specific question for infeasible subsets into a rather simple and well-understood set cover problem, we can now use this new formalization to actually compute infeasible subsets.
As already argued, \emph{minimum infeasible subsets} may be hard to compute as they are one of the original NP-hard problems, so we instead use heuristics in the spirit of~\cite{Chvatal1979} to obtain \emph{minimal infeasible subsets}.

Though this gives us a nice way to obtain infeasible subsets, there are still a few open questions when integrating this approach with an actual CAD implementation. First of all, we need to obtain the sets $C_c$ from our CAD and may realize that our CAD, in fact, does not evaluate all constraints on every sample point.
We usually only evaluate until we find a single constraint that invalidates a particular sample point and avoid further evaluations to eliminate unnecessary overhead.

We could go ahead and use these partial sets, accepting some interesting consequences.
In this case, all sets are singleton sets and thus we essentially have no combinatorial problem. We can simply collect all constraints that are used at some point to invalidate a sample point and are done, potentially saving a lot of work.
It however also means that which constraints are part of the infeasible subset is determined by the order in which we evaluate constraints on a sample point.

Alternatively, we can evaluate all sample points with all constraints and use the full sets. This gives us full flexibility on which covers to compute and allows finding minimal infeasible subsets of the actual problem. We find that these additional evaluations are not that costly in practice, at least when compared to the effort we put in the CAD computation in the first place.

We can also understand this problem as a linear programming problem as described in~\cite{Jaroschek2015}.
We want to note, however, that the set cover formulation is quite promising as we usually have a large number of sample points and only a few constraints, thus also the underlying implementation of~\cite{Jaroschek2015} employs a set cover heuristic.

We finally observe that the set cover problem is usually very regular and has a lot of redundancies.
It is common, that only a single constraint invalidates a particular sample point. Such an \emph{essential constraint} must be part of any infeasible subset and we can thus eliminate it from the set cover problem, as well as all sample points that are covered by it. 
We also know that the CAD usually has multiple sample points for a single sign-invariant region, and these sample points should be completely identical within the set cover problem. We can thus remove such \emph{duplicate columns}, conceding that this may very well change the selections of the greedy heuristics from~\cite{Chvatal1979}.

Systematically applying these techniques is surprisingly powerful in practice, sufficiently reducing the problem size to allow for an optimal brute force solution of the set cover problem instead of the heuristic approach referenced above, as shown in~\cite{Hentze2017}.

A classical question in the context of infeasible subsets is whether the size of the infeasible subset is the best ranking criterion. One could imagine that selecting \emph{easy constraints} is beneficial -- as this increases their activity in the SAT solver and thus encourages working on easier parts of the problem -- or selecting constraints of \emph{small decision level} could speed up the search -- by excluding larger parts of the Boolean search space.
We can easily enhance the set cover problem to a \emph{weighted set cover problem} to allow for arbitrary weights of the constraints, and appropriate heuristics exist for the weighted variant as well.

Note, however, that this might only end up as a proxy to modify the SAT solvers decision heuristic. Past research -- as well as our practical experience -- suggests that it is usually a bad idea to mess with the decision heuristic. Adding these criteria almost consistently either does not change the overall performance at all or even impairs it.

This should not come as a surprise as recent progress on SAT decision heuristics points towards structural properties of the clauses (like \emph{literal block distance}~\cite{Audemard2009}) or the whole formula (like \emph{community structure}~\cite{Liang2015}) or even optimizing for behavioral properties of the solver (like \emph{learning rate}~\cite{Liang2016}), but away from properties of the individual literals.
It is not clear to us, however, how such structural properties can be properly integrated into the approach described above and we, therefore, leave it for future research.

Another interesting direction would be to investigate whether adding multiple infeasible subsets at once can be beneficial.
It could be possible to construct different infeasible subsets and provide the SAT solver with more diverse knowledge.
We conjecture, however, that it might be difficult to have multiple infeasible subsets that do not introduce a lot of redundant information.

To evaluate these different possibilities, we propose the following different heuristics for computing infeasible subsets where ``preprocessing'' includes selecting essential constraints and removing duplicate columns as described in~\cite{Hentze2017}: \SolverCADMISTrivial returns the trivial infeasible subset; \SolverCADMISGreedy employs the standard greedy heuristic from~\cite{Chvatal1979}; \SolverCADMISGreedyPre applies preprocessing before using the greedy heuristic; \SolverCADMISGreedyWeighted uses the standard greedy heuristic where constraints are weighted according to their complexity; \SolverCADMISExact computes a \emph{minimum} covering after preprocessing; \SolverCADMISHybrid first uses preprocessing, then the greedy heuristic until at most $12$ constraints are left, and then computes a \emph{minimum} covering.
The threshold of $12$ constraints is taken from~\cite{Hentze2017} and makes sure that we only compute a minimum covering if it is sufficiently fast.

\begin{table}
	\centering
	\input{experiments/output/table-cad-mis}
	\caption{Experimental results for heuristics for minimal infeasible subsets.}\label{tbl:cad:mis-results}
\end{table}

We see the impact of the different heuristics -- or rather the lack thereof -- in \cref{tbl:cad:mis-results}. As already noted, the choice of the heuristic is almost irrelevant on the considered benchmark set and the differences do not seem to be statistically significant. Therefore, we refrain from a deeper analysis here and refer to~\cite{Hentze2017} which contains some interesting observations: for example, in these experiments the number of required theory calls decreased significantly with \SolverCADMISHybrid (compared to \SolverCADMISGreedy), though this is not reflected in the overall solver performance.

We note that many interesting questions and possible further heuristics are left open and untested here. The impact of heuristics for infeasible subsets have proven to be minor at best on our benchmarks -- mostly due to the low Boolean complexity -- but some results from~\cite{Hentze2017} and practical experience with other theories indicate that one should nevertheless keep this topic in mind for future research.
