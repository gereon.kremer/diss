\section{Equational constraints\label{sec:cad:equational-constraints}}

\silentdefindex{cad:equational-constraints}
An important optimization for CAD that we already discussed in \cref{ssec:cad:equational-constraints} are \emph{equational constraints} as initially described in~\cite{Collins1998}.
Essentially, we employ equalities to not only simplify the lifting process -- by removing sample points that do not satisfy the equalities -- but also to avoid certain projection steps.
Given the significant practical impact on some formulae, we now integrate this optimization into the CAD proof system.

We have already discussed some details of using equational constraints in \cref{ssec:cad:equational-constraints}, in particular the existence of both the \emph{restricted} and \emph{semi-restricted} projection operator and the restrictions on their applicability.
We do not aim to incorporate these in detail here, but rather give a rough template on how to do so.

The main idea is to store which equalities are used as equational constraints in a new part of the state we call $EQ$ and to maintain a separate queue called $Q_*$ that contains everything that is disabled due to the equational constraints used to simplify the projection. For the sake of an easy presentation, we now present a simple -- and probably suboptimal -- scheme to deal with this issue and refer more efficient implementations to the reader. We assume that making the following adaptions reasonably efficient is rather straightforward for anyone implementing an incremental CAD along the lines of what was described before.

\begin{algorithm}
	\caption{Reduce projection based on equational constraints}\label{algo:cad:EqC:reduce-projection}

	\SetKwFunction{ReduceProjection}{ReduceProjection}
	\KwIn{constraints $C$, polynomials $P$, equational constraints $EQ \subseteq C$}
	\Fn{\ReduceProjection{$C$, $P$, $EQ$}}{
		$P' = P \cap \{ p \mid p \signcondition 0 \in C \}$ \Comment*[r]{polynomials from input constraints}
		\While{$P'$ changed}{
			Let $O = \{ \{p\}, \{p,q\} \mid p \in P', q \in EQ \}$ \;
			$P' = P' \cup \{ p \in P \mid origins(p) \cap O \neq \emptyset \}$ \;
		}
		\Return{$P'$}
	}
\end{algorithm}

Let us first define a helper method to remove polynomials from the projection due to equational constraints in \cref{algo:cad:EqC:reduce-projection}. This method, that we call \ReduceProjection, iteratively collects polynomials that are still active under a restricted set of origins, consisting of single projection steps or projection steps involving one of the equational constraint polynomials.

\begin{algorithm}
	\caption{Reduce queue based on equational constraints}\label{algo:cad:EqC:reduce-queue}
	
	\SetKwFunction{ReduceQueue}{ReduceQueue}
	\KwIn{polynomials $P$, sample points $S$, queue $Q$, \newline equational constraints $EQ \subseteq C$}
	\Fn{\ReduceQueue{$P$, $S$, $Q$, $EQ$}}{
		\Return{$Q \cap \left( \{ (p,p) \mid p \in P \} \cup (P \times EQ) \cup (EQ \times P) \cup (S \times P) \right)$}
	}
\end{algorithm}

Similarly, we define the helper method \ReduceQueue in \cref{algo:cad:EqC:reduce-queue} that restricts the queue to all elements consistent with the new set of projection polynomials and sample points and also removes projection candidates that would yield inactive projection polynomials due to the equational constraint polynomials.

This now allows us to define the new proof rule \rulename{EqC-Cleanup} that moves everything from either $P$, $S$, or $Q$ to $Q_*$ which should be disabled due to the active equational constraint polynomials $EQ$. We also define the inverse rule \rulename{EqC-Restore} to restore all elements from $Q_*$, intended to be called when a polynomial is removed from $EQ$.
\begin{align*}
	\ProofRule{EqC-Cleanup}{\State{C,P,S,Q,Q_*,EQ}}{\State{C,P',S' \cup S_I,Q',Q'_*,EQ}}{
		\textrm{$P' = \ReduceProjection{C, P, EQ}$} \\
		\textrm{$S' = \{ s \in S \mid \Exists{o \in origins(s)} o \subseteq P' \}$} \\
		\textrm{$S_I = \textrm{intermediate samples from $S$ required for $S'$}$} \\
		\textrm{$Q' = \ReduceQueue{P', $S' \cup S_I$, Q, EQ}$} \\
		\textrm{$Q'_* = Q_* \cup (P \setminus P') \cup (S \setminus (S' \cup S_I)) \cup (Q \setminus Q')$}
	} \\
	\ProofRule{EqC-Restore}{\State{C,P,S,Q,Q_*,EQ}}{\State{C,P',S',Q',\emptyset,EQ}}{\textrm{$P' = P \cup polynomials(Q_*)$} \\
		\textrm{$S' = S \cup samples(Q_*)$} \\
		\textrm{$Q' = Q \cup (Q_* \setminus (P' \cup S'))$}
	}
\end{align*}

To actually make use of this technique, we invoke the new proof rule \rulename{EqC-Cleanup} whenever \rulename{Add-Poly-Finished} was used and let any invocation of \rulename{EqC-Restore} be followed by \rulename{EqC-Cleanup}:
\begin{align*}
	\rulename{Add-Poly-Finished} & \rightarrow \rulename{Add-Poly-Finished} \circ \rulename{EqC-Cleanup} \\
	\rulename{EqC-Restore} & \rightarrow \rulename{EqC-Restore} \circ \rulename{EqC-Cleanup}
\end{align*}

Note that we did not yet define how the set of equational constraint polynomials $EQ$ should be maintained. We define a new proof rule \rulename{EqC-Select} that enables as many equational constraints from $C$ as possible by adding their polynomials to $EQ$ and analogously a proof rule to deactivate a given equational constraint polynomial. Both are designed to be integrated with \rulename{Add-Constraint} and \rulename{Delete-Constraint}, respectively.
\begin{align*}
	\ProofRule{EqC-Select}{\State{C,P,S,Q,Q_*,EQ}}{\State{C,P,S,Q,Q_*,EQ \cup N}}{\textrm{$L(P) \coloneqq \{ level(p) \mid p \in P \}$} \\
		\textrm{$N = \{ p \mid p = 0 \in C \}$ such that } \\
		\textrm{$|L(EQ \cup N)| = |EQ \cup N|$}
	} \\
	\ProofRule{EqC-Unselect $c$}{\State{C,P,S,Q,Q_*,EQ}}{\State{C,P,S,Q,Q_*,EQ'}}{\textrm{$EQ' = EQ \setminus \{ p \mid c = p \signcondition 0 \}$}}
\end{align*}

Consequently, we invoke \rulename{EqC-Select} after \rulename{Add-Constraint} and forward the removed constraint from \rulename{Delete-Constraint} to \rulename{EqC-Unselect}. Note that after unselecting an equational constraint polynomial we may be able to select another one on the same level and we therefore invoke \rulename{EqC-Select} again.
Additionally, we add the new state components $Q_*$ and $EQ$ to all other proof rules and let them be the empty set initially.
\begin{align*}
	\rulename{Add-Constraint} & \rightarrow \rulename{Add-Constraint} \circ \rulename{EqC-Select} \\
	\rulename{Delete-Constraint $c$} & \rightarrow \rulename{Delete-Constraint $c$} \circ \rulename{EqC-Unselect $c$} \circ \rulename{EqC-Select} \\
\end{align*}

As we have already mentioned, the actual implementation may look somewhat different. In particular, one might want to employ a more efficient mechanism to restore elements from $Q_*$ -- instead of restoring everything and filtering again -- and implement \ReduceProjection level-by-level instead of a generic fixed-point iteration.
Another interesting question is which heuristic should be implemented to select an equational constraint in \rulename{EqC-Select}.

\begin{table}
	\centering
	\input{experiments/output/table-cad-equational}
	\caption{Experimental results for equational constraints.}\label{tbl:cad:equational-results}
\end{table}

A brief overview of the impact of using equational constraints as implemented in~\cite{Haehn2018a,Haehn2018b} is given in \cref{tbl:cad:equational-results}.
We compare (for Hong's, McCallum's, and Brown's projection operators) our implementation without equational constraints, with the restricted projection operator (\texttt{EC}), and the semi-restricted projection operator (\texttt{ECS}).
Though literature indicates that equational constraints can have a substantial impact, it makes no consistent difference in our use case.
We conjecture that the possible effect of equational constraints is reduced by the incremental setting and superseded by the syntactic variable elimination that we describe in the following \cref{ssec:cad:resultant-rule}.

\subsection{Variable elimination and the resultant rule\label{ssec:cad:resultant-rule}}

\silentindex{cad:resultant-rule}\silentindex{resultant-rule}
An interesting topic concerning equational constraints is the \emph{resultant rule} as described in \cref{def:resultant-rule}, taken from~\cite{Collins1998}.
We could integrate the resultant rule into the proof system defined above, as well as into an actual implementation thereof.
Following the idea of postponing all computations as long as possible, it seems tempting to apply the resultant rule lazily and generate new equational constraints dynamically.

However, we want to caution everyone planning to go down this road for several reasons. First of all, this adds a whole new layer of complexity that we have to deal with. To just give one example, enabling an equational constraint may invalidate origins of another equational constraint on a lower level and thus lead to disabling another one -- which in turn might reactivate yet another one even further down. Getting all this right -- on top of all the bookkeeping we have to do anyway -- is anything but trivial.

Secondly, we observe a very fundamental issue in the interaction of equational constraints and incrementality. Adding constraints only leads to \emph{things being added} to our CAD and removing constraints only leads to \emph{things being removed} from our CAD. Also, we only remove polynomials (and sample points) that we are sure we do not need anymore (except if the constraint is later added again).
When adding an equational constraint, however, we also remove, and when removing an equational constraint we restore what we removed before.
This significantly raises the amount of ``induced changes'', and possibly the amount of information we need to recompute.

Finally, we may very well argue that eagerly applying the resultant rule can be very beneficial, even if we disregard the advantages due to less bookkeeping and simpler code or even using equational constraints at all. The resultant rule gives rise to new equational constraints that might allow to \emph{syntactically eliminate variables} as mentioned in \cref{ssec:cad:heuristics:other} and shown in \cref{ex:cad:variable-elimination-with-resultant-rule}, and the number of variables is one of the main drivers of run-time complexity in CAD.
An eagerly applied combination of the resultant rule and syntactic variable elimination may thus be very beneficial and outweigh the possible benefits of incrementality by far, also allowing to be applied to CAD implementations that do not exploit equational constraints internally.

\begin{example}{Variable elimination with the resultant rule}{cad:variable-elimination-with-resultant-rule}
	Consider the input formula $\varphi \equiv y^2x^2 = 0 \land y^2-1 = 0$ and assume that we eliminate $y$ first.
	The resultant rule states that 
	\[
		(y^2x^2 = 0 \land y^2-1 = 0) \implies \Res_y(y^2x^2, y^2-1) = 0
	\]
	Observe that neither $y^2x^2 = 0$ nor $y^2-1 = 0$ can be used to syntactically eliminate $y$ (or $x$) because neither defines a unique solution for either variable.
	While $y^2x^2 = 0$ yields a solution of the form $y = 0 \lor x = 0$, $y^2-1 = 0$ gives $y = 1 \lor y = -1$.
	The resultant rule however gives $x^4 = 0$ with the unique solution $x = 0$ that we can use to eliminate $x$ and obtain $\varphi' \equiv y^2-1 = 0$.
\end{example}

We thus devise four variants to evaluate the impact when equational constraints are used and when they are not: without any preprocessing (\SolverCADPP and \SolverCADECSPP); with the resultant rule only (\SolverCADPPRR and \SolverCADECSPPRR); with syntactic variable elimination only (\SolverCADPPVE and \SolverCADECSPPVE); with the resultant rule and syntactic variable elimination (\SolverCADPPVERR and \SolverCADECSPPVERR).

\begin{table}
	\centering
	\input{experiments/output/table-cad-pp}
	\caption{Experimental results for the resultant rule.}\label{tbl:cad:pp-results}
\end{table}

Additionally, we show results for the regular (fully incremental) CAD and when equational constraints are used.
We see in \cref{tbl:cad:pp-results} that these preprocessing techniques have essentially no impact for satisfiable instances but can make a big difference in case of unsatisfiability.
Both, the resultant rule and the variable elimination, improve the solver significantly, combining them however yields worse results than using variable elimination alone.

\begin{figure}
	\centering
	\begin{subfigure}{0.49\textwidth}
		\includetikz{06f-scatter-cadpp-1}
		\vspace*{-1em}
		\caption{\SolverCADPP vs. \SolverCADPPRR}\label{fig:cad:pp-scatter-1}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includetikz{06f-scatter-cadpp-2}
		\vspace*{-1em}
		\caption{\SolverCADPPRR vs. \SolverCADPPVE}\label{fig:cad:pp-scatter-2}
	\end{subfigure}

	\begin{subfigure}{0.49\textwidth}
		\includetikz{06f-scatter-cadpp-3}
		\vspace*{-1em}
		\caption{\SolverCADPPRR vs. \SolverCADPPVERR}\label{fig:cad:pp-scatter-3}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includetikz{06f-scatter-cadpp-4}
		\vspace*{-1em}
		\caption{\SolverCADPPVERR vs. \SolverCADPPVE}\label{fig:cad:pp-scatter-4}
	\end{subfigure}

	\caption{Comparison of unsatisfiable instances.}\label{fig:cad:pp-scatter}
\end{figure}

Furthermore, the comparisons in \cref{fig:cad:pp-scatter} show that these different heuristics essentially follow a strict order in that a ``worse'' heuristic solves almost no examples a ``better'' heuristic can solve.
We see this from the fact that only very few benchmark instances end up in the lower right triangle.

We observe that the resultant rule in some way \emph{emulates} variable elimination in that computing the resultant of the form $\Res_x(x - p, q)$ -- where $p$ does not contain $x$ -- is essentially equivalent to substituting $x = p$ into $q$.
The performance of the resultant rule -- compared to variable elimination -- thus seems to suggest that it merely does a poor job at simulating variable elimination, but has no real benefit on its own, at least on this benchmark set and if (syntactic) variable elimination is performed.

This even holds true when equational constraints are used, which should benefit in particular from new equalities generated by the resultant rule.
We thus consider the resultant rule a nice tool that one should consider for particular problem classes, but not beneficial in general for our use case.
