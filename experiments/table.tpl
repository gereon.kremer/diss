\begin{tabularx}{\linewidth}{lYYYYYYYYY}
	\textbf{Solver}
	& \multicolumn{3}{c}{\textbf{SAT}}
	& \multicolumn{3}{c}{\textbf{UNSAT}}
	& \multicolumn{3}{c}{\textbf{overall}}
	\\
	\hline
{%- for s in solvers|sort(attribute = 'solved') %}
	{% if s["solver"] in override %}{{ texName(override[s["solver"]]) }}{% else %}{{ texName(s["solver"]) }}{% endif %}
	& {{ s["sat"] }} & {{ "%0.1f"|format(s["sat"] / numberOfFiles() * 100) }}~\% & {{ "%0.2f"|format(s["avgsattime"]) }} 
	& {{ s["unsat"] }} & {{ "%0.1f"|format(s["unsat"] / numberOfFiles() * 100) }}~\% & {{ "%0.2f"|format(s["avgunsattime"]) }} 
	& {{ s["solved"] }} & {{ "%0.1f"|format(s["solved"] / numberOfFiles() * 100) }}~\% & {{ "%0.2f"|format(s["avgtime"]) }} 
	\\
{%- endfor %}
\end{tabularx}
