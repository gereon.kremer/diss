\section{Heuristic choices}\label{sec:cad:heuristic-choices}

The proof system presented before provides a framework to determine the satisfiability of a sequence of sets of constraints by employing a CAD-style method.
However, it does not yet yield a deterministic method as it leaves a number of choices how to use the proof system.
We now discuss these choices, present some possible heuristics, and evaluate their impact in practice.

For an experimental comparison, we use one particular combination of heuristics as a baseline and vary one single heuristic in every experiment.
This particular baseline solver considers only the degree for the projection order and both the level and the type for the lifting order, denoted by \SolverCADPOd{} and \SolverCADLOlt{}, respectively.

\subsection{Variable ordering\label{ssec:cadSMT:heuristics:varorder}}

\silentdefindex{cad:variable-ordering}
While most of the presented heuristics have no notable effect if one aims for a complete CAD anyway, the \emph{variable ordering} is known to have a significant impact on the size of a complete CAD.
The problem of selecting a good variable ordering has already been studied in the literature, for example, in~\cite{Brown2004,Dolzmann2004,Huang2014,England2014}.
Though we carried out our own experiments here, we would generously describe them as fruitless: we could mostly confirm that the complexity of CAD seems to grow with some ``inherent algebraic hardness''.

In most cases, changing the variable order does not change the solver behavior, and if it does the effects are extremely difficult to predict.
In our view, this is also reflected by the fact that current research on this topic makes use of machine learning techniques, which could be interpreted malignly as surrendering to an inherently difficult problem.

Beyond that, we have an additional level of complications in our scenario: we not only want to compute one complete CAD but a sequence of possibly incomplete CADs. Even worse, we can not even say beforehand \emph{which CADs} we need to compute as this depends on how the SAT solver behaves, which in turn depends on the infeasible subsets we generate.
We discuss these issues to some degree in~\cite{Nalbach2019}, though in the context of \MCSAT, but the underlying problem is essentially the same.

Therefore, we refrain from further analysis of variable orderings here and just note that we use what is called the \emph{triangular ordering} in~\cite{England2014} based on all constraints from the formula and keep it static over the whole run.
We acknowledge that this heuristic was suggested in the context of the RegularChains library we briefly described in \cref{ssec:intro:related:variant-of-cad} and thus it is not clear whether it is a particularly good heuristic for a regular CAD projection.

\subsection{Queue ordering\label{ssec:cadSMT:heuristics:queueorder}}

\silentdefindex{cad:queue-ordering}
Another major heuristic is of course when to work on which queue element from $Q$, that is when to apply \rulename{Project} or \rulename{Lift} on which projection or lifting candidate. We model this as a global ordering on the elements in $Q$ and call this ordering the \emph{CAD scheduler}.
In practice, we usually decompose this scheduler into one part that decides whether we should project or lift and separate orderings on the projection candidates and lifting candidates.
Our current (rather naive) heuristic is to always perform a complete lifting before the next projection step is computed.

We also experimented with other heuristics -- for example, completing lifting on all rational sample points before computing a projection step and only considering real algebraic sample points when the projection is complete, and some variants thereof -- but contrary to our expectations, we could not match the performance of the naive approach.
We thus only show results for this naive variant here, still convinced, though, that significant improvements are possible, in particular on restricted inputs.

Within this general ordering that one could describe as \emph{lifting first}, we can now impose an ordering on the projection candidates and the lifting candidates separately.
Our approach is to apply \emph{lexicographical} orderings to combine multiple properties of each candidate into one consistent ordering.

\begin{table}
	\centering
	\input{experiments/output/table-cad-po}
	\caption{Experimental results for different projection orders.}\label{tbl:cad:projection-results}
\end{table}

For projection candidates, we consider the level of the involved polynomials (\texttt{L} and \texttt{l} prefer \emph{high} and \emph{low} levels), their type (\texttt{P} and \texttt{S} prefer \emph{paired} and \emph{single} projection candidates), and finally their degree (\texttt{D}).
Some results for varying \emph{projection orders} based on these properties are shown in \cref{tbl:cad:projection-results}.

\begin{table}
	\centering
	\input{experiments/output/table-cad-lo}
	\caption{Experimental results for different lifting orders.}\label{tbl:cad:lifting-results}
\end{table}

For the lifting candidate, or rather its sample points, we consider the absolute value (\texttt{a}), the level (\texttt{l}), the approximate size of its representation in memory (\texttt{s}), and its type (\texttt{t}, preferring integers over rationals over real algebraic numbers).
The respective experimental results for some combinations of these properties are shown in \cref{tbl:cad:lifting-results}.

As we can see, both orderings have a limited but noticeable impact on the overall performance of the solver.
However, our experience shows that the differences do not persist over time -- or rather implementation and configuration changes -- but seem to correlate with other components of the solver in obscure ways.
In~\cite{Kremer2020}, we evaluated the very same orderings on projection and lifting candidates, but of course, the solver around it has changed since, as well as the benchmarks that are considered. While we noted in~\cite{Kremer2020} that using the level of a projection candidate seems to be beneficial, the most recent results shown in \cref{tbl:cad:projection-results} suggest the opposite conclusion.

\subsection{Other heuristics}\label{ssec:cad:heuristics:other}

Apart from these obvious heuristics, one must make a number of further choices when implementing the presented proof system.

\paragraph{Syntactic variable elimination.}
The main driver of the asymptotic complexity of the CAD method is the number of variables. Hence, eliminating variables should improve the performance drastically, at least if we can do so cheaply.
Therefore, we propose to check for equalities that yield a unique solution for one variable (usually equalities that are \emph{linear} in this variable) and use them to eliminate this variable from the set of constraints before starting the actual CAD computation.

However, changing the set of variables is fundamentally opposed to the idea of incrementality. We thus propose to keep the full variable ordering intact and simply let the polynomials skip the levels that correspond to eliminated variables.

\paragraph{Factorization.}
As already mentioned, it can make sense to factorize every polynomial and add the factors to the projection instead of the original polynomial.
Given that we need to make the set of polynomials a \emph{finest square-free basis} anyway for most projection operators -- notably McCallum's, Lazard's, and Brown's -- we always factorize all polynomials and thereby get a \emph{finest irreducible basis}.

We used to simply ignore that we need a finest square-free basis in the past and note that this did neither cause incorrect results nor did the overall performance seem to degrade significantly.
Obtaining a finest square-free basis without computing a full factorization turns out to be much more difficult than one would think, mostly because of the incremental nature of our projection and in particular the removal of polynomials as we already note in~\cite{Kremer2020}.
We thus now always use full factorization as it not only resolves all theoretical issues but also provides a (very small) performance increase.

\paragraph{Sample point generation.}
The proof system features the \rulename{Lift} rule that vaguely states that one should construct the ``samples for $R$'', $R$ being a list of real roots. While the samples need to cover all sign-invariant regions -- one below the smallest root, one above the largest root, and one between every two consecutive roots -- it is not clear how exactly to select them.

While a number of possible heuristics may seem reasonable -- the midpoint, an integer or more towards zero, maybe even selecting more than a single point -- the more important question is whether it actually makes a difference.
We have looked at this in~\cite{Kremer2020} and concluded that the effect of this selection seems to be negligible.

\paragraph{Subroutines.}
Most parts of the proof system make use of more or less complex subroutines that all come with their own heuristics that influence their exact result (or at least their performance) and how they should be chosen in the context of CAD -- or even in combination with the other heuristics -- is mostly unclear.
We briefly mention a few of them to give a rough feeling for the breadth of this problem.

Even for storing numbers as our most basic elements, there is a significant variety of possible options. We have already discussed the representation of real algebraic numbers in \cref{sec:preliminaries:ran}, which implicitly assumed some way to store rationals. Multiple options exist here, either based on fractions of arbitrarily large integers as in GMP~\cite{Granlund} or CLN~\cite{Haible1996}, or based on arbitrarily large floating-point numbers as in MPFR~\cite{Fousse2007}. For certain applications, \emph{binary rationals} or \emph{dyadic rationals} (with denominators being powers of two) have been shown to be particularly efficient as well~\cite{Moura2013a}. We use rationals from GMP unless explicitly stated otherwise.

The basic objects we use in CAD are polynomials, which again feature a vast amount of different representations, each with technical implementation details that are crucial for practical performance.
Even well-established software packages like Maple still change their internal representations for multivariate polynomials significantly from time to time, as witnessed by~\cite{Monagan2014}.

For more specialized cases like univariate polynomials or fully factored polynomials (as used by \QEPCADB), other representations are usually preferred.
Our implementation features sparse multivariate polynomials (as a list of terms with sparse exponent vectors) and dense univariate polynomials (with possibly multivariate coefficients).

Resultants and discriminants can be obtained in a variety of ways. They are usually defined as determinants of the Sylvester matrix -- or some of its variants or the Bézout matrix -- but are usually computed more efficiently via subresultant sequences. Methods to compute them have a long history and are continuously improved, for example~\cite{Collins1967,Collins1971b,Geddes1992,Ducos2000}, though with sometimes unclear benefits and trade-offs. Our current implementation is based on~\cite{Ducos2000}.

Real root isolation is used in the \rulename{Lift} rule to construct new sample points. A range of different possibilities was explored in~\cite{Kremer2013}, mostly aiming to exploit approximative methods as preconditioning to arguably naive bisection in the spirit of~\cite{Collins1976} or~\cite{Sagraloff2012}.
Many questions arise in this context: should we fully factor a polynomial before isolating its roots? Where should we perform splits for bisection? Is it worth using numerical approximations beforehand? How should we count the roots within an interval?

Given the sheer number of design decisions that must be made, combined with the many side effects that any of these could have on all the others, one quickly realizes that it is essentially hopeless to obtain an optimal configuration.
We rather try to optimize every issue ``locally'' -- or in combination with a very limited number of other heuristics -- and simply hope that the side effects on uncared for subroutines are negligible.
