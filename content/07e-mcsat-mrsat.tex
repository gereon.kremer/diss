\section{Model-Refining Satisfiability Calculus}\label{sec:mcsat:mrsat}

While many different factors contribute to the practical efficiency of modern solving technologies like SAT solvers, one important one is the ability to \emph{pick up valuable information} when its \emph{cheap to obtain} and then use it again afterward.
In particular, Boolean constraint propagation, but also conflict resolution with clause learning, collects such information when it is easy to do and makes it explicit to the other components of the solver -- either by adding it to the trail or crafting a new clause.

Of course, one would like to have similar options in \MCSAT to learn new facts in the theory cheaply. While \rulename{T-Propagate} aims to do this, we found it hard to apply and computationally expensive in many cases.
One major shortcoming in practice that we did not seem to be able to overcome was that any theory reasoning was restricted to the theory variables already assigned (and the next one due to be assigned).
We thus propose to extend \MCSAT, allowing not only to \emph{construct} the theory model but also to \emph{refine} the \emph{not yet assigned} parts of the model.

\begin{definition}{Model-refinement satisfiability calculus}{model-refinement-satisfiability-calculus}\silentdefindex{mcsat:mrsat}
	Let $CD_x \subseteq D_x$ be the \emph{current domain} of a variable $x$, implying that for every satisfying assignment $\alpha$ we have that $\alpha_x \in CD_x$, and write $x \in CD_x$.
	We introduce a new type of trail elements of the form $E \rightarrow x \in CD_x$ where $E$ is a subset of the preceding trail and add the following proof rule:
	\begin{align*}
		\ProofRule{Refine-Model}{\State{M, \Clauses}}{\State{\Seq{M, E \rightarrow x \in CD'_x}, D, \Clauses}}{\textrm{$E \subseteq M$ such that} \\ \textrm{$E \implies x \in CD'_x \subsetneq CD_x \in M$}}
	\end{align*}
	We allow to use the information that $\overline{x} \in CD_{\overline{x}}$ throughout the remaining proof system, in particular when evaluating literals with not yet assigned variables in the $\Value$ function or to detect conflicts if a current domain becomes empty.
\end{definition}

This extension from \cref{def:model-refinement-satisfiability-calculus} that we call \emph{model-refinement satisfiability calculus} shall maintain a \emph{current domain} for every variable, usually an interval (for arithmetic variables).
In regular \MCSAT, this current domain is $(\infty,\infty)$ and becomes a point interval once we invoke \rulename{T-Decide} for this variable.

We note that the propagations added to the trail integrate seamlessly with conflict resolution, but may introduce problems with the issue of termination. In particular, we need to impose an equivalent of the finite-basis property on the implementation of the \rulename{Refine-Model} rule as well, even worse on the \emph{combination} of the \rulename{Refine-Model} rule and the $\Explain$ function.

An implementation of the \rulename{Refine-Model} rule could use interval constraint propagation (ICP) in the spirit of~\cite{Benhamou2006,Moore2009,Schupp2013} on the current domains, refining them based on constraints and theory decisions from the trail.
Apart from regular propagations, one could even implement \emph{splits} as decisions of new literals in \rulename{Decide}.
Note, however, that ICP-style refinements most probably do not satisfy such a finite-basis property.

We can finally exploit this information in multiple ways.
If any current domain becomes empty, we immediately have a Boolean conflict consisting of the propagations that imply the empty set.
Whenever we perform a theory decision, we can restrict the search for a feasible assignment to the current domain of the respective variable.
Whenever we evaluate constraints -- either to find conflicts, perform theory propagations, or check for semantic propagations -- we can not only evaluate over the theory model but also over the current domains for not yet assigned variables.

One can very well argue that this \emph{only integrates ICP into \MCSAT} or that we \emph{only make knowledge explicit} that is already there.
While this may be true, both of this can be very valuable: after all, \emph{making existing knowledge explicit} is the whole point of these kinds of solvers and the integration of ICP into \MCSAT might give us the power to reason about not yet assigned variables as well, making our solver more robust against possibly bad variable orderings.

Furthermore, this technique is more general than iteratively restricting the model with ICP. It could, for example,
\begin{enumerate*}
	\item allow to decide meaningful new literals in \rulename{Decide} based on the current domain (other than splits),
	\item integrate other propagation schemes beyond ICP, for example taking into account the Boolean structure of the problem, or
	\item be applied to any other theory that is beyond ICP.
\end{enumerate*}
Note that we have not implemented any of this yet, and thus any claims about the benefits and possible improvements are only (more or less informed) guesses.
