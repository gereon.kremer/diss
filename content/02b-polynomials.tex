\section{Polynomials}\label{sec:preliminaries:polynomials}

Most of this thesis centers around techniques for nonlinear real arithmetic and thus needs to work on polynomials.
In the following, we give a brief introduction to polynomials and some non-trivial methods and properties.

\begin{definition}{Variables}{variables}\silentdefindex{variable}
	We denote \emph{variables} by $x_1, \dots, x_n$ and associate every variable $x_i$ with a \emph{domain} $D_{x_i}$. If no domain is given or clear from the context, we assume the domain $\R$.
\end{definition}

In many of the following ideas and algorithms, we impose a total order on variables. We usually assume the canonical order as defined below for all examples, but could plug in any arbitrary order. The importance of selecting a \emph{good order} is discussed later, for example, in \cref{ssec:cadSMT:heuristics:varorder}.

\begin{definition}{Variable order}{variableorder}\silentdefindex{variable-order}
	A \emph{variable order} is a \emph{total order} on a set of variables $\overline{x} = \{ x_1, \dots, x_n \}$. We use $x_1 < \cdots < x_n$ as the \emph{canonical order}.
\end{definition}

We usually aim to associate every variable to a value from its domain so that this \emph{variable assignment} satisfies our formula.
We call such a mapping a \emph{model}, following common naming in mathematical logic.

\begin{definition}{Variable assignment}{variable-assignment}\silentdefindex{variable-assignment}
	Let $\overline{x} = \{ x_1, \dots, x_n \}$ be variables with domains $D_1, \dots, D_n$.
	We call 
	\[
		\A_{\overline{x}} = \{ x_1 \mapsto d_1, \dots, x_n \mapsto d_n \} \textrm{ with } d_i \in D_i \textrm{ for } i \in \{1, \dots, n\}
	\]
	a \emph{variable assignment for $\overline{x}$}, denote the \emph{(set of all) variable assignments of $\overline{x}$} by
	\[
		\mathbfcal{A}_{\overline{x}} = \{ \A_{\overline{x}} \mid d_1 \in D_1, \dots, d_n \in D_n \}
	\]
	and write $\A$ and $\mathbfcal{A}$ if the set of variables is clear from the context.
	To retrieve a variable assignment for an individual variable from $\A$, we commonly write $(x_i \mapsto d_i) \in \A$ or $\A(x_i) = d_i$.
\end{definition}

As the domains of our variables are usually the real numbers -- and all other domains from \cref{def:numbers} can be embedded canonically into the real numbers -- we oftentimes interpret a variable assignment for variables $\overline{x}$ as an element of $\R^n$, assuming some variable ordering.
Thus, we can \emph{identify} a variable assignment with a point in the $n$-dimensional real space.

The fundamental operational objects for most of the methods we present here are polynomials. We define polynomials over arbitrary rings, but already note that their coefficients come from $\Q$ for most parts of this work.
We observe that most of these methods are almost exclusively interested in the roots of polynomials, and we can thus go from coefficients from $\Q$ to coefficients from $\Z$ without loss of generality (though we acknowledge that the \emph{polynomial rings} over $\Q$ and $\Z$ are very different in nature).
An example for this is given in \cref{ex:simplify-polynomials}.

\begin{definition}{Polynomials}{polynomials}\silentdefindex{polynomial}
	Let $R$ be some ring and $x$ a variable.
	A \emph{polynomial} with $x$ over $R$ has the form
	\[
		p = \sum_{i=0}^n c_i \cdot x^i
	\]
	where $\overline{c} \in R^n$ are \emph{coefficients} from $R$. We define the \emph{degree $\Degree(p) = n$} for $p \neq 0$ and $\Degree(0) = -\infty$ and the \emph{coefficients $\Coeffs(p) = \{ c_i \mid 0 \leq i \leq n \}$} as usual.
	We call $p$ \emph{(affine) linear} if $\Degree(p) = 1$ and \emph{constant} if $\Degree(p) \leq 0$.
	We write $R[x]$ for the \emph{set of all polynomials} with variable $x$ over $R$ and call it \emph{the polynomial ring with $x$ over $R$}. 

	We oftentimes identify a polynomial $p$ with its polynomial function $p: D_x \rightarrow D_x, x \mapsto p(x)$ (assuming $R \subseteq D_x$) to \emph{evaluate} $p$ at some point $x \mapsto \alpha$ with $\alpha \in D_x$ and write $p(\alpha)$.
\end{definition}

If $R$ is a set of numbers -- usually $\Q$ or $\Z$ -- we call polynomials from $R[x]$ \emph{univariate}.
However, $R$ can itself be a polynomial ring as well, allowing for polynomials over multiple variables that we call \emph{multivariate}.
We usually denote such a polynomial ring by $R[\overline{x}]$ where $R$ is a set of numbers and $\overline{x}$ is some set of variables.
Note that the order of the variables is not relevant here, and we can thus give a standard form for multivariate polynomials that is not based on a recursive application of \cref{def:polynomials}, but uses all variables from $\overline{x}$ directly as given in the following \cref{def:multivariate-polynomials}.

\begin{definition}{Multivariate polynomials}{multivariate-polynomials}\silentdefindex{polynomial-multivariate}
	Let $R$ be some ring and $\overline{x} = \{ x_1, \dots, x_n \}$ variables.
	A \emph{multivariate polynomial} with $\overline{x}$ over $R$ has the form
	\[
		p = \sum_i c_i \cdot \prod_j x_j^{e^i_j}
	\]
	where $e^i_j \in \N$ are \emph{exponents} and $c_i \in R$ \emph{coefficients}.
	We write $R[\overline{x}]$ for the \emph{set of all polynomials} with variables $\overline{x}$ over $R$ and call it \emph{the polynomial ring $R[\overline{x}]$}.
	We call the summands the \emph{terms of $p$}, $\Coeffs(p) = \overline{c}$ the \emph{coefficients of $p$}, and $\overline{e^i}$ the \emph{exponent vectors} (for every term).
	We assume that the terms are ordered according to a variable ordering on $\overline{x}$, or rather a derived term ordering, and usually use the \emph{degree reverse lexicographical} ordering.
	Under such an ordering we define the \emph{leading term $\Lterm(p)$}, \emph{leading coefficient $\Lcoeff(p)$}, and \emph{trailing coefficient $\Tcoeff(p)$}, as well as the \emph{total degree $\TDegree(p)$}.
\end{definition}

We note that we can directly convert polynomials between the representations given in \cref{def:polynomials} and \cref{def:multivariate-polynomials}. We consider these the \emph{canonical representations} for univariate and multivariate polynomials, respectively.
It is sometimes convenient -- in particular in the context of CAD -- to \emph{univariately represent} a multivariate polynomial.

\begin{definition}{Univariately represented polynomial}{univariately-represented-polynomial}\silentdefindex{polynomial-univariate}
	Let $p \in R[\overline{x}]$ be a multivariate polynomial and $x_i \in \overline{x}$.
	We say that $p$ is \emph{univariately represented}, denoting $p$ as specified in \cref{def:polynomials} and its coefficients from $R[\overline{x} \setminus \{x_i\}]$ in any representation, usually as in \cref{def:multivariate-polynomials}.
	We call $x_i$ \emph{the main variable of $p$}.
	We usually assume that $x_i$ is the \emph{largest} variable among $\overline{x}$ (under the variable ordering) and say that the \emph{level of $p$ is $|\overline{x}|$} and write $level(p) = |\overline{x}|$.
	If $p \in R$ (that is $\overline{x} = \emptyset$) we define $level(p) = 0$.
\end{definition}

Note that we have defined multiple ``canonical'' representations for polynomials, and there are even more ways to write polynomials that are all meaningful in their own way -- all of which induce the same polynomial function and allow to convert arbitrarily from one to another.
For example, $x_1^2 - 2 x_1 x_2 - 3 x_2^2$ (as a \emph{multivariate polynomial} in $\Z[x_1,x_2]$, or \emph{distributive representation}) can also be written as $(1) \cdot x_1^2 + (-2 x_2) \cdot x_1^1 + (-3 x_2^2) \cdot x_1^0$ (as a \emph{univariate polynomial} in $\Z[x_2][x_1]$, or \emph{recursive representation}) or even as $(x_1 - 3 x_2) \cdot (x_1 + x_2)$. These different notations allow to easily obtain different information, for example, the (total) degree or the constant part of the polynomial, the structure with respect to one ``main'' variable or polynomial factors.

We recognize that different representations have different trade-offs in practice and choosing a suitable representation can have a huge impact on real-world performance.
Though the choice of representation is sometimes crucial for performance and the conversion between different representations is not always computationally easy, we ignore this issue for the most part of this work.

We are oftentimes interested in a \emph{factorization} of a polynomial $p$, usually over $R[\overline{x}]$.
As a thorough introduction into this topic is beyond the scope of this work, we use notions like the \emph{factorization $p$}, \emph{irreducible normalized factors of $p$} ($\Factors(p)$), $p$ being \emph{square-free} and the \emph{square-free part of $p$}, the \emph{content of $p$} ($\Content(p)$) and the \emph{primitive part of $p$} ($\Primitive(p)$), the \emph{(finest) square-free basis} and the \emph{(finest) irreducible basis} of sets of polynomials without definition and refer to any of~\cite{Artin1991,Geddes1992}.
Another important property of polynomials (at least in the context of this work) are their \emph{real roots}, intuitively the variable assignments that make a polynomial evaluate to zero.

\begin{definition}{Real roots}{real-roots}\silentdefindex{real-roots}
	Let $p \in R[\overline{x}]$ be a polynomial.
	We call $\Roots(p) \coloneqq \{ \alpha \in \R^n \mid p(\alpha) = 0 \}$ the \emph{real roots of $p$}.
	If $p \neq 0$ is univariate we observe that $|\Roots(p)| \leq \Degree(p)$.
\end{definition}

In fact, we are solely interested in the \emph{real roots} of polynomials in most parts of this work.
This allows us to not only rewrite polynomials but actually simplify them by normalizing the coefficients or removing multiple factors and thereby multiple roots as we show in the following \cref{ex:simplify-polynomials}.

\begin{example}{Simplifying polynomials}{simplify-polynomials}
	Let $p = -4 x^3 + 12 x - 8$ be a polynomial and we assume that we are only interested in the real roots of $p$.
	The following derivations show how we can use coefficient normalization and the removal of multiple factors to safely replace this polynomial by $x^2+x-2$:
	\begin{align*}
		&& -4 x^3 + 12x - 8 &= -4 \cdot (x^3-3x+2) \\
		& \implies & \Roots(-4 x^3 + 12x - 8) &= \Roots(x^3-3x+2) \\
		&& x^3-3x+2 &= {(x-1)}^2 \cdot (x+2) \\
		& \implies & \Roots(x^3-3x+2) &= \Roots((x-1) \cdot (x+2)) \\
		&& (x-1) \cdot (x+2) &= x^2+x-2
	\end{align*}
\end{example}
	
We finally define some operations on polynomials that are used, in particular, for the projection operators in \cref{sec:cad:projection-operators}: \emph{reducta}, \emph{resultants}, and \emph{discriminants}.

\begin{definition}{Reducta and reducta sets}{reducata-sets}\silentdefindex{reducta}
	Let $P \subseteq R[x]$ be a set of polynomials and $p \in P$.
	We define
	\begin{align*}
		red(p) \coloneqq& p - \Lterm(p) \\
		red^0(p) \coloneqq& p \\
		red^k(p) \coloneqq& red(red^{k-1}(p)) \\
		RED(p) \coloneqq& \{ red^k(p) \mid 0 \leq k \leq \Degree(p) \} \\
		RED(P) \coloneqq& \bigcup_{p \in P} RED(p)
	\end{align*}
	We call $red(p)$ the \emph{reductum of $p$}, $red^k(p)$ the \emph{k'th reductum of $p$}, $RED(p)$ the \emph{reducta set of $p$}, and $RED(P)$ the \emph{reducta set of $P$}.
\end{definition}

All further components of the projection operators that we define later rest on \emph{resultants} and various related (or rather derived) concepts: \emph{subresultants}, \emph{principal (sub-)resultant coefficients}, and \emph{discriminants}.
We only give brief definitions and refer to relevant literature on this topic like~\cite{Basu2010} or~\cite{Geddes1992}.

\begin{definition}{Resultant}{resultant}\silentdefindex{resultant}
	Let $p,q \in R[x]$ be univariately represented polynomials with $\Degree(p) = n$ and $\Degree(q) = m$. The \emph{resultant} of $p$ and $q$ is the determinant of the Sylvester matrix:
	\[
		\Res_x(p, q) = Det \begin{pmatrix}
			p_n & \multicolumn{2}{c}{\cdots} & p_0 & & & \\
			& \ddots & & & \ddots & & \\
			& & \ddots & & & \ddots & \\
			& & & p_n & \multicolumn{2}{c}{\cdots} & p_0 \\
			q_m & \cdots & q_0 & & & & \\
			& \ddots & & \ddots & & & \\
			& & \ddots & & \ddots & & \\
			& & & \ddots & & \ddots & \\
			& & & & q_m & \cdots & q_0 \\
		\end{pmatrix}.
	\]
	If $p$ and $q$ are univariate, or the main variable $x$ is clear from the context, we usually write $\Res(p,q)$ instead of $\Res_x(p,q)$.
\end{definition}

Note that other variants of the Sylvester matrix exist, mostly obtained by permutations of rows and columns, which sometimes have certain computational benefits.
Another type of matrix is the \emph{Bézout matrix}, whose determinant also turns out to be the resultant of two polynomials.
The Sylvester matrix also allows to compute \emph{subresultants} as the determinants of certain submatrixes.

In practice, however, resultants and subresultants are usually not computed using determinants, but using \emph{subresultant pseudo-remainder sequences}.
These sequences allow to iteratively compute all subresultants and ultimately the resultant, which is commonly considered the $0$th element of this sequence.
Our implementation, for example, uses a variant of what is presented in~\cite{Ducos2000}.

In contrast to the definition of resultants, we usually compute a simplified version of the resultant. As we are only interested in the roots of the resultant, we can safely make it square-free and remove any constant (or non-vanishing) factors.
We also use these simplifications whenever we use resultants throughout this work, hence the polynomials that result from resultant computations are not necessarily identical to the resultants one computes with other systems like Maple.

As the definition of subresultants and principal subresultant coefficients is very technical, we refer to~\cite{Geddes1992,Ducos2000} for exact characterizations and denote the set of all \emph{principal subresultant coefficients} of two polynomials $p,q$ by $PSC(p,q)$ without definition.

Apart from this rather technical and unintuitive definition, a very instructive characterization for resultants is as follows.
Let $p,q$ be polynomials of degree $d$ and $e$ with roots $\alpha_i$ and $\beta_j$ respectively, then we have
\[
	\Res(p,q) = {\Lcoeff(p)}^e \cdot {\Lcoeff(q)}^d \cdot \prod_{\substack{1 \leq i \leq d \\ 1 \leq j \leq e}} (\alpha_i - \beta_j)
\]
in an algebraic closure of the according polynomial ring, as the resulting polynomial might not factor into linear factors in the polynomial ring itself. 

If we assume that $p$ and $q$ are univariate polynomials, then $\alpha_i$ and $\beta_j$ are algebraic numbers and the resultant is zero if and only if $p$ and $q$ have a common root.
If we instead let $p$ and $q$ be multivariate polynomials in some main variable, $\alpha_i$ and $\beta_j$ themselves contain the remaining variables, that one could consider them \emph{parameters}. Now, the resultant has a root -- in the remaining variables -- wherever any $\alpha_i$ and $\beta_j$ coincide (or \emph{meet}).
Hence the resultant can be seen as an indicator for intersections of the root surfaces of multivariate polynomials.

While resultants provide indicators for common roots of two polynomials, we are also sometimes interested in multiple roots of a single polynomial.
Multiple roots can be recovered using the (normalized) resultant of a polynomial and its derivative which is commonly called the \emph{discriminant}.

\begin{definition}{Discriminant}{discriminant}\silentdefindex{discriminant}
	Let $p \in R[x]$ be a univariately represented polynomial with $\Degree(p) = n$. The \emph{discriminant} of $p$ is defined as follows in terms of the resultant of $p$ and its partial derivative with respect to $x$:
	\[
		\Disc_x(p) = {(-1)}^{\frac{n \cdot (n-1)}{2}} \cdot \frac{\Res_x(p, p')}{\Lcoeff(p)}
	\]
\end{definition}
