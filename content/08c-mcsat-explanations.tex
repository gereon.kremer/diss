
\section{Explanation functions}\label{sec:mcsat:explanations}

\silentindex{mcsat:explanation-function}
\MCSAT itself is theory-agnostic but requires an explanation function as described in \cref{sec:mcsat:explanation-functions} as an important theory reasoning component.
We now describe several different explanation methods (for arithmetic theories) that we have implemented.
As already noted, some explanation functions are \emph{incomplete} (for example, restricted to linear constraints) and we let such explanation functions return $\bot$ if they are unable to construct an explanation.
Recall that we required an assignment finder to produce a set of conflicting constraints in case of infeasibility, and thus we assume any explanation function to work on a \emph{minimal infeasible} set of constraints.

\subsection{CAD-based explanations\label{ssec:mcsatimpl:explanation:cad}}

\silentdefindex{mcsat:explanation:cad}
The first explanation method, that was originally proposed in~\cite{Jovanovic2012}, is based on the idea to construct a single CAD cell around the current (partial) model.
An example of such a single CAD cell is depicted in \cref{ex:mcsat:single-cad-cell}.

\begin{figure}
	\begin{center}
		\includetikz{08c-cad-cell}
	\end{center}

	\caption{Example of a single CAD cell}\label{fig:mcsat:single-cad-cell}
\end{figure}

\begin{example}{A single CAD cell}{mcsat:single-cad-cell}
	Consider the CAD cell depicted in \cref{fig:mcsat:single-cad-cell}.
	The first variable $x_1$ is bounded by constants ($0 < x_1 < 1$) illustrated by the bold line at the bottom. In the second dimension, $x_2$ is bounded by polynomials in terms of $x_1$ ($x_1^3 - 2x_1^2 < x_2 < 2 - x_1^2$) and the resulting two-dimensional CAD cell is shown by the blue area.
	
	In the third dimension, $x_3$ is bounded by polynomials in $x_1$ and $x_2$ -- here we have $0 < x_3 < 2 - 0.2 x_1^2 - 0.2 x_2^2$. The lower bound $0$ is illustrated by the yellow plane while the red surface represents the upper bound. The full three-dimensional CAD cell is thus the space between the yellow and red surfaces.
	
	Note how the projection of the higher-dimensional cells onto the lower dimensions are identical to the lower-dimensional cells.
	A possible model within this cell would be $(0.5, 0.25, 1)$, indicated by the black dot.
\end{example}
	
To generate such a CAD cell, we use a somewhat reduced projection and then only explore the lifting around the partial model to obtain the borders of the cell that contains the partial model. An algorithmic description is given in \cref{algo:mcsat:explanation:nlsat}.

\begin{algorithm}
	\caption{CAD-based explanation function}\label{algo:mcsat:explanation:nlsat}

	\SetKwFunction{ExplainNLSAT}{Explain$_{CAD}$}
	\Fn{\ExplainNLSAT{$M$}}{
		$\alpha \coloneqq $ model from $M$ \;
		$L \coloneqq$ literals that cause the infeasibility of $M$ \;
		$P \coloneqq$ model based projection of polynomials from $L$ \;
		$C \coloneqq \true$ \;
		\For{$k = 1, 2, \dots$}{
			$Z \coloneqq$ real roots of $P_k[\alpha_1, \dots, \alpha_{k-1}]$ \;
			\If{$\alpha_k \in Z$}{
				$C \coloneqq (C \land x_k = \alpha_k)$ \nllabel{algo:mcsat:explanation:nlsat:eq} \;
			}
			\Else{
				$l,u \coloneqq$ closest roots from $Z$ below and above $\alpha_k$ \;
				$C \coloneqq (C \land l < x_k \land x_k < u)$ \nllabel{algo:mcsat:explanation:nlsat:ineq} \;
			}
		}
		\Return{
			$L \implies \neg C$
		}
	}
\end{algorithm}

Note that the constraints constructed in \cref{algo:mcsat:explanation:nlsat:eq,algo:mcsat:explanation:nlsat:ineq} are denoted as regular constraints, comparing $x_k$ with the root of some polynomial. However, we actually need to construct \emph{extended polynomial constraints} as defined in \cref{ssec:preliminaries:fol:extensions} from the respective roots, and we only use the above notation to make the algorithm more concise.

In~\cite{Jovanovic2012}, an adaptation of \index{projection-collins-third} is presented that makes use of the partial model to avoid certain projection factors.
We instead decided to employ \index{projection-lazard} here (in combination with the modified lifting) and exploit the partial model in the spirit of~\cite{Brown2015}, that is we only compute resultants that involve those polynomials that yield the closest bounds $l$ and $u$.

Note that we initially used \index{projection-mccallum} instead, but occasionally observed incorrect explanations (excluding a region that was too large) witnessing the incompleteness of this projection operator -- something we did not observe in the wild for regular SMT-style theory solving.
Before switching to \index{projection-lazard}, we used \index{projection-hong} to obtain a sound explanation, which proved to be not significantly worse in terms of the overall solver performance.

This explanation function satisfies the finite-basis property following \cref{thm:mcsat:finite-basis-by-qe}, as it only ever introduces new constraints whose polynomials are from the projection of the input constraints.
If the variable order is never changed, all new constraints ever introduced stem from the (finite) full projection of all input polynomials.

\subsection{OneCell explanations\label{ssec:mcsatimpl:explanation:onecell}}

\silentdefindex{mcsat:explanation:onecell}
Shortly after~\cite{Jovanovic2012} motivated the need to efficiently construct individual CAD cells, another possibility we call \emph{OneCell} was proposed.
While the first version in~\cite{Brown2013} only considered the case of \emph{open cells}, all corner cases are covered in~\cite{Brown2015}.
Though the fundamental idea of restricting the projection using the knowledge of the partial model is still similar to the previous approach, \emph{OneCell} rather resembles a depth-first search traversing the projection polynomials. In many cases, OneCell needs to consider significantly less polynomials and thus yields larger cells.

We refrain from a more detailed explanation here and refer to~\cite{Brown2013} for a somewhat gentle introduction and~\cite{Brown2015} for a full technical description of the method.
Our own implementation -- being the first complete implementation of~\cite{Brown2015} to the best of our knowledge -- is described in~\cite{Neuss2018}.

We can use the same argument for termination as for the general CAD-based explanation function: as all new constraints are based on polynomials from the full projection of the input polynomials the finite-basis property holds, following \cref{thm:mcsat:finite-basis-by-qe}.

\subsection{Fourier--Motzkin variable elimination\label{ssec:mcsat:Fourier-Motzkin}}

\silentdefindex{mcsat:explanation:fm}
Practical experience shows that we can identify a linear conflict in many cases, even if the overall input is nonlinear. In such cases, we might want to use a method suited for linear problems instead of calling a rather time-consuming method based on CAD.
The somewhat obvious choice -- that is also suggested in~\cite{Moura2013} and used in~\cite{Jovanovic2013} -- is the Fourier--Motzkin variable elimination.
While we assume a fundamental knowledge of this method in general, we feel that it is work noting a few interesting issues.

Analogously to the argument for CAD-based explanation functions, the finite-basis property is once again satisfied due to \cref{thm:mcsat:finite-basis-by-qe} as all constraints stem from the set of constraints obtained by performing Fourier--Motzkin variable elimination, as long as the variable ordering remains unchanged.

\subsubsection{Nonlinear constraints}
The Fourier--Motzkin approach is not technically constrained to linear problems, but only to those where the variable that is to be eliminated only occurs linearly.
This allows to extend it to an interesting subclass of nonlinear arithmetic where we can compute explanations without relying on more expensive methods like CAD or VS.

Considering two bounds $q_1 < p_1 \cdot x$ and $p_2 \cdot x < q_2$ we can eliminate $x$, resulting in $q_1 \cdot p_2 < q_2 \cdot p_1$.
Additionally, we need to make sure that $p_1$ and $p_2$ do not change their sign by adding side conditions like $p_1 < 0$ and $p_2 > 0$.
This comparably simple extension of Fourier--Motzkin variable elimination covers many cases where the variable that could not be assigned is only used linearly within nonlinear constraints.

Note that there are two possible improvements:
firstly, generating an explanation with Fourier--Motzkin should be significantly faster compared to a VS-based or CAD-based method;
secondly, the explanations due to Fourier--Motzkin tend to cover larger regions and thus possibly reduce the overall number of theory conflicts we need to process.

\subsubsection{Disequality constraints}

The Fourier--Motzkin variable elimination, in general, only works on sets of linear inequalities.
While equalities can easily be converted to two weak inequalities, a disequality essentially introduces a case split and can thus not be dealt with.
The previously suggested approach from~\cite{Jovanovic2013} introduces an explicit case split that is lifted to the Boolean reasoning engine.
Though this solution looks nice and clean at first sight, it has a significant drawback: it does not \emph{enforce} a Boolean decision but rather ``offers'' it to the SAT solver which may very well ignore it.

We instead propose another (a bit more technical) variant.
Let us inspect the \emph{covered region} of a constraint, that is the part of the real line that \emph{conflicts} with this constraint.
The (minimal) set of conflicting constraints has an interesting property:
given that the variable we consider only occurs linearly, every inequality constraint covers a region that is bounded on one side but unbounded on the other. Equality constraints, on the other hand, exclude everything but a single point and disequality constraints exclude only a single point.
Thus a conflict can only have one of two forms as \cref{fig:mcsat:linear-conflicts} illustrates:
\begin{enumerate*}
	\item two regions overlap or
	\item two regions with strict bounds meet (possibly an equality) and the remaining point interval is excluded by a disequality.
\end{enumerate*}

\begin{figure}
	\begin{center}
		\includetikz{08c-fm-conflicts}
	\end{center}
	\vspace*{-1em}
	\caption{Possible linear conflicts}\label{fig:mcsat:linear-conflicts}
\end{figure}

If the two inequalities stem from an equality -- we have $x \neq p$ and $x = q$ where $p$ and $q$ evaluate to the same value under the theory model -- we propose to use the explanation $(x \neq p \land x = q) \implies p \neq q$.
Otherwise -- with $x \neq p$ and two weak inequalities $x \leq q_1$ and $x \geq q_2$ where $p$, $q_1$, and $q_2$ all evaluate to the same value under the theory model -- we construct the explanation $(x \neq p \land x \leq q_1 \land x \geq q_2 \land q_1 = q_2) \implies q_1 \neq p$.
In both cases, we push the conflict to a lower theory level, forcing the solver to backtrack the previous theory decision or the assignment of one of the constraints.

\subsection{Virtual substitution\label{ssec:mcsat:virtual-substitution}}

\silentdefindex{mcsat:explanation:vs}
Another possibility that is in some sense between Fourier--Motzkin for linear constraints and CAD for nonlinear constraints is the virtual substitution.
Virtual substitution is a quantifier elimination method for constraints of bounded degrees. The common virtual substitution is based on the existence of solution formulae and thus only works up to degree four~\cite{Abel1826}, though implementations tend to provide only support for up to degree two due to the complexity of the solution formulae of higher degree.

Virtual substitution can also be adapted to be used as an explanation function as shown in~\cite{Abraham2017} and~\cite{Nalbach2017}. Our implementation fully supports constraints up to degree two and allows for (limited) cooperation with CAD-based explanation functions in that it supports root expressions of low degree.
Once again, this approach satisfies the finite-basis property due to \cref{thm:mcsat:finite-basis-by-qe}.

Note that variants of the virtual substitution exist that employ a symbolical representation of polynomial roots. Thereby, the need for an explicit solution formula is avoided and the method can be extended to arbitrary -- but fixed -- degrees. More on this topic can be found in~\cite{Kosta2015} and~\cite{Kosta2016}, though this is not used in our implementation.

\subsection{Interval constraint propagation}

\silentdefindex{mcsat:explanation:icp}
We have already mentioned \emph{interval constraint propagation} (ICP) as a generic method for solving constraint systems using domain propagation by interval arithmetic.
Though it is not a quantifier elimination method, we can adapt it for an explanation function anyway and use it as an example of how to employ regular SMT-style theory solvers.

For this, we depart from the notion of \emph{constructing new constraints} that describe a cell and rather \emph{return} to the regular notion of SMT-style theory solving. We simply take all constraints from the trail and hope that they yield a conflict \emph{independent of the theory model}.
If this is the case, we argue that obtaining a conflict consisting solely of existing constraints is in general preferable to an explanation with new constraints.

Our reasoning is that introducing new literals grows the Boolean search space and should, therefore, be avoided, if possible. If we can instead resolve the current conflict by purely Boolean reasoning, we should do so.
We acknowledge that this goes against the idea of \MCSAT to some degree, but we rather see it as a supplementary solving technique, in some sense running \MCSAT explanations and regular SMT-style theory solving in parallel.

\begin{algorithm}
	\caption{ICP-based explanation function}\label{algo:mcsat:explanation:icp}
	
	\SetKwFunction{ExplainICP}{Explain$_{ICP}$}
	\Fn{\ExplainICP{$M$}}{
		$\alpha \coloneqq $ model from $M$ \;
		$I_v \coloneqq (-\infty,\infty)$ for every variable $v$ \;
		$Q \coloneqq \{ (c, 1) \textrm{ for every constraint }c \}$ \;
		\While{\true}{
			\If{$Q = \emptyset$}{
				\Return{$\bot$}
			}
			\If{$I_v = \emptyset$ for some $v$}{
				\Return{all $c$ that contributed to $I_v = \emptyset$}
			}
			\If{$\alpha_v \not\in I_v$ for some $v$}{
				Let $e$ be a constraint that separates $\alpha_v$ from $I_v$ \;
				\Return{$e$ and all $c$ that contributed to $\alpha_v \not\in I_v$}
			}
			\If{$|I_v| < threshold$ for some $v$}{
				\Return{$\bot$}
			}
			Remove some $(c,priority)$ from $Q$ \;
			\If{$priority \geq threshold$}{
				Use $c$ to contract some $I_v$ \;
				Update $priority$ accordingly \;
				Insert $(c, priority)$ into $Q$ \;
			}
		}
	}
\end{algorithm}

We chose ICP for this experiment as we can easily integrate limited reasoning specific to the theory model in addition to regular SMT-style theory solving. In particular, we do not need to determine infeasibility but can also check easily whether the theory model has been excluded.
Furthermore, we hope that its ability to reason across all variables, independent of any variable ordering, may be a valuable addition to the other explanation functions that are heavily oriented towards a common variable ordering.

For ICP, in particular, we collect all constraints from the trail and run standard propagations in the spirit of~\cite{Benhamou2006,Schupp2013} until we either \emph{exclude the current theory model} or find \emph{infeasibility} of the whole constraint set, roughly as shown in \cref{algo:mcsat:explanation:icp}.

The experimental results we present in the following show that interval constraint propagation is a valuable addition to the other explanation functions which are mostly based on algebraic methods.

Note that interval constraint propagation is notorious for its parameters and their intricate intra-dependencies, and we did not spend a lot of effort on tuning them for this purpose yet.
In particular, we rely on thresholds for the size of the intervals and the priorities of the constraints for termination, but also need to properly update these priorities and select which variable should be updated by a specific constraint.

We attribute the improvements in practice mostly to the ability to argue independently of the variable ordering, considering not yet assigned variables as well.
As we consider this a fundamental flaw -- or rather a potential for improvement -- in \MCSAT, we propose an alternative, more fundamental, integration of interval constraint propagation into \MCSAT in \cref{sec:mcsat:mrsat} that we think is even more promising.

\subsection{Composition of explanation functions}

\silentdefindex{mcsat:explanation:composition}
Following the spirit of~\cite{Corzilius2015}, we aim to combine different solving techniques in the hope to employ the best method for every individual task.
We propose two possible compositions of multiple explanation functions detailed in the following.

\begin{definition}{Sequential composition}{mcsat:explanation:sequential}
	Let $E_1, \dots, E_k$ be explanation functions and allow all but $E_k$ to fail under certain conditions. Let $E$ be a new explanation function defined as follows:
	\[
		E(M) \coloneqq E_i(M) \textrm{ with } i = \min \{ i \mid E_i(M) \textrm{ does not fail } \}
	\]
	We call $E$ the \emph{sequential composition} of $E_1, \dots, E_k$.
\end{definition}

\emph{Sequential composition} can be used for multiple explanations that have a clear priority. For example, we might want to try Fourier--Motzkin first, continue with virtual substitution if nonlinear constraints are present, and finally resort to a CAD-based explanation if high degrees are present.

\begin{definition}{Parallel composition}{mcsat:explanation:parallel}
	Let $E_1, \dots, E_k$ be explanation functions and $E$ a new explanation function defined as follows:
	\[
		E(M) \coloneqq E_i(M) \textrm{ with } i = \argmin_i \{ \textrm{run time of } E_i(M) \}
	\]
	We call $E$ the \emph{parallel composition} of $E_1, \dots, E_k$.
\end{definition}

\emph{Parallel composition} aims to use the fastest result in a multi-threaded setting. We could, for example, run multiple versions of the Fourier--Motzkin explanation with different selections of constraints. Note that this composition can easily be changed to use the \emph{simplest} explanation (instead of the fastest).

Under certain conditions, the composition of multiple explanation functions preserves the finite-basis property -- given that the variable order remains unchanged. We formalize this in the following \cref{thm:mcsat:finite-basis-composition} based on \cref{thm:mcsat:finite-basis-by-qe}.

\begin{theorem}{Finite-basis property of compositions}{mcsat:finite-basis-composition}\silentindex{mcsat:finite-basis-property}
	Let $E_1, \dots, E_k$ be explanation functions that satisfy the finite-basis property for the reasons described in \cref{thm:mcsat:finite-basis-by-qe} with \emph{equivalent variable orderings} and $E$ their (sequential or parallel) composition.
	Then, $E$ also satisfies the finite-basis property for a static variable order.
\end{theorem}
\begin{proof}
	We resume the proof of \cref{thm:mcsat:finite-basis-by-qe} and change the iterative application of the explanation function as follows:
	instead of applying a single explanation function, we apply all explanation functions $E_1, \dots E_k$ in parallel and afterwards conjoin their results.
	By the same reasoning as before, the combined explanation function satisfies the finite-basis property.
\end{proof}