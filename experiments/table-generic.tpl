\begin{tabularx}{\linewidth}{ {{- tabular_spec -}} }
	\toprule
{% for h in head -%}
	\textbf{ {{- h -}} } {% if not loop.last %}& {% endif %}
{%- endfor -%}
	\\
	\midrule
{%- for d in data %}
	{% for c in d -%}
	{{ c }} {% if not loop.last %}& {% endif %}
	{%- endfor -%}
	\\
{%- endfor %}
	\bottomrule
\end{tabularx}
