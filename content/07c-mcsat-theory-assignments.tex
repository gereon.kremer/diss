\section{Constructing theory assignments}\label{sec:mcsat:assignment-finding}

The first novel component of \MCSAT, compared to regular \CDCLT-style SMT solving, is the creation of theory assignments. Note that we do not allow to add \emph{any} theory assignment but require the resulting trail to be consistent -- the theory assignment should at least be consistent with the theory literals already present in the trail.
In general, we define an \emph{assignment finder} as follows:

\begin{definition}{Assignment finder}{assignment-finder}\silentdefindex{mcsat:assignment-finder}
	Let $M$ be an \MCSAT trail and $v$ a theory variable with domain $D$ such that $v$ is the smallest variable that is unassigned in $M$.
	We call a function
	\[
		f(M, v) \rightarrow D \cup \Powerset(M)
	\]
	an \emph{assignment finder} if the following holds:
	If $\Seq{M, v \rightarrow \alpha_v}$ is consistent for some $\alpha_v \in D$, such an $\alpha_v$ is returned.
	Otherwise some $P \in \Powerset(M)$ that is already infeasible over the partial model induced by $M$ is returned.
\end{definition}

We observe that \emph{simply guessing} some value -- for example by \emph{random sampling} -- may not be a good idea as we need to realize if no value exists. Instead, we need a way to ensure that we find a suitable value -- if one exists -- but can also detect if no suitable value exists with certainty.
In this case, the assignment finder should not only indicate infeasibility but provide a \emph{witness} $P$ which is very similar to an infeasible subset.

Though we discuss the practical implementation of such an assignment finder in more detail later, we give a brief idea of the fundamental concept here, at least for a minimalistic assignment finder for an arithmetic theory.

Let us assume that we want to assign $v$ -- all \emph{smaller} variables are already assigned and all \emph{larger} variables are still unassigned.
We observe that we only need to consider constraints that involve $v$ and -- possibly -- \emph{smaller} variables.
We can then (at least conceptually) substitute the model for the smaller variables into these constraints and obtain a \emph{univariate} problem.

Decomposing the possible values for $v$ into finitely many regions through real root isolation is then essentially a lifting step in CAD as discussed in \cref{sec:cad:lifting}.
Thus, many concepts we discussed in the context of CAD carry over as well, in particular, the notion of \emph{sign-invariant} regions and the idea that sample points are seen as \emph{representatives} of such sign-invariant regions.
In this sense, an assignment finder is looking for a satisfying region that can be used to construct a feasible assignment, or certifies infeasibility if no satisfying region exists.
