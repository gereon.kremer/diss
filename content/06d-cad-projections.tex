\section{Projection operators}\label{sec:cadSMT:projection-operators}

\silentindex{cad:projection}\silentindex{projection}
There exists quite a variety of different projection operators, and we have presented the most important ones in \cref{sec:cad:projection-operators}.
It is well-known that different projection operators produce sets of polynomials that differ significantly in their size for a traditional cylindrical algebraic decomposition, oftentimes making a huge difference in whether a particular problem can be solved in practice.
In general, a smaller set of polynomials not only reduces the computational effort in the projection itself but also yields less sample points that need to be computed.
We have given some results on the overall size of the projection in \cref{ssec:cad:projection:comparison}.

In our scenario, the same arguments hold in case of unsatisfiability -- as we need to generate a full CAD -- but whether satisfiable instances can benefit from this, in general, is not immediately obvious. While one might argue that a smaller projection is beneficial as we can exclude unsatisfiable cells faster, it may very well be better to have more polynomials of which some are ``easy'' if these easy polynomials are sufficient to heuristically guide the lifting process towards a satisfying cell.

\begin{table}
	\centering
	\input{experiments/output/table-cad-proj}
	\caption{Experimental results for different projection operators.}\label{tbl:cad:projection-operators}
\end{table}

Some further discussion, as well as experiments on the effects of the different projection operators for SMT solving in practice, can be found in~\cite{Viehmann2016,Viehmann2017}, though we need to caution that some implementation issues were discovered after the publication of~\cite{Viehmann2016} that affect the last section of the experimental results. An overall summary of the solver performance with varying projection operators is shown in \cref{tbl:cad:projection-operators}.

\begin{figure}
	\centering
	\begin{subfigure}{0.47\textwidth}
		\centering
		\includetikz{06d-scatter-cad-proj-1}
		\vspace*{-1em}
		\caption{\SolverCADProjHong vs. \SolverCADProjMcCallum}\label{fig:cad:proj-scatter-1}
	\end{subfigure}\hfill
	\begin{subfigure}{0.47\textwidth}
		\centering
		\includetikz{06d-scatter-cad-proj-3}
		\vspace*{-1em}
		\caption{\SolverCADProjMcCallum vs. \SolverCADProjBrown}\label{fig:cad:proj-scatter-3}
	\end{subfigure}

	\caption{Comparison of projection operators.}\label{fig:cad:projection-scatter}
\end{figure}

These results support the theory that the solver benefits from smaller projection sets, though the impact may not be as strong as expected.
A more detailed analysis in \cref{fig:cad:projection-scatter} shows no surprising behavior -- contrary to the first (erroneous) findings in~\cite[Chapter 6]{Viehmann2016}. We see that for the most part \SolverCADProjHong, \SolverCADProjMcCallum, and \SolverCADProjBrown solve the same problem instances in a similar amount of time. As always, a limited number of outliers exist that benefit from a larger projection set in some way.

Another issue we already discussed in \cref{sec:cad:projection-operators} is the problem of completeness, or rather incompleteness, of some projection operators.
Examples that provoke this behavior exist in the literature, but as we have already noted in~\cite{Viehmann2017}, this does not seem to be a real issue on our benchmark set.
Recall that using McCallum's or Brown's projection operators may lead to missing certain sample points and thereby determining unsatisfiability though a satisfying solution exists.

A key contribution of Lazard is not only its projection operator but also its modified lifting process we discussed in \cref{ssec:cad:lifting:lazard}.
Using this lifting process, McCallum's projection operator (as a superset of Lazard's projection operator, if the optimization from~\cite{Seidl2003} is not used) immediately becomes complete as well, and thus only Brown's projection operator is left as incomplete.

Note that we do not implement the ``mitigations'' discussed in \cref{ssec:cad:lifting:incompleteness} (for example adding delineating polynomials) but simply rely on Lazard's lifting procedure for all projection operators and accept the theoretical incompleteness of Brown's projection operator, verifying that it does not lead to incorrect results on our benchmark set.
