\begin{tabularx}{\linewidth}{S[table-format=4.1]@{\,}s[table-unit-alignment = left]XS}
	\toprule
	\multicolumn{2}{c}{\textbf{timeout}} && \textbf{solved} \\
	\midrule
{%- for d in data %}
	{{ "%4.1f"|format(d.0) }} & \second && {{ d.1 }} \\
{%- endfor %}
	\bottomrule
\end{tabularx}
