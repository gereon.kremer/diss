SRC=$(shell find . -type f -name '*.bib' -or -name '*.cls' -or -name '*.sty' -or -name '*.tex' -or -name '*.tikz')
# Attention: All changes must be made to \tikzexternalize[system call] in dissertation.cls as well!
PDFLATEX=TEXINPUTS=./style:${TEXINPUTS} lualatex --synctex=1 -interaction=nonstopmode -shell-escape -jobname=build/paper
PDFLATEXCOVER=TEXINPUTS=./style:${TEXINPUTS} lualatex --synctex=1 -interaction=nonstopmode -shell-escape -jobname=build/paper-cover
PDFLATEXSLIDES=TEXINPUTS=./style:${TEXINPUTS} lualatex --synctex=1 -interaction=nonstopmode -shell-escape -jobname=build/slides

.PHONY: all clean folders experiments missing

default: build/paper.pdf

all: build/paper-cover.pdf build/paper-online.pdf build/paper-print.pdf build/slides.pdf

clean:
	rm -rf build/ externalized/

experiments:
	make -C experiments/

folders:
	mkdir -p externalized/paper/ build/plots/paper/
	mkdir -p externalized/slides/ build/plots/slides/
	ln -s -r -f paper.tex build/paper.tex
	ln -s -r -f slides.tex build/slides.tex

build/paper.pdf: paper.tex ${SRC} folders
	-biber build/$(shell basename $< .tex)
	-bib2gls -g build/paper
	make -j4 -f build/paper.makefile
	$(PDFLATEX) $<

build/paper-cover.pdf: paper-cover.tex folders
	$(PDFLATEXCOVER) $<

build/paper-online.pdf: paper.tex ${SRC} folders
	sed -i '1 s/^.*$$/\\documentclass[online,publication]{paper}/' paper.tex
	$(PDFLATEX) $<
	-biber build/$(shell basename $< .tex)
	-bib2gls -g build/paper
	make -j4 -f build/paper.makefile
	$(PDFLATEX) $<
	$(PDFLATEX) $<
	mv build/paper.pdf build/paper-online.pdf

build/paper-print.pdf: paper.tex ${SRC} folders
	sed -i '1 s/^.*$$/\\documentclass[print,publication]{paper}/' paper.tex
	$(PDFLATEX) $<
	-biber build/$(shell basename $< .tex)
	-bib2gls -g build/paper
	make -j4 -f build/paper.makefile
	$(PDFLATEX) $<
	$(PDFLATEX) $<
	mv build/paper.pdf build/paper-print.pdf

build/slides.pdf: slides.tex ${SRC} folders
	$(PDFLATEXSLIDES) $<
	-biber build/$(shell basename $< .tex)
	make -j4 -f build/slides.makefile
	$(PDFLATEXSLIDES) $<
	$(PDFLATEXSLIDES) $<

missing:
	grep "Glossary entry" build/paper.log

sanity:
	grep -Inri -E "\\\[a-zA-Z]+ --" content/ || true
	grep -Inri "\\.\\.\\." content/ || true
	grep -Inri "a \\\MCSAT" content/ || true
	grep -Inri "\\\bar" content/ || true
	grep -Inri "non-linear" content/ || true
	grep -Inri "nonchronological" content/ || true
	grep -Inri "nonreachab" content/ || true
	grep -Inri "nonrepeat" content/ || true
	grep -Inri "nonunivariate" content/ || true
	grep -Inri "nonvanish" content/ || true
	grep -Inri "straight-forward" content/ || true
	grep -Inri "first order" content/ || true
	grep -Inri "formulas" content/ || true
	grep -Inr "[^\\]false" content/ || true
	grep -Inr "[^\\]true" content/ || true
	grep -Inr "[^\\]undef[^a-z,]" content/ || true
	grep -Inri ":=" content/ || true
	grep -Inri -E "\\\StateEQ[^{]" content/ || true
	grep -Inr "\\\Vdash" content/ || true
	grep -Inr "\\\exists" content/ || true
	grep -Inr "\\\forall" content/ || true
	grep -Inr "\\\subset[^a-z]" content/ || true
	grep -Inr "branch\\&bound" content/ || true
	grep -Inr "\\\rulename{elimination}" content/ || true
	grep -Inri "z3" content/ || true
	grep -Inri -E "first-order [^l]" content/ || true

erika-diff:
	meld ../diss-erika ../diss-erika-original .

erika-snapshot:
	rsync -avR --delete bibliography.bib paper.cls paper.tex Makefile content experiments/Makefile experiments/*.py experiments/*.tpl style  ../diss-erika/
	cp build/paper.pdf ../diss-erika/paper_`date +%m-%d`.pdf

detex:
	rm -rf detexed/
	mkdir -p detexed/
	for f in `ls content/`; do detex -c -r content/$$f > detexed/$$f.txt ; done
