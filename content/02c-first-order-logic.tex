\section{First-order logic}\label{sec:preliminaries:fol}

We now depart from the algebraic world of polynomials into the realm of logic. Within this work, the basic logical objects -- except for Boolean variables and constants -- are \emph{(arithmetic) constraints}, consisting of a polynomial and a \emph{sign condition}.

\begin{definition}{Sign condition}{sign-condition}\silentdefindex{sign-condition}
	We define a \emph{sign condition} to be a function from a \emph{sign} (negative, zero or positive) to a Boolean value, formally $\sigma: \{-,0,+\} \rightarrow \B$.
	Except for the trivial sign conditions $\cdot \mapsto \true$ and $\cdot \mapsto \false$, sign conditions are what we commonly know as \emph{relations} $\{<, \leq, =, \neq, \geq, >\}$.

	We usually write $p \signcondition 0$ instead of $\sigma(\sgn(p))$ -- where $\sgn: \R \rightarrow \{-,0,+\}$ denotes the \emph{sign function} -- to allude to the intuition of $\sigma$ being a relation.
\end{definition}

We observe that $p \signcondition p'$ can equivalently be rewritten to $p - p' \signcondition 0$ and thus this formalism of sign conditions can be used to define what we commonly know as \emph{polynomial (in\nobreakdash-)equalities}.
For what we call \emph{constraints}, we use a slightly more generic definition, allowing for other ``arithmetic expressions'' than polynomials.

\begin{definition}{Constraint}{constraint}\silentdefindex{constraint}
	Let $p$ be an arithmetic expression and $\sigma$ a sign condition.
	We call the pair $(p,\sigma)$ an \emph{(arithmetic) constraint} with the semantics of $p \signcondition 0$.
	We call $p$ its \emph{left-hand side} and $\sigma$ its \emph{relation}.
\end{definition}

Note that we can determine the truth value of a constraint in multiple ways. If its left-hand side is a numeric constant -- or we can evaluate it to one using a variable assignment -- we can simply compare it to zero.
If it is not, though, we can sometimes evaluate it anyway using further knowledge about the left-hand side: for example, we know that $x^2+1$ is strictly positive and thus $x^2+1>0$ is true.

We have only seen polynomials as arithmetic expressions that could be used as left-hand sides of constraints, however, we trust the reader to know further arithmetic functions like square roots, cosines, or logarithms.
While polynomials are sufficient for most of this work, we eventually introduce another variant in \cref{ssec:mcsatimpl:explanation:cad} that compares variables against the \emph{$k$th root of some polynomial}.

Based on constraints, we can finally construct more complex logical objects that we call \emph{formulae}.
Formulae are in some sense \emph{the language of mathematics}, allowing to concisely formulate statements and problems in a very general way.
We classify formulae into \emph{logics} with well-defined rules for syntax and semantics.
A large variety of logics exists for different (practical or theoretical) purposes.
While some logics consider only Boolean constructs -- Boolean variables and {Boolean connectives} like $\lor$ or $\land$ -- most involve some kind of \emph{theory $\T$}.
The theory adds \emph{predicates} or \emph{theory atoms} -- in our case constraints -- that in some way allow to map \emph{theory expressions} to the Boolean level.
Some logics add \emph{quantifiers} like $\exists$ or $\forall$ while sometimes more specialized operators are used (for example, in LTL).

This work mostly considers \emph{first-order logics}, which are the focus of \emph{satisfiability modulo theories} solving. Apart from the standard Boolean structure, it allows for theory atoms as specified before and quantification over the theory variables from these theory atoms.

\begin{definition}{First-order logic}{first-order-logic}\silentdefindex{logic:first-order}
	Let $\T$ be some theory with variables $\overline{x}$ and \emph{theory atoms} $\mathscr{A}$, $\mathcal{B}$ the set of Boolean variables, and $\B = \{ \false, \true \}$ the Boolean constants.
	A \emph{first-order logic formula} $\varphi$ is one of the following:
	\begin{align*}
		\varphi & \Coloneqq b \mid c \mid p & b \in \mathcal{B}, c \in \B, p \in \mathscr{A} \\
		\varphi & \Coloneqq \varphi \lor \varphi \mid \neg \varphi \\
		\varphi & \Coloneqq \Exists{x} \varphi &
	\end{align*}
	We call Boolean constants, Boolean variables, and theory atoms \emph{first-order logic atoms} and write $\Phi_\T$ for the set of all such first-order logic formulae.
\end{definition}

We note that $\lor$ and $\neg$ are what we call \emph{functionally complete} and can be used to construct all other Boolean connectives like $\land$ (and) and $\lxor$ (xor) as syntactic sugar. Similarly, $\forall x$ (universal quantification) can be constructed from $\exists x$ and $\neg$.

Throughout this work, we oftentimes manipulate or relate formulae and sometimes need to distinguish explicitly between \emph{syntactic} operations and \emph{semantic reasoning}.
We usually employ semantic reasoning to argue about the actual meaning of a formula instead of how we write it down as this allows us to pass over certain technical details.

\begin{definition}{Semantic deduction}{semantic-deduction}\silentdefindex{semantic-deduction}
	Let $\overline{x}$ be variables, $\varphi$ and $\varphi'$ formulae, and $\A$ an assignment over $\overline{x}$.
	If $\varphi$ evaluates to \true when $\overline{x}$ is substituted by $\A$, we say that \emph{$\A$ satisfies $\varphi$} and write $\A \models \varphi$.
	We say that $\varphi'$ \emph{follows from} $\varphi$, denoted by $\varphi \models \varphi'$, if
	\[
		\Forall{\A \in \mathbfcal{A}} \A \models \varphi \implies \A \models \varphi'.
	\]
\end{definition}

It is oftentimes convenient to assume some normal form for first-order logic formulae. 
Throughout this thesis -- unless stated otherwise -- we assume all first-order logic formulae to be in \emph{prenex normal form} and the quantifier-free part to be in \emph{conjunctive normal form} (which implies \emph{negation normal form}) and simply call them ``formulae''.

\begin{definition}{Prenex normal form}{prenex-normal-form}\silentdefindex{prenex-normal-form}
	A formula $\varphi$ is in \emph{prenex normal form} if it has the form
	\[
		\varphi \Coloneqq Q^1_{x_1} \dots Q^n_{x_n} \varphi'
	\]
	with quantifiers $Q^i \in \{ \exists, \forall \}$ and $\varphi'$ containing no quantifiers.
	We call $\varphi'$ the \emph{quantifier-free part of $\varphi$}.
\end{definition}

If $\varphi$ has multiple \emph{identical subsequent} quantifiers, we allow to combine them into a single \emph{quantifier block} and write $Q^1_{x_1, \dots, x_k} \varphi'$.
We observe that we can arbitrarily reorder variables within such a quantifier block without changing the semantics of $\varphi$.

A given formula can be transformed to prenex normal form by pushing all quantifiers towards the front of the formula, taking care that 
\begin{enumerate*}
	\item the relative order of quantifiers does not change,
	\item variables are renamed if they are referenced by multiple quantifiers, and
	\item quantifiers are properly changed if pushed over a negation.
\end{enumerate*}

\begin{definition}{Negation normal form}{negation-normal-form}\silentdefindex{negation-normal-form}
	A (quantifier-free) formula is in \emph{negation normal form} if negations only occur immediately before \emph{atoms}.
\end{definition}

Similar to how we converted formulae to prenex normal form, we can transform any (quantifier-free) formula to negation normal form by pushing all negations inside towards the atoms using De Morgan's laws.

\begin{definition}{Conjunctive normal form}{conjunctive-normal-form}\silentdefindex{conjunctive-normal-form}
	A (quantifier-free) formula is in \emph{conjunctive normal form} if it is a conjunction of disjunctions of (possibly negated) atoms. We write
	\[
		\varphi \Coloneqq \bigwedge_i \bigvee_j a_{ij}
	\]
	where $a_{ij}$ are atoms or negated atoms.
	We call $a_{ij}$ \emph{literals} and $\bigvee_j a_{ij}$ \emph{clauses}, identify a clause with its corresponding set of literals, and a formula with the corresponding sets of its clauses, allowing for notations like $a_{ij} \in c \in \varphi$.
\end{definition}

It is rather easy to see that any (quantifier-free) formula can be converted to conjunctive normal form. One possibility is to
\begin{enumerate*}
	\item negate it,
	\item convert to \emph{disjunctive normal form} with a Quine-McCluskey method as in~\cite{McCluskey1956},
	\item negate it again, and
	\item bring it to negation normal form.
\end{enumerate*}
Unfortunately, this method can produce exponentially large formulae -- just as for \emph{disjunctive normal form}.

Tseitin found a way around this in~\cite{Tseitin1968} by introducing fresh Boolean variables, devising a possibility to construct \emph{equisatisfiable} formulae that are only linearly larger.
The fundamental idea is to replace every \emph{subformula} by a fresh Boolean variable and require equivalence of the subformula and the Boolean variable.
Instead of a deeply nested formula, we obtain a conjunction of very shallow subformulae of constant size which can then be transformed to conjunctive normal form individually.

\begin{definition}{Tseitin's transformation}{tseitin-transformation}\silentdefindex{tseitin-transformation}
	Let $\varphi$ be a (quantifier-free) formula and $t(\phi) = x_\phi$ for every subformula $\phi$ of $\varphi$ where $x_\phi$ are fresh (Boolean) variables.
	The result of \emph{Tseitin's transformation} is $\varphi' \wedge t(\varphi)$ where $\varphi'$ is constructed inductively as follows:
	\begin{align*}
		\varphi &\Coloneqq b \mid c \mid p & \varphi' \coloneqq t(\varphi) &\leftrightarrow \varphi \\
		\varphi &\Coloneqq \varphi_1 \lor \varphi_2 & \varphi' \coloneqq t(\varphi) &\leftrightarrow (t(\varphi_1) \lor t(\varphi_2)) & \wedge \varphi_1' \wedge \varphi_2' \\
		\varphi &\Coloneqq \neg \varphi_1 & \varphi' \coloneqq t(\varphi) &\leftrightarrow \neg t(\varphi_1) & \wedge \varphi_1'
	\end{align*}

	We note that the resulting formula is technically not yet in conjunctive normal form, but a conjunction of subformulae of constant size. Transforming every subformula individually with a generic method as described above yields a formula in conjunctive normal form with only linear overhead.
\end{definition}

We have defined first-order logic for arbitrary theories and shown how to convert arbitrary formulae to conjunctive normal form.
Throughout this work, we focus on first-order logic with \emph{nonlinear real arithmetic}, commonly defined as real variables with addition, multiplication, and comparisons as defined in \cref{def:nonlinear-arithmetic}.

\begin{definition}{Nonlinear real arithmetic}{nonlinear-arithmetic}\silentdefindex{nonlinear-real-arithmetic}
	Let $\overline{x}$ be a set of real-valued variables and $=,<$ the relations with the usual semantics. We define terms and predicates of \emph{nonlinear real arithmetic} as follows:
	\begin{align*}
		t &\Coloneqq 0 \mid 1 \mid x \in \overline{x} \mid t + t \mid t \cdot t \\
		p &\Coloneqq t = t \mid t < t
	\end{align*}
\end{definition}

The predicates from \cref{def:nonlinear-arithmetic} nicely coincide with the constraints from \cref{def:constraint} where arithmetic expressions are restricted to polynomials.

Note that we use the term \emph{nonlinear real arithmetic} for both the arithmetic theory and the first-order logic over said theory.
We use the following other theories analogously without explicit definition as well: \emph{nonlinear integer arithmetic} ($\overline{x}$ ranges over $\Z$ instead of $\R$), \emph{linear real arithmetic} (without $t \cdot t$ or, equivalently, restricted to linear polynomials), and \emph{linear integer arithmetic} (both over $\Z$ and only linear).

While a formula has many interesting properties, we mainly focus on the question of \emph{satisfiability}.
In this work, we consider the satisfiability of \emph{quantifier-free} formulae where all variables are implicitly quantified existentially.
We observe that checking for satisfiability can be used to derive \emph{validity} as well by checking whether the negation of a formula is unsatisfiable.

\begin{definition}{Satisfiability}{satisfiability}\silentdefindex{satisfiability}
	We call a quantifier-free formula $\varphi$ \emph{satisfiable} if it has a variable assignment $\A$ such that $\A \models \varphi$.
	If no such $\A$ exists then $\varphi$ is \emph{unsatisfiable} and thus $\varphi \equiv \false$.
	We call $\A$ \emph{a model for $\varphi$}.
\end{definition}

\subsection{Extended polynomial constraints}\label{ssec:preliminaries:fol:extensions}

While we generally operate in the realm of first-order logic as defined before, we sometimes need to extend it a bit to allow for more expressive statements.
Note that the constraints we are about to present are not \emph{more expressive} in the strict logical sense -- they can be formulated equivalently within first-order logic -- but allow for a much shorter and more concise formulation.

We want to state that some variable is smaller than some root of a polynomial at several places in this work, for example, in \cref{sec:cadSMT:qe} and \cref{sec:mcsat:explanations}.
In particular, we want these polynomials to be multivariate and evaluate these expressions with respect to some theory model.
We call such constraints \emph{extended polynomial constraints} and define them as follows, mostly following the corresponding definition in~\cite{Jovanovic2012} or what is called an \emph{indexed root expression} in~\cite{Brown1999}.

\begin{definition}{Extended polynomial constraints}{extended-constraints}\silentdefindex{extended-constraints}
	Let $p \in \Q[z,\overline{x}]$ be a polynomial, $v$ some variable, and $\signcondition$ a sign condition.
	We call $v \signcondition \Root(p, k)$ an \emph{extended polynomial constraint} where $\Root(p, k)$ refers to the \emph{$k$th root of $p$ in $z$} (for fixed values of $\overline{x}$), $v$ \emph{the left-hand side} and $\Root(p, k)$ \emph{the root expression} of this extended polynomial constraint.

	To evaluate $v \signcondition \Root(p, k)$ we first compute the $k$th root of $p$ with respect to a model $\A$. If the number of roots is smaller than $k$, the result is $\false$, otherwise the result is $v \signcondition \alpha_k$ where $\alpha_k$ is the $k$th root of $p$.
\end{definition}

Note that this definition fails to give proper semantics for the evaluation, in particular, if $p$ does not become univariate in $z$ over $\A$.
Our intuition is, that such a constraint is a \emph{constraint on $v$} and that it \emph{does not apply yet}, much like a regular constraint does not give a Boolean result if it does not fully evaluate.

One special case that exists, however, is when $v$ is already assigned in $\A$ but some $x_i \in \overline{x}$ is not.
The constraint then intuitively becomes a \emph{constraint on $x_i$}, though it is not clear at all how to deal with this case.
Whether this particular situation actually occurs and how we can deal with it depends on how we construct and use such constraints, and we thus defer this discussion to \cref{ssec:mcsat:heuristics:variable-ordering}.
