import logging
import xml.etree.ElementTree as ET

import database

def remove_prefix(s, prefix):
	if s.startswith(prefix):
		return s[len(prefix):]
	return s

def get_solver(s):
	if s.startswith("src.smtrat_master.build."):
		return s[24:]
	if s.startswith("src.smtrat_master2.build."):
		return s[25:]
	return s

# load xml files
def addResult(run, file, timeout):
	solver = get_solver(run.attrib["solver_id"])
	filename = file.attrib["name"]
	filename = remove_prefix(filename, "QF_NRA/")
	answer = None
	runtime = None
	for res in run.find('./results'):
		if res.get('name') == 'answer':
			answer = res.text
		elif res.get('name') == 'runtime':
			runtime = int(res.text) / 1000.0
		if answer != None and runtime != None:
			break

	if answer == "memout":
		runtime = timeout + 10
	if answer == "timeout":
		runtime = timeout + 20
	database.insert(solver, filename, answer, runtime)

def load(filename):
	logging.info("Loading %s" % filename)
	tree = ET.parse(filename)
	root = tree.getroot()
	timeout = int(root.find("./information/info[@name='timeout']").get('value'))

	for file in root.findall('./benchmarks/*'):
		for run in file.findall('./*'):
			addResult(run, file, timeout = timeout)

def add_info_result(run, file):
	filename = file.attrib["name"]
	filename = remove_prefix(filename, "QF_NRA/")
	answer = None
	stats = run.find('./statistics')
	if stats is not None:
		for res in stats:
			database.add_file_property(filename, res.get('name'), res.text)

def load_info(filename):
	logging.info("Loading infos from %s" % filename)
	tree = ET.parse(filename)
	root = tree.getroot()

	for file in root.findall('./benchmarks/*'):
		for run in file.findall('./*'):
			add_info_result(run, file)