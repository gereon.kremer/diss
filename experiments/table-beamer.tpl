\begin{tabularx}{\linewidth}{X
S[table-format=4.0]
S[table-format=4.0]
S[table-format=5.0]}
	\toprule
	\textbf{Solver}
	& \textbf{SAT}
	& \textbf{UNSAT}
	& \textbf{overall}
	\\
	\midrule
{%- for s in solvers|sort(attribute = 'solver')|sort(attribute = 'solved') %}
	{% if s["solver"] in override %}{{ texName(override[s["solver"]]) }}{}{% else %}{{ texName(s["solver"]) }}{}{% endif %}
	& {{ s["sat"] }} & {{ s["unsat"] }} & {{ s["solved"] }} \\
{%- endfor %}
	\bottomrule
\end{tabularx}
