\section{Definition}\label{sec:mcsat:definition}

\MCSAT is given as a proof system in~\cite{Moura2013} and we start with this presentation as well. Our work on \MCSAT includes the integration of \MCSAT into a regular SAT solver, and we give an algorithmic description later on.

As we later see, the common termination argument for the \MCSAT proof system relies on the \emph{finite basis property}. Essentially, we require that all new theory constraints come from some finite set of constraints.
We call such a set a \emph{finite basis $\Basis$} and note that it usually depends not only on the input constraints but also on which theory methods are employed for the explanation function that we discuss in \cref{sec:mcsat:explanations}.

Similar to \CDCLT in \cref{ssec:cdcl-proof-system} and, in particular, \cref{def:cdcl-state}, \MCSAT works on a \emph{\MCSAT state} as defined below.
The proof rules look similar to the ones from \cref{ssec:cdcl-proof-system} but have some notable differences. We start with a few special notations and then define the \MCSAT proof system.

\begin{definition}{\MCSAT state and trail}{mcsat-trail}\silentdefindex{mcsat:state}\silentdefindex{mcsat:trail}
	Let an \emph{\MCSAT trail} extend a \DPLL trail with an additional type of elements:
	\begin{align*}
		x \mapsto \alpha_x &&& \textrm{Theory assignment of $x$ to theory value $\alpha_x$}
	\end{align*}

	Following \cref{def:sequence}, we use the following notation for a trail $M$:
	\[
		M = \Seq{N, L_1, C_1 \rightarrow L_2, \neg L_3, x \mapsto 2}
	\]
	where $N$ is the prefix of $M$ and itself a trail, $L_1$ and $\neg L_3$ are Boolean decisions, $C \rightarrow L_2$ is a Boolean propagation and $x \mapsto 2$ is a theory assignment.
	We call a trail \emph{complete} if it contains assignments for all (Boolean and theory) variables. We call a trail \emph{satisfying} if its model satisfies the formula, usually implying that it is also \emph{complete}.
	We call the combination of an \MCSAT trail and a set of clauses $\Clauses$ a \emph{\MCSAT state}.
	We distinguish two types of states, an \emph{\MCSAT search state} denoted as $\langle M, \Clauses \rangle$ and a \emph{\MCSAT conflict state} denoted as $\langle M, \Clauses \rangle \Conflicts C$ where $C$ is a clause such that $M \models \neg C$.
\end{definition}

In regular \CDCL (and \CDCLT), a literal's value is always determined by a \emph{Boolean} assignment -- either a decision or propagation -- unless it is still unassigned.
As \MCSAT incorporates a theory model, a literal can now also have a value due to the theory model if it evaluates to \true or \false over this theory model.
This carries the risk of having \emph{conflicting} assignments: having a literal that is propagated to be \true but evaluating to \false on the theory model. We impose a certain consistency on the trail to avoid such a case.

\begin{definition}{\MCSAT evaluation of literals}{mcsat-literal evaluation}\silentdefindex{mcsat:evaluation}
	Let $M$ be a trail and $L$ a literal.
	We call $\Value_\B(L,M)$ and $\Value_T(L,M)$ the \emph{Boolean value of $L$ (under $M$)} and the \emph{theory value of $L$ (under $M$)}, respectively, and define them accordingly.
	\begin{align*}
		\Value_\B(L,M) &= \begin{cases}
			\true & \textrm{ if } L \in M \\
			\false & \textrm{ if } \neg L \in M \\
			\Undef & \textrm{otherwise}
		\end{cases} \\
		\Value_T(L,M) &= \begin{cases}
			\true & \textrm{ if } v[M](L) = \true \\
			\false & \textrm{ if } v[M](L) = \false \\
			\Undef & \textrm{otherwise}
		\end{cases}
	\end{align*}
	
	We call a trail \emph{consistent} if it represents a proper partial assignment and the Boolean value and the theory value are compatible:
	\[
		\Forall{L \in M} \left( \neg L \not\in M \land \Value_T(L,M) \neq \false \right)
	\]
	This allows us to soundly define the \emph{value of $L$} $\Value(L,M)$:
	\[
		\Value(L,M) = \begin{cases}
			\Value_\B(L,M) & \textrm{ if } \Value_\B(L,M) \neq \Undef \\
			\Value_T(L,M) & \textrm{otherwise}
		\end{cases}
	\]
	We extend this function to obtain the \emph{value of a clause $C$} and analogously the \emph{the value of a set of clauses $\Clauses$} as follows:
	\[
		\Value(C,M) = \bigvee_{l \in C} \Value(l, M)
		\qquad
		\Value(\Clauses,M) = \bigwedge_{C \in \Clauses} \Value(C, M)
	\]
\end{definition}

Another interesting property that is checked at multiple places in the proof system we are about to define is \emph{infeasibility}. Informally, we say that a trail is infeasible if it can not be extended to contain a \emph{full model} (both Boolean and theory) in a consistent way.

\begin{definition}{\MCSAT trail infeasibility}{mcsat:trail-infeasibility}\silentdefindex{mcsat:infeasibility}
	Let $M$ be an \MCSAT trail.
	We call $M$ \emph{feasible} if it can be extended to a trail $N = \Seq{M, \dots}$ such that $N$ is \emph{consistent} and \emph{satisfying}.
\end{definition}

We already note here that this definition has a \emph{global view} on feasibility in that deciding upon the feasibility of a (possibly empty) trail is effectively identical to our original problem of deciding upon the satisfiability of a formula.
Therefore, practical implementations usually perform a \emph{partial check} for feasibility only, where \emph{partial} refers to the ``lookahead'': like proposed in~\cite{Jovanovic2012}, we only check whether the next theory variable (according to some ordering) can be assigned properly. When we advance towards a \emph{complete} trail this partial check eventually becomes a complete check.
For a more detailed discussion of this, we refer to \cref{sec:mcsat:practicability}.

We now define the \MCSAT proof system.
Similar to~\cite{Moura2013}, we split the proof rules into multiple parts that we call \emph{Boolean reasoning}, \emph{conflict analysis}, and \emph{theory reasoning}.

\begin{definition}{\MCSAT{} -- Boolean reasoning}{mcsat-boolean-reasoning}\silentdefindex{mcsat:boolean-reasoning}
	The \MCSAT proof rules for \emph{Boolean reasoning} are defined as follows.
	\begin{align*}
		\ProofRule{Decide}{\State{M, \Clauses}}{\State{\Seq{M, L}, \Clauses}}{\textrm{$L \in \Basis$ and $\Value(L,M) = \Undef$}} \\
		\ProofRule{Propagate}{\State{M, \Clauses}}{\State{\Seq{M,C \rightarrow L}, \Clauses}}{\textrm{$C = (L_1 \lor \dots \lor L_n \lor L) \in \Clauses$,} \\ \textrm{$\Forall{i} \Value(L_i,M) = \false$,} \\ \textrm{$\Value(L,M) = \Undef$}} \\
		\ProofRule{Conflict}{\State{M, \Clauses}}{\State{M, \Clauses} \Conflicts C}{\textrm{$C \in \Clauses$, $\Value(C) = \false$}} \\
		\ProofRule{Sat}{\State{M, \Clauses}}{sat}{\textrm{$\Value(\Clauses,M) = \true$}} \\
		\ProofRule{Forget}{\State{M, \Clauses}}{\State{M, \Clauses \setminus \{ C \}}}{\textrm{$C \in \Clauses$ is a learned clause}} \\
		\ProofRule{Restart}{\State{M, \Clauses}}{\State{\Seq{}, \Clauses}}{}
	\end{align*}
\end{definition}

The part of the \MCSAT proof system shown in \cref{def:mcsat-boolean-reasoning} takes care of \emph{Boolean exploration} (Boolean decisions and propagations), detecting conflicts or satisfiability, and ``maintenance tasks'' such as removing learned clauses or restarting the solver.
Note that the original presentation in~\cite{Jovanovic2012} does not contain the \silentindex{restart}\rulename{Restart} rule.
Once a conflict has been detected we switch from the \emph{search state} to the \emph{conflict state}.

\begin{definition}{\MCSAT{} -- Conflict analysis}{mcsat-conflict-analysis}\silentdefindex{mcsat:conflict-analysis}
	The \MCSAT proof rules for \emph{conflict analysis} are defined as follows.
	\begin{align*}
		\ProofRule{Resolve}{\State{\Seq{M, D \rightarrow L}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts R}{\textrm{$\neg L \in C$,} \\ \textrm{$R = \Resolution_L(C,D)$}} \\
		\ProofRule{Consume$_1$}{\State{\Seq{M, D \rightarrow L}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts C}{\textrm{$\neg L \not\in C$}} \\
		\ProofRule{Consume$_2$}{\State{\Seq{M, L}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts C}{\textrm{$\neg L \not\in C$}} \\
		\ProofRule{Backjump}{\State{\Seq{M, N}, \Clauses} \Conflicts C}{\State{\Seq{M, C \rightarrow L}, \Clauses}}{\textrm{$C = (L_1 \lor \dots \lor L_n \lor L)$,} \\ \textrm{$\Forall{i} \Value(L_i,M) = \false$,} \\ \textrm{$\Value(L,M) = \Undef$,} \\ \textrm{$N$ starts with a decision}} \\
		\ProofRule{Unsat}{\State{M, \Clauses} \Conflicts \false}{unsat}{} \\
		\ProofRule{Learn}{\State{M, \Clauses} \Conflicts C}{\State{M, \Clauses \cup \{C\}} \Conflicts C}{\textrm{$C \not\in \Clauses$}}
	\end{align*}
\end{definition}

Contrary to \CDCLT where conflict resolution is essentially unspecified, we provide reasonably detailed rules for how to perform conflict resolution in \cref{def:mcsat-conflict-analysis}.
However, these rules specify exactly what modern \CDCL-style SAT solvers do: resolution-based backtracking until the first unique implication point with clause learning -- though the clause learning is technically optional here.

\begin{definition}{\MCSAT{} -- Theory reasoning}{mcsat-theory-reasoning}\silentdefindex{mcsat:theory-reasoning}
	The \MCSAT proof rules for \emph{theory reasoning} are defined as follows.
	\begin{align*}
		\ProofRule{T-Propagate}{\State{M, \Clauses}}{\State{\Seq{M, E \rightarrow L}, \Clauses}}{\textrm{$L \in \Basis, \Value(L,M) = \Undef$,} \\ \textrm{$\Infeasible(\Seq{M, \neg L})$,} \\ \textrm{$E = \Explain(\Seq{M, \neg L})$}} \\
		\ProofRule{T-Decide}{\State{M, \Clauses}}{\State{\Seq{M, x \mapsto \alpha_x}, \Clauses}}{\textrm{$x$ is a theory variable from $\Clauses$,} \\ \textrm{$v[M](x) = \Undef$,} \\ \textrm{$\Consistent(\Seq{M, x \mapsto \alpha_x})$}} \\
		\ProofRule{T-Conflict}{\State{M, \Clauses}}{\State{M, \Clauses} \Conflicts E}{\textrm{$\Infeasible(M)$,} \\ \textrm{$E = \Explain(M)$}} \\
		\ProofRule{T-Consume}{\State{\Seq{M, x \mapsto \alpha_x}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts C}{\textrm{$\Value(C,M) = \false$}} \\
		\ProofRule{T-Backjump-Decide}{\State{\Seq{M, x \mapsto \alpha_x, N}, \Clauses} \Conflicts C}{\State{\Seq{M, L}, \Clauses}}{\textrm{$C = (L_1 \lor \dots \lor L_n \lor L)$,} \\ \textrm{$\Exists{i} \Value(L_i,M) = \Undef$,} \\ \textrm{$\Value(L,M) = \Undef$}}
	\end{align*}
\end{definition}

\cref{def:mcsat-theory-reasoning} finally enhances this proof system with theory-specific reasoning.
The \rulename{T-Propagate} rule enables us to inject new implications when the current theory model implies a theory constraint.
\rulename{T-Decide} performs a theory decision and thereby extends the theory model.
\rulename{T-Conflict} recognizes clauses that are conflicting \emph{in the theory}, rather than on the Boolean level.
Finally, \rulename{T-Consume} and \rulename{T-Backjump-Decide} extend the conflict resolution from \cref{def:mcsat-conflict-analysis} to gracefully handle theory decisions.

\begin{definition}{\MCSAT proof system}{mcsat-proof-system}\silentdefindex{mcsat:proof-system}\silentdefindex{proof-system:mcsat}
	The \emph{\MCSAT proof system} consists of the proof rules for \emph{Boolean reasoning}, \emph{conflict analysis},ysy and \emph{theory reasoning} as defined above.
\end{definition}
