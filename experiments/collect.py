#!/usr/bin/python3

import glob
import logging
import math
import random
import sys

logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)

import database
import demo
import loader
import render

def load(pattern, single_load = False):
	if single_load: database.reset()
	for file in glob.glob("data/" + pattern):
		loader.load(file)
	database.commit()

for file in glob.glob("data/stats_z3_*.xml"):
	loader.load_info(file)
for file in glob.glob("data/stats_analyzer_*.xml"):
	loader.load_info(file)
load("stats_z3_*.xml", False)
database.use_solver_as_reference('z3')

load("stats_nia_*.xml")

load("stats_CAD_*.xml")
load("stats_MCSAT_*.xml")

database.sanitize_results()

render.table_overview('table-overview.tex')

demo.load_demo_solvers()
demo.render_demo_solvers()

render.plot_data('plot-%s.data', ['z3'])
render.table_solved_within('table-solved-within-z3.tex', 'z3', [0.1, 1, 5, 10, 20, 60, 120, 300, 600, 1200])
render.table_properties('table-z3-nlsat-properties.tex', [
	('Overall', [('time', None, None)]),
	('In \\NLSAT', [('nlsat-propagations', 1, None), ('nlsat-stages', 1, None), ('nlsat-conflicts', 1, None), ('nlsat-decisions', 1, None)]),
	('At least one propagation', [('nlsat-propagations', 1, None)]),
	('At least one stage', [('nlsat-stages', 1, None)]),
	('At least five propagations', [('nlsat-propagations', 5, None)]),
	('At least one conflict', [('nlsat-conflicts', 1, None)]),
	('At least five stages', [('nlsat-stages', 5, None)]),
	('At least one Boolean decision', [('nlsat-decisions', 1, None)]),
	('At least five conflicts', [('nlsat-conflicts', 5, None)]),
	('At least five Boolean decision', [('nlsat-decisions', 5, None)]),
])

render.table_properties('table-z3-nlsat-preprocessor.tex', [
	('Linear', [('arith-assert-lower', 1, None), ('arith-assert-upper', 1, None), ('arith-pivots', 1, None)]),
	('Bitvectors', [('bv-bit2core', 1, None), ('bv-dynamic-eqs', 1, None)]),
	('SAT solver', [('conflicts', 1, None), ('decisions', 1, None), ('propagations', 1, None)]),
	('Combined', [
		('arith-assert-lower', 1, None), ('arith-assert-upper', 1, None), ('arith-pivots', 1, None),
		('bv-bit2core', 1, None), ('bv-dynamic-eqs', 1, None),
		('conflicts', 1, None), ('decisions', 1, None), ('propagations', 1, None)
	])
])

render.table('table-z3.tex', [
		'z3',
	], template = 'table2', override = {
		'z3': "\\Zthree",
	}
)

render.file_statistics("files-statistics.tex")
for prop in [
	"analyzer_num_constraints", 
	"analyzer_num_variables",
	"analyzer_num_variables_boolean",
	"analyzer_num_variables_arithmetic_real",
	"analyzer_num_variables_arithmetic_int",
	"analyzer_num_equalities",
	"analyzer_cad_projection_collins_size",
	"analyzer_cad_projection_hong_size",
	"analyzer_cad_projection_mccallum_size",
	"analyzer_cad_projection_lazard_size",
	"analyzer_cad_projection_brown_size",
	"analyzer_cad_projection_collins_deg_max",
	"analyzer_cad_projection_hong_deg_max",
	"analyzer_cad_projection_mccallum_deg_max",
	"analyzer_cad_projection_lazard_deg_max",
	"analyzer_cad_projection_brown_deg_max",
	"analyzer_cad_projection_collins_deg_sum",
	"analyzer_cad_projection_hong_deg_sum",
	"analyzer_cad_projection_mccallum_deg_sum",
	"analyzer_cad_projection_lazard_deg_sum",
	"analyzer_cad_projection_brown_deg_sum",
	"analyzer_cad_projection_collins_tdeg_max",
	"analyzer_cad_projection_hong_tdeg_max",
	"analyzer_cad_projection_mccallum_tdeg_max",
	"analyzer_cad_projection_lazard_tdeg_max",
	"analyzer_cad_projection_brown_tdeg_max",
	"analyzer_cad_projection_collins_tdeg_sum",
	"analyzer_cad_projection_hong_tdeg_sum",
	"analyzer_cad_projection_mccallum_tdeg_sum",
	"analyzer_cad_projection_lazard_tdeg_sum",
	"analyzer_cad_projection_brown_tdeg_sum",
	"analyzer_constraint_deg_max",
	"analyzer_constraint_deg_sum",
	]:
	render.file_property("files-info-%s.data" % prop, prop)
for prop in ["analyzer_answer"]:
	render.file_property_str("files-info-%s.data" % prop, prop)


render.writeScatterData("scatter.data", "smtrat_FU", "smtrat_NO")

#render.writeDebugTable("table-all.tex")

# Write data for incrementality levels
render.table('table-cad-incrementality.tex', [
		'smtrat_CAD_Inone_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_INO_LOLT_MISG_PB_POD_PPVERR',
		#'smtrat_CAD_INU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_ISO_LOLT_MISG_PB_POD_PPVERR',
		#'smtrat_CAD_ISU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVERR',
	], template = 'table2', override = {
		'smtrat_CAD_Inone_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncNone',
		'smtrat_CAD_INO_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncNaive',
		#'smtrat_CAD_INU_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncNaive-u',
		'smtrat_CAD_ISO_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncSimple',
		#'smtrat_CAD_ISU_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncSimple-u',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncFull',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncFull-hide',
	}
)

render.table('table-cad-incrementality-beamer.tex', [
		'smtrat_CAD_Inone_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_INO_LOLT_MISG_PB_POD_PPVERR',
		#'smtrat_CAD_INU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_ISO_LOLT_MISG_PB_POD_PPVERR',
		#'smtrat_CAD_ISU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVERR',
	], template = 'table-beamer', override = {
		'smtrat_CAD_Inone_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncNone',
		'smtrat_CAD_INO_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncNaive',
		#'smtrat_CAD_INU_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncNaive-u',
		'smtrat_CAD_ISO_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncSimple',
		#'smtrat_CAD_ISU_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncSimple-u',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncFull',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADIncFull-hide',
	}
)

render.plot_data('plot-%s.data', [
		'smtrat_CAD_IFU_LOLT_MISG_PC_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PH_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PM_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PL_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PMp_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
	])

render.plot_data('plot-%s-sat.data', [
		'smtrat_CAD_INU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_ISU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
	], condition = 'answer=\'sat\'')
render.plot_data('plot-%s-unsat.data', [
		'smtrat_CAD_INU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_ISU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
	], condition = 'answer=\'unsat\'')

# Write data for cad projection order
render.table('table-cad-proj.tex', [
		'smtrat_CAD_IFU_LOLT_MISG_PC_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PH_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PM_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PMp_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PL_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
	], template = 'table2', override = {
		'smtrat_CAD_IFU_LOLT_MISG_PC_POD_PPVERR': "\\SolverCADProjCollins",
		'smtrat_CAD_IFU_LOLT_MISG_PH_POD_PPVERR': "\\SolverCADProjHong",
		'smtrat_CAD_IFU_LOLT_MISG_PM_POD_PPVERR': "\\SolverCADProjMcCallum",
		'smtrat_CAD_IFU_LOLT_MISG_PMp_POD_PPVERR': "\\SolverCADProjMcCallumPartial",
		'smtrat_CAD_IFU_LOLT_MISG_PL_POD_PPVERR': "\\SolverCADProjLazard",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': "\\SolverCADProjBrown",
	}
)
render.scatter('scatter-cad-proj-hong-mccallum.data',
	'smtrat_CAD_IFU_LOLT_MISG_PH_POD_PPVERR',
	'smtrat_CAD_IFU_LOLT_MISG_PM_POD_PPVERR'
)
render.scatter('scatter-cad-proj-hong-brown.data',
	'smtrat_CAD_IFU_LOLT_MISG_PH_POD_PPVERR',
	'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR'
)
render.scatter('scatter-cad-proj-mccallum-brown.data',
	'smtrat_CAD_IFU_LOLT_MISG_PM_POD_PPVERR',
	'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR'
)

render.table('table-cad-proj-beamer.tex', [
		'smtrat_CAD_IFU_LOLT_MISG_PC_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PH_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PM_POD_PPVERR',
#		'smtrat_CAD_IFU_LOLT_MISG_PMp_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PL_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
	], template = 'table-beamer', override = {
		'smtrat_CAD_IFU_LOLT_MISG_PC_POD_PPVERR': "\\SolverCADProjCollins",
		'smtrat_CAD_IFU_LOLT_MISG_PH_POD_PPVERR': "\\SolverCADProjHong",
		'smtrat_CAD_IFU_LOLT_MISG_PM_POD_PPVERR': "\\SolverCADProjMcCallum",
#		'smtrat_CAD_IFU_LOLT_MISG_PMp_POD_PPVERR': "\\SolverCADProjMcCallumPartial",
		'smtrat_CAD_IFU_LOLT_MISG_PL_POD_PPVERR': "\\SolverCADProjLazard",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': "\\SolverCADProjBrown",
	}
)

# Write data for cad projection order
render.table('table-cad-po.tex', [
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POlD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POLD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POPD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POSD_PPVERR',
	], template = 'table2', override = {
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': "\\SolverCADPOd",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POlD_PPVERR': "\\SolverCADPOld",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POLD_PPVERR': "\\SolverCADPOlld",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POPD_PPVERR': "\\SolverCADPOpd",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POSD_PPVERR': "\\SolverCADPOsd",
	}
)

render.table('table-cad-po-beamer.tex', [
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POlD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POLD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POPD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POSD_PPVERR',
	], template = 'table-beamer', override = {
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': "\\SolverCADPOd",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POlD_PPVERR': "\\SolverCADPOld",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POLD_PPVERR': "\\SolverCADPOlld",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POPD_PPVERR': "\\SolverCADPOpd",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POSD_PPVERR': "\\SolverCADPOsd",
	}
)

# Write data for cad lifting order
render.table('table-cad-lo.tex', [
		'smtrat_CAD_IFU_LOLS_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLTA_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLTSA_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLTS_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOS_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOTLSA_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOTSA_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOTS_MISG_PB_POD_PPVERR',
	], template = 'table2', override = {
		'smtrat_CAD_IFU_LOLS_MISG_PB_POD_PPVERR': "\\SolverCADLOls",
		'smtrat_CAD_IFU_LOLTA_MISG_PB_POD_PPVERR': "\\SolverCADLOlta",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': "\\SolverCADLOlt",
		'smtrat_CAD_IFU_LOLTSA_MISG_PB_POD_PPVERR': "\\SolverCADLOltsa",
		'smtrat_CAD_IFU_LOLTS_MISG_PB_POD_PPVERR': "\\SolverCADLOlts",
		'smtrat_CAD_IFU_LOS_MISG_PB_POD_PPVERR': "\\SolverCADLOs",
		'smtrat_CAD_IFU_LOTLSA_MISG_PB_POD_PPVERR': "\\SolverCADLOtlsa",
		'smtrat_CAD_IFU_LOT_MISG_PB_POD_PPVERR': "\\SolverCADLOt",
		'smtrat_CAD_IFU_LOTSA_MISG_PB_POD_PPVERR': "\\SolverCADLOtsa",
		'smtrat_CAD_IFU_LOTS_MISG_PB_POD_PPVERR': "\\SolverCADLOts",
	}
)

render.table('table-cad-lo-beamer.tex', [
		'smtrat_CAD_IFU_LOLS_MISG_PB_POD_PPVERR',
#		'smtrat_CAD_IFU_LOLTA_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
#		'smtrat_CAD_IFU_LOLTSA_MISG_PB_POD_PPVERR',
#		'smtrat_CAD_IFU_LOLTS_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOS_MISG_PB_POD_PPVERR',
#		'smtrat_CAD_IFU_LOTLSA_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOTSA_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOTS_MISG_PB_POD_PPVERR',
	], template = 'table-beamer', override = {
		'smtrat_CAD_IFU_LOLS_MISG_PB_POD_PPVERR': "\\SolverCADLOls",
#		'smtrat_CAD_IFU_LOLTA_MISG_PB_POD_PPVERR': "\\SolverCADLOlta",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': "\\SolverCADLOlt",
#		'smtrat_CAD_IFU_LOLTSA_MISG_PB_POD_PPVERR': "\\SolverCADLOltsa",
#		'smtrat_CAD_IFU_LOLTS_MISG_PB_POD_PPVERR': "\\SolverCADLOlts",
		'smtrat_CAD_IFU_LOS_MISG_PB_POD_PPVERR': "\\SolverCADLOs",
#		'smtrat_CAD_IFU_LOTLSA_MISG_PB_POD_PPVERR': "\\SolverCADLOtlsa",
		'smtrat_CAD_IFU_LOT_MISG_PB_POD_PPVERR': "\\SolverCADLOt",
		'smtrat_CAD_IFU_LOTSA_MISG_PB_POD_PPVERR': "\\SolverCADLOtsa",
		'smtrat_CAD_IFU_LOTS_MISG_PB_POD_PPVERR': "\\SolverCADLOts",
	}
)

# Write data for equational constraints
render.table('table-cad-equational.tex', [
		'smtrat_CAD_IFH_LOLT_MISG_PH_POD_PPVERR',
		'smtrat_CAD_IFH_LOLT_MISG_PM_POD_PPVERR',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFEC_LOLT_MISG_PH_POD_PPVERR',
		'smtrat_CAD_IFEC_LOLT_MISG_PM_POD_PPVERR',
		'smtrat_CAD_IFEC_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFECS_LOLT_MISG_PH_POD_PPVERR',
		'smtrat_CAD_IFECS_LOLT_MISG_PM_POD_PPVERR',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PPVERR',
	], template = 'table2', override = {
		'smtrat_CAD_IFH_LOLT_MISG_PH_POD_PPVERR': '\\SolverCADECHH',
		'smtrat_CAD_IFH_LOLT_MISG_PM_POD_PPVERR': '\\SolverCADECHM',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADECHB',
		'smtrat_CAD_IFEC_LOLT_MISG_PH_POD_PPVERR': '\\SolverCADECECH',
		'smtrat_CAD_IFEC_LOLT_MISG_PM_POD_PPVERR': '\\SolverCADECECM',
		'smtrat_CAD_IFEC_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADECECB',
		'smtrat_CAD_IFECS_LOLT_MISG_PH_POD_PPVERR': '\\SolverCADECECSH',
		'smtrat_CAD_IFECS_LOLT_MISG_PM_POD_PPVERR': '\\SolverCADECECSM',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADECECSB',
	}
)

# Write data for preprocessing for equational constraints (resultant rule and variable elimination)
render.table('table-cad-pp.tex', [
		#'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PP',
		#'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVE',
		#'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPRR',
		#'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PP',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVE',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPRR',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PP',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PPVE',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PPRR',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PPVERR',
	], template = 'table2', override = {
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PP': '\\SolverCADPP',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVE': '\\SolverCADPPVE',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPRR': '\\SolverCADPPRR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADPPVERR',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PP': '\\SolverCADPP-hide',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVE': '\\SolverCADPPVE-hide',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPRR': '\\SolverCADPPRR-hide',
		'smtrat_CAD_IFH_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADPPVERR-hide',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PP': '\\SolverCADECSPP',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PPVE': '\\SolverCADECSPPVE',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PPRR': '\\SolverCADECSPPRR',
		'smtrat_CAD_IFECS_LOLT_MISG_PB_POD_PPVERR': '\\SolverCADECSPPVERR',
	}
)
render.scatter('scatter-cad-pp-ne-nenr.data', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPRR', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PP', where = '(answer1=\'unsat\' OR answer2=\'unsat\')')
render.scatter('scatter-cad-pp-ne-nr.data', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPRR', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVE', where = '(answer1=\'unsat\' OR answer2=\'unsat\')')
render.scatter('scatter-cad-pp-nr-ne.data', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVE', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPRR', where = '(answer1=\'unsat\' OR answer2=\'unsat\')')
render.scatter('scatter-cad-pp-full-ne.data', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPRR', where = '(answer1=\'unsat\' OR answer2=\'unsat\')')
render.scatter('scatter-cad-pp-nr-full.data', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVE', 'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR', where = '(answer1=\'unsat\' OR answer2=\'unsat\')')


# Write data for minimal infeasible subsets
render.table('table-cad-mis.tex', [
		'smtrat_CAD_IFU_LOLT_MISE_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISGPP_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISGW_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISH_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MIST_PB_POD_PPVERR',
	], template = 'table2', override = {
		'smtrat_CAD_IFU_LOLT_MISE_PB_POD_PPVERR': "\\SolverCADMISExact",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': "\\SolverCADMISGreedy",
		'smtrat_CAD_IFU_LOLT_MISGPP_PB_POD_PPVERR': "\\SolverCADMISGreedyPre",
		'smtrat_CAD_IFU_LOLT_MISGW_PB_POD_PPVERR': "\\SolverCADMISGreedyWeighted",
		'smtrat_CAD_IFU_LOLT_MISH_PB_POD_PPVERR': "\\SolverCADMISHybrid",
		'smtrat_CAD_IFU_LOLT_MIST_PB_POD_PPVERR': "\\SolverCADMISTrivial",
	}
)
render.table('table-cad-mis-beamer.tex', [
		'smtrat_CAD_IFU_LOLT_MISE_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISGPP_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISGW_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MISH_PB_POD_PPVERR',
		'smtrat_CAD_IFU_LOLT_MIST_PB_POD_PPVERR',
	], template = 'table-beamer', override = {
		'smtrat_CAD_IFU_LOLT_MISE_PB_POD_PPVERR': "\\SolverCADMISExact",
		'smtrat_CAD_IFU_LOLT_MISG_PB_POD_PPVERR': "\\SolverCADMISGreedy",
		'smtrat_CAD_IFU_LOLT_MISGPP_PB_POD_PPVERR': "\\SolverCADMISGreedyPre",
		'smtrat_CAD_IFU_LOLT_MISGW_PB_POD_PPVERR': "\\SolverCADMISGreedyWeighted",
		'smtrat_CAD_IFU_LOLT_MISH_PB_POD_PPVERR': "\\SolverCADMISHybrid",
		'smtrat_CAD_IFU_LOLT_MIST_PB_POD_PPVERR': "\\SolverCADMISTrivial",
	}
)

# Write data for nonlinear integer arithmetic
render.table('table-cad-nia.tex', [
		'smtrat_NIASolver',
		'smtrat_NIABB',
		'smtrat_NIABlast',
	], template = 'table2', override = {
		'smtrat_NIASolver': "\\SolverCADNIAFull",
		'smtrat_NIABB': "\\SolverCADNIABB",
		'smtrat_NIABlast': "\\SolverCADNIABlast",
	}
)
render.table('table-cad-nia-beamer.tex', [
		'smtrat_NIASolver',
		'smtrat_NIABB',
		'smtrat_NIABlast',
	], template = 'table-beamer', override = {
		'smtrat_NIASolver': "\\SolverCADNIAFull",
		'smtrat_NIABB': "\\SolverCADNIABB",
		'smtrat_NIABlast': "\\SolverCADNIABlast",
	}
)


render.scatter_solvers('scatter-cad-solvers.data', 'smtrat_CAD_.*')

render.table('table-mcsat-af.tex', [
		'smtrat_MCSAT_AF_FMOCNL_TF',
		'smtrat_MCSAT_SMT_FMOCNL_TF',
	], template = 'table2', override = {
		'smtrat_MCSAT_AF_FMOCNL_TF': '\\SolverMCSATAF',
		'smtrat_MCSAT_SMT_FMOCNL_TF': '\\SolverMCSATSMT',
})

render.plot_data('plot-%s.data', [
	'smtrat_MCSAT_AF_FMOCNL_TF',
	'smtrat_MCSAT_SMT_FMOCNL_TF',
])

render.table('table-mcsat-explanations.tex', [
		'smtrat_MCSAT_AF_FMICPOCNL_TF',
		'smtrat_MCSAT_AF_FMICPVSOCNL_TF',
		'smtrat_MCSAT_AF_FMOCNL_TF',
		'smtrat_MCSAT_AF_FMVSOCNL_TF',
		'smtrat_MCSAT_AF_NL_TF',
		'smtrat_MCSAT_AF_OCNL_TF',
	], template = 'table2', override = {
		'smtrat_MCSAT_AF_FMICPOCNL_TF': '\\SolverMCSATExFMICPOCNL',
		'smtrat_MCSAT_AF_FMICPVSOCNL_TF': '\\SolverMCSATExFMICPVSOCNL',
		'smtrat_MCSAT_AF_FMOCNL_TF': '\\SolverMCSATExFMOCNL',
		'smtrat_MCSAT_AF_FMVSOCNL_TF': '\\SolverMCSATExFMVSOCNL',
		'smtrat_MCSAT_AF_NL_TF': '\\SolverMCSATExNL',
		'smtrat_MCSAT_AF_OCNL_TF': '\\SolverMCSATExOCNL',
})

render.plot_data('plot-%s.data', [
	'smtrat_MCSAT_AF_FMICPOCNL_TF',
	'smtrat_MCSAT_AF_FMICPVSOCNL_TF',
	'smtrat_MCSAT_AF_FMOCNL_TF',
	'smtrat_MCSAT_AF_FMVSOCNL_TF',
	'smtrat_MCSAT_AF_NL_TF',
	'smtrat_MCSAT_AF_OCNL_TF',
])

render.table('table-mcsat-varorders.tex', [
		'smtrat_MCSAT_AF_FMOCNL_BF',
		'smtrat_MCSAT_AF_FMOCNL_NLSAT',
		'smtrat_MCSAT_AF_FMOCNL_RND',
		'smtrat_MCSAT_AF_FMOCNL_TF',
		'smtrat_MCSAT_AF_FMOCNL_TFDYN',
		'smtrat_MCSAT_AF_FMOCNL_UNIFORM',
		'smtrat_MCSAT_AF_FMOCNL_UNIFORMTF',
		'smtrat_MCSAT_AF_FMOCNL_UV',
		'smtrat_MCSAT_AF_FMOCNL_UVactive',
	], template = 'table2', override = {
		'smtrat_MCSAT_AF_FMOCNL_BF': '\\SolverMCSATVOBF',
		'smtrat_MCSAT_AF_FMOCNL_NLSAT': '\\SolverMCSATVONLSAT',
		'smtrat_MCSAT_AF_FMOCNL_RND': '\\SolverMCSATVORND',
		'smtrat_MCSAT_AF_FMOCNL_TF': '\\SolverMCSATVOTF',
		'smtrat_MCSAT_AF_FMOCNL_TFDYN': '\\SolverMCSATVOTFDYN',
		'smtrat_MCSAT_AF_FMOCNL_UNIFORM': '\\SolverMCSATVOUNIFORM',
		'smtrat_MCSAT_AF_FMOCNL_UNIFORMTF': '\\SolverMCSATVOUNIFORMTF',
		'smtrat_MCSAT_AF_FMOCNL_UV': '\\SolverMCSATVOUV',
		'smtrat_MCSAT_AF_FMOCNL_UVactive': '\\SolverMCSATVOUVactive',
})

render.plot_data('plot-%s.data', [
	'smtrat_MCSAT_AF_FMOCNL_BF',
	'smtrat_MCSAT_AF_FMOCNL_NLSAT',
	'smtrat_MCSAT_AF_FMOCNL_RND',
	'smtrat_MCSAT_AF_FMOCNL_TF',
	'smtrat_MCSAT_AF_FMOCNL_TFDYN',
	'smtrat_MCSAT_AF_FMOCNL_UNIFORM',
	'smtrat_MCSAT_AF_FMOCNL_UNIFORMTF',
	'smtrat_MCSAT_AF_FMOCNL_UV',
	'smtrat_MCSAT_AF_FMOCNL_UVactive',
])
