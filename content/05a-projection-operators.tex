\section{Projection operators}\label{sec:cad:projection-operators}

\silentindex{projection}\silentindex{cad:projection}
We discussed that -- given a set of ``input'' polynomials -- we want to construct a set of lower-dimensional polynomials that induce a lower-dimensional CAD, and then use the sample points of this lower-dimensional CAD to construct sample points for our current problem.
The main requirement for the lower-dimensional polynomials is that they should have roots wherever the \emph{number or order} of the input polynomials \emph{real roots} change, that is where they ``stop being delineable''.
To construct these lower-dimensional polynomials, we now make use of \emph{reducta}, \emph{resultants} and \emph{discriminants} as already defined in \cref{sec:preliminaries:polynomials}.

Since CAD was proposed in~\cite{Collins1975}, several methods to compute such lower-dimensional polynomials have been developed and we call them \emph{projection operators}.
We focus on the projection operators suitable for arbitrary inputs and briefly mention a few more that are intended for more specialized cases at the end of this section.

The first projection operator is due to Collins in~\cite{Collins1974}, but Collins himself already notes in~\cite[Section 5]{Collins1975} that many polynomials are unnecessary in most cases. Hong showed in~\cite{Hong1990} how to reduce Collins' projection operator significantly to what we call \emph{Hong's projection operator}.

McCallum used a rather different mathematical point of view in~\cite{McCallum1988} to motivate another projection operator. Though it uses the same ingredients, it is again substantially smaller than the ones due to Collins or Hong. \emph{McCallum's projection operator} however has an important caveat: for problems of dimension larger than three, the CAD construction may fail or be incomplete in the sense that some cells may be missing from the result -- or rather cells are incorrectly ``merged'' and in this sense a representative is missing.
We refer to this issue as \emph{completeness} (or \emph{incompleteness}) of a projection operator.
Brown improved upon McCallum's operator again in~\cite{Brown2001} with respect to the number of constructed polynomials, but still suffers from the deficiency of McCallum's operator and even adds additional sources of incompleteness.

Lazard published another improvement on McCallum's projection operator in~\cite{Lazard1994}.
In contrast to McCallum's and Brown's operators, Lazard ensured correctness for all degrees by a slight modification in the lifting process. However, it never came into widespread use as a gap in his proof was noticed in~\cite{Collins1998} and~\cite{Brown2001}.
Lazard's projection operator only came to the fore when McCallum and Hong closed this gap in~\cite{McCallum2016} -- a full proof was just proposed in~\cite{McCallum2019} -- and thereby provide us with a \emph{complete} projection operator that essentially supersedes all but Brown's operator.

\subsection{Intuition}

Though we do not dive into the strict mathematical reasoning behind the different projection operators, we give some intuition on what a projection operator does and what it means geometrically. For the purpose of this section, we consider \emph{McCallum's projection operator} -- ignoring preconditions and the question of completeness.
This projection operator is (roughly) defined as follows.
\[
	Proj(P) = \{ \Coeffs(p), \Disc(p) \mid p \in P \} \cup \{ \Res(p,q) \mid p,q \in P \}
\]
We observe that we essentially have three components: coefficients of a polynomial, the discriminant of a polynomial, and the resultant of two polynomials, and give some intuition on the role of these components in \cref{ex:projection-components}.

\begin{example}{CAD projection}{projection-components}
	We consider an input formula with the two polynomials $p = {(y+1)}^2-x^3+3x-2$ and $q = (x + 1) \cdot y - 3$.
	We observe that the roots of the first polynomial form a tie-like shape, while the second polynomial is responsible for the two hyperbolas.
	
	\begin{minipage}[t]{0.49\linewidth}
		\begin{center}
			\includetikz{05a-projection-components-regions}
		\end{center}
	\end{minipage}\hfill{}
	\begin{minipage}[t]{0.49\linewidth}
		\begin{center}
			\includetikz{05a-projection-components-samples}
		\end{center}
	\end{minipage}
	
	In the $x$-dimension (that is shown at the bottom of the right figure) we can identify four points where the \emph{number or order of roots} change, and thus separate sign-invariant regions, which fall into the following four categories:
	\begin{enumerate*}
		\item $x=2$ because $p$ and $q$ intersect at $(2,1)$,
		\item $x=1$ because $p$ intersects with itself at $(1,-1)$,
		\item $x=-2$ because $p$ \emph{turns around} at $(-2,-1)$, and
		\item $x=-1$ because $q$ has an asymptote at $x=-1$.
	\end{enumerate*}
	Let us consider the individual components of the projection set $Proj(\{p,q\})$, cleaned from constant and multiple factors as described at the end of \cref{sec:preliminaries:polynomials}:
	\begin{align*}
		Proj(P) =& \{ & \Coeffs(p) &= \{x^3-3x+1 \}, \\
		&& \Coeffs(q) &= \{x+1\}, \\
		&& \Disc(p) &= {(x-1)}^2 \cdot (x+2), \\
		&& \Disc(q) &= 1, \\
		&& \Res(p,q) &= (x-2) \cdot (x^4+4x^3+6x^2+7x+7) & \}
	\end{align*}
	
	We observe that the resultant covers all intersections of $p$ and $q$ -- note that the second factor of the resultant has no real roots.
	The discriminant, on the other hand, covers the cases where a polynomial induces a separation by itself, either by self-intersection or by turning around.
	Roots in the coefficients finally indicate singularities, in this case at $-1$.
	
	The sets of cylinder boundaries may very well overlap if some of these components happen to share common polynomial factors, for example, because multiple of the above criteria coincide.
	Note, however, that all projection operators may also be subject to \emph{imprecision} in the sense that polynomial factors are generated that indeed yield new cell boundaries that are \emph{spurious}.
\end{example}

\subsection{Collins' projection operator}

In the first publications on the cylindrical algebraic decomposition, multiple (improving) projection operators were proposed. Though the authorship is not completely clear, we attribute this first group of projection operators to Collins. We call all of them \emph{Collins' projection operator} and mean the last of them in \cref{def:third-collins-projection} if not further specified.

We start with the formulation of Collins' projection operator from the first paper~\cite{Collins1974}. Having already seen the general structure of more recent projection operators in the last section, it is the only one that does not follow the pattern we have outlined. Instead, it really looks like the first working solution to finding a projection operator at all, essentially including everything that might be useful.

\begin{definition}{First version of Collins' projection operator}{first-collins-projection}
	Let $P \subset \Z[\overline{x}]$ and $Q = P \cup \{ p \cdot q \mid p,q \in P, p \neq q \}$.
	\defindex{projection-collins-first} as of~\cite{Collins1974} is defined as follows:
	\begin{align*}
		Proj_{Collins}(P) =& \bigcup_{q \in RED(Q)} PSC(q, q')
	\end{align*}
\end{definition}

In the very next year,~\cite{Collins1975} already contains a significantly improved version that starts to show the features we have highlighted above: the separation into coefficients, something for every polynomial individually and as well for every pair of polynomials. In particular, note that we get rid of the multiplication of polynomials that usually leads to an expensive degree growth -- though of course another degree growth is hidden within the principal subresultant coefficients.

\begin{definition}{Second version of Collins' projection operator}{second-collins-projection}
	Let $P \subset \Z[\overline{x}]$. \defindex{projection-collins-second} is defined as follows:
	\begin{align*}
		Proj_{Collins}(P) =& \{ \Lcoeff(p) \mid p \in RED(P) \} \\
		&\cup \bigcup_{p \in RED(P)} PSC(p,p') \\
		&\cup \bigcup_{p,q \in RED(P)} PSC(p,q)
	\end{align*}
\end{definition}

Later on, yet another variant was presented in~\cite{Arnon1984} that again features an improvement, though much more subtle than the previous one. Observe that the combination of $\Lcoeff(p)$ and $PSC(p,p')$ is really just a rewriting, but the modification in the last part actually removes $PSC(p^*,q^*)$ where $p^*$ and $q^*$ are from the reducta set of the same polynomial.

\begin{definition}{Third version of Collins' projection operator}{third-collins-projection}
	Let $P \subset \Z[\overline{x}]$. \defindex{projection-collins-third} is defined as follows:
	\begin{align*}
		Proj_{Collins}(P) =& \bigcup_{p \in RED(P)} (\{ \Lcoeff(p) \} \cup PSC(p,p')) \\
		&\cup \bigcup_{\substack{p,q \in P \\ p < q}} \bigcup_{\substack{p^* \in RED(p) \\ q^* \in RED(q)}} PSC(p^*,q^*)
	\end{align*}
\end{definition}

This last version from \cref{def:third-collins-projection} is what is usually considered to be \emph{the} projection operator due to Collins and we use it as such for the following experiments.

\subsection{Hong's projection operator}

A few years later, Hong managed to improve Collins' projection operator in~\cite{Hong1990} in a seemingly small but (in practice) significant way. Note that we can use the essentially same proofs for this modified construction, also retaining full theoretical completeness and the same mathematical foundations.
Hong showed that for the pairwise computations of the pseudo resultant coefficients, we only need to consider the whole reducta set for one of the polynomials.

An interesting remark appears in~\cite{Seidl2003}, noting that ``the construction of the chain of reducta can be stopped as soon as the first constant leading coefficient appears''.
This observation immediately transfers to leading coefficients that do \emph{not vanish} anywhere (like $x^2 + 1$) and also holds for Collins' projection operator. Furthermore, the same argument also applies to McCallum's projection operator that is shown below.

\begin{definition}{Hong's projection operator}{hong-projection}
	Let $P \subset \Z[\overline{x}]$. \defindex{projection-hong} is defined as follows:
	\begin{align*}
		Proj_{Hong}(P) =& \bigcup_{p \in RED(P)} (\{ \Lcoeff(p) \} \cup PSC(p,p')) \\
		&\cup \bigcup_{\substack{p,q \in P \\ p < q}} \bigcup_{p^* \in RED(p)} PSC(p^*,q)
	\end{align*}
\end{definition}

\subsection{McCallum's projection operator}

Around the same time, McCallum devised another improvement in~\cite{McCallum1984} and -- only for dimension three -- in~\cite{McCallum1988}. This time the projection is approached from a different mathematical angle, namely from complex analytic geometry, based on~\cite{Zariski1965}.

We previously considered the whole set of principal subresultant coefficients for every polynomial (and its derivative) from the reducta set and for every pair of polynomials (where one is from the reducta set).
McCallum showed that we can replace all these by a single discriminant (for every polynomial) and a single resultant (for every pair of polynomials).
We observe that this construction is not fundamentally different though, as we know that $psc_0(p,q) = \Res(p,q)$, but ``only'' allows us to consider the polynomial (instead of its reducta set) and the resultant (instead of the set of principal subresultant coefficients), yielding way fewer polynomials in most cases.

\begin{definition}{McCallum's projection operator}{mccallum-projection}
	Let $P \subset \Z[\overline{x}]$ be a set of polynomials, $\Content(P)$ their content and $P'$ the finest square-free basis of their primitive parts $\Primitive(P)$. \defindex{projection-mccallum} is defined as follows:
	\begin{align*}
		Proj_{McCallum}(P) =& \{ \Coeffs(p), \Disc(p) \mid p \in P' \} \\
		& \cup \{ \Res(p,q) \mid p,q \in P' \} \cup cont(P)
	\end{align*}
\end{definition}

This greatly simplified projection operator has a significant drawback compared to those of Hong or Collins. In certain cases, the projection is \emph{incomplete} in the sense that we may not be able to do the lifting process properly.
We discuss the reasons and consequences in \cref{ssec:cad:lifting:incompleteness}.
Furthermore, this projection operator assumes that $P$ is not some arbitrary set of polynomials, but a \emph{finest square-free basis}.
We discuss this issue briefly in \cref{ssec:cad:heuristics:other} and note that it can be achieved by completely factorizing all polynomials, which appears reasonable anyway. This even yields an \emph{irreducible basis}, which is in some sense even stronger than a square-free basis.

Similar to the remark about Hong's projection operator from~\cite{Seidl2003}, it is sufficient to consider the coefficients in McCallum's projection operator only until a constant (or rather not vanishing anywhere) coefficient appears, starting from the leading coefficient.
We call this variant \emph{partial McCallum's projection operator}.

\subsection{Brown's projection operator}

Brown improved on McCallum's projection operator in~\cite{Brown2001} and showed that it is oftentimes enough to consider only the leading coefficient instead of all coefficients.
Naturally, it inherits the downsides of McCallum's projection operator concerning its incompleteness. Removing all other coefficients from the projection may even lead to additional problems in the lifting phase, though a method is given to detect and handle these cases in~\cite{Brown2001}.

\begin{definition}{Brown's projection operator}{brown-projection}
	Let $P \subset \Z[\overline{x}]$ be a set of polynomials, $\Content(P)$ their content, and $P'$ the finest square-free basis of their primitive parts $\Primitive(P)$. \defindex{projection-brown} is defined as follows:
	\begin{align*}
		Proj_{Brown}(P) =& \{ \Lcoeff(p), \Disc(p) \mid p \in P' \} \\
		& \cup \{ \Res(p,q) \mid p,q \in P' \} \cup \Content(P)
	\end{align*}
\end{definition}

\subsection{Lazard's projection operator}\label{ssec:cad:projection:lazard}

Lastly, \emph{Lazard's projection operator} was published in~\cite{Lazard1994} but it was mostly disregarded as its correctness proof contained a flaw that was only corrected more than twenty years later in~\cite{McCallum2016,McCallum2019}. The projection operator itself is very similar to McCallum's and Brown's projection operators -- it uses the leading and the trailing coefficient.
One could very well argue that Lazard's main contribution is a slight change in the lifting process that resolves the incompleteness issue.

\begin{definition}{Lazard's projection operator}{lazard-projection}
	Let $P \subset \Z[\overline{x}]$ be a set of polynomials, $\Content(P)$ their content, and $P'$ the finest square-free basis of their primitive parts $\Primitive(P)$. \defindex{projection-lazard} is defined as follows:
	\begin{align*}
		Proj_{Lazard}(P) =& \{ \Lcoeff(p), \Tcoeff(p), \Disc(p) \mid p \in P' \} \\
		& \cup \{ \Res(p,q) \mid p,q \in P' \} \cup \Content(P)
	\end{align*}
\end{definition}

Similar to the projection operators due to McCallum and Brown, a problem -- in the correctness proof -- arises if we substitute a partial sample point into a polynomial to compute new sample points and this polynomial vanishes identically on this partial sample point. The solution to this, at least for Lazard's projection operator, is rather simple at first glance. We substitute one variable at a time (say $x \mapsto \alpha_x$) and if the polynomial vanishes identically we know that $(x - \alpha_x)$ divides the polynomial $q$. In this case we simply replace $q$ by $q / (x - \alpha_x)$ and continue from there.
We discuss the details of this approach in \cref{ssec:cad:lifting:lazard}.

\subsection{Relation between projection operators}\label{ssec:cad:projection:comparison}

We have presented five different, though roughly similar, projection operators which naturally yields the questions of how they relate to each other.
We give some brief results on their qualitative relation of the resulting sets of polynomials and the (theoretical) applicability in a complete way, as well as a quantitative comparison of the resulting projection sets.
We acknowledge that there are further questions of possible interest, ranging from a comparative analysis of their mathematical background and motivation over a more detailed quantitative comparison to the actual impact on the overall performance for a practical solver that we do not consider here.

We observe that $Proj_{Hong}(P) \subseteq Proj_{Collins}(P)$ as the only difference is that Hong's projection operator only includes $psc_k(p,q)$ for $q \in P$ instead of $q \in RED(P)$, and we know that $P \subseteq RED(P)$.
The case is even easier for the projection operators based on McCallum. $Proj_{Brown}(P)$, $Proj_{Lazard}(P)$, and $Proj_{McCallum}(P)$ only differ in which coefficients are included: we have the leading coefficient for $Proj_{Brown}$, leading and trailing coefficient for $Proj_{Lazard}$, and all coefficients for $Proj_{McCallum}$.

To relate $Proj_{Hong}(P)$ and $Proj_{McCallum}(P)$, we note that the first principal subresultant coefficient \emph{is} the resultant -- this is why we can use the principal subresultant coefficients to compute the resultant in the first place -- and thus we always have $\Res(p,q) \in PSC(p,q)$. Combined with how we defined the reducta set, this almost immediately yields that $Proj_{McCallum}(P)$ is included in $Proj_{Hong}(P)$.
Thus, we get
\begin{align*}
	Proj_{Brown}(P) &\subseteq Proj_{Lazard}(P) \\
	&\subseteq Proj_{McCallum}(P) \\
	&\subseteq Proj_{Hong}(P) \\
	&\subseteq Proj_{Collins}(P) \\
\end{align*}

Recall, however, that $Proj_{Brown}$ and $Proj_{McCallum}$ have the important unpleasant property that they may be incomplete on certain examples, even if these are rather pathological. In~\cite{Lazard1994}, alongside $Proj_{Lazard}$, a (slightly) modified lifting procedure was introduced that resolves any incompleteness issues with $Proj_{Lazard}$ -- and thereby also $Proj_{McCallum}$, at least if the trailing coefficient is retained and not removed as proposed in~\cite{Seidl2003} -- which we discuss in \cref{ssec:cad:lifting:lazard}.
$Proj_{Brown}$ still suffers from other sources of incompleteness, though.

This mix and match approach to combining projection operators, special techniques (like equational constraints as shown in \cref{ssec:cad:equational-constraints}), and changes to the lifting process allows to quickly come up with interesting variants of CAD.
It is, however, important to keep in mind that the theoretical foundation for the correctness hangs by a thread: while it is oftentimes retained by rather simple arguments, it can be invalidated by seemingly innocent changes.
For example, $Proj_{McCallum}$ can be used to form a complete CAD when we employ Lazard's lifting scheme. However, while removing coefficients as proposed in~\cite{Seidl2003} for $Proj_{McCallum}$ is ``safe'' (it stays ``as incomplete'' as it is), it destroys the theoretical foundation of $Proj_{Lazard}$ and thereby the reasoning why Lazard's lifting scheme makes $Proj_{McCallum}$ complete as well.

The above discussion points to three possible directions for future research on projection operators:
\begin{enumerate*}
	\item finding new projection operators that are sound without an adapted lifting, most probably between Hong's and McCallum's projection operators,
	\item exploiting special cases or specific structures of constraints and formulae like equational constraints that we discuss in the following or
	\item employ new basic building blocks that replace (sub-)resultants.
\end{enumerate*}
Current work mostly focuses on the second one, as research on CAD, in general, seems to focus more on integrating CAD into specific applications recently. After all, the whole work of this thesis arguably falls into this category.

While we performed some more extensive analysis about the shape and size of projections for different projection operators in~\cite{Viehmann2016,Viehmann2017}, we give a brief overview here to get a rough feeling for the magnitude of differences in size. In \cref{fig:cad:full-projection-sizes}, we show the results of computing a \emph{full projection} of \emph{all polynomials} within a formula for all nonlinear real formulae from \SMTLIB and extracting the overall number of polynomials (if possible within at most \SI{30}{\minute} and \SI{8}{\giga\byte}). While this estimate is rather pessimistic -- usually the Boolean structure does not require having all polynomials in a CAD at once -- it gives a pretty good handle on the approximate size.

\begin{figure}
	\centering
	\includetikz{05a-full-projection-sizes}
	\caption{Overall projection size}\label{fig:cad:full-projection-sizes}
\end{figure}

Following the previous discussion, the results in~\cref{fig:cad:full-projection-sizes} show that the overall sizes of the projection sets follow a strict ordering.
Also, the distances between the individual curves roughly match our expectation: both moving from Collins' to Hong's and from Hong's to McCallum's projection operator makes a significant difference while McCallum's, Lazard's, and Brown's projection operators are pretty close to each other.

As we discuss at various places, for example, in~\cite{Viehmann2017,Kremer2020}, having a smaller projection in terms of the number of polynomials does not automatically make the overall solving process faster.
For example, it may be beneficial to have additional polynomials with smaller degrees that are preferred for the lifting.
Additionally, the actual run time depends on several other factors that have nothing at all to do with the projection.
Some experimental results on the impact of different projection operators in practice are given in \cref{sec:cadSMT:projection-operators}.

\begin{figure}
	\centering
	\includetikz{05a-full-projection-stats}
	\caption{Polynomial degrees of projections}\label{fig:cad:full-projection-poly-degrees}
\end{figure}

We show another indicator in \cref{fig:cad:full-projection-poly-degrees}, namely the sum of the degrees of all polynomials.
For comparison with the input problems, the dotted line shows the sum of the degrees of all input polynomials.
As we can see, the number of polynomials, as well as their degrees, starts growing quickly at some point although the degree of the input stays comparably small. Hence the steep growth is not due to large input problems, but rather indicates an ``algebraic hardness'' of some kind.

Altogether, these observations roughly match conventional wisdom about CAD, combined with our analysis of the benchmark set:
many problems can be solved reasonably fast, but it is not uncommon to see (near) worst-case behavior in practice.
Also keep in mind, that this only considers two contributing factors -- the number of polynomials and their degree -- while we have not looked at coefficient growth and how these effects multiply once we use these polynomials to construct algebraic numbers in the lifting.

\subsection{Equational constraints}\label{ssec:cad:equational-constraints}

We observer that the aforementioned projection operators solely work on polynomials and completely ignore the sign conditions attached to the input polynomials.
They provide everything needed to produce a \emph{sign-invariant CAD} for the input polynomials, though we usually construct a CAD with respect to input \emph{constraints}, or even a formula containing constraints.
In these cases it is sufficient to construct a CAD that is \emph{truth-invariant} (or \emph{truth-table-invariant}) with respect to the input constraints.

In general, we can avoid lifting partial sample points that falsify the input formula as described in~\cite{Hong1990}, but this, unfortunately, does nothing to simplify the projection.
Collins found a way to do exactly that in~\cite{Collins1998} by exploiting \emph{equational constraints} -- equations that are implied by the input formula.
Essentially, we not only avoid lifting sample points that falsify an equation but also skip certain projection steps whose results can not contribute to a satisfying sample for this equation.

While Collins's proposal in~\cite{Collins1998} contains the important ideas but is somewhat vague on the details, McCallum subsequently provided correctness proofs and more details in~\cite{McCallum1999} and~\cite{McCallum2001}.
Let $p$ be the polynomial of an equational constraint, then the fundamental observation is that we only need to work on the cells that satisfy $p = 0$ and can safely ignore cells where $p \neq 0$ -- and in particular higher-dimensional cells and cell boundaries within these.

Considering the role of the individual components of the projection operators as described in \cref{ex:projection-components}, Collins essentially observed the following: the projection of $p$ alone is sufficient to describe the cells where $p = 0$ and considering the resultants of $p$ and the remaining polynomials also allows us to take care of all intersections of these other polynomials with $p$. This immediately motivates the \emph{restricted projection} as defined in~\cite{Collins1998} and analyzed more closely in~\cite{McCallum1999} and~\cite{McCallum2001}.

\begin{definition}{Restricted projection operator}{restricted-projection}\silentdefindex{projection:restricted}
	Let $P \subset \Z[\overline{x}]$ be a set of polynomials, $\Content(P)$ their content, and $P'$ the finest square-free basis of their primitive parts $\Primitive(P)$. Furthermore, let $E \in P'$ be an equational constraint polynomial. The \emph{restricted projection operator} is defined as follows:
	\begin{align*}
		Proj_{restricted}(P) =& \Content(P) \cup \Coeffs(E) \cup \{ \Disc(E) \} \cup \bigcup_{p \in P'} \Res(E,p)
	\end{align*}
\end{definition}

Note that the \emph{restricted projection operator} is based on McCallum's projection operator, but can be defined analogously for all projection operators defined above (with the exception of Collins' first projection operator).
The rigorous proof in~\cite{McCallum1999} works for up to three dimensions only, but~\cite{McCallum2001} generalizes to more dimensions with a small caveat. For any dimension, we can safely use the \emph{restricted projection operator} for the first and the last projection step. For all projection steps between, however, we need to use the \emph{semi-restricted projection operator} instead. Furthermore, we can only use the (semi-) restricted projection operator if it has already been used for all preceding dimensions. \emph{Interruptions} -- using a regular projection operator at some point and continuing using the restricted one again in a lower dimension -- are technically possible, but not shown to be sound.

\begin{definition}{Semi-restricted projection operator}{semi-restricted-projection}\silentdefindex{projection:semi-restricted}
	Let $P \subset \Z[\overline{x}]$ be a set of polynomials, $\Content(P)$ their content, and $P'$ the finest square-free basis of their primitive parts $\Primitive(P)$. Furthermore let $E \in P'$ be an equational constraint polynomial. The \emph{semi-restricted projection operator} is defined as follows:
	\begin{align*}
		Proj_{semi}(P) =& \Content(P) \cup \Coeffs(E) \cup \bigcup_{p \in P'} \left( \Disc(p) \cup \Res(E,p) \right)
	\end{align*}
\end{definition}

The possibility to use the (semi-) restricted projection operator, of course, depends on the \emph{existence} of an equational constraint in the \emph{correct dimension}. While there is nothing we can do if no equational constraint is present, Collins already presented a way to exploit two equational constraints in the \emph{same dimension} by using them to infer an equational constraint in a lower dimension -- that is not part of the input.

Let $E_1 = 0$ and $E_2 = 0$ be two equational constraints in the same dimension. We observe that every satisfying sample point must thus satisfy $E_1 = 0$ and $E_2 = 0$, and, furthermore, that the resultant of two polynomials describes the \emph{common roots} of these polynomials.
Hence, $\Res(E_1, E_2) = 0$ can be used as an equational constraint, something that Collins calls the \emph{resultant rule} in~\cite{Collins1998}.

\begin{definition}{Resultant rule}{resultant-rule}\silentdefindex{resultant-rule}
	Let $E_1 = 0, E_2 = 0$ be two equational constraints and $x$ some variable. Then
	\[
		(E_1 = 0 \land E_2 = 0) \implies \Res_x(E_1, E_2) = 0
	\]
	and, in particular, $\Res_x(E_1, E_2)$ can be used as an equational constraint in the (semi-) restricted projection.
\end{definition}

Note that using the (semi-) restricted projection operator in some dimension reduces the (asymptotic) size of the resulting set of polynomials from quadratically many (due to the pairwise resultants) to linearly many polynomials. This essentially moves the complexity of CAD due to the number of polynomials from $2^{2^n}$ to $2^{2^{n-1}}$, as shown in~\cite{England2015}.
The practical effect of the usage of equational constraints in the context of SMT solving has been studied in~\cite{Haehn2018a} and~\cite{Haehn2018b}, though the resultant rule has not been considered there.

We observe that the \emph{resultant rule} is not specific to using equational constraints in CAD projections, but is a general scheme to infer additional equalities from a given set of equality constraints. However, it needs some \emph{guidance} which variable to compute the resultant for, which is why we can nicely integrate it in a CAD method that inherently works with respect to some variable ordering.

The theory about equational constraints that we have discussed here is only proven to be sound for McCallum's projection operator. Though it is generally expected that it can be transferred to the other projection operators, no formal proof has been given so far.
Some first results to do exactly this for Lazard's projection operator have been presented in~\cite{Nair2019} that justify a reasonable hope that the restricted projection can be used as a generic template for all projection operators.
In our implementation, the restricted projection operator is exactly that: a generic template that can be used for all projection operators, possibly even with interruptions and using the restricted projection operator for all dimensions, disregarding all warranted concerns on the formal correctness of the implementation.

\subsection{More projection operators}

\silentdefindex{other-projections}
Besides the (more or less) well-known projection operators presented above, other projection operators have been proposed that we consider less relevant here, mostly because they are not meant for constructing a general cylindrical algebraic decomposition.
All of the projection operators below are instead targeted at special applications like open CAD, CAD for parameterized systems, or the generation of single CAD cells.

\subsubsection{Projection for open CAD\label{ssec:cad:open-cad}}

\silentdefindex{open-cad}
For some applications it is sufficient to only consider \emph{open regions} -- regions that have full dimension -- and discard regions where a polynomial vanishes that have \emph{volume zero} (in the sense of the Lesbesgue measure).
The reasoning is usually that open regions capture \emph{almost all} solutions, or that such regions where a polynomial vanishes represent a \emph{tipping point} that is either unstable or at the edge of the permissible parameter space, and thus not of interest.
This variant of CAD is sometimes called \emph{open CAD} and we refer to~\cite{McCallum1993,Strzebonski2000} for a more thorough discussion.

Most importantly, open CAD allows for two significant simplifications compared to a regular CAD computation.
Firstly, as we discard the sample points that correspond to polynomial roots, all our sample points are rational and thus all the difficulties concerning the handling of and working with real algebraic numbers disappear.
Secondly, all the issues that cause incompleteness for some of the projection operators manifest over regions where a polynomial vanishes and we can thus greatly simplify the projection operator while retaining completeness as shown in~\cite{Strzebonski2000}.

\begin{definition}{Strzebo\'{n}ski's projection operator for open CAD}{strzebonski-projection-open-cad}
	Let $P \subset \Z[\overline{x}]$ be a set of polynomials, $\Content(P)$ their content, and $P'$ the finest square-free basis of their primitive parts $\Primitive(P)$. \emph{Strzebo\'{n}ski's projection operator for open CAD} is defined as follows:
	\begin{align*}
		Proj_{StrzOpen}(P) =& \{ \Lcoeff(p), \Disc(p) \mid p \in P \} \\
		& \cup \{ \Res(p,q) \mid p,q \in P \}
	\end{align*}
\end{definition}

\subsubsection{Seidl and Sturm's projection operator}

Seidl and Sturm in~\cite{Seidl2003} make the case that for the application of general quantifier elimination we can essentially add assumptions to our formula in an ad-hoc manner. We explore their reasoning and the resulting simplifications in the following.

When doing general quantifier elimination, we assume a quantified formula $\varphi$ from which we want to eliminate some variables $\overline{x}$ while we keep other variables $\overline{y}$. We usually call $\overline{y}$ \emph{parameters}.
We would apply CAD such that we project $\overline{x}$ first and extract a description of the solutions of $\varphi$ in $\overline{y}$ only after lifting $\overline{y}$.

The authors argue that it is sensible to allow our projection to assume certain polynomials in $\overline{y}$ \emph{not to vanish}, as these assumptions directly correspond to ``easily interpretable'' \emph{theory assumptions} that the user had assumed anyway in most cases.
These theory assumptions are generated in an ad-hoc manner to validate the following simplified projection operator based on Hong's projection operator.

\begin{definition}{Seidl and Sturm's projection operator}{seidl-sturm-projection}
	Let $P \subset \Z[\overline{x}]$. \emph{Seidl and Sturm's projection operator} is defined as follows:
	\begin{align*}
		Proj_{S\&S}(P) =& \bigcup_{p \in GRED(P)} (\{ \Lcoeff(p) \} \cup GPSC(p,p')) \\
		&\cup \bigcup_{\substack{p,q \in P \\ p < q}} \bigcup_{p^* \in GRED(p)} GPSC(p^*,q)
	\end{align*}
	where we refer to~\cite{Seidl2003} for the exact definitions of $GRED$ and $GPSC$.
\end{definition}

The essential idea is to \emph{stop} collecting the reducta for the reducta set as soon as the leading coefficient is only defined over $\overline{y}$, and, equivalently, do the same for the principal subresultant coefficients. These reducta (or principal subresultant coefficients) are then only defined over $\overline{y}$, and the authors argue that they should not vanish due to the implicit theory assumptions of the user.

Note that the underlying idea has already been discussed for Hong's projection operator, but we could only exploit it when we could show that the respective coefficient would not vanish -- for example, for constants.

In the context of general quantifier elimination, this approach provides the user with two pieces of information. Firstly, a list of the automatically generated algebraic assumptions, and secondly, a formula that is equivalent to the input formula under these assumptions.
The authors argue that the assumptions are ``easily interpretable'' and their examples suggest that they mostly characterize degenerate cases that the user did not care for anyway.

For the purpose of SMT solving, however, we aim at determining satisfiability for a specific formula in a fully automated way. Handing assumptions back to the user during the solving is not desirable here, because this might happen very frequently -- for every theory call -- and there may not even be a user involved. Hence, we do not consider this projection operator in this work.


\subsubsection{Local projections}

\silentdefindex{local-projections}
In some applications, we may only want to look at a very specific part of a CAD -- usually, a single region that is identified by a given sample point. In such a scenario, it seems natural to remove polynomials from the projection that do not contribute to the ``interesting'' part of the CAD.

The question of generating \emph{a generalization} or \emph{an abstraction} of a single point to a single CAD cell was first motivated in~\cite{Jovanovic2012} -- a special case of the \MCSAT approach that we present in \cref{ch:mcsat} -- but was adopted as an interesting question in general, for example, in~\cite{Brown2013,Strzebonski2014,Brown2015}.

Multiple variants of \emph{local projections} have been proposed with various benefits and downsides, and we only give a rough overview here. A more thorough description of the ones we use in \cref{ch:mcsat} is given there.
Note that most of the lifting phase is essentially skipped here because the partial assignment is already given. We only need to ``lift'' a single sample point in every dimension to obtain the borders of the cell we want to construct.

\paragraph{Jovanović and de Moura's model-based projection operator.}
The proposal of \NLSAT in~\cite{Jovanovic2012} -- the instantiation of \MCSAT with a CAD-based explanation function -- sparked the need for a method that \emph{explains why a partial assignment is unsatisfiable}. One possible implementation, the one proposed in~\cite{Jovanovic2012}, is using a CAD-based method to obtain the region that contains the partial assignment.

Their version of a local projection essentially takes Collins' projection operator and for every \emph{component} -- that is the coefficients, principal subresultant coefficients of a polynomial and its derivative, and the pairwise principal subresultant coefficients -- only considers the first one that does not vanish on the given partial assignment.

\paragraph{Brown and Ko\v{s}ta's single-cell method.}
With~\cite{Brown2013}, Brown departs from the traditional separation into projection and lifting, but instead understands the problem as \emph{refining the cell} incrementally using the input constraints, where every refinement is done by adding a polynomial to the projection. In this approach, the projections may be smaller and also the region may be larger -- conceivably beneficial for \NLSAT that can exclude a larger region from its search space.
The original method from~\cite{Brown2013} only considers the case of open cells, but~\cite{Brown2015} extends it to work for all cells -- at the cost of various special cases and additional algorithmic complexity.

\paragraph{Strzebo\'{n}ski's local projection operator.}
Given the above ideas, Strzebo\'{n}ski proposed another projection operator in~\cite{Strzebonski2014} that adaptively uses either McCallum's or Hong's projection operator, depending on whether a source of incompleteness was detected while using McCallum's projection operator. Additionally, it is complemented with the use of equational constraints.
