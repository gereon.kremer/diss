\section{Lifting}\label{sec:cad:lifting}

\silentindex{cad:lifting}
Recall that we aim to construct a CAD and decided to represent a single cell using what we called a \emph{sample point}.
This sample point is a \emph{representative} for a cell, as the cell is \emph{sign-invariant} on the set of input polynomials (or at least \emph{truth-invariant} under the input formula).
We already devised \emph{projection operators} that project certain properties of our CAD into lower dimensions and now describe how we can effectively construct a set of sample points that finally represent a CAD.

The fundamental intuition is the following: given a $k$-dimensional CAD $C_k$, every cell $C \in C_k$ induces a cylinder $C \times \R$ which is separated in $(k+1)$-dimensional cells by the roots of the $(k+1)$-dimensional polynomials in \emph{a nice way} -- they are \emph{delineable} as described in \cref{ssec:cad:cylindricity-delineability}. Having delineable cells yields that it does not matter which $k$-dimensional point we select from a cell, they all yield an equivalent set of $(k+1)$-dimensional sample points.
The $(k+1)$-dimensional CAD $C_{k+1}$ consists of exactly these $(k+1)$-dimensional cells over every $k$-dimensional cell from $C_k$, represented by $(k+1)$-dimensional sample points.

Furthermore, delineability (and cylindricity of the cells) implies a particularly nice relation between $k$-dimensional and $(k+1)$-dimensional sample points.
Given $s_k \in C \in C_k$, we can construct sample points $\overline{s_{k+1}} \in C \times \R$ by simply \emph{appending} certain values for dimension $k+1$ to $s_k$, and these values are derived from the roots of the $(k+1)$-dimensional polynomials of the projection.

Our goal is to select one sample point from each sign-invariant region of the polynomials over some $C \in C_k$ represented by some sample point $s_k$.
As we have already identified the roots of polynomials as \emph{boundaries} of these sign-invariant regions, the selection is rather natural:
apart from the roots themselves -- representing all regions where some polynomial vanishes -- we select one sample point between every two consecutive roots, as well as one value below the smallest root and above the largest root.

\begin{definition}{Lifting operator}{lifting-operator}\silentdefindex{cad:lifting}
	Let $P \subset \Q[x_1, \dots x_{k+1}]$ be a set of polynomials and $s_k$ a $k$-dimensional sample point.
	The result of the \emph{lifting operator} of $P$ with $s_k$ is the following set of $(k+1)$-dimensional sample points where $\alpha_1, \dots \alpha_m$ are the (ordered) real roots of $P$ with respect to the model induced by $s_k$.
	\[
		\{ s_k \} \times \{ \alpha_1, \dots \alpha_m, r_0, \dots p_m \mid r_0 < \alpha_1 < r_1 < \alpha_2 < \cdots < \alpha_m < r_m \}
	\]
	We sometimes call $\alpha_i$ the \emph{root samples} and $r_i$ the \emph{non-root samples} or \emph{intermediate samples} of $s_k$ over $P$.
\end{definition}

The delineability of $P$ ensures that all $s_k$ from a particular $k$-dimensional cell yield an equivalent -- though not necessarily identical -- result in the sense that the number or roots $m$ remains constant, the $\alpha_i$ from each $s_k$ belongs to the same surface, and the $p_i$ for every $s_k$ belongs to the same open sign-invariant region. We can also see this graphically in the following \cref{ex:cad-lifting}.

\begin{example}{CAD lifting}{cad-lifting}

	\begin{center}
		\includetikz{05b-lifting-samples}
	\end{center}

	Recall the polynomials $p = {(y+1)}^2 - x^3+3x-2$ and $q = (x + 1) \cdot y - 3$ from \cref{ex:projection-components}. We focus on the one-dimensional cell $(1,2)$ in the $x$-dimension and the cylinder above it. We chose $x = 1.5$ as indicated by a vertical line.

	To lift it, we substitute $x = 1.5$ into all polynomials and obtain $p[x/1.5] = {(y+1)}^2-0.875$ and $q[x/1.5] = 2.5 \cdot y - 3$. The roots of these polynomials are $-0.065$, $-1.94$, and $1.2$, respectively, indicated by the red crosses. Note that by construction, these are exactly the intersections of the vertical line and the polynomials' varieties.
	
	Assume we instead lift other values for $x$, as indicated by the paler lines. Though the resulting sample points change (in general the intermediate sample points might change as well), there is a direct one-to-one correspondence to the sample points for every other value for $x$.
	Observe that the roots converge (and eventually collapse) when we move towards the cylinder boundary $x = 1$, thus changing the number of roots and posing a boundary to the region to maintain delineability.
\end{example}

\subsection{Incompleteness of projection operators}\label{ssec:cad:lifting:incompleteness}

We already discussed that some projection operators are what we called ``incomplete'' for certain inputs.
While it is not that easy to construe an example that witnesses incompleteness (by provoking an incorrect result) -- after all, we need at least four variables -- it is reasonably easy to get a rough feeling for the fundamental problem.

The main idea is that under special circumstances we may \emph{lose roots} during the lifting because a polynomial \emph{vanishes identically}. If we lift $x = 0$ with $p = x \cdot y$ we obviously ``lose'' the root $y = 0$ because $p[x/0]$ vanishes identically. Of course, this example would not lead to incorrect results, simply because it actually \emph{is irrelevant} how we choose $y$ for $x = 0$ in this example.
More complex examples exist, however, where roots are lost in this way and consequently we fail to compute a full CAD and may return incorrect results.
Note, however, that this case is very rare in practice: for a regular SMT strategy we have not observed any incorrect results due to this issue; the \MCSAT approach discussed later, in particular the approach presented in \cref{ssec:mcsatimpl:explanation:cad}, however, triggered this issue in our experiments.

It may be interesting to realize that the underlying mathematical effects and their consequences for the respective proofs of correctness differ for different projection operators: the term ``well-orientedness'' -- and in particular the scenarios that break it -- are substantially different in~\cite{McCallum1984,McCallum1988} and in~\cite{Brown2001}, and for equational constraints in~\cite{McCallum1999,McCallum2001} the notion is different once again.
Essentially it seems that ``well-orientedness'' is always used for ``everything is fine'' for whatever this means for the projection operator at hand.

This is also reflected by how these issues can be resolved or at least mitigated.
For McCallum's projection operator, we can oftentimes resolve the issue by adding so-called \emph{delineating polynomials}, for example, based on partial derivatives of the vanishing polynomial.
While the modified lifting process due to Lazard -- which we discuss in the following \cref{ssec:cad:lifting:lazard} -- resolves these cases even without the need for such \emph{delineating polynomials}, Brown's projection operators adds additional sources of incompleteness.

\subsection{Lazard's lifting process\label{ssec:cad:lifting:lazard}}

\silentdefindex{cad:lifting-lazard}
As already mentioned in \cref{ssec:cad:projection:lazard} and presented in~\cite{Lazard1994}, Lazard's projection operator requires a change to the regular lifting process to maintain correctness.
We have seen that for the regular lifting we take a partial assignment over $\{x_1, \dots, x_k\}$ and a polynomial $q$ over $\{ x_1, \dots, x_{k+1}\}$ and consider \emph{isolating the real roots} of $q$ in $x_{k+1}$ with respect to the partial assignment as a \emph{single, atomic} operation.

Unfortunately, $q$ may vanish identically on the partial assignment, thus leaving no polynomial -- or, more specifically, the zero polynomial -- to compute the roots of.
This is the underlying reason for the incompleteness of \index{projection-mccallum} and \index{projection-brown} that we discussed in \cref{ssec:cad:lifting:incompleteness}.

The proposed modification to the lifting aims to directly avoid this \emph{nullification}: we substitute the assignment $\alpha_x$ for every $x \in \{x_1, \dots, x_k\}$ \emph{individually} and check whether the polynomial already vanishes identically.
If so, we know that $x - \alpha_x$ divides the polynomial and we replace $q$ by $q / (x - \alpha_x)$, as shown in \cref{algo:lift-lazard-abstract}.
Ultimately, we thereby discover real roots of all factors of $q$, even if they would usually be ``shadowed'' by other factors that vanish identically over the given variable assignment.
The order in which we consider the variables is crucial for the correctness of the whole procedure and needs to coincide with the variable ordering or the current CAD computation, following~\cite{McCallum2016,McCallum2019}.

\begin{algorithm}[ht]
	\caption{Lifting with Lazard's projection}\label{algo:lift-lazard-abstract}

	\SetKwFunction{LazardLifting}{LazardLifting}
	\Fn{\LazardLifting{$\alpha$, $q$}}{
		\For{$x \mapsto \alpha_x \in \A$}{
			\While{$q[x/\alpha_x] \equiv 0$}{
				$q \coloneqq q / (x - \alpha_x)$ \;
			}
			$q \coloneqq q[x/\alpha_x]$ \;
		}
		\Return{$\Roots(q)$}
	}
\end{algorithm}

As already discussed in \cref{sec:preliminaries:ran}, we can not easily compute with real algebraic numbers (unless we work in some powerful computer algebra system). \cref{algo:lift-lazard-abstract} suggests to compute with polynomials whose coefficients stem from some extension field $\Q(\alpha)$ to perform the polynomial division, but it is not immediately clear how to properly work with such coefficients.
Just like in \cref{sec:preliminaries:ran}, we exploit that $\Q(\alpha)$ is isomorphic to $\Q[\xi]/\langle p_\alpha \rangle$ and perform these operations in $\Q[\xi]/\langle p_\alpha \rangle[x]$ instead, representing $\alpha$ symbolically in our polynomials using a fresh variable $\xi$.

Note that we only care about whether $q$ \emph{vanishes identically} and it is thus sufficient to consider what we called \emph{algebraic cancellation} in \cref{sec:preliminaries:ran} in the loop of \cref{algo:lift-lazard-abstract}.
Hence, we only need to slightly extend \cref{algo:ran:algebraic-cancellation} to adapt \cref{algo:lift-lazard-abstract} as shown in the following \cref{algo:lift-lazard}.

Instead of directly constructing the next extension field like in \cref{algo:ran:algebraic-cancellation:qprime} of \cref{algo:ran:algebraic-cancellation}, we need to split this process into multiple steps.
We check whether $q$ \emph{would vanish identically} over the \emph{next} extension field $\Q^*[\xi_x]/\langle f \rangle$ but perform the division over the \emph{current} extension field $\Q^*$.
Only when $q$ no longer vanishes identically (over $\Q^*[\xi_x]/\langle f \rangle$), we actually update $\Q^*$ to move to the next extension field in \cref{algo:lift-lazard:qprime,algo:lift-lazard:embed}.


\begin{algorithm}
	\caption{Lifting with Lazard's projection}\label{algo:lift-lazard}

	\SetKwFunction{Lazard}{Lazard}
	\Fn{\Lazard{$\A$, $q$}}{
		$\Q^* \coloneqq \Q$, $\A^* \coloneqq \emptyset$ \;
		\For{$x \mapsto \alpha_x \in \A$}{
			Let $p_x$ be the minimal polynomial of $\alpha_x$ \Comment*[r]{$p_x$ in variable $\xi_x$}
			Let $f \in \Factors(p_x, \Q^*[\xi_x])$ such that $f(\A^*, \xi_x \mapsto \alpha_x) = 0$ \;
			\If{$f$ is linear in $\xi_x$}{
				Substitute $\xi_x - f$ for $x$ in $q$ \;
			}
			\Else{
				Let $q \coloneqq q[x/\xi_x]$ \;
				\While{$id_{\Q^*[\xi_x]/\langle f \rangle}(q) \equiv 0$}{
					Let $q \coloneqq q / p$ \Comment*[r]{Calculation in $\Q^*$}
				}
				\nllabel{algo:lift-lazard:qprime} Let $\Q^* \coloneqq \Q^*[\xi_x]/\langle f \rangle$ \;
				\nllabel{algo:lift-lazard:embed} Let $q \coloneqq id_{\Q^*}(q)$ \Comment*[r]{Embedding $q$ into new $\Q^*$}
				Let $\A^* \coloneqq \A^* \cup \{ \xi_x \mapsto \alpha_x \}$ \;
			}
		}
		\Return{$\Roots(q, \A^*)$}
	}
\end{algorithm}

Given that \cref{algo:lift-lazard} is one of the main building blocks of a CAD implementation, we should try to make it as efficient as possible in practice.
First of all, we can greatly simplify the process if $\alpha_x$ is rational.
Another interesting idea is to cache the sequence of extension fields for consecutive lifting steps on the same variable assignment or variable assignments with a common prefix. This of course only works if a static variable ordering is used.

It seems tempting to try to modify the variable ordering, hoping to simplify the algebraic operations. For example, one could try to substitute rational assignments first, leaving polynomials with less variables for computationally more expensive algebraic operations. However, the whole theory behind the modified lifting procedure relies on a single static variable ordering.
We can directly see that modifying the variable ordering changes the results in the following -- not overly contrived -- \cref{ex:Lazard:changing-variable-ordering}.

\begin{example}{Different variable orderings}{Lazard:changing-variable-ordering}
	Let $\A = \{x_1 \mapsto 0, x_2 \mapsto 0\}$ be the current variable assignment and $p = x_1^2 x_2 x_3 + x_1 x_2^2 (x_3 - 1)$ the polynomial we want to use for lifting.
	We now use the modified lifting procedure due to Lazard with the two different variable orderings $x_1 < x_2 < x_3$ and $x_2 < x_1 < x_3$. In both cases, we aim to extend the sample point represented by $\A$ to the third dimension.

	For $x_1 < x_2$, we first consider $p$ with respect to $x_1 \mapsto 0$, realize that $p$ vanishes identically and divide by $x_1$ to obtain $x_1 x_2 x_3 + x_2^2 (x_3 - 1)$. After substitution, we have $x_2^2 (x_3 - 1)$ and substituting $x_2$ in the same way leaves us with $x_3 - 1$ such that we obtain $1$ as the single real root of $p$ in $x_3$ over $\A$.

	For $x_2 < x_1$, we first consider $p$ with respect to $x_2 \mapsto 0$ and see that $p$ vanishes identically as well. We divide by $x_2$ to obtain $x_1^2 x_3 + x_1 x_2 (x_3 - 1)$. Substitution leaves the other part of the polynomial which is $x_1^2 x_3$ and substituting $x_1$ in the same way results in $x_3$. Now, the single real root of $p$ over $\A$ is $0$.
\end{example}

\cref{ex:Lazard:changing-variable-ordering} shows that the results of the modified lifting scheme depend on the variable ordering.
The underlying theory from~\cite{Lazard1994,McCallum2016,McCallum2019} only guarantees correctness if the variable ordering used by the entire CAD is employed, and we thus need to stick to the single static variable ordering.
