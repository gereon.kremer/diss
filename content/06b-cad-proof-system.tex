\section{Proof system\label{sec:cadSMT:proof-system}}

\silentdefindex{cad:proof-system}\silentdefindex{proof-system:cad}
The incremental nature of the CAD method proposed here heavily relies on postponing operations by putting them in a queue and trying to terminate without executing all operations that are still enqueued.
Following our observation from \cref{ex:projection-components}, we decompose the projection into \emph{projection steps}, while the lifting is naturally separated into \emph{lifting steps}, as defined in the following.

\begin{definition}{Projection step}{cad:projection-step}\silentdefindex{cad:projection-step}
	Let $P$ be a set of polynomials and $Proj$ some CAD projection operator.
	We call the execution of $Proj(\{p,q\})$ with $p,q \in P$ a \emph{projection step}.
	We denote a \emph{projection candidate} -- representing a future, not yet executed, projection step -- by $(p,q)$ and call it \emph{single} (\emph{paired}) if $p = q$ ($p \neq q$).
\end{definition}

Note that (almost) all the projection operators we presented in \cref{sec:cad:projection-operators} naturally decompose into a sequence of projection steps.
It is important to realize that the notion of projection candidates immediately leads to an incremental building of the projection as executing (or evaluating) projection candidates (usually) yields new polynomials that then give rise to new projection candidates.

\begin{definition}{Lifting step}{cad:lifting-step}\silentdefindex{cad:lifting-step}
	Let $s$ be a (partial) sample point of dimension $k$ and $p$ a polynomial (from the projection) of dimension $k+1$.
	In particular, we also allow $k=0$ and thus $s$ be the \emph{empty sample point}.
	We call the process of substituting $s$ into $p$ and using the real roots of the resulting polynomial to extend $s$ to a set of $(k+1)$-dimensional sample points a \emph{lifting step}.
	Similar to a projection candidate, we call $(s,p)$ a \emph{lifting candidate}, representing a future, not yet executed, lifting step.
\end{definition}

Note that the generation of $(k+1)$-dimensional sample points needs to be performed with respect to the already existing sample points over $s$.
As we assume that the sample points are stored within some tree structure, the existing sample points over $s$ are easily accessible without additional overhead.

As for projection candidates, the notion of lifting candidates immediately yields an incremental method to compute the lifting.
Making both the projection and lifting incremental yields a system that always has to choose
\begin{enumerate*}
	\item whether to perform a projection step or a lifting step and
	\item which projection (or lifting) step to perform.
\end{enumerate*}
We conceptually combine projection candidates and lifting candidates in a single queue to obtain maximum flexibility and discuss how to implement this heuristic in \cref{sec:cad:heuristic-choices}.

In order to properly support the removal of constraints, we also need some way to track various dependencies: which polynomials originate from a certain constraint, which polynomials are the result of which projection candidates, and which sample points were produced by lifting of which sample point with which polynomial.

As for the sample points producing other sample points by means of lifting steps, we propose to store them in a \emph{lifting tree}.
Every sample point has a parent -- the sample point that was used to construct it -- with the \emph{empty sample point} being the (unique) root and removing a sample point implicitly removes the whole subtree rooted in it. Though multiple sample points may produce the same \emph{value} on a higher dimension, we consider these different as we identify a sample point in our tree with the full $k$-dimensional variable assignment.

All other dependencies do not come in a nice tree structure: multiple different projection steps may produce the same polynomial and there is no point in storing them separately. Therefore, we propose the concept of \emph{origins} which essentially list all the reasons for a projection polynomial (or a sample point) to exist and thereby also allow to recognize when it can be removed.

\begin{definition}{Origins of polynomials and sample points}{cad:origins}\silentdefindex{cad:origins}
	Let $p$ be a polynomial and $s$ a sample point.
	We call $o$ an \emph{origin of a projection polynomial $p$} if either $o = \{p\}$ and $p \signcondition 0$ is an input constraint or $o$ is a set of polynomials and $p \in Proj(o)$.
	We call $o = \{p\}$ an \emph{origin of a sample point $s$} if $p$ is a polynomial and $s$ is the result of a lifting step involving $p$.

	We denote the set of multiple such origins for $p$ (or $s$) by $origins(\cdot)$.
	We denote adding an origin $o$ to this set by $origins(\cdot) \PlusEQ o$.
	We write $origins(\cdot) \MinEQ p$ for the removal of all origins that contain the polynomial $p$.

	If $origins(p) = \emptyset$ (or $origins(s) = \emptyset$) we say that $p$ (or $s$) \emph{has no origins}, implying that we can remove it from the state of our proof system.
\end{definition}

Intuitively, an \emph{origin} is the reason for something to exist in our proof system and \emph{origins} collect several of these. If the origins are empty, any reason for this projection polynomial (or sample point) \emph{ceased to exist} and we can (and maybe should) remove it. Note that when we remove a projection polynomial, we also need to remove this polynomial from the origins of all other polynomials (and sample points), possibly triggering a whole series of removals that descends through the levels.

Intermediate sample points are a notable exception to this intuition. Their reason to exist is not simply a polynomial, but the existence of (neighboring) root sample points. We thus do not assign origins to intermediate sample points, but instead create (and remove) them as we create (and remove) root sample points. We trust that the handling of intermediate sample points is simple enough for anyone aiming to implement this, so that we only give a rather superficial description of what needs to be done in the following proof system, in particular in the rules \rulename{Add-Sample-Finished} and \rulename{Delete-Poly-Finished}.

For the presentation of the proof system for CAD, we now fix a common notation. We denote the set of input constraints by $C$ and use $P$ and $S$ for sets of polynomials and sample points, respectively. As in the above definitions, $(p,q)$ denotes projection candidates while $(s,p)$ denotes lifting candidates and we call the queue containing all of them $Q$.

Our proof system can be in one of the following states, always starting in the default state. The Add-P (Remove-P) states are used do model some details of adding (removing) polynomials to (from) our system while Add-S does the same for adding sample points.
We have additional states SAT and UNSAT to indicate satisfiability and unsatisfiability.
\begin{align*}
	\textrm{Default:~} & \State{C,P,S,Q} && \textrm{Default state} \\
	\textrm{Add-P:~} & \State{C,P,S,Q} +_o P' && \textrm{Add polynomials $P'$ with origins $o$} \\
	\textrm{Remove-P:~} & \State{C,P,S,Q} - P' && \textrm{Remove polynomials $P'$} \\
	\textrm{Add-S:~} & \State{C,P,S,Q} +_o S' && \textrm{Add samples $S'$ with origins $o$} \\
	\textrm{SAT:~} & \State{C,P,S,Q} \vdash SAT && \textrm{Found constraints to be consistent} \\
	\textrm{UNSAT:~} & \State{C,P,S,Q} \vdash UNSAT && \textrm{Found constraints to be inconsistent} \\
\end{align*}

Note that we deliberately did not define the SAT and UNSAT states to be final. Instead, we allow our proof system to continue from there by adding new constraints or removing existing constraints to form a new set of input constraints. This, of course, requires our proof system to be able to continue from an already computed CAD -- possibly incompletely computed in the case of SAT.

Compared to most other proof systems, we allow for two extensions, namely the \emph{composition of rules} and taking \emph{external input} as described in \cref{sec:preliminaries:proof-systems}.
We define our initial state to be $\State{\emptyset,\emptyset,\emptyset,\emptyset}$ which requires adding constraints before any computation can take place.
We now present the actual proof rules, starting with the ones to add and remove constraints.
\begin{align*}
	\ProofRule{Initial}{~}{\State{\emptyset,\emptyset,\emptyset,\emptyset}}{} \\
	\ProofRule{Continue from SAT}{\State{C,P,S,Q} \vdash SAT}{\State{C,P,S,Q}}{} \\
	\ProofRule{Continue from UNSAT}{\State{C,P,S,Q} \vdash UNSAT}{\State{C,P,S,Q}}{} \\
	\ProofRule{Add-Constraint $c$}{\State{C,P,S,Q}}{\State{C \cup \{c\},P,S,Q} +_c P'}{\textrm{$P' = \{p \mid c = p \signcondition 0 \}$}} \\
	\ProofRule{Delete-Constraint $c$}{\State{C \DisjointUnion \{c\},P,S,Q}}{\State{C,P,S,Q} - P'}{\textrm{$P' = \{p \mid c = p \signcondition 0 \}$}}
	\intertext{
		These rules allow to continue from the SAT and UNSAT states, usually to modify the set of constraints and then solve the modified problem. Note that we technically allow to add and remove constraints whenever we are in the default state -- essentially anytime. Though this is possible and sound, we usually assume that constraints are only added and removed directly after we left the SAT or UNSAT state and before any other reasoning takes place.
	}
	\ProofRule{Project}{\State{C,P,S,Q \DisjointUnion \{(p,q)\}}}{\State{C,P,S,Q} +_{(p,q)} P'}{\textrm{$P' = Proj(p,q)$}} \\
	\ProofRule{Add-Poly}{\State{C,P,S,Q} +_o P' \DisjointUnion \{p\}}{\State{C,P \cup \{p\},S,Q'} +_o P'}{\textrm{$origins(p) \PlusEQ o$} \\ \textrm{$Q_P = \{(p,q) \mid q \in P \cup \{p\} \land level(p) = level(q) \}$} \\ \textrm{$Q_L = \{(s,p) \mid s \in S \land level(s) = level(p) \}$} \\ \textrm{$Q' = Q \cup Q_P \cup Q_L$}} \\
	\ProofRule{Add-Poly-Finished}{\State{C,P,S,Q} +_o \emptyset}{\State{C,P,S,Q}}{}
	\intertext{
		The execution of a projection candidate is performed by the \rulename{Project} rule which switches to the Add-P state. \rulename{Add-Poly} (leading to \rulename{Add-Poly-Finished} eventually) takes care of actually inserting the polynomials into $P$ and adding the appropriate queue entries.
	}
	\ProofRule{Lift}{\State{C,P,S,Q \DisjointUnion \{(s,p)\}}}{\State{C,P,S,Q} +_p R}{\textrm{$R = \Roots(p,s)$}} \\
	\ProofRule{Add-Sample}{\State{C,P,S,Q} +_p S' \DisjointUnion \{s\}}{\State{C,P,S \cup \{s\},Q'} +_p S'}{
		\textrm{$origins(s) \PlusEQ p$} \\
		\textrm{$Q' = Q \cup \{(s,q) \mid q \in P \land level(s) = level(q) \}$}
	} \\
	\ProofRule{Add-Sample-Finished}{\State{C,P,S,Q} +_p \emptyset}{\State{C,P,S \cup S',Q \cup Q'}}{\textrm{$S' = \textrm{missing intermediate sample points}$} \\ \textrm{$Q' = \{(s,p) \mid s \in S', p \in P \land level(s) = level(p) \}$}}
	\intertext{
		Executing a lifting step is rather similar to executing a projection step in that we immediately execute the lifting step and then process the set of root sample points that should be added.
		The intermediate sample points that need to be considered in addition to the real roots $R$ are generated with respect to the already existing sample points from $S$ and inserted after all root sample points have been processed.
		Note that the intermediate sample points have no origins themselves.
	}
	\ProofRule{Delete-Poly}{\State{C,P \cup \{p\},S,Q} - P' \DisjointUnion \{p\}}{\State{C,P,S,Q} - P' \cup P''}{\textrm{$\Forall{p \in P} origins(p) \MinEQ p$} \\ \textrm{$\Forall{s \in S} origins(s) \MinEQ p$} \\ \textrm{$P'' = \{ p \in P \mid origins(o) = \emptyset \}$}} \\
	\ProofRule{Delete-Poly-Finished}{\State{C,P,S,Q} - \emptyset}{\State{C,P,S',Q'}}{\textrm{$S_O = \{ s \in S \mid origins(s) \neq \emptyset\}$} \\ \textrm{$S_I = \textrm{ intermediate sample points}$} \\ \textrm{Prune obsolete sample points from $S_I$} \\ \textrm{$S' = S_I \cup S_O$} \\ \textrm{$Q' = Q \cap ((P \times P) \cup (S' \times P))$}}
	\intertext{
		As discussed before, the removal of polynomials is mainly organized using the origins. To remove a single polynomial $p$, we remove it from the set of projection polynomials $p$ and prune it from the origins of all other polynomials and all sample points.
		We then add all polynomials with empty origins to the set of polynomials that are still to be removed.
		Once all polynomials have been removed, we remove all sample points with empty origins together with all intermediate sample points that are now obsolete, and additionally remove all queue entries that contain a polynomial or a sample point that has been removed.\endgraf
		Remember that we implicitly remove the whole subtree of a sample point, though we denote the removal of sample points using sets here.
	}
	\ProofRule{SAT}{\State{C,P,S,Q}}{\State{C,P,S,Q} \vdash SAT}{\textrm{$\Exists{s \in S} s \models C$}}
	\intertext{
		Considering that we regard CAD as a search method for a satisfying sample, detecting satisfiability is conceptually simple.
		As soon as some sample point that satisfies all input constraints exists, we allow to switch to the SAT state immediately.
		Note that we retain the computed state in order to continue with adding (or removing) constraints.
	}
	\ProofRule{UNSAT}{\State{C,P,S,\emptyset}}{\State{C,P,S,\emptyset} \vdash UNSAT}{\textrm{$\neg\Exists{s \in S} s \models C$}}
	\intertext{
		The UNSAT rule is based on the claim that this proof system eventually produces a full CAD which we discuss in the following \cref{ssec:cad:correctness} in more detail. Once the queue is empty, the underlying CAD is complete and we can soundly conclude unsatisfiability from the absence of a satisfying sample point.
		Once again, we retain the computed state to be able to continue after adding (or removing) constraints.
	}
\end{align*}

Most simple optimizations can be integrated into this rule system, and we showcase this for the example of adding polynomials as their set of factors. We first define two additional proof rules to factorize polynomials before adding (or deleting) them.
\begin{align*}
	\ProofRule{Add-Poly-Factorized}{\State{C,P,S,Q} +_o P'}{\State{C,P,S,Q} +_o P''}{\textrm{$P' \neq P'' = \cup_{p \in P'} \Factors(p)$}} \\
	\ProofRule{Delete-Poly-Factorized}{\State{C,P,S,Q} - P'}{\State{C,P,S,Q} - P''}{\textrm{$P' \neq P'' = \cup_{p \in P'} \Factors(p)$}}
\end{align*}

The integration works by constructing a new proof system by replacing some of the old proof rules by new composed proof rules as follows:
\begin{align*}
	\rulename{Add-Constraint} & \rightarrow \rulename{Add-Constraint} \circ \rulename{Add-Poly-Factorized} \\
	\rulename{Project} & \rightarrow \rulename{Project} \circ \rulename{Add-Poly-Factorized} \\
	\rulename{Delete-Constraint} & \rightarrow \rulename{Delete-Constraint} \circ \rulename{Delete-Poly-Factorized} \\
\end{align*}

\subsection{Correctness and completeness}\label{ssec:cad:correctness}

For the presented proof system to be useful, we want it to be correct and complete.
While correctness means that the answer is correct if we terminate, (refutational) completeness ensures the existence of a finite proof for every finite input, and thus termination on every (finite) input.
For this proof system, we thus need to prove that
\begin{enumerate*}
	\item the input is satisfiable if we enter the SAT state,
	\item the input is unsatisfiable if we enter the UNSAT state, and
	\item we always eventually enter the SAT or UNSAT state.
\end{enumerate*}

We observe that the proof system allows for multiple satisfiability checks in a sequential manner. For the purpose of this proof, we only consider starting from a default state and disallowing adding or removing constraints until we enter either the SAT state or the UNSAT state.
The whole argument that follows rests on the following claim:
the internal state of the proof system -- the polynomials in $P$ and the sample points in $S$ -- \emph{converges} against a full CAD and eventually \emph{is} a full CAD, once the queue $Q$ is empty.

This claim rests on the observation that the presented proof system performs a regular CAD computation where the individual computations are merely \emph{reordered}.
If we indeed perform the very same computations -- only in a different order -- we \emph{inherit} all formal guarantees, in particular, that the problem is unsatisfiable if no satisfying sample point has been found.
Of course, we inherit all limitations as well, in particular, the incompleteness due to the projection operators of McCallum or Brown.

The basic steps of CAD are
\begin{enumerate*}
	\item compute all projection polynomials,
	\item lift all sample points with all projection polynomials, and
	\item check for satisfying sample points.
\end{enumerate*}
We observe that our definition of \emph{projection steps} and \emph{projection candidates} were defined such that they merely \emph{decompose} any of the existing projection operators and allow for an incremental execution where the queue stores the current progress. Hence, once the queue is empty, the projection operator has been completely executed.
Similarly, \emph{lifting steps} and \emph{lifting candidates} only decompose the (usually recursive) lifting process, and once the queue is depleted all sample points have been constructed.

We can easily conclude the above properties from here.
If we enter the SAT state then we have found a satisfying sample point.
If we enter the UNSAT state then there is no satisfying sample point and, as at this point $Q = \emptyset$, the CAD is complete.
As we compute a full CAD -- in particular nothing more -- we eventually terminate, and once we have $Q = \emptyset$ the only two remaining options are to enter the SAT state or the UNSAT state (in the slightly restricted scenario we consider for this argument).
