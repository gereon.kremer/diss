\chapter{Conclusion}\label{ch:conclusion}

In this work, we presented various techniques for SMT solving of nonlinear real arithmetic with a special focus on CAD-based procedures for theory solving.
Our work mainly centered around an embedding of CAD as a theory solver for traditional (lazy) SMT solving as presented in \cref{ch:cad-SMT} and the implementation of the more recent \MCSAT framework with explanation functions based on CAD in \cref{ch:mcsat-implementation}.

For both subjects, we recalled basic definitions and techniques, proposed novel techniques concerning both the theoretical framework and an actual implementation, and extended it beyond the traditional scope of SMT solving.
In the following, we give some more details on the contributions of this thesis, differentiating it from previous work, and the future work that was (in part) already brought up within this work, but neither implemented nor properly  dealt with yet.

\section{Contributions}\label{sec:conclusion:contributions}

We already gave a brief overview of our contributions in \cref{sec:introduction:contributions} that we extend now that we have seen the details of this work.
Note that we include the author's contributions to published work here, but also make explicit where the presentation in this thesis exceeds the published works listed in \cref{ssec:introduction:contributions:relevant-publications}.
Unless stated otherwise, everything mentioned below has been implemented in \SMTRAT~\cite{Corzilius2015} which we consider a contribution in itself.

The author's contributions already start in \cref{sec:preliminaries:ran} on the topic of real algebraic numbers, a crucial part of a sufficiently efficient implementation of CAD and most CAD-based methods.
Given this importance, it has already been studied quite a bit and, thus, our presentation does not contain any fundamentally novel techniques or insights.
However, discussions of how to implement the important methods are extremely sparse: algebraic works tend to simply assume knowledge about \emph{how} to implement real algebraic numbers while others merely use it as a starting point like~\cite{Moura2013a}.
In this sense, our presentation is unique in that it discusses how to actually implement real algebraic numbers from building blocks that are easy enough for a computer scientist to understand and implement.

Similarly, \cref{ch:cad} (and in particular \cref{sec:cad:projection-operators}) does not contain novel techniques for CAD, but a yet not written summary of existing methods for CAD, in particular projection operators and a comparative analysis thereof which is in parts based on~\cite{Viehmann2016,Viehmann2017}.
Again, we did not focus on the theoretical background (which is both mathematically deep and very diverse) but aimed at making the discussion sufficiently easy to understand, giving a feeling for the impact of the different methods in practice.

In \cref{ch:cad-SMT} we first presented a novel formulation of CAD as a proof system that aims to bridge the gap between the mathematical presentations of CAD that prevail in the computer algebra community and the much more practical and algorithmic description from~\cite{Kremer2020}.
These formulations of CAD aim to make an \emph{incremental} implementation as easy as possible to allow using it as a theory solver in the sense of~\cite{Kremer2018a}.
Of course, the proof system is heavily based on~\cite{Kremer2020} and could be seen as a mere reformulation of the algorithms presented there.

This proof system was then extended using known techniques like full factorization, equational constraints, equation inference based on the resultant rule, infeasible subset generation, or branch and bound for integer problems.
While all of these techniques are more or less well-known, we combined them into a single framework that allows for a seamless combination of them.
This of course based on previously published work on equational constraints~\cite{Haehn2018a,Haehn2018b}, infeasible subsets~\cite{Hentze2017} and nonlinear integer arithmetic~\cite{Kremer2016}.

Furthermore, the implementation -- which was targeted at SMT problems and heavily focussed on incrementality -- was applied to quantifier elimination and an adaptation for optimization was presented.
While quantifier elimination found its way into our actual implementation in~\cite{Neuhaeuser2018}, the work on optimization has not, but still waits to be implemented.

We then turned towards the \MCSAT framework, a comparably recent alternative to what we called \emph{(lazy) SMT solving}, that yielded particularly good results with its implementation for nonlinear real arithmetic in \Zthree~\cite{Jovanovic2012}.
Starting from a slightly revised definition of the underlying proof system (compared to~\cite{Moura2013}), we introduced a novel variant we call \emph{model-refining satisfiability calculus} and proposed a way to employ \MCSAT for optimization problems, though both have not been implemented yet.

In \cref{ch:mcsat-implementation}, we discussed our implementation of \MCSAT, the only \MCSAT-based implementation targeted at nonlinear arithmetic apart from \Yices and \Zthree~\cite{Jovanovic2012} (which arguably can be considered the same, given that the same author implemented them in close succession).

We proposed a novel embedding of \MCSAT into a \CDCL-style SAT solver -- in some sense approaching implementations of regular SMT solving and \MCSAT{} -- and proposed a novel assignment finder as well as multiple novel explanation functions and methods to compose them. Again, some of these were already published in~\cite{Nalbach2017,Abraham2017,Neuss2018}.
This novel implementation of \MCSAT was evaluated, in particular, for varying explanation functions and varying variable orderings in \cref{sec:mcsat:experiments}, in parts based on~\cite{Nalbach2019}.

Finally, we picked up a thread concerning a more theoretical view on proof systems (like \CDCLT or \MCSAT) from~\cite{Robere2018} and studied the theoretical power of \MCSAT in comparison to \ResST and \CDCLST.
The first part is concerned with the notion of \emph{proof complexity} and establishes the equivalence of \MCSAT and \ResST in this context, closely based on~\cite{Kremer2019}.
In the second part, we depart from proof complexity and directly show what we call \emph{algorithmic equivalency} between \MCSAT and \CDCLST. Finally, we discuss certain shortcomings of our proof when compared to actual implementations and how this affects the power of \MCSAT implementations in comparison to the \CDCLST proof system and implementations thereof, as well as the importance of the novel concepts in \MCSAT for its practical and theoretical performance.


\section{Future work}\label{sec:conclusion:future-work}

We can easily imagine improvements and adaptations for almost all of the presented concepts and techniques and the scope of this thesis is limited due to restrictions in space and time, but not for the lack of open questions or (more or less) novel ideas that deserve thorough consideration.
We thus only give a few directions for future research that we feel are particularly promising and important (in no specific order).

In \cref{ssec:cadSMT:heuristics:queueorder}, we discussed how we can arbitrarily interleave the projection and lifting process in an incremental CAD.
Though we did some brief experiments on this issue, the most effective heuristic in practice was to strictly prefer lifting steps.
We feel that this does not align with concepts like \emph{open CAD} and our general idea of incrementality, speculating that better schedulers should exist.

The proof system of \MCSAT feels somewhat convoluted and cumbersome and we propose to aim for a more elegant version of \MCSAT.
Our understanding of \MCSAT is to treat the theory reasoning much like the Boolean reasoning, but the proof system handles them very differently, though we do not see a fundamental reason for this.
Furthermore, we propose to simplify the proof system with respect to conflict analysis and define it more alike to \CDCLST.

The discussion in \cref{sec:mcsat:practicability} indicate that the main reason for the practical effectiveness of \MCSAT might be that it provides a constructive way to obtain \emph{useful} lemmas while this is a notoriously hard problem within \CDCLST.
It might thus be interesting whether we can embed parts of \MCSAT into \CDCLST, purely to generate such useful lemmas.
On the other hand, approaching a theory problem in a \emph{conflict-driven} manner may yield interesting new decision procedures. Both NuCAD~\cite{Brown2015a} and the cylindrical algebraic coverings approach from~\cite{Abraham2020} indicate that such methods are competitive in first experiments and deserve more research.

Finally, we observe that the growing number of industrial applications leads to the wish to extend SMT solving beyond the question of satisfiability. While ``SMT solving'' has always incorporated the question for a model and \emph{unsatisfiable cores} have long been studied for propositional problems and are slowly adapted for SMT solving as well, one important component for real-world applications is \emph{optimization}.
While several groups work on \emph{optimization modulo theories}, no approach has gained any traction for \emph{nonlinear} optimization yet.
We have laid out how to use a CAD-based theory solver as well as \MCSAT for optimization, and note that the CAD-based method gives strong guarantees on the results, arguably even stronger than other methods for nonlinear optimizations as we discuss in \cref{ssec:introduction:incomplete:numerical}.
We thus expect that an effective optimization based on CAD may be extremely useful in practice if it scales sufficiently, which is, of course, a notoriously hard problem for any CAD-based approach.
