% !TEX root = main.tex
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{paper}

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% General structure
% - setup package and handle options
% - load memoir class
% - utility packages
% - general page layout
% - special pages layout
% - general typesetting
% - symbols and fonts
% - special formatting
%   - algorithms
%   - colors
%   - lists
%   - references
%   - tables
%   - tikz & pgfplots
%   - units
%   - todos
% - includes
%   - bibliography
%   - glossaries
%   - hyphenation
%   - macros
%   - tcolorbox
% - deferred options due to cross-dependencies

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Setup options
\newif\ifdissertation@online
\newif\ifdissertation@print
\newif\ifdissertation@publication
\newif\ifdissertation@gitcommit

% online: adapts the layout for viewing as pdf
\DeclareOption{online}{
	\dissertation@printfalse
	\dissertation@onlinetrue
}
% print: adapts the layout for printing
\DeclareOption{print}{
	\dissertation@printtrue
	\dissertation@onlinefalse
}
% gitcommit: shows the last git commit on the title page
\DeclareOption{gitcommit}{
	\dissertation@gitcommittrue
}
% publication: shows information for publication with AIB
\newcommand{\PrintAIBSummary}{}
\newcommand{\PrintAIBTitle}{}
\newcommand{\PrintPublicationList}{}
\DeclareOption{publication}{
	\renewcommand{\PrintAIBSummary}{
		\clearforchapter\thispagestyle{empty}~\newpage
		\input{style/aibsummary}
	}
	\renewcommand{\PrintPublicationList}{
		\clearforchapter
		\input{style/publist}
	}
	\ifdissertation@online
	\renewcommand{\PrintAIBTitle}{
		\input{style/aibtitlepage}
		\clearforchapter
	}
	\fi
}
% default is online
\ExecuteOptions{online}
\ProcessOptions\relax


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Load memoir class
\ifdissertation@online
	\LoadClass[11pt,a4paper,openany,draft]{memoir}
\fi
\ifdissertation@print
	\LoadClass[11pt,a4paper,twoside,openright,draft]{memoir}
\fi
\titlingpageend{\clearforchapter}{\clearforchapter}


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Utility packages
\usepackage{etoolbox}


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% General page layout
% memoir, p.14 & p.16
% Computer modern, 10pt -> 127 alphabet length -> 26 picas
% Computer modern, 11pt -> 139 alphabet length -> 28 picas

%\usepackage{showframe}

\ifdissertation@online
	\setbinding{0mm}
	\settypeblocksize{*}{34pc}{1.618}
	\setlrmargins{*}{*}{1}
	\setulmargins{*}{*}{1}
\fi
\ifdissertation@print
	\setstocksize{303mm}{216mm}
	\settrimmedsize{297mm}{210mm}{*}
	\setbinding{13mm}
	\settypeblocksize{*}{34pc}{1.618}
	\setlrmargins{*}{*}{2}
	\setulmargins{*}{*}{1}
\fi
\checkandfixthelayout
\fixthelayout

% makes sure that empty pages (due to clearpage) are actually empty
\usepackage{emptypage}
% allows for absolute positioning
\usepackage[absolute]{textpos}


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Special pages layout
% rwth title page
\usepackage[final]{RWTH-titlepage}
% show git commit on title page
\ifdissertation@gitcommit
\renewcommand{\maketitlehookd}{
	\verbatiminput{\string"| git log HEAD^..HEAD \string"}
}
\fi

% make abstract a bit wider
\addtolength{\absleftindent}{-0.45cm}
\addtolength{\absrightindent}{-0.45cm}

% make part pages empty
\aliaspagestyle{title}{empty}
\aliaspagestyle{part}{empty}
\aliaspagestyle{chapter}{empty}
\setsecnumdepth{subsubsection}

\cftpagenumbersoff{part}
\setlength{\cftbeforepartskip}{4.0em \@plus\p@}
% width of page numbers in the toc
\setpnumwidth{4ex}
% right margin around page numbers in the toc
\setrmarg{6ex}
% width reserved for part numbers in the toc
\setlength{\cftpartnumwidth}{6ex}

% remove "Chapter X" from chapter pages
\renewcommand*{\printchaptername}{}
\renewcommand*{\printchapternum}{}

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% General typesetting

% Make typesetting a bit better
\usepackage{microtype}
% Babel
\usepackage[main=english,ngerman]{babel}
% Hyperlinks
\usepackage[hidelinks]{hyperref}
% provides the proof environment
\usepackage{proof}
% subfigures
\usepackage{subcaption}
\captionsetup[subfigure]{aboveskip=1pt,belowskip=3pt}
% negation for more complex relations
\usepackage{centernot}

% move chapters out of part
% after hyperref!
\usepackage{bookmark}

% penalties
% https://tex.stackexchange.com/questions/51263/what-are-penalties-and-which-ones-are-defined
%\hyphenpenalty=50
%\exhyphenpenalty=50
%\binoppenalty=700
%\relpenalty=500
%\clubpenalty=150
\clubpenalty=500
%\widowpenalty=150
\widowpenalty=500
%\displaywidowpenalty=50
%\brokenpenalty=100
%\predisplaypenalty=10000
%\postdisplaypenalty=0
%\floatingpenalty = 20000


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Symbols and fonts
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathpartir}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{stmaryrd}
% after mathrsfs!
\usepackage[scr]{rsfso}
% after amsmath!
\usepackage{accents}
\usepackage{nicefrac}


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Special formatting

%%% Algorithms
% Attention: we use a slightly customized version here
% https://listes.lirmm.fr/sympa/arc/algorithm2e-discussion/2018-09/msg00005.html
\usepackage[ruled,vlined,linesnumbered,algochapter]{algorithm2e}
\DontPrintSemicolon
\SetKwProg{Fn}{Function}{}{}
\SetKwComment{Comment}{$\triangleright$\ }{}
\DecMargin{1em}

%%% Colors
% provides rwth colors
\usepackage{rwthcolors}
% some default colors
\newcommand{\cOne}{blue100}
\newcommand{\cTwo}{\cOne!25}
\newcommand{\cThree}{green100}
\newcommand{\cFour}{\cThree!25}
\newcommand{\cGuard}{green50}
\newcommand{\cReset}{green100}


%%% Lists
% Inline lists
\usepackage[inline]{enumitem}

%%% References
% after amsmath!
% after algorithm2e!
% provides \cref to automatically typeset the type of a reference.
\usepackage[capitalise,nameinlink,noabbrev]{cleveref}

%%% Tables
\usepackage{booktabs}
\usepackage{tabularx}
\newcolumntype{Y}{>{\small\raggedleft\arraybackslash}X}
\renewcommand\TX@error@width{0pt}

%%% Tikz & pgfplots
\usepackage{tikz}
% tikz libraties
\usetikzlibrary{automata,calc,external,intersections,patterns,positioning,shapes.misc}
% tikz externalization
\tikzsetexternalprefix{externalized/paper/}
\tikzexternalize[
	mode=list and make,
	system call={TEXINPUTS=./style:${TEXINPUTS} lualatex \tikzexternalcheckshellescape -halt-on-error -interaction=batchmode -file-line-error -jobname "\image" "\texsource"},
]
\tikzset{
	external/verbose IO=false,
	external/verbose optimize=false,
}
\tikzset{
	subset/.style={
		draw=none,
		edge node={node [sloped, allow upside down, auto=false]{$\subsetneq$}}
	},
	subseteq/.style={
		draw=none,
		edge node={node [sloped, allow upside down, auto=false]{$\subseteq$}}
	},
	isomorph/.style={
		draw=none,
		edge node={node [sloped, allow upside down, auto=false]{$\simeq$}}
	},
}
% pgfplots
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
% wrapper for including tikz
\newcommand{\includetikz}[1]{
	\tikzsetnextfilename{#1}
	\tikzset{
		every plot/.style={prefix=build/plots/paper/plot-#1-},
	}
	\input{content/pictures/#1.tikz}
}

%%% Units
\usepackage[binary-units=true]{siunitx}
\robustify\bfseries
\sisetup{detect-weight=true,zero-decimal-to-integer}
% Usage: \bfseries <number>

%%% Todos
\usepackage{todonotes}


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Includes
\input{style/bibliography}
\input{style/glossaries}
\input{style/hyphenation}
\input{style/macros}
\input{style/tcolorbox}


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Deferred options

% Todos: prevent externalization
% after bibliography!
\makeatletter
\renewcommand{\todo}[2][]{\tikzexternaldisable\@todo[inline,#1]{#2}\tikzexternalenable}
\makeatother

% ToC: set depth
% after tikz!
\settocdepth{section}


% finish current part, add spacing to ToC
\newcommand{\EndPart}{
	\bookmarksetup{startatroot}%
	\phantomsection%
	\addtocontents{toc}{\vspace{0.75\cftbeforepartskip}}%
}

% paragraph layout
% after algorithm2e!
\setlength{\parindent}{0ex}
\setlength{\parskip}{1ex}
