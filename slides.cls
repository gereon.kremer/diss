% !TEX root = slides.tex
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{slides}

\LoadClass[aspectratio=169,t,hyperref={pdfpagelabels=false}]{beamer}

\usetheme{rwth}

\input{style/bibliography_beamer}
\input{style/macros_beamer}
\usepackage{style/rwthcolors}

\newcommand{\todo}[1]{{\scriptsize \textcolor{red100}{\mbox{TODO: #1}}}}

\usepackage{setspace}
\usepackage{stmaryrd}
\usepackage{centernot}
\usepackage{accents}
\usepackage{xspace}
\usepackage{booktabs}
\usepackage{tabularx}
\newcolumntype{Y}{>{\small\raggedleft\arraybackslash}X}
\renewcommand\TX@error@width{0pt}

%%% Units
\usepackage[binary-units=true]{siunitx}
\robustify\bfseries
\sisetup{detect-weight=true,zero-decimal-to-integer}
% Usage: \bfseries <number>

\renewcommand<>{\emph}[1]{\textcolor{blue100}{#1}}

\newcommand{\backupbegin}{
   \newcounter{finalframe}
   \setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
   \setcounter{framenumber}{\value{finalframe}}
}

%%% Tikz & pgfplots
\usepackage{tikz}
% tikz libraties
\usetikzlibrary{arrows.meta,automata,calc,external,intersections,patterns,positioning,shapes.callouts,shapes.misc}
% tikz externalization
\tikzsetexternalprefix{externalized/slides/}
%\tikzexternalize

\newcommand*{\overlaynumber}{\number\beamer@slideinframe}
\tikzset{
	subset/.style={
		draw=none,
		edge node={node [sloped, allow upside down, auto=false]{$\subsetneq$}}
	},
	subseteq/.style={
		draw=none,
		edge node={node [sloped, allow upside down, auto=false]{$\subseteq$}}
	},
	isomorph/.style={
		draw=none,
		edge node={node [sloped, allow upside down, auto=false]{$\simeq$}}
	},
}

\tikzexternalize[
	mode=list and make,
	system call={TEXINPUTS=./style:${TEXINPUTS} lualatex \tikzexternalcheckshellescape -interaction=batchmode -halt-on-error -file-line-error -jobname "\image" "\texsource"},
]
\tikzset{
	external/verbose IO=false,
	external/verbose optimize=false,
}

% pgfplots
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
% wrapper for including tikz
\newcommand{\includetikz}[1]{
	\tikzsetnextfilename{#1-\overlaynumber}
	\tikzset{
		every plot/.style={prefix=build/plots/slides/plot-#1-\overlaynumber-},
	}
	\input{content-slides/pictures/#1.tikz}
}

\newlength{\citefootlinepos}
\newlength{\citefootlinewidth}
\newcommand{\citefootline}[1]{
	\setlength{\citefootlinepos}{\dimexpr(\paperheight-8mm)}
	\setlength{\citefootlinewidth}{\dimexpr(\paperwidth-10mm)}
	\begin{textblock*}{\citefootlinewidth}(5mm,\citefootlinepos)
		\raggedright
		#1
	\end{textblock*}
}
