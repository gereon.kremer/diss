\section{Optimization}

\silentdefindex{mcsat:optimization}
As already noted in \cref{sec:cadSMT:optimization}, extending SMT approaches to support optimization tasks is both interesting in itself, but also greatly beneficial for practical applications.
Having seen \MCSAT as an alternative approach to SMT, we propose to extend \MCSAT to optimization queries as well.
As before, we assume optimization to mean minimization.

Similar to what we presented in \cref{sec:cadSMT:optimization}, we propose a rather simple approach that we have not yet implemented, though.
When using regular CAD, we could exploit \emph{global knowledge} about the problem -- in the form of a full projection -- to immediately select an optimal assignment.
We can not do that in \MCSAT, but rather have to iteratively search for an optimal solution. 

We keep the idea to reduce our objective function to a single variable and assign this objective variable \emph{first}.
Also note that the fundamental issue concerning unboundedness and optimal solutions from open cells remain.
To perform optimization, we propose an adapted version of \MCSAT as shown in \cref{algo:mcsat:optimization}.

\begin{algorithm}[H]
	\caption{Optimization with \MCSAT}\label{algo:mcsat:optimization}
	
	\SetKwFunction{OptMCSAT}{OptMCSAT}
	\Fn{\OptMCSAT{$\varphi$, $v$}}{
		\While{\true}{
			Select optimal feasible region $R$ for $v$ with \rulename{T-Decide} \;
			\If{no feasible region exists}{
				\Return{UNSAT} \nllabel{algo:mcsat:optimization:unsat}
			}
			\ElseIf{$R$ is bounded}{
				\rulename{T-Decide} $v \mapsto r$ \emph{(almost) optimal} from $R$ \;
				\If{\MCSAT finds satisfiability}{
					\nllabel{algo:mcsat:optimization:satisfiability}
					\Return{SAT and (almost) optimal assignment}
				}
			}
			\Else{
				\Repeat{\MCSAT finds unsatisfiability}{
					\nllabel{algo:mcsat:optimization:unboundedness-loop}
					Backtrack until $v$ is unassigned \;
					Let $\mathit{last}$ be the last assignment to $v$ \;
					\rulename{T-Decide} $v \mapsto r \in R$ with $r = 2 \cdot \min \{ \BoundU{R}, \mathit{last}, -1 \}$ \;
					\If{threshold reached}{
						\Return{UNBOUNDED}
					}
				}
			}
			\MCSAT \rulename{Restart} to empty the trail
		}
	}
\end{algorithm}

The method shown in \cref{algo:mcsat:optimization} proceeds as follows.
We first obtain the optimal feasible region for the objective variable with respect to the univariate constraints.
If no such region exists we determine infeasibility in \cref{algo:mcsat:optimization:unsat}, otherwise, we have a region $R$ that contains the optimal solution.
If $R$ is bounded we can use \rulename{T-Decide} to assign $v \mapsto r$ where $r$ is the optimal -- or ``a good'' -- value and return $SAT$ in case this assignment leads to a full model. Otherwise \MCSAT automatically discards the region around $r$ and we continue with selecting a new region $R$.

We need to make sure that the excluded regions for the objective variable -- for example if \MCSAT does not find satisfiability in \cref{algo:mcsat:optimization:satisfiability} -- are ``visible'' when we identify the currently optimal region. Otherwise, we may enter an infinite loop, selecting the same region over and over again.
To ensure this, we need to employ a variable ordering that at least processes \emph{univariate} literals as Boolean decisions \emph{before} performing a theory decision on $v$.
Though this may seem self-evident, it conflicts with some of the heuristics we present in \cref{sec:mcsat:experiments}.

The region $R$ may, however, be unbounded as well.
In this case, we enter a nested loop in \cref{algo:mcsat:optimization:unboundedness-loop} that assigns $v$ to exponentially growing values $r$ until we either find the assignment to be unsatisfiable -- in this case we let \MCSAT exclude a region around this $r$ and try with a new optimal region in the hope that the new optimal region is bounded now -- or reach a threshold and consider the problem to be unbounded.
The exact value for $r$ can be one of three variants: if we just started testing values we start with the upper bound of the region (the lower bound is $-\infty$); if we already did some iterations, we continue with the last assigned values and only make it smaller; finally, we make sure that we start with a negative value so that we have a proper initial value and ``making it smaller'' works by simply multiplying it with a constant.

Note that the threshold may bound the size of $r$ (possibly depending on the coefficients of the constraints) but could also consider the number of iterations or similar indicators.
Finally, after we have processed a particular region $R$ we restart \MCSAT and start over with a new optimal region.

Given a fixed threshold for the case of unboundedness, this method terminates as it only explores finitely many regions $R$ -- following the argument for termination of regular \MCSAT{} -- and the inner loop only checks finitely many values as well.
