\section{Proof complexity}\label{sec:mcsat:proof-complexity}

When having different methods to solve the same problem -- for example \CDCLST and \MCSAT{} -- one would like to have a solid mathematical way to compare them in a meaningful way.
The most fundamental properties are \emph{soundness} and \emph{completeness}, accompanied by more pragmatic measures like \emph{asymptotic complexity} of (average case or worst case) run time or memory consumption.

In the case of \CDCLST and \MCSAT, we know that both of them are sound and complete, and assuming that the Boolean satisfiability problem indeed has exponential complexity ($P \neq NP$), these two proof systems have the same asymptotic complexity.
Note that this asymptotic complexity is -- at least in the case of nonlinear real arithmetic -- dominated by the theory queries and thus the above claim only holds if both the number and complexity of theory queries are comparable among the two proof systems.
Note that \MCSAT allows for the introduction of new theory atoms, and we, therefore, consider \CDCLST instead of \CDCLT.

The notion of \emph{proof complexity} provides another angle at this, essentially asking for the \emph{asymptotic size} of the \emph{smallest proof} for a given class of problems that an algorithm -- formulated as a proof system -- can construct.
We observe that we defined both \MCSAT and \CDCLST as \emph{deductive proof systems} as presented in \cref{sec:preliminaries:proof-systems} and use these formulations here.

Given that we argue about the \emph{smallest proof}, this approach assumes that all heuristic decisions are nondeterministic -- given by some all-knowing oracle -- transforming a proof system into a (nondeterministic) algorithm.
Furthermore, at least in our scenario, it only deals with \emph{unsatisfiable} instances since a \emph{proof for satisfiability} is always short, simply given by a variable assignment or rather the construction thereof.

\subsection{A primer on proof complexity}

Using proof complexity as a measure for proof systems dates back at least as far as~\cite{Tseitin1968} where it is used for the \emph{resolution proof system} for propositional logic -- though it is still called the \emph{annihilation rule}.
In contrast to Tseitin's work, however, who measured the size of the proof in terms of the \emph{number of clauses}, we consider the \emph{number of rule applications}.
The following discussion is based on the definition of proof systems and proofs from \cref{sec:preliminaries:proof-systems}.

\begin{definition}{Proof complexity}{proof-complexity}\silentdefindex{proof-complexity}
	Let $\varphi$ be an unsatisfiable formula and $\Proof_\varphi \subset \ProofSystem^\vdash$ the set of proofs for $\varphi \models \false$ from a proof system $\ProofSystem$.
	We call $\Proof \in \Proof_\varphi$ with $|\Proof|$ minimal among $\Proof_\varphi$ the \emph{shortest proof for $\varphi$} (in $\ProofSystem$) and $|\Proof|$ the \emph{(proof) complexity of $\varphi$}, also denoted as $\ProofSystem(\varphi)$.
\end{definition}

Note that while the (proof) complexity of $\varphi$ is unique, the shortest proof is not.
If we consider the resolution proof system for propositional logic, a proof can be seen as a sequential representation of a \emph{resolution tree}. While there can be multiple different resolution trees that prove the same formula unsatisfiable, there can also be multiple \emph{serializations} of one such tree which all result in proofs of the same length.

Though the above definition specifies the proof complexity as a natural number $n$, we usually use it as an asymptotic measure with respect to the \emph{size} of formulae from a certain class of formulae. Thus, we usually talk about \emph{polynomially sized} or \emph{exponentially sized} proofs and (ab)use the \emph{big O notation} we know from run-time analysis.
We always assume the asymptotic measure to be relative to the size of the formula, usually in terms of the number of symbols used when writing it down.

There are usually two different types of statements about the proof complexity of some formula classes:
\begin{enumerate*}
	\item the formula class has a certain proof complexity within $\ProofSystem$ or
	\item the formula class has a smaller proof complexity within $\ProofSystem_1$ than within $\ProofSystem_2$.
\end{enumerate*}
While the first assertion provides some insight into a single proof system, the second is suited to compare the \emph{power} of two different proof systems.
We formally define the relation between two proof systems by introducing the notion of \emph{simulation of proof systems}.

\begin{definition}{Simulation of proof systems}{simulation-of-proof-systems}\silentdefindex{proof-system:simulation}
	Let $\ProofSystem_1$ and $\ProofSystem_2$ be two proof systems and $\Phi$ a set of formulae.
	We say that $\ProofSystem_1$ \emph{(polynomially) simulates} $\ProofSystem_2$ if for every $\varphi \in \Phi$ we have that $\ProofSystem_1(\varphi)$ is at most polynomially longer than $\ProofSystem_2(\varphi)$ and write $\ProofSystem_1 \PCGEQ_{\Phi} \ProofSystem_2$.
	Conversely, if $\ProofSystem_1 \mathrel{{\centernot\PCGEQ}_{\Phi}} \ProofSystem_2$ we know that there is some subclass $\Phi' \subset \Phi$ such that $\ProofSystem_1(\varphi)$ is \emph{superpolynomially longer} than $\ProofSystem_2(\varphi)$ for all $\varphi \in \Phi'$.
	We then write $\ProofSystem_2 \PCGT_{\Phi'} \ProofSystem_1$.
	If $\ProofSystem_1 \PCGEQ_{\Phi} \ProofSystem_2$ and $\ProofSystem_2 \PCGEQ_{\Phi} \ProofSystem_1$ we say that $\ProofSystem_1$ and $\ProofSystem_2$ are \emph{bisimilar} and write $\ProofSystem_2 \PCEQ_{\Phi} \ProofSystem_1$.
	
	If $\Phi$ is the whole set of formulae from the respective logic, or if the set of formulae is clear from the context, we only write $\PCGEQ$, $\PCGT$, and $\PCEQ$.
\end{definition}

Note that the relations $\PCGEQ$ and $\PCGT$ indicate relations of the \emph{(expressive) power} of proof systems which is reciprocal to the relation of the lengths of the respective proofs.
Also note that the simulation relations do not induce a \emph{total ordering} on proof systems in general (on any meaningful class of formulae $\Phi$). There may very well be two proof systems $\ProofSystem_1$, $\ProofSystem_2$ such that $\ProofSystem_1 \centernot\PCGEQ \ProofSystem_2$ and $\ProofSystem_2 \centernot\PCGEQ \ProofSystem_1$.
Of course, we usually have at least either $\ProofSystem_1 \PCGEQ_{\Phi} \ProofSystem_2$ or $\ProofSystem_2 \PCGEQ_{\Phi} \ProofSystem_1$ for any sufficiently small class $\Phi$.


\subsection{Proof complexity of \ResST and \MCSAT}\label{ssec:mcsat:proof-complexity}

We now assess the proof complexity of \MCSAT by comparing it to another proof system called \ResST which has already been studied in~\cite{Robere2018}, and in particular been related to \CDCLST. Note that most of what is presented in this section originates from~\cite{Kremer2019} with little to no changes.

We have already given the basic \emph{resolution proof system} that only deals with propositional logic in \cref{def:resolution-proof-system}. To cope with first-order logic theories, this proof system is commonly enhanced to what we call the \emph{\ResT proof system}.

\begin{definition}{\ResT and \ResST proof systems}{resolution-theory}\silentdefindex{proof-system:rest}
	Let $\varphi$ be an input formula in conjunctive normal form.
	Assume two clauses from $\varphi$ that share some theory atom $y$, though with opposite polarity.
	The \rulename{Resolution} rule looks as follows:
	\begin{align*}
		\ProofRule{Resolution}{(x_1 \lor \dots \lor x_i \lor y), (\neg y \lor z_1 \lor \dots \lor z_j)}{(x_1 \lor \dots \lor x_i \lor z_1 \lor \dots \lor z_j)}{}
		\shortintertext{Additionally, we can derive tautologies (in the theory) using the \rulename{Theory derivation} rule or the \rulename{Strong theory derivation} rule:}
		\ProofRule{Theory derivation}{\varphi}{\varphi \land C}{\textrm{$T \models C$,} \\ \textrm{$l \in \varphi$ for all $l \in C$}} \\
		\ProofRule{Strong theory derivation}{\varphi}{\varphi \land C}{\textrm{$T \models C$}}
	\end{align*}
	
	The \emph{\ResT proof system} consists of the \rulename{Resolution} rule and the \rulename{Theory derivation} rule.
	The \emph{\ResST proof system} consists of the \rulename{Resolution} rule and the \rulename{Strong theory derivation} rule.
\end{definition}

Similar to how we extended \CDCLT to \CDCLST in \cref{def:cdcl-proof-system}, we can extend \ResT to \ResST, allowing the introduction of new theory atoms.
We can observe that $\ResST \PCGEQ \ResT$ immediately, and~\cite{Robere2018} even suggests that $\ResST \PCGT \ResT$.

We know from~\cite{Robere2018} that $\ResT \PCEQ \CDCLT$ and as well $\ResST \PCEQ \CDCLST$ -- being called \DPLLT and \DPLLST in spite of their own note that encourages to properly distinguish between \DPLL and \CDCL.
Having observed certain similarities of \CDCLST and \MCSAT, we now aim to compare \MCSAT and \ResST to state the following theorem for \MCSAT, similar to the one given in~\cite{Robere2018} for \CDCLST.

\begin{theorem}{Proof complexity of \ResST and \MCSAT}{resst-mcsat-pc-equivalence}\silentindex{proof-system:mcsat}\silentindex{proof-system:rest}
	The \ResST proof system and the \MCSAT proof system are \emph{bisimilar} with respect to their proof complexity on \emph{first-order logic with any theory}.
\end{theorem}

We prove \cref{thm:resst-mcsat-pc-equivalence} in two steps: firstly, we show $\MCSAT \PCGEQ \ResST$ and, secondly, show that $\ResST \PCGEQ \MCSAT$.
In both cases, we show that we can simulate every rule individually, which actually proves a slightly stronger statement: we not only construct \emph{some other proof} (with polynomial overhead) but essentially \emph{the same proof}.
This observation is the starting point of the subsequent comparison in \cref{sec:mcsat:equivalency}.

\begin{proof}
To show that $\MCSAT \PCGEQ \ResST$, it suffices to show that \MCSAT can simulate both the \rulename{Resolution} rule and the \rulename{Strong theory derivation} rule with only polynomial overhead.
Note that both proof systems operate on the same input formula $\varphi$ in conjunctive normal form, seen as a set of clauses $\Clauses$.

\paragraph{Simulating the \rulename{Resolution} rule.}

Assuming that the set of clauses $\Clauses$ contains both $(C \lor L)$ and $(D \lor \neg L)$, we need to add $(C \lor D)$ to $\Clauses$.

Let us first handle a special case. Assume that we have some literal in $C$ whose negation is contained in $D$. In this case, $(C \lor D) \equiv \true$ and there is nothing to do.
Similarly, if $(C \lor D) \in \Clauses$ we do not need to learn the clause.
From here on we assume that $(C \lor D) \not\equiv \true$ and $(C \lor D) \not\in \Clauses$.
Starting from an empty trail, the clause $(C \lor D)$ can be learned using the \MCSAT proof rules as follows:

\vspace*{-\abovedisplayskip}
\begin{align*}
	\shortintertext{We start by applying the \rulename{Decide} rule for all literals $L_i \in C \cup D$. Note that all $L_i \in \Basis$ and all $L_i$ are undefined in $M$ as $M = \Seq{}$ initially.}
	\ProofRule{Decide}{\State{M, \Clauses}}{\State{\Seq{M, \neg L_i}, \Clauses}}{\textrm{$L_i \in \Basis$,}\\\textrm{$\Value(L_i,M) = \Undef$}}
	\shortintertext{Now both $C$ and $D$ evaluate to $\false$ over the trail and we can apply the \rulename{Propagate} rule with $(C \lor L)$ to propagate $L$.}
	\ProofRule{Propagate}{\State{M, \Clauses}}{\State{\Seq{M, (C \lor L) \rightarrow L}, \Clauses}}{\textrm{$\Value(C,M) = \false$,} \\ \textrm{$\Value(L,M) = \Undef$}}
	\shortintertext{Due to $\Value(L,M) = \true$, and thus $\Value(\neg L, M) = \false$, the second clause $(D \lor \neg L)$ is conflicting and we apply the \rulename{Conflict} rule.}
	\ProofRule{Conflict}{\State{\Seq{M, (C \lor L) \rightarrow L}, \Clauses}}{\State{\Seq{M, (C \lor L) \rightarrow L}, \Clauses} \Conflicts (D \lor \neg L)}{\textrm{$(D \lor \neg L) \in \Clauses$,} \\ \textrm{$\Value(D \lor \neg L) = \false$}}
	\shortintertext{We perform conflict resolution using the \rulename{Resolve} rule and obtain $(C \lor D)$.}
	\ProofRule{Resolve}{\State{\Seq{M, (C \lor L) \rightarrow L}, \Clauses} \Conflicts (D \lor \neg L)}{\State{M, \Clauses} \Conflicts (C \lor D)}{\textrm{$L \in (C \lor L)$,} \\ \textrm{$R = \Resolution_L(C \lor L, D \lor \neg L)$}}
	\shortintertext{To add this clause to the set of clauses $\Clauses$, we use the \rulename{Learn} rule.}
	\ProofRule{Learn}{\State{M, \Clauses} \Conflicts (C \lor D)}{\State{M, \Clauses \cup \{(C \lor D)\}} \Conflicts (C \lor D)}{\textrm{$(C \lor D) \not\in \Clauses$}}
	\shortintertext{We have achieved our goal of adding $(C \lor D)$ to $\Clauses$ and return to the initial state with the \rulename{Restart} rule.}
	\ProofRule{Restart}{\State{M, \Clauses \cup \{(C \lor D)\}} \Conflicts (C \lor D)}{\State{\Seq{}, \Clauses \cup \{(C \lor D)\}}}{}
\end{align*}

We observe that this sequence of proof rules is polynomial in the size of the clause $(C \lor D)$ (we need $|C| + |D| + 5$ rule applications) and we return to the same initial state afterward.
No theory reasoning was used in the \MCSAT rule applications and we thus pay no \emph{hidden costs} in the form of theory reasoning.

\paragraph{Simulating the \rulename{Strong theory derivation} rule.}

We need to create some arbitrary clause $C$ which is valid in the theory $T$, that is $T \models C$.
Similar to the previous simulation, we assume that $C \not\equiv \true$ and $C \not\in \Clauses$ as there is nothing to do in these cases.

Our main hurdle is that \MCSAT does not allow for learning arbitrary clauses but only the current conflict clause.
Therefore, we have to construct an (artificial) conflict that yields the desired clause.
We assume that our finite basis $\Basis$ includes all literals that ever occur in the (finite) \ResST proof.
We can construct and learn an arbitrary (valid) clause $C$ using the \MCSAT proof rules as follows:

\begin{align*}
	\shortintertext{%
		We use the \rulename{Decide} rule repeatedly to add $\neg L$ for every $L \in C$ to the trail.
		Note that the \rulename{Decide} rule allows to decide any literal from $\Basis$, independent of whether they appear in the input formula.
	}
	\ProofRule{Decide}{\State{M, \Clauses}}{\State{\Seq{M, \neg L}, \Clauses}}{\textrm{$L \in \Basis$,} \\ \textrm{$\Value(L,M) = \Undef$}}
	\shortintertext{As $T \models C$ but $\Value(C,M) = \false$, we now have $\Infeasible(M)$ and can apply the \rulename{T-Conflict} rule with $E = C$. Note that $\Infeasible(M)$ might not be capable of detecting this conflict in practical implementations, and we discuss this issue later in \cref{sec:mcsat:practicability}.}
	\ProofRule{T-Conflict}{\State{M, \Clauses}}{\State{M, \Clauses} \Conflicts C}{\textrm{$\Infeasible(M)$,} \\ \textrm{$C = \Explain(M)$}}
	\shortintertext{Now we can learn the desired clause $C$ using the \rulename{Learn} rule.}
	\ProofRule{Learn}{\State{M, \Clauses} \Conflicts C}{\State{M, \Clauses \cup \{C\}} \Conflicts C}{\textrm{$C \not\in \Clauses$}}
	\shortintertext{Finally, we return to the initial state with the \rulename{Restart} rule.}
	\ProofRule{Restart}{\State{M, \Clauses \cup \{C\}} \Conflicts C}{\State{\Seq{}, \Clauses \cup \{C\}}}{}
\end{align*}

We observe that we need $|C| + 3$ proof rule applications to learn an arbitrary clause (that is neither \true nor already present in $\Clauses$) and return to the initial state.
This concludes our proof that $\MCSAT \PCGEQ \ResST$.
\end{proof}

We now continue with the second part of the proof and show $\ResST \PCGEQ \MCSAT$ by simulating all (relevant) proof rules from $\MCSAT$ within $\ResST$.

\begin{proof}
We observe that clauses can exist in three separate places within \MCSAT: the set of clauses $\Clauses$, the trail $M$, and the current conflict clause $C$.
When simulating \MCSAT with \ResST, we make sure that the set of clauses that \ResST operates on includes the set of clauses $\Clauses$, all clauses from $M$, and the conflict clause $C$.
Thus, when \MCSAT concludes unsatisfiability by inferring the empty clause, we can do the same within \ResST.

Note that \ResST retains all clauses that it constructs as it is not designed to \emph{forget} clauses while \MCSAT may drop clauses occasionally with the \rulename{Forget} rule.
Though removing clauses can bring advantages in practice, the number of additional clauses is linear in the number of rule applications -- \MCSAT needs at least one rule to construct every rule in the first place -- and the practical overhead of additional clauses -- for example, due to larger lookup tables -- are polynomial at most if proper data structures are used.

To prove that \ResST simulates \MCSAT, it thus suffices to show that all clauses that ever occur in the \MCSAT derivation can also be constructed using the \ResST proof rules.
We assume that, initially, both proof systems start with the same set of input clauses and identify the rules where the \MCSAT rule system constructs new clauses: \rulename{Resolve}, \rulename{T-Propagate} and \rulename{T-Conflict}.

All other rules either do not manipulate any clauses at all (\rulename{Decide}, \rulename{Sat}, \rulename{Consume}, \rulename{Unsat}, \rulename{T-Decide}, \rulename{T-Consume}), move clauses between any of the three places (\rulename{Propagate}, \rulename{Conflict}, \rulename{Backjump}, \rulename{Learn}), or drop existing clauses (\rulename{Forget}, \rulename{T-Backjump-Decide}). All those can be ignored for the purpose of this proof, as we only aim to provide \ResST with the same clause set.

\paragraph{Simulating the \rulename{Resolve} rule.}

The \rulename{Resolve} rule takes the two clauses $C$ (the current conflict clause) and $D$ (from the trail $M$) and produces the resolvent with respect to some literal $L$.
By construction, we know that \ResST has both $C$ and $D$ available and can thus apply its own \rulename{Resolution} rule to produce the same resolvent.

\paragraph{Simulating the \rulename{T-Propagate} and \rulename{T-Conflict} rules.}

Both the \rulename{T-Propagate} rule and the \rulename{T-Conflict} rule construct a new clause $E$ by calling the \MCSAT $\Explain$ method.
This method produces ``a valid theory lemma'' (as specified in~\cite{Moura2013}), thus we have $T \models E$ and can obtain these clauses using the \rulename{Strong theory derivation} rule.

As we have simulated all \MCSAT rules that can be used to construct new clauses, we can now take any \MCSAT proof and convert it into a \ResST proof by using the presented simulations for the \rulename{Resolve}, \rulename{T-Propagate}, and \rulename{T-Conflict} rule and removing all other proof steps.
The simulations shown above only require a single proof step each in \ResST, and thus the proof in \ResST is at most as long as the \MCSAT proof.
\end{proof}

We have shown $\MCSAT \PCGEQ \ResST$ and $\ResST \PCGEQ \MCSAT$, and thus $\ResST \PCEQ \MCSAT$, concluding the proof for \cref{thm:resst-mcsat-pc-equivalence}.
Note that this is yet another indication that \MCSAT is essentially equivalent to \CDCLST, as they are both bisimilar to \ResST (in terms of proof complexity) for arbitrary first-order logics.
As we already noted, this proof requires the $\Infeasible$ method to be more powerful than it usually is in practical implementations, and we discuss this issue in \cref{sec:mcsat:practicability}.
