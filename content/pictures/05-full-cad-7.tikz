% !TEX root = ../../main.tex

\input{content/pictures/tikz-macros}

\begin{tikzpicture}[scale=1]
	\input{content/pictures/05-full-cad-utils}
	
	% 1d cells
	\fill[pattern=dots, pattern color=black!30]
		(-3.1,-3.2) -- (-3.1,-2.8) -- (-1.19,-2.8) -- (-1.19,-3.2) -- cycle;
	\fill[pattern=north west lines, pattern color=black!30]
		(1.69,-3.2) -- (1.69,-2.8) -- (-1.19,-2.8) -- (-1.19,-3.2)  -- cycle;
	\fill[pattern=dots, pattern color=black!30]
		(1.69,-3.2) -- (1.69,-2.8) -- (3.1,-2.8) -- (3.1,-3.2) -- cycle;
	\draw[thick] (1.69,-3.2) -- (1.69,-2.8) (-1.19,-2.8) -- (-1.19,-3.2);

	% 2d cylinders
	\fill[pattern=dots, pattern color=black!30]
		(-2,3) -- plot [domain=-2:-1.2] ({\x}, {\x*\x-1}) -- (i1-up) -- cycle;
	\fill[pattern=north west lines, pattern color=black!30]
		(-3,3) -- plot [domain=-2:-1.19] ({\x}, {\x*\x-1}) -- (-3,-0.5) -- cycle;
	\fill[pattern=dots, pattern color=black!30]
		(-3,-2) -- (-3,-0.5) -- (intersection-1) -- (i1-down) -- cycle;
	\fill[pattern=north west lines, pattern color=black!30]
		(i1-up) -- (intersection-1) -- (intersection-2) -- (i2-up) -- cycle;
	\fill[pattern=dots, pattern color=black!30]
		(intersection-1) -- (intersection-2) -- plot [domain=-1.2:1.7] ({\x}, {\x*\x-1}) -- cycle;
	\fill[pattern=north west lines, pattern color=black!30]
		(i1-down) -- (intersection-1) -- plot [domain=-1.2:1.7] ({\x}, {\x*\x-1}) -- (intersection-2) -- (i2-down) -- cycle;
	\fill[pattern=dots, pattern color=black!30]
		(i2-up) -- plot [domain=1.7:2] ({\x}, {\x*\x-1}) -- cycle;
	\fill[pattern=north west lines, pattern color=black!30]
		(intersection-2) -- plot [domain=1.7:2] ({\x}, {\x*\x-1}) -- (3,3) -- (3,2.5) -- cycle;
	\fill[pattern=dots, pattern color=black!30]
		(intersection-2) -- (3,2.5) -- (3,-2) -- (i2-down) -- cycle;
	%\fill[pattern=dots, pattern color=black!30]
	%	(-3.1,-2) -- (-3.1,3) -- (i1-up) -- (i1-down) -- cycle;
	%\fill[pattern=north west lines, pattern color=black!30]
	%	(i2-down) -- (i2-up) -- (i1-up) -- (i1-down)  -- cycle;
	%\fill[pattern=dots, pattern color=black!30]
	%	(i2-down) -- (i2-up) -- (3.1,3) -- (3.1,-2) -- cycle;

	\tikzgrid{-3}{3}{-2}{3}
	\tikzaxis{-3}{3}{-3}{3}
	\tikzxaxis{-3}{3}{-3}

	\drawconstraints

	\draw[thick] plot [samples=200,range=-4:-2.25, domain=-3:3] function {(2*x*x-x-4)*0.15 - 3};

	\draw[thick,densely dotted] (i1-up) -- (i1-1d);
	\draw[thick,densely dotted] (i2-up) -- (i2-1d);

	\draw[thick, black!75, densely dotted] (-1.75,-3) -- (-1.75,3);
	\draw[thick, black!75, densely dotted] (0.5,-3) -- (0.5,3);
	\draw[thick, black!75, densely dotted] (2.5,-3) -- (2.5,3);

	\fill (-1.75,-3) circle (2pt);
	\fill (-1.75,-1) circle (2pt);
	\fill (-1.75,0.125) circle (2pt);
	\fill (-1.75,1) circle (2pt);
	\fill (-1.75,2) circle (2pt);
	\fill (-1.75,2.75) circle (2pt);

	\fill (-1.19,-3) circle (2pt);
	\fill (-1.19,-1) circle (2pt);
	\fill (intersection-1) circle (2pt);
	\fill (-1.19,2) circle (2pt);

	\fill (0.5,-3) circle (2pt);
	\fill (0.5,-1.5) circle (2pt);
	\fill (0.5,-0.75) circle (2pt);
	\fill (0.5,0) circle (2pt);
	\fill (0.5,1.25) circle (2pt);
	\fill (0.5,2) circle (2pt);

	\fill (1.69,-3) circle (2pt);
	\fill (1.69,0) circle (2pt);
	\fill (intersection-2) circle (2pt);
	\fill (1.69,2.5) circle (2pt);

	\fill (2.5,-3) circle (2pt);
	\fill (2.5,0) circle (2pt);
	\fill (2.5,2.25) circle (2pt);
	\fill (2.5,2.75) circle (2pt);
\end{tikzpicture}
