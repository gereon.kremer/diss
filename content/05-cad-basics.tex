\chapter{Cylindrical Algebraic Decomposition}\label{ch:cad}

As already discussed, the \emph{cylindrical algebraic decomposition} (CAD) is the most popular \emph{complete} method to deal with nonlinear real arithmetic problems. For some background on its history and alternative approaches we refer to \cref{sec:related-work}.
We now go into some more detail on what CAD is about, how we compute it, and what we can do with the results. As always, we put special emphasis on parts relevant for embedding CAD into an SMT solver and pass over many theoretical issues here.
We encourage anyone interested in the mathematical details to study the papers mentioned throughout this chapter and hope to at least mention all noteworthy issues.

The term ``cylindrical algebraic decomposition'' describes both a mathematical object with certain interesting properties and an algorithm to construct such objects. We thus use this term for either and usually rely on the context for which of the two we mean.

\section{General idea}

The fundamental idea of the CAD is to reduce a question that is concerned with $\R^n$ to an equivalent question about a finite number of ``things''.
We observe that $\R$ has uncountably many elements and, therefore, any naive approach -- one that fails to abstract from a single element from $\R$ -- is doomed to fail as it is already impossible to enumerate all candidates, not to mention the issues of termination or run time complexity.
By producing an equivalent problem over a finite number of representatives (for certain subsets of $\R$), the solutions to many questions get more approachable.

We already used the term \emph{representative}, and it indicates a general idea of how to obtain such a finite set. We decompose $\R^n$ into finitely many subsets such that all elements of a particular subset are in some sense \emph{the same} (\emph{equivalent}) for the question we want to answer.
Intuitively, we may expect these subsets to look \emph{reasonable} in some sense, for example, they should be connected, disjoint, together cover $\R^n$ (thus we also call them a \emph{partition}), and be somewhat \emph{smooth}.

The smallest decomposition -- in the number of subsets -- is induced by the equivalence classes given by the above notion of equivalence. Obtaining it may, however, be costly and not worth the effort in practice, and CAD usually gives us a finer decomposition that exhibits further structural properties. We also note that an incomplete decomposition -- in which elements that we consider different in general are in the same subset -- may also be sufficient to solve certain problems, in particular \emph{satisfiability}.

There are two fundamentally different ways to represent a partition of $\R^n$ and it heavily depends on the application which of these is preferable. As already indicated, we can choose \emph{a representative} for every partition as a \emph{single point}.
We may also want to store the \emph{borders of the partition} -- assuming that a partition is a connected set -- which tends to give us a lot more information.
The CAD works with representatives internally, but we can also extract \emph{full cell representations} consisting of the description of the borders without a lot of additional effort.

To approach the question of how to obtain such a decomposition, we start with a quick look at the equivalence relation that we use to partition $\R^n$ in our application. We consider how many equivalence classes exist in theory and how we can identify the border between two subsets.

Any (first-order logic) real arithmetic question of the form \emph{Does $X$ hold?} results in a statement (a logical sentence) that evaluates to either \true or $\false$, thereby splitting $\R^n$ into the part where $X$ holds and the rest where $X$ does not hold.
When we only consider this logical level, the equivalence at its core is discrete: there is no ``process'' that makes the evaluation \emph{less true} when moving from a satisfying assignment towards a conflicting assignment, but only a sudden change from satisfiability to unsatisfiability.
In this sense, there are only two equivalence classes: the \emph{\true set} and the \emph{$\false$ set}.

\begin{definition}{Equivalence classes of a formula}{equiv-formula}
	Let $\varphi$ be a quantifier-free first-order logic real arithmetic formula.
	We define the \emph{equivalence classes of $\varphi$} for \true and \false inductively by
	\[
		\EquivTrue(\varphi) \coloneqq
		\begin{cases}
			\R^n & \textrm{ if } \varphi = \true \\
			\emptyset & \textrm{ if } \varphi = \false \\
			\{ \A \mid \A(x) \equiv \true \} & \textrm{ if } \varphi = x \in \mathcal{B} \\
			\{ \A \mid \A(p) \equiv \true \} & \textrm{ if } \varphi = p \in \mathscr{A} \\
			\EquivFalse(\varphi') & \textrm{ if } \varphi = \neg \varphi' \\
			\EquivTrue(\varphi_1) \cup \EquivTrue(\varphi_2) & \textrm{ if } \varphi = \varphi_1 \lor \varphi_2
		\end{cases}
	\]
	and $\EquivFalse(\varphi) \coloneqq \R^n \setminus \EquivTrue(\varphi)$.
\end{definition}

This somewhat changes if we peek into the statement $X$, or more precisely the quantifier-free part of $X$. We assume that we pick a certain variable assignment -- we instantiate all quantifiers -- and analyze how our equivalence relation is composed for each of the possible ways to construct a formula. We observe that we still have this binary decomposition for Boolean variables and predicates, but we may very well have changes for the evaluation of parts of the formula (for individual predicates or subformulae) without changing the overall evaluation of $X$.

We observe, maybe not particularly unexpected, that the equivalence classes of a formula are separated only when a predicate changes its evaluation result (discarding the separation due to Boolean variables). Hence, we can obtain the borders of all partitions that (possibly) separate the equivalence classes by considering all points where any of the predicates change their evaluation.

Constructing a decomposition based on where predicates change their evaluation might not yield the \emph{smallest decomposition} as discussed before, but may very well result in more partitions than necessary.
If a predicate changing its evaluation does not change the evaluation of the whole formula, we construct separate partitions that could both be merged into the same equivalence class.

We oftentimes chose not to do this for two reasons: firstly, once we have the two partitions and checked their evaluation result, we are done working on these partitions and merging them is only additional work without any benefit. Secondly, we usually assume certain structural properties about these partitions -- for example, \emph{cylindricity} -- that oftentimes get lost when merging. For an example where merging actually makes sense, and a discussion of what needs to be considered, we refer to~\cite{Neuhaeuser2018}.

Reviewing the type of constraints we consider here finally brings us to the notion of \emph{sign-invariant regions}.
Recall that constraints are of the form $p \signcondition 0$ where $p$ is a polynomial and $\sigma$ is a \emph{sign condition} or \emph{relation symbol}.
A constraint may thus change its evaluation only at variable assignments where the evaluation of the polynomial changes its sign, which are exactly the real roots of the polynomial.
We call the partitions where no polynomial from a given set changes its \emph{sign-invariant} regions.

\begin{definition}{Sign-invariant regions}{sign-invariant-region}\silentdefindex{sign-invariant-region}
	Let $P \subset \Q[\overline{x}]$ be a set of polynomials.
	We call $R \subseteq \R^n$ a \emph{sign-invariant region} of $P$ if 
	$\Forall{p \in P} \Forall{r_1, r_2 \in \R^n} \Sign(p(r_1)) = \Sign(p(r_2))$.
\end{definition}

This gives us a way to construct a decomposition of the real space by constructing all real roots of the considered polynomials. Furthermore, it shows why a \emph{finite} decomposition of $\R^n$ that represents the equivalence classes exists in the first place: every set of points that is not separated by a root of some polynomial is equivalent, every one of the finitely many polynomials only has a finite number of (connected) root surfaces, and, being polynomials, their root surfaces only cross finitely many times.
As we further observe that sign-invariance with respect to \emph{all polynomials} from a given formula immediately implies \emph{truth-invariance} of the formula, we can use this not only to study sets of polynomials but arbitrary formulae involving such polynomials.

This reduction of a problem about \emph{formulae} to a problem dealing with \emph{polynomials} introduces an abstraction that may force us to consider more partitions than required: though a polynomial changes its sign, the formula might not.
Reconsidering that we study formulae may be beneficial later on for what we call a \emph{truth-table invariant CAD}~\cite{Brown1998,Bradford2016}, possibly paving the way for future optimizations.
For now, we only consider a CAD arguing about polynomials which is sufficient and defer this point, for example to \cref{sec:cad:equational-constraints}.

The CAD method provides an algorithmic framework for how to construct such a finite decomposition effectively. It proceeds \emph{dimension-wise} and constructs a CAD in some dimension in a way such that it can be \emph{extended} to a higher-dimensional CAD.
This extension is done in a very direct way: given the representatives of a $k$-dimensional CAD, we obtain the representatives of the $(k+1)$-dimensional CAD by extending every representative with one or more values from the $(k+1)$st dimension.

In other words, the representatives of the $k$-dimensional CAD are exactly the representatives of the $(k+1)$-dimensional CAD \emph{projected onto $\R^k$}.
Similarly, the \emph{full-cell representations} of a $k$-dimensional cell can directly be extended to the representations of the corresponding $(k+1)$-dimensional cells.

Of course, we want to construct the $(k+1)$-dimensional CAD, irrespective of \emph{which} sample points were used for the $k$-dimensional CAD.
As we argued that all sample points from one region are equivalent, it should not make a difference which one was selected.
This immediately yields that the projections of two $(k+1)$-dimensional CAD cells onto $\R^k$ must be \emph{either identical or disjoint}. The intuitive argument is as follows: if the projections of two cells $C_1$, $C_2$ overlap but are not identical, it would be possible to select a representative $s_k \in C_1 \setminus C_2$ -- recall that all samples within a cell are supposed to be equivalent. However, this representative can not be extended to a sample point $s_{k+1} \in C_2$ and, thus, we may not obtain a sample point from $C_2$ at all.

\subsection{Cylindricity and delineability\label{ssec:cad:cylindricity-delineability}}

\silentdefindex{cad:cylindricity}\silentdefindex{cad:delineability}
The above observation about how two CAD cells relate results in two crucial concepts that describe how a CAD is built: \emph{cylindricity} and \emph{delineability}.
While \emph{cylindricity} describes how the cells look like and how they are arranged relative to each other, \emph{delineability} gives a criterion to identify such cells.
We now give very brief definitions and some intuitive descriptions of these concepts here and refer to any of~\cite{Collins1975,Arnon1984,McCallum1988} for more detailed definitions and more extensive discussions.
As we have already argued, the projections of two $(k+1)$-dimensional CAD cells onto $\R^k$ must be \emph{either identical or disjoint}, and we formalize this statement in \cref{def:cylindricity}.

\begin{definition}{Cylinders and cylindrically arranged cells}{cylindricity}
	Let $C$ be a set of $k$-dimensional cells.
	We say that the cells $C$ are \emph{arranged in cylinders} (or simply \emph{are cylindrical}) if
	\[
		\Forall{c_1, c_2 \in C} \left( \Projection_{k-1}(c_1) = \Projection_{k-1}(c_2) \right) \lor \left( \Projection_{k-1}(c_1) \cap \Projection_{k-1}(c_2) = \emptyset \right)
	\]
	where $\Projection_{k-1}$ denotes projection onto $(k-1)$-dimensional space.
	For any $c \in C$, we call $c \times \R$ a \emph{($k$-dimensional) cylinder over $c$}. 
\end{definition}

For cells that are \emph{arranged in cylinders}, sample points for all $(k+1)$-dimensional cells from a particular cylinder can be obtained by extending \emph{any} sample point from the $k$-dimensional cell underlying this cylinder, as the projection of every cell from this cylinder onto $\R^k$ \emph{is identical}.

To obtain a set of cells that is cylindrical, we need some way to identify cells $C$ such that their sign-invariant regions over $C$ are arranged cylindrically -- in particular $C$ must be so that the projection onto $R^k$ of every sign-invariant cell that intersects $C \times \R$ is $C$, and not only some subset of $C$.
We can reformulate this in terms of the root surfaces of the involved polynomials as what we call \emph{delineability}: we can use a cell $C$ if the \emph{number and order} of roots of all polynomials are invariant over $C$.

\begin{definition}{Delineability}{delineability}
	Let $P \subsetneq \R[x_1, \dots, x_k+1]$ be a set of polynomial over $k+1$ variables and $C$ a $k$-dimensional cell.
	We call $P$ \emph{delineable over $C$} if the number of real roots of polynomials from $P$ and their multiplicities are constant on $C$.
\end{definition}

Intuitively, a set of polynomials is delineable over a cell if their root surfaces are properly ``stacked'' above this cell in the sense that every two root surfaces are either identical (over this cell) or do not touch each other (over this cell).

Apparently, we need some way to construct such a lower-dimensional CAD, in particular, we need to obtain an appropriate set of lower-dimensional polynomials that induce the lower-dimensional CAD.
The characterization of \emph{delineability} paves the way to a constructive method by identifying polynomials that have roots where the \emph{number or order} of roots of any polynomials change.
We discuss how we identify and characterize these places (or rather borders) in the following \cref{sec:cad:projection-operators}.

Inspired by the relation between the CAD cells -- defined via the projection of higher-dimensional cells -- we call this process \emph{projection}, while the stepwise construction of sample points is called \emph{lifting}.
In literature, the terms \emph{elimination} and \emph{construction} are sometimes used as well.
Before we continue with more detailed descriptions of these individual components, let us consider a full example for a CAD that illustrates the aforementioned concepts.

\subsection{CAD by example}

We now show the construction of a full CAD by the example of the two constraints $y - x^2 + 1 > 0$ and $2 y - x - 2 < 0$, combined into the input formula $\varphi \coloneqq (y - x^2 + 1 > 0) \land (2 y - x - 2 < 0)$.
In the following figures, we show the root surfaces of the corresponding polynomials $p_1 \coloneqq y - x^2 + 1$ and $p_2 \coloneqq 2 y - x - 2$.

The satisfying regions for each constraint are depicted in \cref{fig:full-cad:1} as striped areas and we observe that the whole formula is satisfied by the region in between the two curves.
Note that we only consider what are commonly called \emph{full-dimensional} (or \emph{open}) cells in this example.

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includetikz{05-full-cad-1}
		\caption{Solutions of $\varphi$}\label{fig:full-cad:1}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includetikz{05-full-cad-2}
		\caption{Projection of cells onto $x$-axis}\label{fig:full-cad:2}
	\end{subfigure}
	\caption{Decompositions based on two constraints}
\end{figure}

We discussed that every CAD cell is \emph{sign-invariant} with respect to the polynomials from $\varphi$, or at least \emph{truth-table invariant} with respect to $\varphi$.
A first attempt might be to construct two cells: one for the satisfying region and one for the rest as depicted in \cref{fig:full-cad:2}.
Considering the projections of these cells $C_1$ and $C_2$ onto the $x$-axis, however, we observe that they are not arranged cylindrically as their projections onto the $x$ axis intersect but are not identical.

To obtain a cylindrical arrangement of cells, we need to \emph{split $C_2$} at the boundaries of $C_1$ into multiple cells as shown in \cref{fig:full-cad:3}.
This already yields five cells that are now cylindrically arranged and \emph{truth-table invariant}.
Note how both $C_1$ and $C_5$ are not \emph{sign-invariant} as both $c_1$ and $c_2$ have root surfaces within these cells.
A sign-invariant CAD thus has both $C_1$ and $C_5$ separated into multiple cells by the root surfaces of $c_1$ and $c_2$ as shown in \cref{fig:full-cad:4}.
Also note that we have ignored the fact that sign-invariance also recognizes the root surfaces as cells themselves and instead assumed the notion of open CAD (as described in \cref{ssec:cad:open-cad}).

\begin{figure}
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includetikz{05-full-cad-3}
		\caption{Truth-table invariant cells}\label{fig:full-cad:3}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includetikz{05-full-cad-4}
		\caption{Sign-invariant cells}\label{fig:full-cad:4}
	\end{subfigure}
	\caption{CAD cells based on two constraints}
\end{figure}

It is usually difficult to obtain a truth-table invariant CAD directly, at least if no equational constraints as described in \cref{ssec:cad:equational-constraints} are present and allow for techniques like the ones from~\cite{Bradford2016}.
We would need to compute a sign-invariant CAD first and then merge adjacent cells, if appropriate. We thus assume to work with sign-invariant CADs for most of this work as the effort of merging cells is not necessary to decide upon the satisfiability of a formula.
A notable exception is quantifier elimination as described in \cref{sec:cadSMT:qe}, where merging cells allows for smaller resulting formulae.

The algorithmic construction of this CAD proceeds in the aforementioned two stages: projection and lifting. While the projection constructs lower-dimensional polynomials from the input polynomials, the lifting uses these dimension-wise to build sample points that represent the regions depicted in \cref{fig:full-cad:4}.
As we give more detailed descriptions of both the projection and the lifting in the two subsequent sections, we only give a very brief overview here.

From $\varphi$ we extract the polynomials $P = \{y - x^2 + 1, 2 y - x - 2 \}$. The projection additionally yields $Proj(P) = \{2 x^2 - x - 4\}$ which has real roots exactly at the boundaries of the projections of the cells that we identified in \cref{fig:full-cad:2}: $\approx -1.19$ and $\approx 1.69$.
The lifting procedure now constructs a $1$-dimensional CAD, represented by $1$-dimensional sample points, as shown in \cref{fig:full-cad:5}.

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.9\textwidth}
		\centering
		\includetikz{05-full-cad-5}
		\caption{$1$-dimensional CAD}\label{fig:full-cad:5}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includetikz{05-full-cad-6}
		\caption{2-dimensional cylinders}\label{fig:full-cad:6}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includetikz{05-full-cad-7}
		\caption{$2$-dimensional CAD}\label{fig:full-cad:7}
	\end{subfigure}
	\caption{Building a $2$-dimensional CAD}
\end{figure}

The first step is to construct $2$-dimensional cylinders over the $1$-dimensional CAD cells, as depicted in \cref{fig:full-cad:6}.
Note how every sample that represents a cell of the $1$-dimensional CAD induces a line in every $2$-dimensional cylinder: all representatives of $2$-dimensional cells will be constructed on the corresponding line.
To subdivide the cylinders into $2$-dimensional cells, we consider the root surfaces of every $2$-dimensional polynomial. As we ensure delineability, we know that for every representative of the $1$-dimensional CAD -- and thereby every line in the $2$-dimensional cylinder -- the real roots of the polynomials over this representative -- or its intersections with the line -- are \emph{equivalent} in the sense that their number and order remains the same.

The construction of these representatives is shown in \cref{fig:full-cad:7}.
We start by computing the real roots of every $2$-dimensional polynomial over every $1$-dimensional representative -- recall \cref{ssec:ran:computation} for how to do this -- and additionally select sample points below the smallest root, between every two consecutive roots, and above the largest root.
Altogether, we obtain $21$ sample points (of dimension $2$) for this example, including two sample points for the rightmost cylinder that lie outside of \cref{fig:full-cad:7}.

In the following, we present different existing methods for the \emph{projection operator} we denoted by $Proj$ in \cref{sec:cad:projection-operators} and provide some more details on the lifting procedure in the subsequent \cref{sec:cad:lifting}.
