\chapter{Implementation}\label{ch:mcsat-implementation}

\silentindex{mcsat-implementation}
We defined \MCSAT to be agnostic of the actual theory that is being used, given that appropriate subroutines for the theory reasoning are provided.
Due to the scope of this thesis, and to ease the presentation, we now focus on the case of real arithmetic. Hence, all theory variables range over $\R$ and only subroutines for (linear or nonlinear) real arithmetic are discussed.
Implementations for other theories exist, though, for example, for the theory of uninterpreted functions as described in~\cite{Jovanovic2013}, for nonlinear integer arithmetic in~\cite{Jovanovic2017a}, for bit-vectors in~\cite{Zeljic2016,Graham2017}, or generic improvements for theory combination in~\cite{Bobot2018}.

We have already observed in the definition of \MCSAT in \cref{sec:mcsat:definition} that one major part of \MCSAT is the Boolean reasoning which essentially is a \CDCL-style SAT solver.
Over time, all competitive implementations of SAT solvers have accumulated many techniques that make their usage fast in practice that are not (directly) mentioned in this definition.
Well-known examples of this are decision heuristics (see \cref{ssec:cdcl:decision}), restarts and clause removal (see \cref{ssec:cdcl:restart}), or the \emph{two watched literal scheme} (see \cref{ssec:cdcl:watched-literal}) -- not to mention low-level implementation tricks to make all this (for example) cache efficient.

It seems both pointless and infeasible to replicate all this in the context of a novel \MCSAT solver. Instead, we propose to enhance an existing \CDCL-style SAT solver -- or even a \CDCLT-style SMT solver -- to support \MCSAT-style SMT solving.
Thereby, we can simply inherit all the infrastructure and implementation for the SAT-solving part and ``only'' need to extend it appropriately.

We concede that we are giving up some flexibility in how we integrate the Boolean reasoning within the \MCSAT proof system. Furthermore, we may inherit a certain amount of \emph{technical debt} or simply design decisions that were appropriate for a SAT solver, but may not be for an \MCSAT solver.
However, we hope to profit from a mature implementation of the Boolean reasoning.

In the following, we discuss how we extended \SMTRAT, usually used as a \CDCLT-style SMT solver as described in~\cite{Corzilius2015}, for the \MCSAT framework.
This discussion includes not only our current implementation but also future extensions and alternative design choices (that we rejected for certain reasons).
Subsequently, we discuss our methods for finding assignments and generating explanations and, finally, present a selection of experimental results.
