\section{Changing perception}

We have introduced and understood CAD as a rather monolithic method to decompose the solution space into regions that are invariant with respect to the evaluation of the input formula. These regions then allowed us to answer certain questions about the formula, including whether it is satisfiable.

In this section, we build another intuition about the nature of CAD, which helps us motivate what we call \emph{incremental CAD}.
We restrict to the case of SMT solving, that is our formula is purely existentially quantified. The main insight to gain is that we are looking for a method that produces a satisfying witness, and once we have obtained such a satisfying assignment, we discard all the other information a CAD offers.

Recall the two components of CAD (projection and lifting) and their intuitive meanings. The projection accumulates polynomials that induce borders of sign-invariant regions and as such holds the algebraic information about the solution space. The lifting, on the other hand, constructs individual sample points that represent these regions.

Slightly twisting our view on the lifting process, we can also understand it as a \emph{heuristic} process of \emph{guessing} and the projection as merely guiding it. Adopting this notion, we can understand the entire CAD method as a procedure to guess sample points that is guided by the algebraic structure of the input, yielding some interesting observations.

For once, we can terminate the CAD method as soon as the lifting has found a single satisfying sample point because the formula is only quantified existentially. We do not have to care about all the other regions and sample points as a single satisfying sample point is all we need to infer satisfiability. This is essentially an application of~\cite{Collins1991} to the purely existential case.

Furthermore, we could hope to find the satisfying sample point without even considering a (full) projection. If only a few polynomials in the projection are enough to guide the lifting towards a satisfying sample point, we can happily ignore the rest of the projection.
Finally, having identified the lifting as the central component for our purpose, we can turn the program flow of the CAD method around.
We start with the lifting process and occasionally -- for example, when no or only expensive lifting steps can be done -- spend a bit of work on the projection and thereby try to \emph{avoid computing} a full projection altogether.

We can summarize our approach with three goals that seem pretty simple:
\begin{enumerate*}
	\item if a satisfying sample point exists, find it as fast as possible and avoid any additional work,
	\item if no satisfying sample point exists, eventually produce a regular CAD and inherit all formal guarantees,
	\item do this in a way that allows for incremental computations and backtracking (in the spirit of \cref{sec:SMT:compliancy}).
\end{enumerate*}

In the following, we use this intuition to define a \emph{proof system} for nonlinear real arithmetic constraints based on CAD.
The following presentation does not shed a lot of light on how to implement this approach efficiently, thus we refer to~\cite{Kremer2020} for details on efficient data structures.
