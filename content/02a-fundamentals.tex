\section{Fundamentals}\label{sec:preliminaries:fundamentals}

We start with some fundamental definitions to set a common notation. Of course, we already build on other elementary concepts here and refer, for example, to~\cite{Artin1991} for definitions of algebraic concepts.

\begin{definition}{Total order}{totalorder}\silentdefindex{total-order}
	Let $\Omega$ be some set of elements. A \emph{total order} $\leq$ on $\Omega$ is a relation that adheres to the following properties:
	\begin{align*}
		\Forall{a,b \in \Omega} & (a \leq b \land b \leq a) \implies a = b && \textrm{(antisymmetry)} \\
		\Forall{a,b,c \in \Omega} & (a \leq b \land b \leq c) \implies a \leq c && \textrm{(transitivity)} \\
		\Forall{a,b \in \Omega} & (a \leq b \lor b \leq a) && \textrm{(connexity)}
	\end{align*}
\end{definition}

\begin{definition}{Ring}{ring}\silentdefindex{ring}\silentdefindex{integral-domain}
	Let $R$ be a set of elements with binary operations $+$ and $\cdot$ such that $(R,+)$ is a commutative group, $(R,\cdot)$ is a semigroup, and the operation $\cdot$ is distributive with respect to $+$.
	We call $(R,+,\cdot)$ a \emph{ring} and usually only denote it by $R$.

	If $(R,\cdot)$ is a monoid, that is $1 \in R$, where $1$ is the multiplicative identity, we call $R$ a \emph{ring with $1$}.
	If $\cdot$ is commutative over $R$, we call $R$ \emph{commutative}.
	If $\Forall{a,b,c \in R} (a \cdot b = a \cdot c \land a \neq 0)\rightarrow b = c$ we call $R$ an \index{integral-domain}.
\end{definition}

Recognizing that rings without $1$ and non-commutative rings are interesting concepts in their own rights, all rings within this work are both \emph{with $1$} and \emph{commutative} and we simply call such objects \emph{rings}.

\begin{definition}{Sets of Numbers}{numbers}\silentdefindex{numbers}
	Let $\B = \{ \true, \false \}$ be the set of Booleans, $\N = \N_0 = \{ 0, 1, \dots \}$ the set of \emph{natural numbers}, $\Z$ the set of \emph{integers}, $\Q$ the set of \emph{rational numbers}, and $\R$ the set of \emph{real numbers}.
	Furthermore, we denote the set of \emph{real algebraic numbers} by $\Rat$ and refer to \cref{sec:preliminaries:ran} for a definition.
\end{definition}

\begin{definition}{Indexed sets}{indexed-sets}\silentdefindex{indexed-sets}
	We write $\overline{\omega}$ for a set of elements $\{\omega_1, \dots, \omega_n\}$ and specify $n$ if relevant and not clear from the context.
\end{definition}

\begin{definition}{Power sets}{powerset}\silentdefindex{powerset}
	Let $\Omega$ be some set of elements. We define the \emph{power set} of $\Omega$ as
	\[
		\Powerset(\Omega) = \{ \omega \mid \omega \subseteq \Omega \}
	\]
\end{definition}

We sometimes talk about \emph{sequences} of certain objects, for example the \emph{trails} of \DPLL, \CDCL, \CDCLT, or \MCSAT.
We fix a common notation in the following \cref{def:sequence}.

\begin{definition}{Sequences}{sequence}\silentdefindex{sequence}
	Let $\Omega$ be some set of elements.
	A (finite) \defindex{sequence} $M$ over $\Omega$ is an ordered (finite) list of elements from $\Omega$ and we write
	\[
		M = \Seq{\omega_1, \dots \omega_k}
	\]
	By abuse of notation, we may also use $M$ as a set and write $M \subseteq \Omega$, $M_1 \subseteq M_2$, $\omega \in M$, or $|M|$.
	We call $M' = \Seq{\omega_{i_1}, \dots, \omega_{i_l}}$ a \emph{subsequence of $M = \Seq{\omega_1, \dots \omega_k}$} if $M' \subseteq M$ and $i_1 < \cdots < i_l$.
	Furthermore, we write $\Seq{M, \omega}$ to \emph{append $\omega$ to $M$} or in general $\Seq{M_1, \omega, M_2}$ for the \emph{concatenation} of $M_1$, $\Seq{\omega}$, and $M_2$.
	We call $M_1$ ($M_2$) a \emph{prefix (suffix) of $M = \Seq{M_1, \omega, M_2}$}.
\end{definition}
