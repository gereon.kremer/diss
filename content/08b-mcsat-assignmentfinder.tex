
\section{Assignment finder}\label{sec:mcsatimpl:assignment-finder}

\silentindex{mcsat:assignment-finder}
The first novel subroutine we need for \MCSAT is for generating a new assignment in the \rulename{T-Decide} rule such that $\Consistent(\Seq{M, x \mapsto \alpha_x})$. We have given some intuition on how such an assignment finder could work in \cref{sec:mcsat:assignment-finding}.

We first present a variant that employs real root isolation to explore all sign-invariant regions of univariate constraints similar to the CAD lifting process.
Then we show an alternative based on a general SMT solving engine that allows us to explore multiple variables at once.

Note that we allow considering constraints that are not \emph{syntactically} univariate here. While implicitly forbidden in~\cite{Jovanovic2012} -- as only syntactically univariate clauses can be \emph{selected} -- we allow to use them in line with~\cite{Moura2013}. Though this implies some interesting corner cases that the assignment finder has to deal with, it also enables significant improvements as the following \cref{ex:mcsat:non-univariate-literals-for-assignment-finder} shows.
Of course, it only makes sense to enhance assignment finders if we also reason about these non-univariate literals in Boolean decisions and propagations -- otherwise, non-univariate literals are never added to the trail.

\begin{example}{Non-univariate literals in assignment finding}{mcsat:non-univariate-literals-for-assignment-finder}
	Let $M = \Seq{ x_1 \cdot x_n + x_2 > 0, x_1 \mapsto 0 }$ and assume that we try to apply \rulename{T-Decide} to find an assignment for $x_2$ next.

	If we were to ignore the constraint -- following~\cite{Jovanovic2012} as it is not univariate -- we might select $x_2 \mapsto 0$ and may further spend a lot of effort on theory variables $x_3 \dots x_{n-1}$ until we eventually realize that $x_2 \mapsto 0$ gives rise to a conflict that is only due to $x_1 \mapsto 0$ and $x_2 \mapsto 0$.

	If we instead consider the constraint right away, we can make use of $x_2 > 0$ and avoid a lot of theory reasoning for a case we can already prove to be infeasible.

	This case may even be more obvious if the non-univariate constraint leads to a direct conflict as for $M = \Seq{ x_1 \cdot x_n > 0, x_1 \mapsto 0 }$.
	
	We can immediately enter conflict resolution if the constraint is added to the trail while~\cite{Jovanovic2012} would wait for theory level $n$ to even look at the constraint and realize its erroneous theory decision early on.
\end{example}

\subsection{Assignment finding based on real root isolation\label{ssec:mcsat:assignment-finder}}

\silentdefindex{mcsat:assignment-finder:rri}
Considering the knowledge about CAD from the previous part of this work, we propose to find assignments as described in the following. Note that we assume this to be how other implementations work as well -- for example~\cite{Jovanovic2012,Jovanovic2013} -- though they do not provide many details on this point.

The approach consists of the following three steps:
\begin{enumerate*}
	\item partially evaluate all theory literals from $M$ over the partial model induced by $M$ and select those whose result is a univariate constraint in the theory variable $v$ that is to be assigned,
	\item compute all real roots of these univariate constraints to obtain a sign-invariant decomposition of the real space for $v$, and
	\item either identify a sign-invariant region that satisfies all constraints or realize that no such region exists and compute an infeasible subset, very similar to what is shown in \cref{sec:cad:infeasible-subsets}.
\end{enumerate*}

The first two steps are essentially equivalent to a single lifting step in CAD and we, therefore, refer to \cref{sec:cad:lifting} for a more detailed description.
If the model contains real algebraic numbers, it may not be possible to directly substitute the model into a constraint -- see the discussion in \cref{sec:preliminaries:ran} -- and the underlying method to isolate real roots should also be able to deal with the case that the polynomial turns out not to be univariate. We abstract from this detail to simplify the presentation.

If a satisfying region exists in the third step, we only need to return some value from this region.
If it does not exist, though, we need to find an infeasible subset and we refer to \cref{sec:cad:infeasible-subsets} for more details on this issue.
This yields the following \cref{algo:mcsat:assignment-finder-nlsat}.

\begin{algorithm}[H]
	\caption{Find an assignment by real root isolation}\label{algo:mcsat:assignment-finder-nlsat}
	
	\SetKwFunction{FindAssignmentRRI}{FindAssignment$_{RRI}$}
	\Fn{\FindAssignmentRRI{$M, v$}}{
		$\alpha \coloneqq $ model from $M$ \;
		$C \coloneqq \{ L \in M \mid L[\alpha] \textrm{ is univariate in } v \}$ \;
		$R \coloneqq \Roots(P, \alpha)$ where $P$ are all polynomials from $C$ \;
		$S \coloneqq \textrm{ sample points based on } R$ \;
		\If{satisfying $s \in S$}{
			\Return{$s$}
		}
		\Else{
			\Return{cover of $C$ for $S$}
		}
	}
\end{algorithm}

\subsection{Generic SMT-based assignment finding\label{ssec:mcsat:assignment-finder:SMT}}

\silentdefindex{mcsat:assignment-finder:SMT}
Let us take a step back and abstract a bit from what we defined to be an assignment finder. We try to find a new theory assignment for some variable that satisfies some set of constraints or gives a subset of these constraints that certifies that no such assignment exists.
A more general formulation of this would be to \emph{extend the model for a set of constraints or produce an infeasible subset} -- which is almost what we called a \emph{theory query} in the regular SMT framework.

Therefore, we propose to use an appropriate \emph{SMT compliant theory solver} for this task, slightly enhanced by a wrapper that takes care of the partial model we already have.
Note that if the constraints contain more than just a single unassigned variable we technically invest more work than necessary as we also obtain a model for all other unassigned variables. We might, however, be able to leverage this additional work as well.
If we obtain a model, we can perform multiple theory decisions at once -- though it is not clear whether this is beneficial -- or cache the additional assignments and use them for the next theory decision if they are still consistent with the trail.

We may, however, also be able to identify a conflict earlier now as we essentially have a lookahead into further theory variables and thus can avoid some work, as the following \cref{ex:mcsat:assignment-finder-lookahead} illustrates.

\begin{example}{Benefits of an assignment finder with lookahead}{mcsat:assignment-finder-lookahead}
	Let $M = \Seq{ x_1 > 0, x_n > 0, x_1 + x_n < 0 }$ and assume we try to invoke \rulename{T-Decide} on $x_1$ next. A regular assignment finder without lookahead could select $x_1 \mapsto 1$ and continue from there, possibly with a lot of unnecessary reasoning about $x_2, \dots x_{n-1}$, until we eventually realize that we have a conflict due to this assignment.
	An assignment finder with lookahead, however, could try to find a full model for these three constraints and immediately identify a conflict.
\end{example}

We formulate the following \cref{algo:mcsat:assignment-finder-SMT} as a thin wrapper around an arbitrary theory solver. We deliberately do not specify how the model $\alpha$ is combined with the theory literals $C$, as this depends on the exact model and the capabilities of the theory solver.

We usually try to substitute $\alpha$ into $C$ if possible, however, we may leave the realm of our constraints -- for example, due to real algebraic numbers in our model. In this case, we may need to inject new constraints that explicitly state equality to real algebraic numbers if the theory solver supports these constraints.
We may also be forced to fail and fall back to the regular assignment finder \FindAssignmentRRI.

\begin{algorithm}[H]
	\caption{Find an assignment by theory solving}\label{algo:mcsat:assignment-finder-SMT}

	\SetKwFunction{FindAssignmentSMT}{FindAssignment$_{SMT}$}
	\Fn{\FindAssignmentSMT{$M, v$}}{
		$\alpha \coloneqq $ model from $M$ \;
		$C \coloneqq $ theory literals from $M$ \;
		\Return{
			$\texttt{call\_theory}(C \land \alpha)$ \;
		}
	}
\end{algorithm}

As already indicated, it could make sense to combine different methods for finding assignments. Different instantiations of \FindAssignmentSMT\ -- with different theory solvers -- could be used that fail if the model is not compatible with the respective theory solver and hand the task over to the next assignment finder, or ultimately \FindAssignmentRRI.
However, we could also employ a whole theory solving strategy, for example, as described in~\cite{Corzilius2015}, to instantiate \FindAssignmentSMT.
