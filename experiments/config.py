import re

def texName(solver):
	d = {
		"smtrat_SAT": "SAT",
		"smtrat_naive": "Naive",
		"smtrat_NO": "NO",
		"smtrat_NU": "NU",
		"smtrat_SO": "SO",
		"smtrat_SU": "SU",
		"smtrat_FU": "FU",
		"smtrat_FU_SC": "SC",
		"smtrat_FU_SI": "SI",
		"smtrat_FU_SL": "SL",
		"smtrat_FU_SR": "SR",
		"smtrat_FU_SZ": "SZ",
		"smtrat_FU_SInf": "SInf",
		"smtrat_LO_Type": "LOType",
		"smtrat_LO_T": "LOT",
		"smtrat_LO_TLSA": "LOTLSA",
		"smtrat_LO_TSA": "LOTSA",
		"smtrat_LO_TS": "LOTS",
		"smtrat_LO_LT": "LOLT",
		"smtrat_LO_LTA": "LOLTA",
		"smtrat_LO_LTS": "LOLTS",
		"smtrat_LO_LTSA": "LOLTSA",
		"smtrat_LO_LS": "LOLS",
		"smtrat_LO_S": "LOS",
		"smtrat_POD": "PO-D",
		"smtrat_POLD": "PO-LD",
		"smtrat_POPD": "PO-PD",
		"smtrat_POSD": "PO-SD",
		"smtrat_POlD": "PO-lD",
	}
	if solver in d: solver = d[solver]
	solver = solver.replace('smtrat_', '')
	solver = re.sub('CAD_I([^_]*)_LO([^_]*)_MIS([^_]*)_P([^_]*)_PO([^_]*)_PP([^_]*)', 'CAD-\\1-\\2-\\3-\\4-\\5-\\6', solver)
	solver = re.sub('MCSAT_([^_]*)_([^_]*)_([^_]*)', 'MC-\\1-\\2-\\3', solver)
	solver = solver.replace('_', '\_')
	return solver

def shortName(solver):
	d = {
		"smtrat_SAT": "SAT",
		"smtrat_naive": "Naive",
		"smtrat_NO": "NO",
		"smtrat_NU": "NU",
		"smtrat_SO": "SO",
		"smtrat_SU": "SU",
		"smtrat_FU": "FU",
		"smtrat_FU_SC": "SC",
		"smtrat_FU_SI": "SI",
		"smtrat_FU_SL": "SL",
		"smtrat_FU_SR": "SR",
		"smtrat_FU_SZ": "SZ",
		"smtrat_FU_SInf": "SInf",
		"smtrat_LO_Type": "LOType",
		"smtrat_LO_T": "LOT",
		"smtrat_LO_TLSA": "LOTLSA",
		"smtrat_LO_TSA": "LOTSA",
		"smtrat_LO_TS": "LOTS",
		"smtrat_LO_LT": "LOLT",
		"smtrat_LO_LTA": "LOLTA",
		"smtrat_LO_LTS": "LOLTS",
		"smtrat_LO_LTSA": "LOLTSA",
		"smtrat_LO_LS": "LOLS",
		"smtrat_LO_S": "LOS",
		"smtrat_POD": "PO-D",
		"smtrat_POLD": "PO-LD",
		"smtrat_POPD": "PO-PD",
		"smtrat_POSD": "PO-SD",
		"smtrat_POlD": "PO-lD",
	}
	if solver in d: solver = d[solver]
	return solver