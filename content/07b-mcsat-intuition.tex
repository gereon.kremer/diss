\section{Intuition}

\silentindex{mcsat:intuition}
The \MCSAT proof system is rather similar to \CDCLT in that it incorporates all the techniques for Boolean reasoning, but it adds theory reasoning into the core solving engine. We try to give some insight into the main ideas of \MCSAT and how it may help in practical solving tasks.

Let us first recall how \CDCLT deals with its theory. The \CDCLT proof system almost exclusively deals with the Boolean reasoning and offloads all theory reasoning to a few very abstract proof rules.
In practice, a \CDCLT solver regularly calls out to a theory solver to check whether the current trail is consistent in the theory. In the case of inconsistency, the negation of a subset of the trail -- an \emph{infeasible subset} -- is learned as a conflict clause and triggers the conflict analysis.

Any other form of theory learning through the generation of lemmas is pretty difficult, as the theory solver has no indication of what to generate. Literally, the task is \emph{generate something we do not know yet}.
Practical experience shows that there is only a fine line between not being able to construct any lemmas and flooding the Boolean reasoning with large amounts of irrelevant or essentially redundant lemmas.

\MCSAT moves some part of the theory reasoning into the core solving component: it integrates the construction of a (partial) theory model. Now, the solver does not only work its way through the Boolean search space but at the same time explores the space of theory solutions.
This allows the solver to guide the Boolean search using knowledge (or assumptions) about the theory fairly easily, hopefully avoiding dead ends that are easy to see in the theory.
Furthermore, the partial model provides a hint to the theory solver \emph{what lemmas to generate}.
