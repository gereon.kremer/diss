import math
import random

import database
import render

def rnd():
	return random.uniform(0.95,1.05)

def expsum(min, max, base = 1.2):
	return (max ** 3) / 100
	return (math.pow(base, min) - math.pow(base, max + 1)) / (1 - base) / 10

def exp(e, base = 1.2):
	return e ** 2
	return math.pow(base, e)

def cap(v):
	return min([v, 120])


def load_demo_solvers():
	database.create_demo_solver("demo-A", lambda i: (
		(2 if i % 2 == 0 else 1)*i*rnd(),
		(2 if i % 2 == 0 else 1)*i*rnd(),
	))
	database.create_demo_solver("demo-B", lambda i: (
		(2 if i % 2 == 1 else 1)*i*rnd(),
		(2 if i % 2 == 1 else 1)*i*rnd(),
	))
	database.create_demo_solver("demo-C", lambda i: (
		(2 if i % 2 == 1 else 1)*i*rnd(),
		(2 if i % 2 == 1 else 1)*i*rnd(),
	))
	database.create_demo_solver("demo-D", lambda i: (
		(4*i**2 if i % 2 == 0 else 1*i**2)/125*rnd(),
		(4*i**2 if i % 2 == 0 else 1*i**2)/125*rnd(),
	))
	database.create_demo_solver("demo-E", lambda i: (
		i / 10.0 if i <= 20 else (4*(i-20)**2 if i % 2 == 0 else 1*(i-20)**2)/50*rnd(),
		i / 10.0 if i <= 20 else (20*(i-20)**2 if i % 2 == 0 else 1*(i-20)**2)/50*rnd(),
	))
	#database.create_demo_solver("demo-D", lambda i: 
	#	(exp(i)/expsum(1,500) if i <= 500 else 140, exp(i)/expsum(1,500)*500 if i <= 500 else 140)
	#)
	#database.create_demo_solver("demo-C", lambda i: 
	#	(i*0.004 if i <= 500 else (i if i <= 510 else 140), 1 if i <= 500 else 140)
	#)
	#database.create_demo_solver("demo-D", lambda i: 
	#	(i*0.04 if i <= 510 else 140, 1 if i <= 500 else 140)
	#)
	database.commit()

def render_demo_solvers():
	render.table('table-demo.tex', ["demo-A", "demo-B", "demo-C", "demo-D", "demo-E"], template = 'table2')
	
	render.scatter('scatter-demo-A-B.data', 'demo-A', 'demo-B')
	render.scatter('scatter-demo-A-C.data', 'demo-A', 'demo-C')
	render.scatter('scatter-demo-A-D.data', 'demo-A', 'demo-D')
	render.scatter('scatter-demo-A-E.data', 'demo-A', 'demo-E')
