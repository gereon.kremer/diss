\section{Experimental results}\label{sec:mcsat:experiments}

We have identified three major heuristics for \MCSAT: the assignment finder, the explanation function, and the variable ordering.
Most of the issues discussed in \cref{ssec:cad:heuristics:other} -- factorizing polynomials, how to generate sample points, and many heuristics in various subroutines -- apply to \MCSAT as well, in particular to the different explanation functions.
However, we focus on the aforementioned three issues now.

For these experiments, we use one base solver and analyze the effects of changing every heuristic individually. As this evidently fails to explore the whole range of possibilities, a more extensive analysis seems meaningful.
Our base solver uses the assignment finder based on real root isolation, combines the explanations based on Fourier--Motzkin variable elimination, OneCell, and CAD and uses the \emph{Theory first} variable ordering.
It is called \SolverMCSATAF{}, \SolverMCSATExFMOCNL, and \SolverMCSATVOTF in the three comparisons.

\subsection{Assignment finder}

\silentindex{mcsat:assignment-finder}
We have described two possible implementations for an assignment finder in \cref{sec:mcsatimpl:assignment-finder}.
The first one -- which we call simply \emph{the assignment finder} and denote by \SolverMCSATAF{} -- considering only univariate constraints, uses real root isolation to identify a region that satisfies all constraints and samples some value from this region.
The second one -- called \emph{SMT assignment finder} amd denoted by \SolverMCSATSMT{} -- employs an arbitrary SMT strategy for theory calls.

We have made the experience that using a full-fledged strategy for the SMT assignment finder essentially bypasses \MCSAT and thereby worsens performance, thus we propose to use an incomplete strategy.
We are simply using a linear theory solver -- based on the simplex method -- which constructs an assignment based on the linear constraints, falling back to the regular assignment finder if this \emph{linear model} does not satisfy the nonlinear constraints.

\begin{table}[h]
	\centering
	\input{experiments/output/table-mcsat-af}
	\caption{Experimental results for different assignment finders.}\label{tbl:mcsat:af-results}
\end{table}

We show a comparison of \MCSAT solvers with the two different assignment finders in \cref{tbl:mcsat:af-results} and, as already indicated, \SolverMCSATSMT{} performs significantly worse than \SolverMCSATAF{} on both satisfiable and unsatisfiable inputs -- at least on the benchmark set we consider.
Nevertheless, we conjecture that a more careful adaption of an SMT-based assignment finder, or any other scheme that employs a larger lookahead, could provide improvements in other scenarios.

\subsection{Explanation functions}

\silentindex{mcsat:explanation-function}
Apart from all the explanation functions, we have also discussed how to combine them. Given that this work is not concerned with concurrency (or parallelism) within an SMT solver, we ignore the possibility of parallel composition and only consider sequentially composed explanation backends.

In our implementation, the CAD-based explanation function is the only \emph{complete} method for nonlinear arithmetic.
The OneCell explanation uses McCallum's projection, following~\cite{Brown2015}, and fails in cases where correctness can not be ensured.
We consider all other explanation functions to be \emph{easy shortcuts} to the CAD-based explanations and thus all proposed explanation backends eventually fall back to them.

For this evaluation, we consider the following explanation backends:
\begin{enumerate*}
	\item CAD (\SolverMCSATExNL),
	\item OneCell and CAD (\SolverMCSATExOCNL),
	\item Fourier--Motzkin, OneCell and CAD (\SolverMCSATExFMOCNL),
	\item Fourier--Motzkin, ICP, OneCell and CAD (\SolverMCSATExFMICPOCNL),
	\item Fourier--Motzkin, VS, OneCell and CAD (\SolverMCSATExFMVSOCNL), and
	\item Fourier--Motzkin, ICP, VS, OneCell and CAD (\SolverMCSATExFMICPVSOCNL).
\end{enumerate*}

\begin{table}
	\centering
	\input{experiments/output/table-mcsat-explanations}
	\caption{Experimental results for different explanation functions.}\label{tbl:mcsat:explanation-results}
\end{table}

A comparison of these explanation backends is given in \cref{tbl:mcsat:explanation-results}, highlighting significant differences in practical performance.
First and foremost, it appears to be extremely beneficial to combine multiple explanation backends as proposed earlier.
We doubt that these major improvements are due to better performance, but rather think that ``easier'' explanation backends like the Fourier--Motzin-based one construct both \emph{easier} but also \emph{more powerful} explanations that exclude larger regions.
One reason might be the fact that CAD-based explanations always construct fully-dimensional explanations while Fourier--Motzkin-based explanations only perform a single elimination step.

\subsection{Variable orderings}

As discussed in \cref{ssec:mcsat:heuristics:variable-ordering}, the variable ordering of both Boolean and theory variables is of major importance and allows for many variants.
We have analyzed the impact of different variable orderings in~\cite{Nalbach2019} and mostly repeat those experiments.

The different orderings are mostly based on the following ingredients:
strictly preferring Boolean or theory over the other; employing a strict ordering or an activity-based dynamic ordering in the spirit of VSIDS as discussed in \cref{ssec:cdcl:decision}; restricting the ordering to only consider \emph{active} literals (or constraints) which are univariate over the current theory model.
For some more aspects and a somewhat more detailed discussion, we refer to~\cite{Nalbach2019}.
We consider the following heuristics:
\begin{description}[nosep, labelindent=3mm]
	\item[\emph{Random} (\SolverMCSATVORND)] uses a random static ordering across all variables. This is only meant as a reference and naturally not intended for a serious solver.
	\item[\emph{Boolean first} (\SolverMCSATVOBF)] strictly prefers Boolean variables using a dynamic ordering for Boolean variables and a static ordering for theory variables.
	\item[\emph{Theory first} (\SolverMCSATVOTF)] strictly prefers theory variables using a static ordering for theory variables and a dynamic ordering for Boolean variables. Note that due to semantic propagations only Boolean variables that do not represent theory constraints are decided.
	\item[\emph{Theory first dynamic} (\SolverMCSATVOTFDYN)] strictly prefers theory variables like \emph{Theory first}, but uses a dynamic ordering for theory variables.
	\item[\emph{Uniform} (\SolverMCSATVOUNIFORM)] uses a uniform dynamic ordering for all variables, increasing the variable activity for both Boolean and theory variables once for every conflict.
	\item[\emph{Uniform + Theory first} (\SolverMCSATVOUNIFORMTF)] follows \emph{Uniform} by using a uniform dynamic ordering for all variables, but strictly prefers theory variables if two variables have the same activity.
	\item[\emph{Univariate} (\SolverMCSATVOUV)] employs a static ordering for theory variables and a dynamic ordering for Boolean variables. Boolean variables are considered for a decision only if the respective theory constraint is univariate under the theory model and the next theory decision is performed when all eligible Boolean variables are assigned.
	\item[\emph{Univariate + active} (\SolverMCSATVOUVactive)] uses the same strategy as \emph{Univariate}, but considers only Boolean variables whose theory constraint is univariate and in addition occur in a \emph{not yet satisfied clause}.
	\item[\emph{NLSAT} (\SolverMCSATVONLSAT)] implements our understanding of the heuristic of~\cite{Jovanovic2012}. Compared to \emph{Univariate + active}, it additionally restricts the Boolean variables to those that occur in \emph{univariate clauses}.
\end{description}

\begin{table}
	\centering
	\input{experiments/output/table-mcsat-varorders}
	\caption{Experimental results for different variable orderings.}\label{tbl:mcsat:varorder-results}
\end{table}

Experimental results for all strategies are given in \cref{tbl:mcsat:varorder-results} and are consistent with what we observed in~\cite{Nalbach2019}.
Note that the results in~\cite{Nalbach2019} were obtained while preprocessing was enabled while we compare only the \MCSAT solver itself.

We are still somewhat puzzled here as we obtain the best results by either strictly preferring theory variables in a fixed order (\SolverMCSATVOTF) or using uniform variable activities (\SolverMCSATVOUNIFORM). All our attempts to find some middle ground and reconcile these strategies yield worse results as we can see at the examples of \SolverMCSATVOUNIFORMTF and \SolverMCSATVOTFDYN.

We can think of two possible resolutions here: 
there could be multiple significantly different strategies that yield somewhat similar results;
alternatively, we are looking at a single (yet unknown) strategy that is nontrivially simulated by both \SolverMCSATVOTF and \SolverMCSATVOUNIFORM while the other strategies for some reason fail to do so.
