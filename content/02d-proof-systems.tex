\section{Deductive proof systems}\label{sec:preliminaries:proof-systems}

A popular framework to perform derivations in a systematic way are (deductive) \emph{proof systems}.
A proof system is a set of \emph{proof rules} that allow deriving new statements from a set of assumptions to eventually obtain a (logically sound) proof in the form of a derivation sequence from the \emph{initial assumptions} $\Phi$ to the \emph{target statement} $\Psi$.
We later consider proof systems that argue about formulae and are at least \emph{sound}: the syntactic transformation is logically sound.

The proof rules are specified so that they can be applied \emph{syntactically} and thus allow to construct a \emph{semantic} proof using only \emph{syntactic} operations.
As we are mostly interested in the satisfiability of formulae, we usually have $\Psi = \false$.
We start with the definition of a single proof rule.

\begin{definition}{Deductive proof rule}{proof-rule}\silentdefindex{proof-rule}
	Let $\overline{\varphi} = \{\varphi_1, \dots, \varphi_n\}$ and $\overline{\psi} = \{\psi_1, \dots, \psi_m\}$ be sets of formulae.
	We call $PR(\overline{\varphi}, \overline{\psi})$ a \emph{proof rule}, commonly write
	\begin{align*}
		\ProofRule{PR}{\varphi_1, \dots, \varphi_k}{\psi_1, \dots, \psi_m}{\varphi_{k+1}, \dots, \varphi_n}
	\end{align*}
	for the proof rule itself, and call $\overline{\varphi}$ the \emph{premises} and $\overline{\psi}$ the \emph{conclusions}. We sometimes split the premises into two parts to simplify the presentation and call $\{\varphi_{k+1}, \dots, \varphi_n\}$ the \emph{side conditions}.

	The proof rule $PR$ is \emph{applicable to a set of formulae $\Phi$} if $f(\overline{\varphi}) \subseteq \Phi$ where $f$ may rename variables from $\overline{\varphi}$ appropriately.
	The result of the \emph{application of $PR$} to a set of formula $\Phi$ is $\Phi \land f(\bigvee \overline{\psi})$ and we write $\Phi \vdash_{PR} \Phi \land f(\bigvee \overline{\psi})$, though we usually omit $f$ to simplify notation.
	We call $\Phi \vdash_{PR} \Psi$ a \emph{derivation} and oftentimes write $\Phi \vdash \Psi$ if $PR$ is either irrelevant or clear from the context.
\end{definition}

The separation of the premises into two sets is purely to simplify the notation and we can move individual formulae into or out of the side conditions without changing the meaning.
We use this distinction (intuitively) to describe \emph{what the rule does} in $\{\varphi_1, \dots, \varphi_k\}$ and \emph{when it is applicable} in $\{\varphi_{k+1}, \dots, \varphi_n\}$.

Though we defined applicability as a purely syntactic property -- $\overline{\varphi} \subseteq \Phi$ up to renaming -- we usually allow $f$ to perform some easy logical reasoning, like conversion to a normal form or deriving immediate corollaries from $\Phi$, as well.
In particular, side conditions are usually not part of the formula but can be easily inferred from it.
We understand proof rules as (almost) syntactic rewriting operators that hide a (more or less) complex reasoning such that it can be automated.

Applying a proof rule requires a \emph{conjunction of premises} but results in a \emph{disjunction of conclusions}, severely limiting the applicability of proof rules to the result of a previous application.
Thus, proof rules usually have only a single conclusion -- all proof rules in this work have this form -- and thereby keep the formula in a conjunctive form.

The above definition of a proof rule poses no restriction on the \emph{logical soundness} of the derivation. However, one can very well argue that a proof rule whose conclusion \emph{does not logically follow} from the assumptions is not particularly useful.
We thus assume all proof rules in this work to be sound in the sense of the following \cref{def:proof-rule-soundness}.

\begin{definition}{Soundness of a proof rule}{proof-rule-soundness}\silentdefindex{proof-rule:soundness}
	We say a proof rule $PR(\overline{\varphi}, \overline{\psi})$ is \emph{sound} if it only allows for \emph{valid derivations}:
	\[
		\Forall{\overline{\varphi}} \left(\bigwedge \overline{\varphi} \vdash \bigvee \overline{\psi}\right) \implies \left(\bigwedge \overline{\varphi} \models \bigvee \overline{\psi}\right)
	\]
\end{definition}

We now combine one or more proof rules into a \emph{proof system}, which can roughly be seen as the equivalent of an algorithm to manipulate formulae.
Note, though, that a proof system only provides certain operations -- the proof rules -- but leaves it to the ``user'' when to \emph{apply} which proof rule to which formulae.
To obtain an actual \emph{proof}, one thus has to provide what some would call a \emph{scheduler} for a given proof system.

\begin{definition}{Deductive proof system}{proof-system}\silentdefindex{proof-system}
	We call a set $\ProofSystem$ of proof rules a \emph{(deductive) proof system}.
\end{definition}

Throughout this work, we use two arguably different interpretations of proof systems, though they both match the general definition from above.
The first one operates on a \emph{set of formulae} and progresses by selecting a subset of them and applying a proof rule to this subset to derive a new formula that is added to the (global) set of formulae.
The most prominent proof system within this work -- the resolution proof system as shown in \cref{def:resolution-proof-system} -- is of this type.

The second variant more closely resembles an algorithm in that it merely manipulates a single formula, usually discarding previous ``versions'' of this formula.
Intuitively, we think of this formula as our \emph{state} and for a proof rule the premises $\overline{\varphi}$ describe (or rather unpack) the state while the side conditions $\overline{\varphi'}$ contain conditions on the state, restricting the applicability of the proof rule.
Formally, we can enhance the underlying theory with a new predicate for the state and thereby adhere to the above definition of a proof rule. This is only a technicality, though, and we, therefore, avoid it here.

It is sometimes convenient, in particular when we extend an existing proof system, to \emph{compose} multiple proof rules into a single one, thereby restricting how the proof system is allowed to operate.
This is no fundamental extension to the definition of proof systems: we can again enhance our underlying theory with predicates that represent \emph{intermediate states}, thereby enforcing this composition completely within the previously defined framework.

\begin{definition}{Composition of deductive proof rules}{proof-rule-composition}\silentdefindex{proof-rule-composition}
	Let $PR_1(\overline{\varphi_1}, \overline{\psi_1})$ and $PR_2(\overline{\varphi_2}, \overline{\psi_2})$ be two proof rules that are ``chainable'':
	\[
		\Forall{\Phi} \left( \textrm{$PR_1$ is applicable to $\Phi$} \implies \textrm{$PR_2$ is applicable to $\Psi$ where $\Phi \vdash_{PR_1} \Psi$}\right)
	\]
	We call $PR(\overline{\varphi_1}, \overline{\psi_2})$ the \emph{composition of $PR_1$ and $PR_2$} and write $PR = PR_1 \circ PR_2$.
\end{definition}

For a proof system as defined here, the initial formula essentially already contains the ``question'' the proof is supposed to answer. As every application of a proof rule yields a logically valid deduction, we can only \emph{steer} a proof system -- by applying different proof rules -- but not change the \emph{destination} -- assuming that the logic is consistent and all proof rules are sound.
In this sense, applying proof rules is only a way to \emph{discover} the right pieces of information that allow deriving the final result, one reasonably easy step at a time.

However, we sometimes want to logically change the formula the proof system is working on.
Our main intention is to work on a sequence of formulae without completely \emph{restarting} the proof system, for example, because we can reuse some partial computations.
In order to do this, we allow for proof rules to take \emph{external inputs}.

\begin{definition}{Deductive proof rule with external input}{proof-rule-with-input}\silentdefindex{proof-rule-with-input}
	Let $PR(\overline{\varphi} \cup \{ i \}, \overline{\psi})$ be a proof rule with $\overline{\varphi} = \{\varphi_1, \dots, \varphi_n\}$, $\overline{\psi} = \{\psi_1, \dots, \psi_m\}$ and an additional input $i$. We write 
	\begin{align*}
		\ProofRule{PR $i$}{\varphi_1, \dots, \varphi_k}{\psi_1, \dots, \psi_m}{\varphi_{k+1}, \dots, \varphi_n}
	\end{align*}
	where both $\overline{\varphi}$ and $\overline{\psi}$ can make use of the input $i$.
\end{definition}

Note that we did not restrict the input to be ``logical'' but instead intend it to be rather ``operational''.
If $i$ would always be a ``logical'' information (a formula) the only reasonably intuitive thing to do would be to \emph{replace $\overline{\varphi}$ by $\overline{\varphi} \land i$} and continuing from there.
This however only allows to ``add to the formula'', but never ``remove from it''.

By ``operational'' we instead mean that $i$ can be an \emph{instruction} like ``assume that $\varphi$ holds'' or analogously ``remove the assumption that $\varphi$ holds'' -- which is fundamentally different from ``assume that $\neg\varphi$ holds''.
Again we can formally add such operational statements as new predicates to our theory, bringing this extension in line with the previous definitions.
It may not be immediately clear how this can be useful yet, and we put the reader off until \cref{sec:cadSMT:proof-system} on this issue.

\begin{definition}{Deductive proof}{proof}\silentdefindex{proof}
	Let $\ProofSystem = \{PR_1, \dots, PR_n\}$ be a proof system, $\Phi$ an \emph{initial formula}, and $\Psi$ a \emph{target formula}.
	A \emph{proof $\Proof$} for $\Phi$ and $\Psi$ consists of a sequence of proof rule applications that derives $\Psi$ from $\Phi$:
	\[
		\Proof \coloneqq \Phi \vdash_{PR_{i_1}} \cdots \vdash_{PR_{i_k}} \Psi
	\]
	with $i_1, \dots, i_k \in \{1, \dots, n\}$, allowing to repeatedly apply proof rules in arbitrary order.
	We denote the \emph{set of all proofs} for a proof system by $\ProofSystem^\vdash$ and call the number of proof rule applications $k$ the \emph{length of $\Proof$} and write $|\Proof| = k$.
\end{definition}

Finally, we should discuss two fundamental properties of (many) proof systems: \emph{soundness} and \emph{completeness}.
As for individual proof rules, soundness ensures that the derivations within a proof system are logically sound and thereby all (syntactic) proofs from $\ProofSystem^\vdash$ are logically valid.
Again, we argue that a proof system that is not sound is not particularly useful and thus all proof systems in this work are sound (which technically already follows from the analogous assumption for proof rules).

Completeness, on the other hand, indicates whether a proof system can produce a proof \emph{for every $\Phi \models \Psi$} that is valid within our logic.
As a proof merely combines syntactic rewriting steps based on the proof rules provided by the proof system, it becomes clear that the proof rules must be carefully crafted such that these \emph{syntactic} operations can be combined to prove all \emph{logical} statements.
As we usually only consider the question of satisfiability, it is mostly sufficient for a proof system to be \emph{refutationally complete} where we can produce a proof \emph{for every $\Phi \models \false$}.

\begin{definition}{Soundness \& Completeness}{sound-complete}\silentdefindex{proof-system:soundness}\silentdefindex{proof-system:completeness}
	We say that a proof system $\ProofSystem$ is \emph{sound} if all of its proofs are \emph{logically valid}:
	\[
		\Forall{\Proof \in \ProofSystem^\vdash} \left( \Proof = \Phi \vdash \cdots \vdash \Psi \right) \implies \left( \Phi \models \Psi \right)
	\]
	If all proof rules of $\ProofSystem$ are sound, we immediately get soundness of $\ProofSystem$ as every step of every proof $\Proof$ is sound.
	We say that a proof system $\ProofSystem$ is \emph{complete} if it allows to construct a proof for every valid $\Phi \models \Psi$:
	\[
		(\Phi \models \Psi) \implies \Exists{\Proof \in \ProofSystem^\vdash} \Proof = \Phi \vdash \cdots \vdash \Psi
	\]
	Moreover, we say that a proof system $\ProofSystem$ is \emph{refutationally complete} if it allows to construct a proof for every valid $\Phi \models \false$:
	\[
		(\Phi \models \false) \implies \Exists{\Proof \in \ProofSystem^\vdash} \Proof = \Phi \vdash \cdots \vdash \false
	\]
\end{definition}
