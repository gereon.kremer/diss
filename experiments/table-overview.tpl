\begin{tabularx}{\linewidth}{l{%- for a in answers %}r@{~}{% endfor %}r}
	\toprule
	\textbf{Solver}
{%- for a in answers %} & {{ a }} {% endfor %} & \#
	\\
	\midrule
{%- for s in solvers|sort %}
	{{ texName(s) }}
	{%- for a in answers %} & {{ solvers[s][a] }}{% endfor %} & {{ solvers[s]['sum'] }}
	\\
	{%- if loop.index % 50 == 0 %}
	\bottomrule
\end{tabularx}

\begin{tabularx}{\textwidth}{l{%- for a in answers %}r@{~}{% endfor %}r}
	\toprule
	\textbf{Solver}
{%- for a in answers %} & {{ a }} {% endfor %} & \#
	\\
	\midrule
	{% endif %}
{%- endfor %}
	\bottomrule
\end{tabularx}
