\chapter{Introduction}

Digital systems are becoming ubiquitous in all aspects of our lives, from obvious examples like smartphones or social media to less evident developments like autonomous aircraft and cars or smart home devices.
While software used to be confined to the particular machine it was running on, more and more systems have a direct physical impact on their environment and preventing malfunction is becoming ever more important: a ``Blue Screen of Death'' or a ``Kernel panic'' on a desktop computer are way less threatening than having an autonomous car shut down on the highway.

This (perceived) threat is reinforced by an inevitable collection of well-documented software failures with fatal consequences:
the Ariane 5 rocket exploded due to an incorrect conversion from floating-point to integers; 
the Therac-25 radiation therapy machine caused at least three deaths from overdoses of radiation, attributed to an overall poor software design prone to concurrency issues;
several airplane incidents have been traced to erroneous behavior of automatic flight control systems, for example, Qantas Flight 72 in 2008 or Lion Air Flight 610 in 2018.

While solid software designs and extensive testing can do their part in preventing such events, \emph{formal verification} presents a more rigorous approach that aims to \emph{prove a component safe} under a certain specification.
While the better part of formal verification is concerned with software, the above examples highlight that the interaction between software and hardware is particularly important and challenging.

One possible approach that has gained significant traction is to model a hardware component in combination with a software controller as a \emph{hybrid system} that allows for \emph{continuous or dynamic behavior}, as well as \emph{discrete control}.
Safety can then be shown by \emph{reachability} (or rather non-reachability) of bad states.
One popular approach to do this eventually encodes this reachability problem as a satisfiability problem and uses \emph{satisfiability modulo theories} (SMT) solvers that can decide upon the satisfiability of \emph{nonlinear real arithmetic}.
While most solvers aimed at this application of SMT employ techniques like \emph{interval constraint propagation} we propose the use of \emph{exact algebraic} procedures like the \emph{cylindrical algebraic decomposition} (CAD) method.
We focus on \emph{first-order logic with nonlinear real arithmetic} as a rigorous language that is independent of the actual application.

Within this work we start with an introduction to algebraic procedures and SMT solving and present two main techniques:
firstly using CAD as a theory solver within a regular SMT solver and
secondly using an alternative framework for SMT solving called \MCSAT whose theory reasoning is also based on CAD.
Note that we do not consider any verification techniques that build on SMT solvers, but only discuss SMT solving itself.

\section{Related work}\label{sec:related-work}

We give a short overview of various works that are in some way or another related to what we discuss in this thesis. We start with some historical notes on decision procedures for nonlinear real arithmetic -- or the theory of the reals -- and give a few more details on important milestones in the development and improvement of CAD. We then name some alternative approaches that are incomplete in that they can not deal with the whole set of nonlinear problems and finally survey the solving techniques used in the SMT community for nonlinear problems.

\subsection{Theory of the reals}\label{ssec:introduction:related:theory-of-the-reals}

\silentindex{cad:quantifier-elimination}
Soon after the formalization of mathematical logic and theories was (mostly) agreed on early in the twentieth century, the desire to have methods to determine the truth of statements within certain logics arose. For this quest for \emph{decision procedures}, Hilbert coined the term \emph{metamathematics}, aiming for mechanical algorithms that could answer whether a given statement (formally \emph{sentence}) is valid (or is a consequence) within a given theory (or an axiomatic system that defines it).

A closely connected (though not identical) question is whether a theory admits \emph{quantifier elimination}, that is whether we can construct a quantifier-free formula that is equivalent to a given quantified formula.
A quantifier elimination method usually constitutes a complete \emph{decision procedure} by eliminating all quantifiers from a sentence (a formula without free variables) and evaluating the resulting sentence (that contains no variables) to either \true or \false.
Based on this, the question for the satisfiability of a formula $\varphi$ with free variables $\overline{x}$ can be rephrased equivalently to whether $\Exists{\overline{x}} \varphi$ is valid.

Arguably one of the easiest quantifier elimination techniques is the recursive variant of enumeration for propositional logic.
It works by instantiating the variable with both \true and \false and constructing their disjunction or conjunction, for existential or universal quantification, respectively, where $\varphi[x/c]$ denotes the syntactic substitution of $c$ for a variable $x$ in $\varphi$.
\begin{align*}
	\Exists{x} \varphi &\Leftrightarrow \left( \varphi[x/\true] \lor \varphi[x/\false] \right) \\
	\Forall{x} \varphi &\Leftrightarrow \left( \varphi[x/\true] \land \varphi[x/\false] \right)
\end{align*}

We can easily generalize this idea for variables of any finite domain $D_x$ and already note that this technique fails for infinite domains:
\begin{align*}
	\Exists{x} \varphi &\Leftrightarrow \bigvee_{d \in D_x} \varphi[x/d] &
	\Forall{x} \varphi &\Leftrightarrow \bigwedge_{d \in D_x} \varphi[x/d]
\end{align*}

Another possibility for quantifier elimination for propositional logic is using the resolution rule to successively eliminate Boolean variables from a formula in conjunctive normal form as we later show in \cref{algo:resolution}. It essentially combines two \emph{clauses} to produce a new clause without the variable that is to be eliminated and gives certain guarantees when original clauses can safely be discarded.
Very similarly, Dines~\cite{Dines1919} and Motzkin~\cite{Motzkin1936} reframed a method originally due to Fourier~\cite{Fourier1825,Fourier1826} to provide quantifier elimination for sets of linear inequalities over real variables -- that is without multiplication among variables -- to what we now call the \emph{Fourier--Motzkin variable elimination}.

The overarching question for decision procedures promptly incited an active field of research that not only rediscovered existing methods but also devised new decision procedures and provided fundamental new insights into the nature of logic itself.

One of the earliest (published) results in this direction is due to Presburger~\cite{Presburger1930} which gives a decision procedure -- again by quantifier elimination -- for formulae with linear constraints over integer variables. (We strongly recommend~\cite{Stansifer1984} and~\cite{Crossley1975} not only for an enjoyable read but also for some historical perspective and hints to further work in this area.)

Shortly after these, some fundamental results emerged that restrict the range of logics for which we can hope to find a decision procedure, most prominently the undecidability results due to Gödel~\cite{Goedel1931} and shortly after due to Church~\cite{Church1936} and his student Rosser~\cite{Rosser1936}.
Most importantly (for us) they show that formulae with nonlinear constraints over integer variables are undecidable in general, that is no complete decision procedure for nonlinear integer problems exists.

Whether nonlinear real arithmetic is decidable remained unsolved for quite some time, though it seems now that Tarski essentially had the result since about 1930~\cite{Church1969}.
It was only (publicly) resolved in~\cite{Tarski1951} and~\cite{Seidenberg1954} which (once again) employed a constructive quantifier elimination method. We refer to~\cite{Dries1988} for a historical and thematical assessment with many references to related work.

All methods above have been superseded by more efficient techniques in most practical applications. Propositional logic, which only consists of Boolean variables and connectives, is commonly dealt with using \CDCL-style SAT solving (see \cref{sec:cdcl}), the simplex method is used for linear real arithmetic and branch-and-bound-based methods are used for the linear integer case. However, all of the original quantifier elimination methods are still useful for more specialized cases as we will see for Fourier--Motzkin elimination in \cref{ssec:mcsat:Fourier-Motzkin}, for example.

Tarski's method is different in this respect in that it exhibits an asymptotic complexity that is \emph{not elementary} -- it can not be bounded by an exponential tower of finite size -- and hence was never seriously used in practice. Nevertheless, it remains one of the most important results in this area, at least sparking the hope for a solution efficient enough for practical applications.

Encouraged and inspired by Tarski's result, a number of alternative (and hopefully more efficient) decision procedures emerged including the cylindrical algebraic decomposition method due to Collins~\cite{Collins1974}, but also other methods due to Grigor'ev and Vorobjov~\cite{Grigorev1988}, Renegar~\cite{Renegar1988}, or Basu, Pollack, and Roy~\cite{Basu1996} which all improved significantly upon the asymptotic complexity of Collins' cylindrical algebraic decomposition (which is doubly exponential in the number of variables).

One might ask why this thesis is concerned with CAD then, and consequently why Collins' work sparked a whole field of sustained and active research while the other methods stayed theoretical side notes.
The answer may be given by Hong~\cite{Hong1991} in that these advances on the asymptotic side are outweighed by huge constants in practice.
The approaches due to Grigor'ev and Renegar are impractical even for trivial problems and the break-even-point is moved so far away that there is essentially no way to reach it. The third approach due to Basu, Pollack, and Roy -- not included in Hong's comparison as it is more recent -- also did not result in an implementation to the best of our knowledge.

Consequently, the cylindrical algebraic decomposition method is the predominant decision procedure (or quantifier elimination method) for nonlinear real problems nowadays, if one wishes to have a complete method.

\subsection{Origins of CAD}

We now give a summary of the genesis of the cylindrical algebraic decomposition method and refer to~\cite{Caviness1998} for more information.
Shortly after Collins received his Ph.D. degree under Rosser -- about twenty years after~\cite{Rosser1936} -- Collins was already concerned with Tarski's work and actually aimed at implementing it at IBM, noting that the ``amount of labor involved in even very simple applications has prohibited any progress in this aspiration to this date''~\cite{Collins1956}.

Though we assume that this project did not yield a practical implementation -- witnessed by the lack of ensuing publications -- Collins stuck to the question of quantifier elimination and made significant advances in the field we now call computer algebra and beyond, including reference counting~\cite{Collins1960}, the implementation of the early computer algebra systems PM~\cite{Collins1966} and SAC-1~\cite{Collins1971a}, and efficient ways to compute subresultants~\cite{Collins1967} and resultants~\cite{Collins1971b}.

In 1973, Collins gave a talk~\cite{Collins1973} presenting a novel method for quantifier elimination that he called ``cylindrical algebraic decomposition'', followed by two papers~\cite{Collins1974,Collins1975}.
They not only contain a detailed description of the algorithms but also give bounds on the asymptotic complexity which is doubly exponential -- way better than the non-elementary complexity of Tarski's method.
Furthermore, they also hint to an ongoing effort to implement it within the SAC-1 computer algebra system.

The implementation apparently proved to be more difficult than expected:
a group around Collins with experience from SAC-1 and SAC-2 started in 1974 but still had major sub-algorithms unimplemented in 1978. Meanwhile, parts of the method were implemented by Müller~\cite{Mueller1978} to solve nonlinear optimization problems.
In 1979, finally, Arnon~\cite{Arnon1981} took up the task to produce a complete CAD implementation that was eventually integrated into SAC-2~\cite{Collins1985}.

This first implementation was then promptly followed by various improvements to CAD -- mostly within the projection operator as we discuss in \cref{sec:cad:projection-operators} -- by several of Collins' students, most prominently McCallum~\cite{McCallum1984,McCallum1985,McCallum1988}, Hong~\cite{Hong1990}, and Brown~\cite{Brown2001}.

At the latest since the second half of the 1980s, the topic was picked up by a larger community as witnessed by being mentioned in~\cite{Weispfenning1988}, some theoretical contributions based on CAD by Davenport and Heintz~\cite{Davenport1988} and another projection operator for CAD due to Lazard~\cite{Lazard1994}.

\subsection{CAD today}

During the first attempts for a full CAD implementation, it became clear that it requires a large amount of nontrivial functionality to work and thus the number of implementations of CAD is still somewhat limited today.
Most notably, we have \QEPCAD~\cite{Hong1991} due to Hong and its successor \QEPCADB~\cite{Brown2003} (mostly due to Brown), as well as implementations within the computer algebra systems \Redlog~\cite{Dolzmann1997}, Mathematica~\cite{Strzebonski2000} and Maple~\cite{Chen2009,Iwane2009}.

Only a few special-purpose versions of CAD exist beyond the above, as far as we know.
The \NLSAT-style explanations that we also discuss in \cref{sec:mcsat:explanations} are implemented in both \Zthree~\cite{Jovanovic2012} and \Yices~\cite{Dutertre2014} based on libpoly~\cite{Jovanovic2017}.
Also, the theorem prover Coq features a custom CAD implementation~\cite{Mahboubi2007} for proofs over nonlinear arithmetic that is tailored to proof generation.

To the best of our knowledge, our implementation in \SMTRAT (and its related projects) is the only other general-purpose implementation of CAD. Earlier versions also exist within our libraries GiNaCRA~\cite{Loup2011} and CArL~\cite{Kremer2018a}.
The current implementation powers not only a theory solver for regular SMT solving as described in~\cite{Corzilius2015}, but also a quantifier elimination method~\cite{Neuhaeuser2018} and our version of \MCSAT~\cite{Nalbach2019}.

\subsection{Variants of CAD}\label{ssec:intro:related:variant-of-cad}

The CAD implemented within Maple -- or rather the RegularChains library~\cite{Chen2009} -- deviates from the traditional CAD framework presented in this work.
While what we call CAD is only concerned with real numbers, it can also be used in the \emph{complex space} as presented in~\cite{Chen2009}. Intuitively, we can understand it as using a larger projection to obtain a CAD-like object in the complex space, using a somewhat easier lifting procedure to obtain sample points, and projecting them into the real space.

Another recent variant of CAD stays in the real space but (partly) departs from the fundamental concept of cylindricity.
In what is called \emph{non-uniform cylindrical algebraic decomposition} or NuCAD~\cite{Brown2015a}, the individual regions may have overlapping projections onto lower-dimensional space. In this sense, every cell itself is still cylindrical, but the entirety of cells is not \emph{arranged cylindrically} relative to each other.
While this approach makes it harder to perform classical quantifier elimination, it is suitable for answering many other questions about a nonlinear problem, promising a significantly reduced number of cells in practice.

We presented a novel interpretation of CAD in~\cite{Abraham2020}. We can understand it as bringing the conflict-driven reasoning style of \MCSAT into a regular CAD-based \CDCLT-style theory solver.
Similar to \MCSAT, we construct a theory model dimension-wise and exclude intervals on every dimension. Once a dimension is fully covered, we combine these intervals into an interval on a lower dimension using a characterization step that closely resembles a CAD projection.
Preliminary experiments suggest, that this approach is competitive to the CAD-based theory solver we present in \cref{ch:cad-SMT}.

\subsection{Other complete methods}

As we already mentioned, the CAD method is not the only complete method that deals with nonlinear real arithmetic.
The first method presented by Tarski~\cite{Tarski1951} had non-elementary complexity and thus it was essentially clear since its discovery that it would not be suitable for implementation -- though the method is constructive.

Later approaches -- we already mentioned Grigor'ev and Vorobjov~\cite{Grigorev1988}, Renegar~\cite{Renegar1988}, and Basu, Pollack, and Roy~\cite{Basu1996} -- all share that they significantly improve upon the theoretical complexity of the cylindrical algebraic decomposition method. Still, to the best of our knowledge, no practical implementation of either of the three methods exists.
For the former two, Hong gave a strong argument in~\cite{Hong1991} in that -- though the asymptotic complexity improved -- the methods are incredibly inefficient in practice.

For the approach due to Basu, Pollack, and Roy~\cite{Basu1996} we could only conjecture why it has not been implemented or at least did not result in any implementation we are aware of.
Our group -- once upon a time -- tried to implement this approach as well but abandoned it in favor of cylindrical algebraic decomposition due to the complexity of the mathematical background and the complexity of the upcoming implementation.

\subsection{Incomplete methods}

Trying to deal with nonlinear problems has a long history -- just think of the Pythagorean theorem -- and has thus spawned a wealth of diverse methods with very different targets, characteristics, and trade-offs.
We have previously focused on methods that give us precise (or symbolic) information on nonlinear (polynomial) equalities and inequalities.
In the following, we give an overview on different approaches that for some reason or another do not satisfy those conditions, for example, because they are approximate (what we call numerical algorithms) or are restricted in their input in that they can deal only with equalities (or inequalities) or only polynomial up to a certain degree.

Note that these other methods are by no means less relevant or \emph{worse}. Quite the contrary, some of the following approaches are well established and routinely used in practice and are in this respect way ahead of CAD.

\subsubsection{Linearization\label{sssec:introduction:related:incomplete:linearization}}

\silentdefindex{linearization}
Possibly the most obvious approach is called \emph{linearization} which tries to describe a nonlinear problem in terms of linear constraints. Note that this method is inherently incomplete as nonlinearity is a qualitative trait that we can not eliminate without loss of information:
nonlinear constraints allow to describe solution spaces that are beyond the linear formalism and thus the linearization of something nonlinear can only ever be an approximation.

We, however, observe in practice that a linear approximation can very well be enough to answer different kinds of interesting questions about a nonlinear problem. Most applications have uncertainties, require tolerances, or are only concerned with a small area around some target point and we are looking for an open solution space that allows for safe over-approximations that can be linear. Even if we encounter equalities -- and thus the solution is restricted to a lower-dimensional space -- linearization techniques may be able to make a linear solver provide a solution that satisfies the nonlinear problem. Some examples can be found in~\cite{Isidori1995,Asarin2007}.

A novel linearization approach for nonlinear SMT problems that proved to be very powerful in practice was presented in~\cite{Cimatti2018,Irfan2018} and was adapted within our solver \SMTRAT in~\cite{Zaman2019}.
Though not finished yet, we hope that further improvements to this approach and proper integration with a CAD-based theory solver -- or even within the \MCSAT framework -- will be fruitful.

\subsubsection{Numerical algorithms\label{ssec:introduction:incomplete:numerical}}

\silentdefindex{numerical-algos}
A wealth of fundamentally different methods to deal with nonlinear problems can be found in the field we call \emph{numerical analysis}, employing numerical approximation in one form or another.
Prominent examples include the \emph{Newton method}, various methods for \emph{linear equation systems} using matrix decompositions, the area commonly called \emph{convex optimization}, eigenvalue problems or a plethora of methods for differential equations.

Some of them can be adapted to be useful in our field, and we have  reported on some work in~\cite{Kremer2013,Kremer2018b}.
Unfortunately, such attempts oftentimes fail due to a very fundamental problem:
numerical analysis is concerned with finding a \emph{good approximation} and gives guarantees on the \emph{convergence} -- that is how fast the approximation improves -- but usually gives no \emph{absolute error bounds} on the numeric results. 

Simply put: if you figure out your approximation is not good enough, you only need to let it run some more time. Figuring out whether your approximation is good enough is usually hard to determine, though.
Furthermore, this research is usually very focused on what one could describe as ``realistic'' problems, which allows these methods to essentially fail for ``obscure'' corner cases, or at least require manual intervention to reformulation of the problem at hand.

This is in stark contrast to our expectations: we want to obtain ``push-button solutions'' that deal with any input problem from some formal language -- first-order logics in our case -- \emph{without} human interaction.
Furthermore, we consider our tools to be \emph{sound and complete} in the sense that we can guarantee that our results are correct -- and not \emph{approximate} or \emph{probably correct} -- and that they \emph{always} work, even for the more obscure cases.
While we endure degraded performance for such corner cases, incorrectness is usually unacceptable.

Thus, we sometimes try to adapt numerical methods, but can usually only do so as fast preprocessing techniques to solve easy cases and need to provide complete methods as fall-backs.
One such case is described in~\cite{Kremer2013} where eigenvalue computations serve as \emph{preconditioning} for the actual root finding method.
The usage of interval arithmetic in our implementation of real algebraic numbers that we describe in \cref{sec:preliminaries:ran} also partly fits this description (though it is also strictly required for some operations).

\subsubsection{Interval constraint propagation\label{sssec:introduction:related:incomplete:icp}}

\silentdefindex{icp}
Instead of abandoning numerical algorithms altogether, we might want to equip such methods with some technique to provide absolute guarantees on their results.
The most prominent approach is called \emph{interval arithmetic}, essentially defining arithmetic operations on intervals that capture the ``real results'' of some operation in an over-approximating way. To give a rough idea, let us consider multiplication:

Let $a,b \in \R$ be represented by two intervals $a \in (\BoundL{a},\BoundU{a})$, $b \in (\BoundL{b},\BoundU{b})$.
Let us assume for now that the intervals are reasonably small as our intuition is that they approximate individual numbers $a$ and $b$. An approximation of the product $a \cdot b$ can then be obtained from the intervals as follows:
\[
	a \cdot b \in (\BoundL{a},\BoundU{a}) \cdot (\BoundL{b},\BoundU{b}) = (
		\min \{\BoundL{a} \cdot \BoundL{b}, \BoundL{a} \cdot \BoundU{b}, \BoundU{a} \cdot \BoundL{b}, \BoundU{a} \cdot \BoundU{b}\},
		\max \{\BoundL{a} \cdot \BoundL{b}, \BoundL{a} \cdot \BoundU{b}, \BoundU{a} \cdot \BoundL{b}, \BoundU{a} \cdot \BoundU{b}\}
	)
\]
This construction not only accumulates the inaccuracy of the two intervals for $a$ and $b$ but possibly introduces more inaccuracy.
Firstly, one oftentimes uses floating-point representations for the interval endpoints which provide great performance, but also introduce additional rounding errors. Note that this is not only an issue for the precision of the result, but we also need to take care that this rounding happens safely -- that is over-approximating -- as we otherwise risk getting incorrect results by losing possible solutions.

Secondly, we may be unable to consider \emph{algebraic} relations between variables.
If for example, we know that $a = b$ it is clear that $a \cdot b = a^2 \geq 0$. If we perform direct interval arithmetic we lose this knowledge and the resulting interval may very well contain negative numbers.
We can sometimes treat such cases specifically, but these relations are oftentimes not that obvious and once this calculation has been performed, it is usually very difficult to exploit them after the over-approximation has been introduced.

The most popular -- and most widely adopted -- method to exploit interval arithmetic for SMT solving is called \emph{interval constraint propagation}~\cite{Benhamou2006}, being implemented in solvers like dReal~\cite{Gao2013}, iSAT~\cite{Loup2013,Scheibler2013}, or raSAT~\cite{Tung2017}, as well as our own solver \SMTRAT~\cite{Schupp2013}.
Its fundamental idea is to use individual constraints to maintain an interval that contains the possible range of values for every variable and shrink these intervals using relatively simple propagations like the following:
\[
	\left( y,z \in (0,1) \land x = y \cdot z \right) \implies x \in (0,1)
\]

This technique is surprisingly effective in excluding large parts of the search space quickly, and oftentimes even proving unsatisfiability.
It is however notoriously difficult to construct satisfying assignments, in particular, if equalities are present.
We mainly propose to use it to quickly shrink the space of solution candidates and then exploit these additional \emph{bounds} in the algebraic methods as described in~\cite{Loup2013} or~\cite{Kremer2019}.

\subsubsection{Virtual substitution}

\silentdefindex{vs}
In~\cite{Weispfenning1988} Weispfenning studies the complexity of quantifier elimination of linear problems over different fields, which also turns out to be doubly exponential -- though only in the number of quantifier alternations instead of the number of variables.
He also gives a method that consequently runs in (singly) exponential time if no quantifier alternations are present.

This method was subsequently implemented within REDUCE~\cite{Loos1993} and extended to quadratic constraints~\cite{Weispfenning1997} to what we call \emph{virtual (term) substitution}. The classical notion of virtual substitution essentially uses (parametric) solution formulae for the roots of polynomials and substitutes their results, thereby eliminating the respective variable. This, however, yields a restriction on the degree as solution formulae only exist for polynomials up to degree four. Even worse, the substitution rules for degree three are sufficiently difficult that they were only implemented recently in~\cite{Kosta2016}.

The restriction on the degree of the polynomial can be lifted by a generalization of virtual substitution~\cite{Kosta2015} which essentially substitutes a characterization of the polynomial root -- as an expression in first-order logic -- instead of an explicit representation. This allows for arbitrary degrees in principle, however, the only automated way to obtain these first-order logic expressions is by means of quantifier elimination. In this sense, one could argue that this method merely reproduces the results of parametric quantifier eliminations obtained from other procedures -- for example CAD.

Despite these restrictions, virtual substitution is impressively effective in practice and certainly is a valuable tool for quantifier elimination and related problems. We thus also implement virtual substitution in our solver and use it in combination with cylindrical algebraic decomposition~\cite{Corzilius2015}.

\subsubsection{Gröbner bases\label{ssec:introduction:incomplete:groebner}}

\silentdefindex{groebner-bases}
Since the formalization of polynomial ideals, computer algebra was looking for a canonical way to represent such ideals and -- if possible -- construct them effectively. Buchberger answered both questions with the introduction of \emph{Gröbner bases} in 1965~\cite{Buchberger1965}, giving conditions that characterize a canonical representation of a set of generators for a polynomial ideal and providing an algorithm to construct this set.
To this day, Gröbner bases are one of the most important tools in computer algebra to deal with a set of polynomials -- or implicitly with a set of equalities.

Essentially, Gröbner bases provide a canonical way to simplify a set of polynomials, retaining the set of common roots and thereby the solutions to the set of polynomial equalities. Though Gröbner bases have some caveats in practice -- it may exhibit doubly exponential run time that is very sensitive to the variable ordering we need to choose -- it is arguably the most successful tool to deal with such problems.

As for our problem of satisfiability of real arithmetic formulae, Gröbner bases have several restrictions that need to be overcome.
Firstly, Gröbner bases only deal with polynomials -- implicitly equalities -- while we usually want to consider inequalities as well.
We can convert inequalities to equalities while introducing new variables as described for example in~\cite{Junges2012} like this.
\[
	p \geq 0 \Leftrightarrow \Exists{y} p - y^2 = 0
\]
Note, however, that practical experience shows that this technique works well in some examples, but does not seem to help a lot in many other cases.

Secondly, Gröbner bases can determine whether some polynomials have a common complex root but usually can not argue about real roots. If we certify that no complex root exists we of course also have unsatisfiability in the reals, but a gap remains where a complex root exists but we do not find a real one.

Given that we are only interested in real solutions we have certain opportunities to apply additional simplifications to our polynomials -- actually computing or approximating what is sometimes called the \emph{real radical} instead of the Gröbner basis.
Some ways to do exactly that are explored in~\cite{Junges2012} and more recently as well in~\cite{Abbott2018,SafeyElDin2018}.

Even if a complex root exists it is not trivial to actually construct one, even more so if one aims to find a real one. Some attempts are made in~\cite{Junges2012}, but again their applicability highly depends on the individual input.


\subsection{Boolean reasoning and SMT solving}

\silentindex{SMT}
The formalization of mathematical logic we discussed at the beginning of \cref{ssec:introduction:related:theory-of-the-reals} did not only launch a plethora of developments on arithmetic theories -- though it seems to have received more attention -- but also sparked the interest in decision procedures for Boolean logics.

Oftentimes -- Fourier--Motzkin being a notable exception -- methods for arithmetic theories naturally incorporated dealing with a Boolean structure. 
Thus the question of satisfiability -- or validity -- for purely Boolean formulae (propositional logic) was well-established but was usually treated very naively.
It took until~\cite{Davis1960} for a method that outperformed simple enumeration in practice. We give an overview of the methods for the satisfiability of purely Boolean formulae in \cref{ch:cdcl}.

From the very beginning, the question of satisfiability for purely Boolean formulae was usually motivated by dealing with more complex logics. Nowadays, we usually consider the satisfiability of first-order logic formulae and call the corresponding satisfiability problem, as well as methods to solve it, \emph{satisfiability modulo theories} (SMT).
That being said, propositional satisfiability is an established field of research on its own which has arguably outgrown its origins due to the impressive practical results.

While the origins of practical approaches for SMT solving are usually dated to the late 1970s and early 1980s, we tend to understand~\cite{Davis1960} as a form of SMT solving already, given that it employs a method for propositional satisfiability to solve the validity of a richer logic.
Early works that approach what we understand as SMT solving today include~\cite{Nelson1979,Shostak1979,Nelson1980,Boyer1990} while the approach that we understand as \emph{(lazy) SMT solving} -- a combination of a regular \emph{SAT solver} and what we call \emph{theory solvers} as described in \cref{ch:SMT} -- emerged in the late 1990s, apparently at multiple places around the same time, for example~\cite{Giunchiglia1996,Armando1999,Bryant1999,Pnueli1999}.


\subsection{SMT solving today}

Shortly after the \emph{lazy SMT solving} framework emerged, an effort was made to draw the whole ``SMT community'' together, or rather form such a community, by establishing both \SMTLIB~\cite{Barrett2016} in 2002 and the \SMTCOMP~\cite{Barrett2005}.
These initiatives and their close cooperation, in particular, made it possible to gather the better part of a comparably diverse community -- that works on very different theories -- using a common input language and a common set of benchmarks that both live in the \SMTLIB initiative.

Within the last five years, more than twenty different solvers have participated within about fifty different logics (about twenty of them involving quantifiers) at the annual \SMTCOMP, witnessing both the wide-spread interest in SMT solving and the diversity of tackled logics.
Note that some of the competing solvers are actually targeted at other problems and rather solve SMT problems as a side issue, for example, \AProVE, \AltErgo, \Redlog, and \Vampire.

Given the scope of this thesis, we want to focus on solvers for nonlinear real arithmetic which is called \QFNRA in \SMTLIB.
Within the last five years, the following seven solvers competed in this logic: \CVC~\cite{Barrett2011}, \MathSAT~\cite{Cimatti2013}, \raSAT~\cite{Tung2017}, \veriTRedlog~\cite{Fontaine2018}, \Yices~\cite{Dutertre2014}, \Zthree~\cite{Moura2008} and our own \SMTRAT~\cite{Corzilius2015}.
While \CVC and \MathSAT employ linearization techniques as described in \cref{sssec:introduction:related:incomplete:linearization}, \raSAT is solely based on interval constraint propagation similar to \cref{sssec:introduction:related:incomplete:icp}, \veriTRedlog integrates the full-fledged computer algebra system \Redlog into the SMT solver \veriT and finally \Yices and \Zthree employ an alternative framework for SMT solving called \MCSAT that we discuss in more detail in \cref{ch:mcsat,ch:mcsat-implementation}.

Our own solver \SMTRAT implements all of these techniques with a particular focus on complete methods. As we have already seen, the only complete method for nonlinear arithmetic -- that has ever been seriously implemented -- is CAD.
As far as we know, there are only two integrations of CAD into SMT solvers:
\begin{enumerate*}
	\item using CAD as a theory solver in regular SMT solving as implemented in \veriTRedlog and \SMTRAT and
	\item adapting CAD as an explanation backend for \MCSAT as implemented in \Yices, \Zthree, and \SMTRAT.
\end{enumerate*}
