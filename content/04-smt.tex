\chapter{Satisfiability modulo theories solving}\label{ch:SMT}

We have seen how algorithmic approaches to decide the satisfiability of a propositional logic formula have developed and improved over the years, starting from~\cite{Davis1960} to the current state-of-the-art \CDCL with a large amount of further techniques and heuristics. We note, however, that these developments have oftentimes been driven by the interest in richer logics like \emph{first-order logic}, not least the original work from~\cite{Davis1960}.

Aiming for automatic solvers for these richer logics, three fundamentally different approaches have emerged. Before the rise of efficient SAT solvers, the predominant approach was to work solely on a set of theory constraints and not bother with the Boolean structure. The prime examples for this are arguably simplex-based linear programming solvers, for example, Gurobi or SCIP. In many cases, a certain amount of Boolean structure can be encoded within the theory as well, which is for example known as \emph{0-1-programming}.

As soon as the propositional satisfiability problem could be solved (sufficiently) quickly, it was leveraged to help solving richer logics.
The early approaches -- that we summarize as \emph{eager SMT solving} -- transform the problem at hand into an equisatisfiable propositional logic formula and solve that one with a SAT solver. At some point, \emph{lazy SMT solving} emerged that uses a SAT solver to process the Boolean structure first and issues \emph{theory queries} (concerning only conjunctions of theory constraints) to dedicated theory solvers.

\section{Eager SMT solving}

The first approaches that employed SAT solvers for richer logics used them as simple black-boxes. In~\cite{Strichman2002}, for example, separation logic formulae are encoded into propositional logic and then solved by some SAT solver (in this case Chaff). This approach is particularly straightforward in that no modifications to the SAT solver are necessary and, thus, the core solving engine can be replaced without any problems.

\begin{definition}{Eager SMT solving}{eager-SMT-solving}\silentdefindex{eager-SMT-solving}
	Let $S$ be a SAT solver and $T$ a theory reasoning engine that transforms an input formula with some theory into an equisatisfiable propositional logic formula.
	We call a solver that first transforms the input to propositional logic using $T$ and then determines the satisfiability using $S$ an \defindex{eager-SMT-solving} program.
\end{definition}

This approach somewhat restricts what we can argue about in a meaningful way.
If the \emph{richer logic} is also \emph{more expressive}, we should not expect an encoding into propositional logic to exist -- at least not a \emph{nice} one, whatever \emph{nice} may be. Theories that argue about real numbers are prime examples where a propositional encoding is notoriously difficult or impossible.

We observe that a SAT solver only has a finite state space: it only reasons about the $2^n$ possible variable assignments for $n$ propositional variables from the input formula. For a real variable, however, a single variable induces an infinitely large state space. Even if we argue that we could do some kind of discretization, it is unclear how to obtain a suitable discretization and how large its state space would turn out to be.

It is important to realize that this property of \emph{being reducible to propositional logic} is not connected to the asymptotic complexity of the problem at hand, all the more if we aim for practical efficiency. A set of linear real arithmetic constraints -- without Boolean combinations -- can be solved in polynomial time and thus asymptotically faster than propositional logic, but oftentimes the fastest method in practice is the simplex method -- with exponential worst-case complexity -- and encoding to propositional logic is not sensible, as discussed above.

Note that the reduction to propositional logic is -- in theory -- possible for every decidable logic. We can \emph{simply solve the problem} and give the propositional encoding \true or $\false$. We consider it obvious that there is no point in doing this, though. If the theory admits \emph{quantifier elimination} -- for example cylindrical algebraic decomposition -- we could use that to obtain the (trivial) encoding.

We can, however, also use it to decompose the state space into finitely many regions -- this is what the cylindrical algebraic decomposition does after all -- and confer on the SAT solver to choose a region and make sure that this region satisfies the Boolean structure.
Note that it is probably worthy of discussion whether this would be considered \emph{eager SMT solving}, as also considering the Boolean structure could easily be integrated into the theory reasoning part -- the original cylindrical algebraic decomposition works on arbitrary formulae.

Though our common perception of \index{eager-SMT-solving} is that we transform the formula and issue a single call to the SAT solver, we can very well craft somewhat more complex schemes in the spirit of \index{eager-SMT-solving}. Consider for example the approach to solve first-order logic from~\cite{Davis1960} -- which produces a sequence of SAT problems -- or modern CEGAR-style approaches like in~\cite{Clarke2004}, which can arguably be described as \emph{eager methods} to answer \emph{SMT-like} questions.

\section{Lazy SMT solving}

\silentdefindex{lazy-SMT-solving}
The \emph{eager SMT solving} scheme quickly became infeasible for richer logics and alternative structure started to emerge towards the end of the 1990s.
In~\cite{Wolfman1999} the authors essentially describe what we now call a \emph{lazy SMT solver} using the simplex method as a theory solver and even denominate some requirement that we, later on, subsume as \emph{SMT compliancy}. Other examples of early solutions that use schemes similar to \emph{lazy SMT} include~\cite{Armando1999,Moura2002,Barrett2002} and we refer to~\cite{Biere2009} for more details on both the history and a properly thorough discussion of lazy SMT solving, as we only give an intuitive explanation now.

What we consider the modern \index{lazy-SMT-solving} approach consists of a slightly modified SAT solver and a theory solver that solely works on \emph{sets of theory constraints}. The SAT solver works on a \emph{Boolean abstraction} of the input formula and enumerates models for this Boolean abstraction. These models are translated to sets of constraints that should be consistent -- if the model translates to the existence of theory assignment -- which is what the theory solver decides upon. If the theory solver deems such a set of constraints consistent, we have proven that the input formula is satisfiable.

\begin{definition}{Boolean abstraction}{boolean-abstraction}\silentdefindex{boolean-abstraction}
	Let $A_T$ be the set of all theory atoms from some theory $T$ and $\overline{b}$ a set of fresh Boolean variables.
	We call an injective function $B_T: A_T \rightarrow \overline{b}$ an \emph{abstraction function} and extend it to arbitrary formulae by point-wise application.

	Let $\varphi$ be some formula over a theory $T$.
	We call $\BAbs = B_T(\varphi)$ the \defindex{boolean-abstraction} (or sometimes \emph{Boolean skeleton}) of $\varphi$.
\end{definition}

The \index{boolean-abstraction} is an \emph{over-approximation} of the input formula $\varphi$ in the sense that it ignores the underlying theory and only retains the Boolean structure of the formula. The resulting formula $\BAbs$ is now processable for a regular SAT solver which can search for a model. Note that this satisfying \emph{Boolean assignment} is not a model for the input formula $\varphi$ but only for $\BAbs$.

\begin{definition}{Theory concretization}{theory-concretization}\silentdefindex{theory-concretization}
	Let $\varphi$ be some formula over a theory $T$, $\BAbs = B_T(\varphi)$ the \index{boolean-abstraction} of $\varphi$, and $\A$ a model for $\BAbs$.
	We define the set of theory atoms that correspond to $\A$ as
	\[
		C_{\varphi}(\A) = \{ a \mid \A(B_T(a)) = \true \} \cup \{ \neg a \mid \A(B_T(a)) = \false \}
	\]
	and call $C_{\varphi}(\A)$ the \emph{concretization of $\A$ over $\varphi$}.
\end{definition}

The SMT solver needs to check whether the Boolean assignment corresponds to a feasible \emph{selection of theory atoms}. In order to do this, we \emph{concretize} the Boolean assignment -- essentially we apply the inverse of the abstraction -- and feed the resulting set of theory atoms to a \emph{theory solver}.
This theory solver determines whether a set of theory atoms is \emph{consistent}, that is whether there is a theory model that satisfies them.

If the theory solver confirms that the set of theory atoms is consistent, we determine the satisfiability of $\varphi$. Otherwise, the current Boolean assignment can not be transformed into a theory model -- and is not a \emph{model} in this sense -- and the SAT solver searches for another Boolean assignment. Note that the theory solver oftentimes provides the SAT solver with a \emph{reason for unsatisfiability} -- usually a subset of the set of theory atoms -- which the SAT solver uses as a new \emph{conflict clause} to perform regular conflict analysis.

Once the SAT solver can not find any further satisfying Boolean assignments -- all Boolean assignments have been rejected by the theory solver -- we determine unsatisfiability. Note that a \emph{proof for unsatisfiability} is much more difficult than just giving a model in the case of satisfiability. We not only have the Boolean reasoning which essentially consists of the resolution steps, but also the theory reasoning that rejected all Boolean assignments.

The described scheme for an SMT solver is completely agnostic of the theory that we try to solve, as long as we have a theory solver that can determine the satisfiability of a set of theory atoms. We call this \emph{\CDCLT-style SMT solving}, alluding to the fact that we usually use a \CDCL-style SAT solver and a theory solver for some theory $T$. See \cref{fig:lazy-SMT-solving} for a schematic overview.
Further functionality, like providing a theory model or a reason for unsatisfiability, can greatly improve practical performance but is not strictly necessary. 

\begin{figure}
	\begin{center}
		\includetikz{04-lazy-smt}
	\end{center}

	\caption{Schematic overview of \CDCLT-style SMT solving}\label{fig:lazy-SMT-solving}\silentindex{SMT}
\end{figure}

We usually distinguish two variants of lazy SMT solving, namely, \emph{full-lazy SMT solving} and \emph{less-lazy SMT solving}, which differ in when (or how often) the theory solver is called.
A full-lazy SMT solver only issues a theory call if the SAT solver obtained a \emph{full Boolean model}, that is if all Boolean variables have been assigned and the Boolean abstraction is satisfied.
In contrast, a less-lazy SMT solver issues theory calls more frequently -- for example after every decision level of the \CDCL-style SAT solver -- on a \emph{partial Boolean model} if the Boolean abstraction is not yet conflicting.

We observe that the two variants only differ if the theory call returns feasibility: if the Boolean model is a full assignment we derive satisfiability of the whole formula if it is however only partial we need to let the SAT solver continue.

Note that these variants have a nontrivial trade-off.
Less-lazy SMT solvers issue more theory calls and require a deeper integration into the SAT solver. The hope is that the additional theory calls are not that expensive -- due to what is called ``incrementality'' and discussed in the following -- but instead allow to detect conflicts earlier, possibly avoiding certain harder theory calls altogether.
In practice, less-lazy SMT solving seems to beat full-lazy SMT solving, at least for ``easier'' logics like linear arithmetic.


\section{SMT compliancy\label{sec:SMT:compliancy}}

\silentdefindex{SMT-compliancy}
We discussed the fundamental requirement for theory solvers -- being able to decide upon the consistency of a set of theory atoms -- but also mentioned some advanced properties. While these are not strictly necessary, they can greatly enhance applicability or practical performance.
The common term \emph{SMT compliancy} usually only subsumes \emph{incrementality}, \emph{backtracking} and the generation of \emph{reasons for unsatisfiability}. However, we feel that we expect more properties from a theory solver that are not necessarily self-evident, and other more advanced properties can be very convenient.

\subsection{Automation}

We usually understand the task of SMT solving as a completely automated process: given an input formula, we derive whether the formula is satisfiable with no user interaction, requiring the theory solver to be completely automated as well.
This is in stark contrast to approaches like \emph{interactive theorem proving} that rely on the user to impose the general solving strategy.
We instead need all components to bring about decisions on heuristics on their own.

\subsection{Soundness \& completeness}

We have not (yet) allowed a solver to \emph{fail}, either by providing an \emph{incorrect result} or being \emph{unable to answer} the posed question.
Statistical approaches for the question of satisfiability exist, and most of them may be incorrect -- though with a small probability.

More commonly, however, certain methods realize that they can not solve a certain problem or do not terminate in certain cases. For example, \emph{virtual substitution} may abort if the degree of some polynomial grows and \emph{Gröbner bases} may fail to prove either satisfiability or unsatisfiability. 
As for termination, regular implementations of \emph{branch and bound} have well-known cases that lead to infinite sequences of branching.

The presented \CDCLT scheme is commonly enhanced such that theory solvers may also return $unknown$, making the SAT solver skip this particular assignment.
If we find the formula to be satisfiable later on, the one $unknown$ answer can be ignored.
Otherwise, the solver as a whole is forced to return $unknown$ as well, as we can not be sure whether the formula is satisfiable.

\subsection{Model generation}

Though we formally ask for the \emph{satisfiability} of some formula, we usually want to have a model in case the formula is indeed satisfiable.
For most theory solvers, we can simply read off the model when we determine satisfiability, for example, when using simplex, interval constraint propagation, virtual substitution, or cylindrical algebraic decomposition.
In other cases, we have to construct a model separately, though the effort is very low as, for example, for Fourier--Motzkin variable elimination.

We thus essentially assume all theory solvers to support the generation of a model.
Of course, some exceptions exist: most notably this is the case for Gröbner bases, though some possible approaches have been proposed in~\cite{Junges2012}.

\subsection{Reasons for unsatisfiability}\label{ssec:SMT:infeasible-subsets}

When a theory solver determines that the given set of theory constraints is unsatisfiable, the SAT solver uses this fact to advance its own search.
We construct a new clause encoding that the theory query $\bigwedge c_i$ is inconsistent and obtain $\bigvee \neg c_i$ -- at least one constraint needs to be $\false$.
Assume the theory query involved $k$ of $n$ theory constraints, this clause \emph{excludes} $2^{n-k}$ possible assignments from the Boolean search space.

Of course, we would like to exclude as many assignments as possible. If we can find a \emph{subset} of the theory constraints that is already unsatisfiable, we can construct a smaller clause. This is a benefit in itself, but also excludes more possible assignments.
While we can find such subsets in theory -- we can simply try all of them -- it heavily depends on the theory solver whether there is an efficient way to do so.

We call such subsets \emph{infeasible subsets} and are usually interested in a (locally) minimal subset.
To find a (globally) minimum subset is a hard problem in most cases and not worth the effort in practice.
Hence, we usually use the term \emph{minimal infeasible subset}.
Note that in general, neither \emph{minimal} nor \emph{minimum} infeasible subsets are unique.

\begin{definition}{Minimal infeasible subset}{minimal-infeasible-subset}\silentdefindex{minimal-infeasible-subset}
	Let $C$ be a set of theory constraints with $C \models\false$.
	We call $C$ \emph{infeasible} and $M \subseteq C$ with $M \models \false$ an \emph{infeasible subset (of $C$)}.
	We say that $M$ is \emph{minimal} if there is no $M' \subsetneq M$ with $M' \models \false$.
	If $M$ is of \emph{minimal cardinality} among all infeasible subsets, we call $M$ a \emph{minimum infeasible subset}.
\end{definition}

We have two fundamentally different approaches to obtain an infeasible subset that mainly depend on the \emph{locality} of the underlying conflict.
We can either enhance the algorithm at hand in a way that (more or less) immediately yields an infeasible subset, or we can mount an a-posteriori analysis in a second stage.

The simplex method, for example, finds unsatisfiability with a local criterion -- when some variable can not be pivoted -- which conveniently yields a local conflict that can be used as an infeasible subset as is described for example in~\cite{Moura2008b}.
The CAD method, on the other hand, only determines unsatisfiability when it exhausted all possible solution candidates, essentially leaving us with no particular information about the conflict.


\subsection{Incrementality}

In the context of lazy SMT solving -- in particular less-lazy SMT solving -- a theory query is not an \emph{isolated computation}.
We can rather understand the sequence of theory queries as an ever-changing problem that is only \emph{extended} or \emph{restricted} by the SAT solver by adding (or removing) constraints to the current set of constraints.

If the theory solver manages to retain information from the previous theory call and merely extends its internal state when a new constraint is added, we can significantly improve practical performance.
Gröbner Bases make for a nice example here, as they essentially only combine existing polynomials to construct new ones until a fixed point is reached.
When adding a new polynomial, we can simply start from the result of the previous computation without having to replicate any work we have already done.

\subsection{Backtracking}

Backtracking is the inverse process of incrementally adding constraints, we remove individual constraints from the current set of constraints.
Again, we can benefit if we manage to retain as much information as possible. This usually means that we have to track which pieces of information originated from which constraints and consequently remove only what originated from the constraints to be removed.

In most cases, this is arguably the most difficult part when integrating a decision procedure into SMT. It oftentimes involves a lot of bookkeeping and usually has certain trade-offs between the granularity or specificity of tracking the origins of data and the computational overhead.
The best strategy is very specific to the decision procedure at hand, and can be anything from ``merely disabling'' certain parts of the state like for simplex~\cite{Dutertre2006} to completely removing currently unused data as proposed in~\cite{Kremer2020}.

The term ``backtracking'' oftentimes implicitly means \emph{in-order} backtracking. It assumes that constraints are added in a certain order and the removal happens in the inverse order. This essentially matches how a \CDCL-style SAT solver manages assignments on a trail.
We note, however, that practical examples exist that motivate \emph{out-of-order} backtracking, allowing for the removal of constraints in an arbitrary order.

Though out-of-order backtracking paves the way for great additional savings, as we can see in the following \cref{ex:out-of-order-backtracking}, it also proves to be a burden on some theory solvers that might exploit the ordering of constraints otherwise.

\begin{example}{Benefits of out-of-order backtracking}{out-of-order-backtracking}
	Consider the following first-order logic formula over a real variable $x$ with $p$ some \emph{difficult} polynomial and its abstraction below.
	\[
		(x < 0 \lor x > 0) \land (x \geq 0 \lor p > 0) \land (x \leq 0 \lor p > 0)
	\]
	\[
		(a \lor b) \land (\neg a \lor c) \land (\neg b \lor c)
	\]

	A typical \CDCL-style SAT solver would assign $\neg a, b, c$ and issue a theory call with $\{x \geq 0, x > 0, p > 0\}$.
	Assuming that this theory call yields UNSAT, it would (after conflict resolution) learn the new clause $(a)$ and backtrack \emph{all assignments} to revise the decision $\neg a$.
	Hence, the next assignments would be $a, c$ and the following theory call $\{ x < 0, p > 0 \}$.

	Note that $p > 0$ was removed and then added again, just so that the SAT solver can maintain its trail properly.
	Instead, we could have removed $x > 0$ and $x \geq 0$, added $x < 0$, and not changed $p > 0$.
	Assuming that $p > 0$ gives rise to a lot of difficult computation in the theory solver, this simple change could provide significant improvements, given that the theory solver can exploit it.
\end{example}

\subsection{Lemma generation}

Most methods that we use within a theory solver provide more information than we need to decide upon the satisfiability.
It is oftentimes possible to extract some pieces of information and formulate them as a \emph{lemma} -- or a \emph{tautology} -- that can be used to \emph{teach the SAT solver} about the theory.
Assuming $x^2 < 0$ being a literal in our formula, we could teach the SAT solver that $\neg (x^2<0)$ is a tautology and thus the literal must be assigned to $\false$, possibly independent of any specific theory query.

\section{Common theory solvers}

Finally, we want to give a rough overview of the techniques that are typically used for theory solving in modern SMT solvers.
Note that the topic of this thesis -- nonlinear arithmetic -- is both comparably new and uncommon in the SMT community, and thus the options we have discussed in \cref{sec:related-work} only apply to very few other SMT solvers.

The better part of the SMT community is concerned with linear arithmetic or completely different theories like \emph{uninterpreted functions} or \emph{bit-precise data types}.
We briefly describe them in the following and refer to~\cite{Kroening2008} for more explanation and further literature on these topics.

\subsection{Uninterpreted functions}

The theory of \emph{(equalities and) uninterpreted functions} introduces function symbols without any semantic other than \emph{being a function}.
They are commonly used to \emph{abstract} from the inner workings of some function or to identify possible implementations for a given specification.

This theory is commonly solved by what is called \emph{congruence closure}: forming equivalence classes based on the equalities and checking them against any disequalities afterward.
Though this approach is arguably simple, actual implementations require quite some considerations to make it efficient in practice.
Note that this theory predates the classical SMT framework and thus some common techniques exist that work outside of the SMT framework, notably the \emph{Ackermann reduction} and \emph{Bryant's reduction}.

\subsection{Bit-precise data types}

In particular, if SMT solvers are used for software verification, it is desirable to be able to model data in a bit-precise way.
Accordingly, theories for fixed-width integers -- commonly known as ``bit-vectors'' -- or fixed-width floating-point numbers exist.
While bit-vector arithmetic has long been supported by many SMT solvers, floating-point arithmetic has only recently found more widespread adoption, most notably since it was added to the \SMTLIB standard after~\cite{Ruemmer2010}.

Though the solving techniques for both are extremely similar -- both are ultimately reduced to propositional logic in most cases -- floating-point arithmetic has proven to be significantly more difficult in practice for two reasons.
While one can oftentimes ignore most bits for bit-vector arithmetic (assuming that a satisfying model is usually ``small'') this is not the case for floating-point arithmetic.
As the interrelations between different bits are much more involved for floating-point arithmetic, one typically needs to consider more bits and thus practical run times are much larger.
Secondly, floating-point arithmetic contains way more theory operations -- for example, square roots or fused multiplication and addition -- and technical details that need to be considered, like different rounding modes or values for infinity or ``not a number''.

\subsection{Linear arithmetic}

The most common theory that incorporates real arithmetic is linear real arithmetic. Given its long history in \emph{linear optimization} (or \emph{operations research}) many applications are routinely formulated as linear real formulae and, thus, it is an important theory for the SMT community as well.

The predominant decision procedure is the simplex method and most current solvers use an integration in the spirit of~\cite{Dutertre2006}.
Other methods, like Fourier--Motzkin variable elimination, are mostly used for preprocessing or in special contexts like the one we describe in \cref{ssec:mcsat:Fourier-Motzkin}.
More specialized methods exist for restricted theories like \emph{difference logic} that even found its way into \SMTLIB~\cite{Barrett2016} as a separate theory.

\subsection{Theory combination}

One sometimes wishes to \emph{combine} multiple theories, usually because the system that one wants to analyze makes use of multiple different formalisms: 
for example, software may use both native integers and floating-point numbers.
Furthermore, uninterpreted functions are sometimes used to \emph{abstract} from functionality that is either not relevant or very hard to argue about, for example, during linearization as described in~\cite{Cimatti2018}.

The goal is to have a framework that allows the modular combination of two (or more) theory solvers for individual theories into a single theory solver for the combined theory.
The first such framework is commonly called \emph{Nelson-Oppen theory combination} and goes back to~\cite{Nelson1979}, but unfortunately, it imposes severe restrictions on the involved theories that are rather hard to lift.
More recently, a more flexible approach called \emph{delayed theory combination} was proposed~\cite{Bozzano2005,Moura2008a} that lifts most of these and is thus used by most modern SMT solvers.

\section{\CDCLT as a proof system\label{ssec:cdcl-proof-system}}

\silentindex{proof-system:cdclt}
It is sometimes convenient to formulate methods like \CDCLT differently than as an algorithm. If we want to argue about certain theoretical properties, we oftentimes use a more declarative approach that is somewhat similar to the proof rules from \cref{ssec:cdcl:deduction}. This gives us a \index{proof-system:cdclt} like presented in~\cite{Nieuwenhuis2006}.

We define \CDCLT such that its proof rules work on a \emph{state}, consisting of what we call a \emph{trail} -- essentially recording the (partial) Boolean assignment -- and the set of clauses that the solver is arguing about.
As for the trail, we simply reuse the \DPLL trail from \cref{def:DPLL-trail}, which slightly differs from~\cite{Nieuwenhuis2006} in that it records the clause that leads to a propagation, though it is not explicitly used in the subsequent proof system.
Furthermore, we allow this clause to be \emph{constructed lazily} as described in~\cite[Section 5]{Nieuwenhuis2006}.
Note that the \DPLL trail contains \emph{literals} which may now be \emph{theory atoms} instead of \emph{Boolean variables} (or their negation) as well.

\begin{definition}{\CDCLT state}{cdcl-state}\silentdefindex{cdcl-state}
	Let $M$ be a \emph{\DPLL trail} as specified in \cref{def:DPLL-trail}.
	We call the combination of a \DPLL trail and a set of clauses $\Clauses$ a \emph{\CDCLT state} and write $\State{M, \Clauses}$.
\end{definition}

For \CDCLT, we strictly distinguish between Boolean reasoning and theory reasoning where the proof system we present is only concerned with Boolean reasoning.
If we write $M \models C$ for a clause $C$, we claim that $L \in M$ for some literal $L \in C$ -- a statement arguing only about the Boolean state contained in the trail. In contrast to that $M \models_T C$ denotes \emph{entailment in the theory $T$}.

The set of rules presented here is taken from~\cite{Nieuwenhuis2006}, but we allow for strong theory derivations as presented in~\cite{Robere2018}, which essentially turns the \rulename{T-Learn} rule into the \rulename{T-Learn$^*$} and gives us the stronger \index{proof-system:cdclst}.
Though we change the notation a bit, we feel that the equivalence of the rules (to~\cite{Nieuwenhuis2006}) is straightforward.

\begin{definition}{\CDCLT and \CDCLST proof systems}{cdcl-proof-system}\silentdefindex{proof-system:cdclt}
	The \index{proof-system:cdclt} consists of the following proof rules.
	\begin{align*}
		\ProofRule{Decide}{\State{M, \Clauses}}{\State{\Seq{M, L}, \Clauses}}{\textrm{$L$ or $\neg L$ occurs in $\Clauses$,} \\ \textrm{$L$ is undefined in $M$}} \\
		\ProofRule{Fail}{\State{M, \Clauses \cup \{C\}}}{FailState}{\textrm{$M \models \neg C$,} \\ \textrm{$M$ contains no decision literals}} \\
		\ProofRule{UnitPropagate}{\State{M, \Clauses}}{\State{\Seq{M,C \rightarrow L}, \Clauses}}{\textrm{$C = D \lor L \in \Clauses$,} \\ \textrm{$M \models \neg D$,} \\ \textrm{$L$ is undefined in $M$}} \\
		\ProofRule{TheoryPropagate}{\State{M, \Clauses}}{\State{\Seq{M, (D \lor L) \rightarrow L}, \Clauses}}{\textrm{$M \models_T D \lor L$ and $M \models \neg D$,} \\ \textrm{$L$ or $\neg L$ occurs in $\Clauses$,} \\ \textrm{$L$ is undefined in $M$}}
		\intertext{As already mentioned, we allow the clause which causes a propagation to be constructed lazily. Following~\cite{Nieuwenhuis2006}, we assume that $(D \lor L)$ is usually only constructed when the \rulename{T-Backjump} rule is applied and merely serves as a placeholder here and in the trail.}
		\ProofRule{T-Backjump}{\State{\Seq{M, L, N }, \Clauses}}{\State{\Seq{M, (C' \lor L') \rightarrow L' }, \Clauses}}{
			\textrm{$C \in \Clauses$ with $\Seq{M, L, N } \models \neg C$,} \\ \textrm{there is some clause $C' \lor L'$ such that:} \\ 
			\textrm{\quad $\Clauses \models_T C' \lor L'$ and $M \models \neg C'$,} \\ 
			\textrm{\quad $L'$ is undefined in $M$,} \\ 
			\textrm{\quad $L'$ or $\neg L'$ occurs in $\Clauses$ or in $\Seq{M, L, N }$}
		} \\
		\ProofRule{T-Learn}{\State{M, \Clauses}}{\State{M, \Clauses \cup \{C\}}}{\textrm{each atom of $C$ occurs in $\Clauses$ or in $M$,} \\ \textrm{$\Clauses \models_T C$}} \\
		\ProofRule{T-Forget}{\State{M, \Clauses \cup {C}}}{\State{M, \Clauses}}{\textrm{$\Clauses \models_T C$}} \\
		\ProofRule{Restart}{\State{M, \Clauses}}{\State{\Seq{}, \Clauses}}{}
		\intertext{We call the proof system obtained by replacing \rulename{T-Learn} by the following \rulename{T-Learn$^*$} rule the \defindex{proof-system:cdclst}.}
		\ProofRule{T-Learn$^*$}{\State{M, \Clauses}}{\State{M, \Clauses \cup {C}}}{\textrm{$\Clauses \models_T C$}}
	\end{align*}
\end{definition}

The \CDCLT proof system (as well as \CDCLST) either ends up in $FailState$ -- indicating that the input formula is unsatisfiable -- or any other state where no further rule can be applied -- indicating that the input formula is satisfiable and the trail contains a complete Boolean model (but not the theory model).
