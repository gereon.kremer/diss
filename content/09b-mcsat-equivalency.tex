\section{Algorithmic equivalency to \CDCLST}\label{sec:mcsat:equivalency}

\silentindex{mcsat-equivalency-cdclt}
Given that \MCSAT and \CDCLST aim to solve the same problem and arguably share many algorithmic ideas, we now discuss how these two methods relate to each other.
We have already shown that \MCSAT is bisimilar to \ResST{} -- and thus also to \CDCLST as shown in~\cite{Robere2018} -- in terms of \emph{proof complexity}.

However, we have already noted that the relation between \MCSAT and \ResST is even stronger. They not only construct \emph{some proof} (with polynomial overhead) but a \emph{logically equivalent proof} and we thus call them \emph{algorithmically equivalent}.
Of course, the proofs are not \emph{identical} -- after all, the proof rules of \MCSAT and \ResST argue about different objects -- but the core reasoning steps are equivalent.
We concretize this fuzzy notion of \emph{algorithmic equivalency} in \cref{def:algorithmic-equivalency}.

\begin{definition}{Algorithmic equivalency}{algorithmic-equivalency}\silentdefindex{proof-system:algorithmic-equivalency}
	Let $\ProofSystem_1$ and $\ProofSystem_2$ be two proof systems and $\StateEQ{}$ a relation on the states of $\ProofSystem_1$ and $\ProofSystem_2$ indicating that two states are \emph{logically equivalent}.
	Let $\Proof_1 = \Phi_0 \vdash \cdots \vdash \Phi_k$ and $\Proof_2 = \Psi_0 \vdash \cdots \vdash \Psi_k$ be proofs in $\ProofSystem_1$ and $\ProofSystem_2$, respectively. We say that $\Proof_1$ and $\Proof_2$ are \emph{equivalent} if $\Phi_i \StateEQ{} \Psi_i$ for all $i = 0, \dots k$.

	We extend this notion of equivalence to proofs that differ in their length.
	Let $\Proof_1$ as before and
	\[
		\Proof_2 = \Psi_0 \vdash \cdots \vdash \Psi_{j-1} \vdash \Psi_j^0 \vdash \cdots \vdash \Psi_j^m \vdash \Psi_{j+1} \vdash \cdots \vdash \Psi_k
	\]
	such that $\Phi_i \StateEQ{} \Psi_i$ for all $i = 0, \dots j-1, j+1, \dots k$ and $\Phi_j \StateEQ{} \Psi_j^i$ for all $i = 0, \dots m$.
	If $m$ is at most polynomially larger than $k$ we consider $\Proof_1$ and $\Proof_2$ equivalent and thereby essentially allow $\Proof_1$ to \emph{squash multiple proof steps (of $\Proof_2$) into one}.

	Let $\ProofSystem_2$ such that it allows for an equivalent proof for every proof in $\ProofSystem_1$. We say that $\ProofSystem_2$ \emph{algorithmically simulates} $\ProofSystem_1$ and write $\ProofSystem_2 \BSGEQ \ProofSystem_1$.
	If both $\ProofSystem_1 \BSGEQ \ProofSystem_2$ and $\ProofSystem_2 \BSGEQ \ProofSystem_1$, we call $\ProofSystem_1$ and $\ProofSystem_2$ \emph{algorithmically equivalent} and write $\ProofSystem_1 \BSEQ \ProofSystem_2$.
\end{definition}

Recalling the proof for the bisimilarity of \ResST and \MCSAT, we observe that we provided simulations for all rules individually and thus essentially already showed \emph{algorithmic equivalency}.
Let us now, however, turn to the actual topic of interest, the relation of \MCSAT and \CDCLST.
Given that we have $\MCSAT \PCEQ \ResST \PCEQ \CDCLST$ and $\MCSAT \BSEQ \ResST$, it seems only reasonable to assume that, in fact, $\MCSAT \BSEQ \CDCLST$.

\begin{theorem}{Algorithmic equivalence of \CDCLST and \MCSAT}{cdcl-mcsat-equivalence}\silentindex{proof-system:mcsat}\silentindex{proof-system:cdclst}
	\CDCLST and \MCSAT are \emph{algorithmically equivalent}.
\end{theorem}

Our approach to prove \cref{thm:cdcl-mcsat-equivalence} is as follows:
we define the \emph{equivalence relation $\StateEQ{}$} on the states of both proof systems as an equivalence relation on the respective trails and then show how they can \emph{algorithmically simulate} each other.
For every rule in one of the two proof systems, we give a polynomial sequence of rule applications in the other proof system that ends in a state that is equivalent according to $\StateEQ{}$.

\subsection{Equivalence of states}

Recall the definitions of the trails that are used in \CDCLST and \MCSAT, respectively.

\begin{reminder}{\DPLL and \MCSAT trails}{trails}
	\vspace*{-1.5\abovedisplayskip}
	\begin{align*}
		\shortintertext{A \DPLL trail contains the following elements:}
		L &&& \textrm{Boolean decision of literal $L$} \\
		C \rightarrow L &&& \textrm{Boolean implication of literal $L$ due to clause $C$}
		\shortintertext{An \MCSAT trail additionally contains the following elements:}
		x \mapsto \alpha_x &&& \textrm{Theory assignment of $x$ to theory value $\alpha_x$}
	\end{align*}
\end{reminder}

We claim that the theory model is \emph{only} a heuristic way to guide the solver to meaningful rule applications -- which in no way should diminish its importance in practice.
Our equivalence, therefore, ignores all theory assignments from the \MCSAT trail.

\begin{definition}{Equivalence of trails}{cdcl-mcsat-trail-equivalence}\silentdefindex{proof-system:trail-equivalence}
	Let $M_{\DPLL}$ be a \DPLL trail and $M_{\MCSAT}$ an \MCSAT trail.
	We define the \emph{reduced \MCSAT trail} $red(M_{\MCSAT})$ as follows:
	\begin{align*}
		red(\Seq{}) &= \Seq{} \\
		red(\Seq{M, L}) &= \Seq{red(M), L} \\
		red(\Seq{M, C \rightarrow L}) &= \Seq{red(M), C \rightarrow L} \\
		red(\Seq{M, x \mapsto \alpha_x}) &= red(M)
	\end{align*}
	We call two trails \emph{equivalent} if $M_{\DPLL} = red(M_{\MCSAT})$ and write $M_{\DPLL} \StateEQ{} M_{\MCSAT}$.
\end{definition}

We now use this notion of \emph{equivalence on trails} to induce \emph{equivalence on states}.
Observe that states of \CDCLST and \MCSAT are almost identical (consisting of a trail and a set of clauses) with the exception of the \MCSAT conflict state that also includes a conflict clause.
As the whole process of conflict resolution is squashed anyway, the equivalence in the following \cref{def:cdcl-mcsat-state-equivalence} ignores the conflict clause.

\begin{definition}{Equivalence of states}{cdcl-mcsat-state-equivalence}\silentdefindex{proof-system:state-equivalence}
	Let $\State{M_{\DPLL}, \Clauses}$ be a \CDCLST state, $\State{M_{\MCSAT}, \Clauses}$ an \MCSAT search state, and $\State{M_{\MCSAT}, \Clauses} \Conflicts C$ an \MCSAT conflict state.
	If $M_{\DPLL} \StateEQ{} M_{\MCSAT}$ we call the \CDCLST state \emph{equivalent} to the \MCSAT search state (or \MCSAT conflict state) and write $\State{M_{\DPLL}, \Clauses} \StateEQ{} \State{M_{\MCSAT}, \Clauses}$ (or $\State{M_{\DPLL}, \Clauses} \StateEQ{} \State{M_{\MCSAT}, \Clauses} \Conflicts C$).
\end{definition}

\subsection{\MCSAT algorithmically simulates \CDCLST}

We show that the \MCSAT proof system algorithmically simulates the \CDCLST proof system.
Similar to the proof in \cref{ssec:mcsat:proof-complexity}, we proceed by giving \MCSAT rule applications for every \CDCLST rule that can be combined to transform any \CDCLST proof into an \MCSAT proof following \cref{def:algorithmic-equivalency}.

\begin{proof}
We show that every rule from \CDCLST can be algorithmically simulated in \MCSAT with only polynomial overhead.
We do not use the \rulename{T-Decide} rule during the simulation of \CDCLST proof rules and thus never obtain \MCSAT trails that contain theory assignments.
This implies $\Value(L,M) = \Value_\B(L,M)$ and $\Value_\B(L,M) = \Undef$ if and only if $L,\neg L \not\in M$.

\paragraph{\CDCLST \rulename{Decide}:}
\begin{align*}
\ProofRule{}{\State{M, \Clauses}}{\State{\Seq{M, L}, \Clauses}}{\textrm{$L$ or $\neg L$ occurs in $\Clauses$,} \\ \textrm{$L$ is undefined in $M$}}
\shortintertext{This rule is directly simulated by the \MCSAT \rulename{Decide} rule after some reformulation on the condition. We observe that $\Basis$ contains all literals from $\Clauses$ and $L$ is undefined in $M$ exactly if $\Value(L,M) = \Undef$.}
\ProofRule{}{\State{M, \Clauses}}{\State{\Seq{M, L}, \Clauses}}{\textrm{$L \in \Basis$,} \\ \textrm{$\Value(L,M) = \Undef$}}
\end{align*}

\paragraph{\CDCLST \rulename{Fail}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses \cup \{C\}}}{FailState}{\textrm{$M \models \neg C$,} \\ \textrm{$M$ contains no decision literals}}
	\shortintertext{%
		The \rulename{Fail} rule can be applied whenever a clause $C$ evaluates to \false and there is no decision in the trail. We eventually want to apply the \MCSAT \rulename{Unsat} rule which, however, requires a conflict state and the conflict clause to be the empty clause. We start by applying the \rulename{Conflict} rule to enter conflict resolution.
	}
	\ProofRule{Conflict}{\State{M, \Clauses}}{\State{M, \Clauses} \Conflicts C}{\textrm{$C \in \Clauses$, $\Value(C) = \false$}}
	\shortintertext{%
		Now we transform the current conflict clause to the empty clause.
		$M$ contains no decision literals and thus only propagations, and, furthermore, for every $L \in C$ we have one propagation $\neg L$ in our trail, as we have $M \models \neg C$. Formally:
	}
	\multispan4{$\displaystyle M = \Seq{L_1, \dots , L_n}, C = (K_1 \lor \dots \lor K_m), \Forall{K_i} \neg K_i \in \{ L_1, \dots, L_n \}$\hfil}\\
	\shortintertext{%
		Therefore, we can apply either the \rulename{Resolve} rule -- if the last propagation from the trail is a literal from $C$ -- or the \rulename{Consume} rule until the trail is empty.
		Note that the above statement -- the negation of every element from $C$ is in the trail -- is invariant under these rules: the \rulename{Resolve} rule eliminates $L$ from $C$ and only adds literals that are \false in $M$ and thus have negations in $M$ while the \rulename{Consume} rule only removes propagations from the trail that do not appear in $C$.
	}
	\ProofRule{Consume}{\State{\Seq{M, D \rightarrow L}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts C}{\textrm{$\neg L \not\in C$}} \\
	\ProofRule{Resolve}{\State{\Seq{M, D \rightarrow L}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts R}{\textrm{$\neg L \in C$,} \\ \textrm{$R = \Resolution_L(C,D)$}}
	\shortintertext{%
		As argued before, these rules are applicable until the trail is empty. Furthermore, we know that $M$ contains the negation of every literal from $C$. As $M = \Seq{}$, we know that $C = () \equiv \false$ and we can, therefore, apply the \rulename{Unsat} rule.
	}
	\ProofRule{Unsat}{\State{\Seq{}, \Clauses} \Conflicts \false}{unsat}{}
\end{align*}

We note that we apply the \rulename{Consume} and \rulename{Resolve} rules as many times as we have propagations in our trail, that is at most $n$ times for $n$ variables in the input formula, and thus get a linear overhead.

\paragraph{\CDCLST \rulename{UnitPropagate}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses}}{\State{\Seq{M,C \rightarrow L}, \Clauses}}{\textrm{$C = D \lor L \in \Clauses$,} \\ \textrm{$M \models \neg D$,} \\ \textrm{$L$ is undefined in $M$}}
	\shortintertext{This rule is directly simulated by the \MCSAT \rulename{Propagate} rule and the conditions are equivalent after reformulation.}
	\ProofRule{Propagate}{\State{M, \Clauses}}{\State{\Seq{M,C \rightarrow L}, \Clauses}}{\textrm{$C = (L_1 \lor \dots \lor L_n \lor L) \in \Clauses$,} \\ \textrm{$\Forall{i} \Value(L_i,M) = \false$,} \\ \textrm{$\Value(L,M) = \Undef$}}
	\shortintertext{%
		Note that the clause $C$ that we use as the explanation (or reason) for the propagation is not necessarily unique, but the one used by \CDCLST is always part of $\Clauses$ so that we can perform the same propagation.
	}
\end{align*}

\paragraph{\CDCLST \rulename{TheoryPropagate}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses}}{\State{\Seq{M, (D \lor L) \rightarrow L}, \Clauses}}{\textrm{$M \models_T D \lor L$, $M \models \neg D$,} \\ \textrm{$L$ or $\neg L$ occurs in $\Clauses$,} \\ \textrm{$L$ is undefined in $M$}}
	\shortintertext{%
		We simulate this rule by the \MCSAT \rulename{T-Propagate} rule which essentially serves the same purpose, though the details of how we show the propagation to be sound differ. As above, the \CDCLST conditions imply $L \in \Basis$ and $\Value(L,M) = \Undef$. Furthermore, we observe that 
		\[
			M \models_T L
			\Leftrightarrow M \not\models_T \neg L
			\Leftrightarrow M, \neg L \models_T \false
			\Leftrightarrow \Infeasible(\Seq{M, \neg L})
		\]
		and can thus apply \rulename{T-Propagate}.
	}
	\ProofRule{T-Propagate}{\State{M, \Clauses}}{\State{\Seq{M, E \rightarrow L}, \Clauses}}{\textrm{$L \in \Basis, \Value(L,M) = \Undef$,} \\ \textrm{$\Infeasible(\Seq{M, \neg L})$,} \\ \textrm{$E = \Explain(\Seq{M, \neg L})$}}
\end{align*}

We note that the original definition of the \rulename{TheoryPropagate} rule from~\cite{Nieuwenhuis2006} seems to avoid the burden of actually coming up with a reason for the propagation. In theory, this does not matter as we consider the theory reasoning to have zero cost.
In practice, however, this burden is only moved to the \CDCLST \rulename{T-Backjump} rule (that we show later) where \CDCLST needs to find \emph{some clause} with this property -- if it is needed for the backtracking. Note that the original presentation of \MCSAT~\cite{Jovanovic2012} proposes to essentially do the same by only calculating the explanations lazily when they are actually needed in the conflict analysis phase.

Also note that $\Infeasible$ is commonly implemented with a finite lookahead, checking whether the theory model can be extended by another single theory variable without rendering the trail inconsistent.
The authors of~\cite{Moura2013} state, however, that $\Infeasible(M)$ is equivalent to the literals from $M$ being unsatisfiable together with the partial model from $M$. 
While having a finite lookahead technically violates the above equivalence $M,\neg L \models_T \false \Leftrightarrow \Infeasible(\Seq{M,\neg L})$, it only \emph{defers} the detection of theory conflicts, but never misses them.
As soon as the last variable is to be assigned, $\Infeasible$ is complete and detects all possible conflicts, and the \MCSAT proof system is robust enough to deal with such conflicts that are found later than expected.

This practical issue may very well make our proofs longer -- due to the reasoning we have to perform until we eventually detect the conflict in practice -- and we discuss this issue in \cref{sec:mcsat:practicability}.

\paragraph{\CDCLST \rulename{T-Backjump}:}
\begin{align*}
	\ProofRule{}{\State{\Seq{M, L, N }, \Clauses}}{\State{\Seq{M, (C' \lor L') \rightarrow L' }, \Clauses}}{
		\textrm{$C \in \Clauses$ with $\Seq{M, L, N } \models \neg C$,} \\ \textrm{there is some clause $C' \lor L'$ such that:} \\ 
		\textrm{\quad $\Clauses \models_T C' \lor L'$ and $M \models \neg C'$,} \\ 
		\textrm{\quad $L'$ is undefined in $M$,} \\ 
		\textrm{\quad $L'$ or $\neg L'$ occurs in $\Clauses$ or in $\Seq{M, L, N }$}
	}
\end{align*}

This rule encapsulates the whole process of conflict analysis and backtracking in one rule whereas \MCSAT details how conflict analysis works by giving a whole set of rules for this case.
\CDCLST, on the other hand, does not specify how the conflict analysis should proceed and -- in theory -- allows for a completely different scheme for conflict analysis and resolution.

This leaves us in a tight spot: the straightforward simulation using the \MCSAT conflict analysis rules will only be able to simulate resolution-based approaches, and while this (should) be able to simulate any other approach -- due to the (refutational) completeness of resolution -- it may fail to do so in polynomial time. We feel that this is, however, the meaningful simulation, as it simulates what \CDCLST solvers do in practice.

We can avoid this issue by going for a more general solution: we can eliminate the \rulename{T-Backjump} rule from the \CDCLST proof system and essentially replace it by the \rulename{T-Learn} and \rulename{UnitPropagate} rules. However, this departs from what we think happens in actual solvers in practice. We thus consider the straightforward variant by far more relevant and instructive, but propose the more general variant as well for theoretical completeness. Therefore, we provide both reductions in this order.

Let us assume now that the clause $C' \lor L'$ is obtained by means of resolution on the propagated literals in $N$ and $L$ is the first unique implication point, implying that $N$ only contains propagations and decisions that are not relevant for the conflict.
To employ the regular conflict analysis scheme, we first need to enter conflict resolution using the \rulename{Conflict} rule. Then, we apply the \rulename{Resolve} or \rulename{Consume} rules to construct the conflict clause $C'$ and eventually use the \rulename{Backjump} rule to leave conflict resolution and perform propagation on the literal $L'$.

\begin{align*}
	\ProofRule{Conflict}{\State{\Seq{M, L, N}, \Clauses}}{\State{\Seq{M, L, N}, \Clauses} \Conflicts C}{\textrm{$C \in \Clauses$, $\Value(C) = \false$}}
	\shortintertext{%
		We already noted that $N$ only contains propagations or decisions that are not relevant for the conflict. This is almost the same situation as when simulating the \CDCLST \rulename{Fail} rule and with essentially the same argument we can apply the \rulename{Consume} rule -- in this case either for propagations or decisions -- and the \rulename{Resolve} rule until $N = \Seq{}$ to obtain some conflict clause $C' \lor L'$.
	}
	\ProofRule{Consume}{\State{\Seq{M, D \rightarrow L}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts C}{\textrm{$\neg L \not\in C$}} \\
	\ProofRule{Consume}{\State{\Seq{M, L}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts C}{\textrm{$\neg L \not\in C$}} \\
	\ProofRule{Resolve}{\State{\Seq{M, D \rightarrow L}, \Clauses} \Conflicts C}{\State{M, \Clauses} \Conflicts R}{\textrm{$\neg L \in C$,} \\ \textrm{$R = \Resolution_L(C,D)$}}
	\shortintertext{%
		Now, we can apply the \rulename{Backjump} rule, given that $N = \Seq{L}$ starts with a decision and the other conditions hold as $L$ is the first unique implication point.
	}
	\ProofRule{Backjump}{\State{\Seq{M, N}, \Clauses} \Conflicts C}{\State{\Seq{M, C \rightarrow L}, \Clauses}}{\textrm{$C = (L_1 \lor \dots \lor L_n \lor L)$,} \\ \textrm{$\Forall{i} \Value(L_i,M) = \false$,} \\ \textrm{$\Value(L,M) = \Undef$,} \\ \textrm{$N$ starts with a decision}}
\end{align*}

For the more general case, we observe that we can eliminate the \rulename{T-Backjump} rule from the \CDCLST proof system and simulate it by applying the \rulename{T-Learn} and \rulename{Restart} rules, restoring the trail $M$ and finally the \rulename{UnitPropagate} rule. Note that this is not far off our intuition: We somehow learn a new clause, backtrack -- by removing something from the trail -- and use the learned fact for propagation. We observe that the new clause $C' \lor L'$ used by the \rulename{T-Backjump} rule only allows for literals from the current formula as $M \models \neg C'$ and $L' \in atoms(\Clauses)$.

Given the \MCSAT equivalents for all the rules used here, we implicitly get a simulation for the \rulename{T-Backjump} rule. The number of rule applications is linear in the size of the trail and thus in the number of variables. Restoring the trail relies on the optimal scheduler to properly rebuild whatever was on the trail before applying the \rulename{Restart} rule, using the \rulename{Decide} and \rulename{UnitPropagate} rules.

\paragraph{\CDCLST \rulename{T-Learn$^*$}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses}}{\State{M, \Clauses \cup \{C\}}}{\textrm{$\Clauses \models_T C$}}
	\shortintertext{%
		\MCSAT does not allow for learning arbitrary clauses, instead it allows for learning the current conflict clause only. Our approach is, therefore, to restart the solver, construct an (artificial) conflict that yields the desired clause, learn it, and finally reconstruct the trail.
	}
	\ProofRule{Restart}{\State{M, \Clauses}}{\State{\Seq{}, \Clauses}}{}
	\shortintertext{%
		We use the \rulename{Decide} rule repeatedly to add $\neg L_i$ for all $L_i \in C$ to the trail. We then have $\Infeasible(M)$ and apply the \rulename{T-Conflict} rule with $E = C$.
	}
	\ProofRule{Decide}{\State{\Seq{}, \Clauses}}{\State{\Seq{\neg L_i, \dots}, \Clauses}}{} \\
	\ProofRule{T-Conflict}{\State{\Seq{\neg L_i, \dots}, \Clauses}}{\State{\Seq{\neg L_i, \dots}, \Clauses} \Conflicts C}{}
	\shortintertext{%
		Now, we only need to apply the \rulename{Learn} rule to add the conflict clause to $\Clauses$, restart again, and restore the trail.
	}
\end{align*}

By simulating the \rulename{T-Learn$^*$} rule, we can also easily simulate the special case \rulename{T-Learn}, exactly as described before.

\paragraph{\CDCLST \rulename{T-Forget}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses \cup {C}}}{\State{M, \Clauses}}{\textrm{$\Clauses \models_T C$}}
\end{align*}
We observe that this rule almost corresponds to the \MCSAT \rulename{Forget} rule, except that \MCSAT only allows to forget learned clauses. \CDCLST instead allows to forget any \emph{redundant} clauses -- in the sense that they may be recovered by the \rulename{T-Learn} rule -- irrespective of whether they were part of the original clause set.

If the clause to forget happens to be a learned clause, we can simply apply the \MCSAT \rulename{Forget} rule. Otherwise, we do nothing and keep the clause. We keep at most as many additional clauses as we had in the original formula and thus the overhead is polynomial.
As all clauses are still entailed by the current clause set $\Clauses$, keeping redundant clauses does not change the semantics of $\models$ or $\models_T$.

\paragraph{\CDCLST \rulename{Restart}:}
\begin{align*}
		\ProofRule{}{\State{M, \Clauses}}{\State{\Seq{}, \Clauses}}{}
\end{align*}
This rule is identical to the \MCSAT \rulename{Restart} rule.

We have shown how to algorithmically simulate every individual \CDCLST proof rule within the \MCSAT proof system and have thus shown that \MCSAT algorithmically simulates \CDCLST.
We observe that we did not use any of the rules that deal with theory assignments -- \rulename{T-Decide}, \rulename{T-Consume} and \rulename{T-Backjump-Decide} -- which indicates that this component of \MCSAT does not contribute to its (theoretical) power. We give some more thoughts to this in \cref{ssec:mcsat:theory-decisions}.
\end{proof}


\subsection{\CDCLST algorithmically simulates \MCSAT}

We have seen in the previous section that \MCSAT is at least as powerful as \CDCLST in the sense that it algorithmically simulates any \CDCLST proof rule.
This leaves the possibility that \MCSAT is a stronger proof system than \CDCLST. We now show that they are in fact algorithmically equivalent, as \CDCLST algorithmically simulates every \MCSAT proof rule.

\begin{proof}
We have seen that \MCSAT does not rely on theory assignments to simulate \CDCLST, and this observation motivates how we deal with \MCSAT proof rules that use theory assignments: we simply ignore them and show that it is safe to do. A closer look at the \MCSAT proof system reveals that theory assignments only restrict the applicability of certain rules, but do not contribute to new clauses in an explicit way.

Furthermore, we have already observed that the level of detail for the specification of the conflict analysis is very different. While \MCSAT gives detailed rules on how to perform the conflict analysis, \CDCLST just contains a single rule which \emph{does everything}. We can thus ignore all \MCSAT steps that deal with conflict resolution and perform everything within a single application of the \rulename{T-Backjump} rule -- or the \rulename{Fail} rule if we determine unsatisfiability -- when \MCSAT leaves the conflict resolution state.
The only exception to this is the \rulename{Learn} rule which does not change the trail or the conflict clause, but the formula and we can trivially simulate it with the \rulename{T-Learn$^*$} rule.

\MCSAT may introduce new literals occasionally, in particular when $\Explain$ is called, from the finite basis $\Basis$. Whenever a new literal $L \not\in atoms(\Clauses)$ is used, we use the \rulename{T-Learn$^*$} rule to learn the clause $L \lor \neg L$ and get $L \in atoms(\Clauses)$.

\paragraph{\MCSAT \rulename{Decide}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses}}{\State{\Seq{M, L}, \Clauses}}{\textrm{$L \in \Basis$,} \\ \textrm{$\Value(L,M) = \Undef$}}
\end{align*}
If the decision literal $L$ is a new literal, we use the above technique to add $L \lor \neg L$ to $\Clauses$ and ensure that $L \in atoms(\Clauses)$.
Now, we can apply the \rulename{Decide} rule to perform the equivalent task in \CDCLST.


\paragraph{\MCSAT \rulename{Propagate}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses}}{\State{\Seq{M,C \rightarrow L}, \Clauses}}{\textrm{$C = (L_1 \lor \dots \lor L_n \lor L) \in \Clauses$,} \\ \textrm{$\Forall{i} \Value(L_i,M) = \false$,} \\ \textrm{$\Value(L,M) = \Undef$}}
\end{align*}
This rule is identical to the \rulename{UnitPropagate} rule up to the usual reformulations.

\paragraph{\MCSAT \rulename{Conflict}:}
We ignore this rule as argued above. We process the whole subsequent conflict analysis when we leave the conflict analysis state with the \rulename{Backjump}, \rulename{Unsat}, or \rulename{T-Backjump-Decide} rules.

\paragraph{\MCSAT \rulename{Sat}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses}}{sat}{\textrm{$\Satisfied(\Clauses,M)$}}
\end{align*}
Instead of a special $sat$ state, \CDCLST signals satisfiability by terminating in any state other than $FailState$ when no further rule can be applied. Therefore, we do nothing in this case.

\paragraph{\MCSAT \rulename{Forget}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses}}{\State{M, \Clauses \setminus \{ C \}}}{\textrm{$C \in \Clauses$ is a learned clause}}	
\end{align*}
This rule is a special case of the \CDCLST \rulename{Forget} rule, restricted to learned clauses.
We can thus apply the \rulename{Forget} rule.

\paragraph{\MCSAT \rulename{Restart}:}
The \rulename{Restart} rules of \MCSAT and \CDCLST are identical.

\paragraph{\MCSAT \rulename{Resolve}:}
We ignore this rule as argued above. We process the whole subsequent conflict analysis when we leave the conflict analysis state with the \rulename{Backjump}, \rulename{Unsat}, or \rulename{T-Backjump-Decide} rules.

\paragraph{\MCSAT \rulename{Consume}:}
We ignore this rule as argued above. We process the whole subsequent conflict analysis when we leave the conflict analysis state with the \rulename{Backjump}, \rulename{Unsat}, or \rulename{T-Backjump-Decide} rules.

\paragraph{\MCSAT \rulename{Backjump}:}
\begin{align*}
	\ProofRule{}{\State{\Seq{M, N}, \Clauses} \Conflicts C}{\State{\Seq{M, C \rightarrow L}, \Clauses}}{\textrm{$C = (L_1 \lor \dots \lor L_n \lor L)$,} \\ \textrm{$\Forall{i} \Value(L_i,M) = \false$,} \\ \textrm{$\Value(L,M) = \Undef$,} \\ \textrm{$N$ starts with a decision}}
	\shortintertext{%
		We can simulate this rule by the \rulename{T-Backjump} rule. We have $L$ in the trail as for \MCSAT $N$ starts with a decision. Furthermore, the clause $C' \lor L'$ can be chosen to be the \MCSAT conflict clause $C$ and has the required properties: it is entailed by $\Clauses$ as it was constructed by resolution from $\Clauses$, $M \models \neg C'$ holds, $L'$ is not assigned, and all literals are from $atoms(\Clauses)$ -- after we added them as described above.
	}
	\ProofRule{T-Backjump}{\State{\Seq{M, L, N }, \Clauses}}{\State{\Seq{M, (C' \lor L') \rightarrow L' }, \Clauses}}{
			\textrm{$C \in \Clauses$ with $\Seq{M, L, N } \models \neg C$,} \\ \textrm{there is some clause $C' \lor L'$ such that:} \\ 
			\textrm{\quad $\Clauses \models_T C' \lor L'$ and $M \models \neg C'$,} \\ 
			\textrm{\quad $L'$ is undefined in $M$,} \\ 
			\textrm{\quad $L'$ or $\neg L'$ occurs in $\Clauses$ or in $\Seq{M, L, N }$}
		}
\end{align*}

\paragraph{\MCSAT \rulename{Unsat}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses} \Conflicts \false}{unsat}{}
\end{align*}
As we would expect, this is essentially a special case of the \rulename{Fail} rule with $C = \false$.
We first add the conflict clause $()$ to $\Clauses$ using the \rulename{T-Learn} rule, restart the solver, and finally apply the \rulename{Fail} rule with the newly added empty clause.

\paragraph{\MCSAT \rulename{Learn}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses} \Conflicts C}{\State{M, \Clauses \cup \{C\}}}{\textrm{$C \not\in \Clauses$}}
\end{align*}
As argued above, the current conflict clause of \MCSAT is a logical consequence of $\Clauses$ at any given time. Hence we can apply the \rulename{T-Learn$^*$} rule to add $C$ to $\Clauses$.

\paragraph{\MCSAT \rulename{T-Propagate}:}
\begin{align*}
	\ProofRule{}{\State{M, \Clauses}}{\State{\Seq{M, E \rightarrow L}, \Clauses}}{\textrm{$L \in \Basis, \Value(L,M) = \Undef$,} \\ \textrm{$\Infeasible(\Seq{M, \neg L})$,} \\ \textrm{$E = \Explain(\Seq{M, \neg L})$}}
	\shortintertext{%
		We recall that $\Explain$ always returns a \emph{valid theory lemma} -- a tautology -- and thus every clause returned by $\Explain$ is a logical consequence of $\Clauses$. We can, thus, use the \rulename{T-Learn$^*$} rule to add the clause $E$ to $\Clauses$ and then perform a Boolean propagation via the \rulename{UnitPropagate} rule instead of the theory propagation. \endgraf
		We can also, alternatively, eliminate the \rulename{T-Propagate} rule within \MCSAT itself. Given that $\Value(L,M) = \Undef$, we can apply the \rulename{Decide} rule to add $\neg L$ to the trail and as $\Infeasible(\Seq{M, \neg L})$ we can apply the \rulename{T-Conflict} rule. The \rulename{Backjump} rule immediately resolves this conflict and replaces the decision $\neg L$ by the propagation $E \rightarrow L$.
	}
	\ProofRule{Decide}{\State{M, \Clauses}}{\State{\Seq{M, \neg L}, \Clauses}}{} \\
	\ProofRule{T-Conflict}{\State{\Seq{M, \neg L}, \Clauses}}{\State{\Seq{M, \neg L}, \Clauses} \Conflicts E}{} \\
	\ProofRule{Backjump}{\State{\Seq{M, \neg L}, \Clauses} \Conflicts E}{\State{\Seq{M, E \rightarrow L}, \Clauses}}{}
\end{align*}

\paragraph{\MCSAT \rulename{T-Decide}:}
We ignore this rule as argued above. Theory assignments are not needed to simulate \MCSAT.

\paragraph{\MCSAT \rulename{T-Conflict}:}
We ignore this rule as argued above. We process the whole subsequent conflict analysis when we leave the conflict analysis state with the \rulename{Backjump}, \rulename{Unsat}, or \rulename{T-Backjump-Decide} rules.

\paragraph{\MCSAT \rulename{T-Consume}:}
We ignore this rule as argued above. We process the whole subsequent conflict analysis when we leave the conflict analysis state with the \rulename{Backjump}, \rulename{Unsat}, or \rulename{T-Backjump-Decide} rules.

\paragraph{\MCSAT \rulename{T-Backjump-Decide}:}
\begin{align*}
	\ProofRule{}{\State{\Seq{M, x \mapsto \alpha_x, N}, \Clauses} \Conflicts C}{\State{\Seq{M, L}, \Clauses}}{\textrm{$C = (L_1 \lor \dots \lor L_n \lor L)$,} \\ \textrm{$\Exists{i} \Value(L_i,M) = \Undef$,} \\ \textrm{$\Value(L,M) = \Undef$}}
	\shortintertext{%
		As we ignore theory assignments, we simulate this rule as follows. First, we remove $N$ from the trail -- restarting the solver and restoring $M$ -- and then use the \rulename{Decide} rule on $L$, which is possible as $\Value(L,M) = \Undef$.
	}
\end{align*}

We have shown how to simulate every \MCSAT proof rule with the \CDCLST proof system and thereby how to algorithmically simulate \MCSAT with \CDCLST, concluding the proof for \cref{thm:cdcl-mcsat-equivalence}.
This proof not only completely ignored theory assignments, but also did not need to bother with the details of conflict analysis.
\end{proof}

This concludes our proof of \cref{thm:cdcl-mcsat-equivalence}, showing that \MCSAT is, in fact, algorithmically equivalent to \CDCLST.
